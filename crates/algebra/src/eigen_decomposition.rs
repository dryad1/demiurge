use derive_builder::Builder;
use faer::sparse::SparseColMat;
use faer::*;
use solvers::SpSolver;

/// Decompose a sparse matrix using reasonable defaults for ARPACK.
/// Returns the array of eigenvalues, followed by a matrix whose columns are
/// their corresponding eigenvectors.
pub fn eigen_decompose_sparse_symmetric_matrix(
    mat: &SparseColMat<usize, f64>,
    eigen_value_count: usize,
) -> (Vec<f64>, Mat<f64>) {
    debug_assert!(mat.nrows() == mat.ncols());

    let mut eigen_vector_storage = vec![0.0; eigen_value_count * mat.nrows()];
    let mut eigenvalue_storage = vec![0.0; eigen_value_count];
    let mut offset = 0;
    let mut sigma_s = 0.0;
    let mut sigma_last = 0.0;
    while offset < eigen_value_count {
        let eig_count = (eigen_value_count - offset).min(50);
        faer_arpack_symmetric_f64(
            &mat,
            &mut eigen_vector_storage,
            &mut eigenvalue_storage,
            offset,
            ArpackConfigSymmetricBuilder::default()
                .eigenvalue_kind(Eigenvaluekind::LargestMagnitude)
                .number_of_eigenvalues(eig_count)
                .number_of_lanczos_vectors((3 * eig_count).min(mat.nrows()))
                .maxiter(mat.nrows() * 100)
                .execution_mode(ExecutionMode::ShiftInvert { sigma: sigma_s })
                .compute_eigen_vectors(true)
                .build()
                .unwrap(),
        )
        .unwrap();

        let mut valid_count = 0;
        let first_valid = eigenvalue_storage[offset..]
            .iter()
            .position(|v| *v > sigma_last)
            .unwrap_or(0);
        let max = *eigenvalue_storage[offset..]
            .iter()
            .max_by(|a, b| a.total_cmp(*b))
            .unwrap();
        let min = *eigenvalue_storage[offset..]
            .iter()
            .max_by(|a, b| a.total_cmp(*b))
            .unwrap();
        for i in 0..eig_count {
            if eigenvalue_storage[i + offset] > sigma_last {
                valid_count += 1;

                eigenvalue_storage[offset + i - first_valid] =
                    eigenvalue_storage[i + offset];
            }
        }

        offset += valid_count;
        sigma_last = max;
        sigma_s = max + 0.4 * (max - min);
    }

    (
        eigenvalue_storage,
        faer::mat::from_column_major_slice_mut(
            &mut eigen_vector_storage[0..mat.nrows() * eigen_value_count],
            mat.nrows(),
            eigen_value_count,
        )
        .to_owned(),
    )
}

#[derive(Debug, Builder)]
pub struct ArpackConfigSymmetric {
    pub eigenvalue_kind: Eigenvaluekind,
    pub number_of_eigenvalues: usize,
    pub number_of_lanczos_vectors: usize,
    pub maxiter: usize,
    pub execution_mode: ExecutionMode,
    pub compute_eigen_vectors: bool,
}

// http://li.mit.edu/Archive/Activities/Archive/CourseWork/Ju_Li/MITCourses/18.335/Doc/ARPACK/Lehoucq97.pdf
fn faer_arpack_symmetric_f64(
    mat: &SparseColMat<usize, f64>,
    eigen_vector_storage: &mut Vec<f64>,
    eigenvalue_storage: &mut Vec<f64>,
    offset: usize,
    arpack_config: ArpackConfigSymmetric,
) -> Result<(), ArpackError> {
    let ArpackConfigSymmetric {
        eigenvalue_kind,
        number_of_eigenvalues,
        number_of_lanczos_vectors,
        maxiter,
        execution_mode,
        compute_eigen_vectors: vectors,
    } = arpack_config;

    debug_assert!(mat.nrows() == mat.ncols());
    let dimension = mat.nrows();

    let mut sigma = 0.0_f64;
    let op: &dyn Fn(ColRef<f64>) -> Col<f64> = match execution_mode {
        ExecutionMode::ShiftInvert { sigma: sig } => {
            let triplets = (0..mat.nrows()).map(|i| (i, i, 1.)).collect::<Vec<_>>();
            let identity =
                SparseColMat::try_new_from_triplets(mat.nrows(), mat.nrows(), &triplets)
                    .unwrap();
            let qr = (mat - sig * identity).sp_qr().unwrap();
            sigma = sig;

            &move |b: ColRef<f64>| qr.solve(b)
        }
        ExecutionMode::Regular => &|b: ColRef<f64>| mat * b,
    };

    let mut ido = 0;
    let mut residual: Col<f64> = Col::zeros(dimension);
    let mut arnoldi_data = vec![0.0; dimension * number_of_lanczos_vectors];
    let mut iparam = [0; 11];
    iparam[0] = 1;
    iparam[2] = maxiter as i32;
    iparam[6] = match execution_mode {
        ExecutionMode::Regular => 1,
        ExecutionMode::ShiftInvert { sigma: _ } => 3,
    };
    let mut ipntr = [0; 11];
    let mut workd = Col::<f64>::zeros(3 * dimension);
    let lworkl = number_of_lanczos_vectors * (number_of_lanczos_vectors + 8);
    let mut workl = Col::<f64>::zeros(lworkl);
    let mut info = 0;

    loop {
        unsafe {
            dsaupd_c(
                &mut ido,
                "I".as_ptr() as *const i8,
                dimension as i32,
                eigenvalue_kind.as_str().as_ptr() as *const i8,
                number_of_eigenvalues as i32,
                f64::EPSILON,
                residual.as_ptr_mut() as *mut f64,
                number_of_lanczos_vectors as i32,
                arnoldi_data.as_mut_ptr() as *mut f64,
                dimension as i32,
                iparam.as_mut_ptr(), // no idea what this does
                ipntr.as_mut_ptr(),  // no idea what this does
                workd.as_ptr_mut() as *mut f64,
                workl.as_ptr_mut() as *mut f64,
                lworkl as i32,
                &mut info,
            );

            let code = DsaupdArpackInfoCode::from_i32(info);
            if code != DsaupdArpackInfoCode::NormalExit {
                return Err(ArpackError::DsaupdError(code));
            }

            match ido {
                -1 | 1 => {
                    let res = workd.subrows(ipntr[0] as usize - 1, dimension);
                    let y = op(res);
                    workd
                        .subrows_mut(ipntr[1] as usize - 1, dimension)
                        .copy_from(&y);
                }
                99 => {
                    break;
                }
                _ => {
                    panic!("ido code: {}", ido);
                }
            }
        }
    }

    let select = vec![false as i32; number_of_lanczos_vectors];
    unsafe {
        dseupd_c(
            vectors as i32,
            "A".as_ptr() as *const i8,
            select.as_ptr(),
            (eigenvalue_storage.as_mut_ptr() as *mut f64).offset(offset as isize),
            (eigen_vector_storage.as_mut_ptr() as *mut f64)
                .offset((offset * dimension) as isize),
            dimension as i32,
            sigma,
            "I".as_ptr() as *const i8,
            dimension as i32,
            eigenvalue_kind.as_str().as_ptr() as *const i8,
            number_of_eigenvalues as i32,
            f64::EPSILON,
            residual.as_ptr_mut() as *mut f64,
            number_of_lanczos_vectors as i32,
            arnoldi_data.as_mut_ptr() as *mut f64,
            dimension as i32,
            iparam.as_mut_ptr(),
            ipntr.as_mut_ptr(),
            workd.as_ptr_mut() as *mut f64,
            workl.as_ptr_mut() as *mut f64,
            lworkl as i32,
            &mut info,
        );
    }

    let code = DseupdArpackInfoCode::from_i32(info);
    if code != DseupdArpackInfoCode::NormalExit {
        return Err(ArpackError::DseupdError(code));
    }

    Ok(())
}

#[derive(Debug, Clone)]
pub enum ExecutionMode {
    Regular,
    ShiftInvert { sigma: f64 },
}

// https://gist.github.com/wpoely86/043d0eb92f7e42563e7f
#[derive(Debug, Clone)]
pub enum Eigenvaluekind {
    LargestMagnitude,
    SmallestMagnitude,
    LargestRealPart,
    SmallestRealPart,
    LargestImaginaryPart,
    SmallestImaginaryPart,
}

impl Eigenvaluekind {
    fn as_str(&self) -> &'static str {
        match self {
            Eigenvaluekind::LargestMagnitude => "LM",
            Eigenvaluekind::SmallestMagnitude => "SM",
            Eigenvaluekind::LargestRealPart => "LR",
            Eigenvaluekind::SmallestRealPart => "SR",
            Eigenvaluekind::LargestImaginaryPart => "LI",
            Eigenvaluekind::SmallestImaginaryPart => "SI",
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum DsaupdArpackInfoCode {
    NormalExit,
    MaximumIterationsReached,
    NoShiftCanBeApplied,
    NisNegative,
    NevIsNegative,
    NcvInvalid,
    ArnoldiIterationsLessThan0,
    WhichInvalid,
    BMatInvalid,
    WorklTooShort,
    ErrorFromTrid,
    StartVectorIs0,
    Iparam6Invalid,
    Iparam6AndGIncompatible,
    Iparam0Invalid,
    NevAndWhichIncompatible,
    CouldNotBuildArnolidFactorization,
    Unknown,
}

impl DsaupdArpackInfoCode {
    fn from_i32(info: i32) -> Self {
        match info {
            0 => Self::NormalExit,
            1 => Self::MaximumIterationsReached,
            3 => Self::NoShiftCanBeApplied,
            -1 => Self::NisNegative,
            -2 => Self::NevIsNegative,
            -3 => Self::NcvInvalid,
            -4 => Self::ArnoldiIterationsLessThan0,
            -5 => Self::WhichInvalid,
            -6 => Self::BMatInvalid,
            -7 => Self::WorklTooShort,
            -8 => Self::ErrorFromTrid,
            -9 => Self::StartVectorIs0,
            -10 => Self::Iparam6Invalid,
            -11 => Self::Iparam6AndGIncompatible,
            -12 => Self::Iparam0Invalid,
            -13 => Self::NevAndWhichIncompatible,
            -14 => Self::CouldNotBuildArnolidFactorization,
            -9999 => Self::CouldNotBuildArnolidFactorization,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum DseupdArpackInfoCode {
    NormalExit,
    NNotPositive,
    NevNotBePositive,
    NcvInvalid,
    WhichInvalid,
    BmatInvalid,
    WorlTooShort,
    ErrorReturnFromTrid,
    StartingVectorIsZero,
    Iparam6Invalid,
    Iparam6AndGIncompatible,
    NevAndWhichIncompatible,
    DsaupdDidNotFindAnyEigenvaluesToSufficientAccuracy,
    HowmnyAndRvecIncompatible,
    HowmnySNotImplemented,
    DseupdAnfDsaupdGotDifferentRitzValues,
    Unknown,
}

impl DseupdArpackInfoCode {
    fn from_i32(info: i32) -> Self {
        match info {
            0 => Self::NormalExit,
            -1 => Self::NNotPositive,
            -2 => Self::NevNotBePositive,
            -3 => Self::NcvInvalid,
            -5 => Self::WhichInvalid,
            -6 => Self::BmatInvalid,
            -7 => Self::WorlTooShort,
            -8 => Self::ErrorReturnFromTrid,
            -9 => Self::StartingVectorIsZero,
            -10 => Self::Iparam6Invalid,
            -11 => Self::Iparam6AndGIncompatible,
            -12 => Self::NevAndWhichIncompatible,
            -14 => Self::DsaupdDidNotFindAnyEigenvaluesToSufficientAccuracy,
            -15 => Self::HowmnyAndRvecIncompatible,
            -16 => Self::HowmnySNotImplemented,
            -17 => Self::DseupdAnfDsaupdGotDifferentRitzValues,
            _ => Self::Unknown,
        }
    }
}

#[derive(Debug)]
pub enum ArpackError {
    DsaupdError(DsaupdArpackInfoCode),
    DseupdError(DseupdArpackInfoCode),
}

// The most useful info is on page 124 here:
// http://li.mit.edu/Archive/Activities/Archive/CourseWork/Ju_Li/MITCourses/18.335/Doc/ARPACK/Lehoucq97.pdf
/*
ARPACK input table

------------------------------------------------------------------------------------
| Parameter | Description                                                          |
|-----------|----------------------------------------------------------------------|
| ido       | Reverse communication flag.                                          |
| nev       | The requested number of eigenvalues to compute.                      |
| ncv       | The number of Lanczos basis vectors to use through the course of     |
|           | the compilation.                                                     |
| bmat      | Indicates whether the problem is standard bmat = 'I' or              |
|           | generalized bmat = 'G'.                                              |
| which     | Specifies which eigenvalues of `A` are to be computed.               |
| tol       | Specifies the relative accuracy to which eigenvalues are to be       |
|           | computed.                                                            |
| iparam    | Specifies the computational mode, number of IRAM iterations, the     |
|           | implicit shift strategy, and outputs various informational           |
|           | parameters upon completion of IRAM. iparam[6] defines the            |
|           | computational mode.                                                  |
 */

/*
ARPACK modes for iparam[6]

| code/bmat | OP                                       | Description                   |
|-----------|------------------------------------------|------------------------------ |
| 1 / I     | OP = A; B = I                            | Solves $Ax = \lambda x$       |
| 3 / I     | OP = (A - \sigma I)^{-1}; B = I          | Solves $Ax = x \lambda$       |
| 2 / G     | OP = M^{-1}A; B = M                      | Solves $Ax = M x \lambda$     |
| 3 / G     | OP = (A - \sigma M)^{-1}M; B = M         | Solves $Ax = Mx \lambda$      |
| 4 / G     | OP = (K - \sigma K_G)^{-1}K; B = K       | Solves $Kx = K_Gx \lambda$    |
| 5 / G     | OP = (A - \sigma M)^{-1}(A + \sigma M); B = M | Solves $Ax = Mx \lambda$ |
]
 */
extern "C" {
    pub fn dsaupd_c(
        ido: *mut ::std::os::raw::c_int,
        bmat: *const ::std::os::raw::c_char,
        n: ::std::os::raw::c_int,
        which: *const ::std::os::raw::c_char,
        nev: ::std::os::raw::c_int,
        tol: f64,
        resid: *mut f64,
        ncv: ::std::os::raw::c_int,
        v: *mut f64,
        ldv: ::std::os::raw::c_int,
        iparam: *mut ::std::os::raw::c_int,
        ipntr: *mut ::std::os::raw::c_int,
        workd: *mut f64,
        workl: *mut f64,
        lworkl: ::std::os::raw::c_int,
        info: *mut ::std::os::raw::c_int,
    );
}

extern "C" {
    pub fn dseupd_c(
        rvec: ::std::os::raw::c_int,
        howmny: *const ::std::os::raw::c_char,
        select: *const ::std::os::raw::c_int,
        d: *mut f64,
        z: *mut f64,
        ldz: ::std::os::raw::c_int,
        sigma: f64,
        bmat: *const ::std::os::raw::c_char,
        n: ::std::os::raw::c_int,
        which: *const ::std::os::raw::c_char,
        nev: ::std::os::raw::c_int,
        tol: f64,
        resid: *mut f64,
        ncv: ::std::os::raw::c_int,
        v: *mut f64,
        ldv: ::std::os::raw::c_int,
        iparam: *mut ::std::os::raw::c_int,
        ipntr: *mut ::std::os::raw::c_int,
        workd: *mut f64,
        workl: *mut f64,
        lworkl: ::std::os::raw::c_int,
        info: *mut ::std::os::raw::c_int,
    );
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sparse_eigen_decomposition() {
        let laplacian_triplets = vec![
            (0, 0, 2.),
            (0, 1, -1.),
            (0, 5, -1.),
            (1, 0, -1.),
            (1, 1, 3.),
            (1, 2, -1.),
            (1, 6, -1.),
            (2, 1, -1.),
            (2, 2, 3.),
            (2, 3, -1.),
            (2, 7, -1.),
            (3, 2, -1.),
            (3, 3, 3.),
            (3, 4, -1.),
            (3, 8, -1.),
            (4, 3, -1.),
            (4, 4, 3.),
            (4, 5, -1.),
            (4, 9, -1.),
            (5, 0, -1.),
            (5, 4, -1.),
            (5, 5, 3.),
            (5, 6, -1.),
            (6, 1, -1.),
            (6, 5, -1.),
            (6, 6, 3.),
            (6, 7, -1.),
            (7, 2, -1.),
            (7, 6, -1.),
            (7, 7, 3.),
            (7, 8, -1.),
            (8, 3, -1.),
            (8, 7, -1.),
            (8, 8, 3.),
            (8, 9, -1.),
            (9, 4, -1.),
            (9, 8, -1.),
            (9, 9, 2.),
        ];

        let mat =
            SparseColMat::try_new_from_triplets(10_usize, 10_usize, &laplacian_triplets)
                .unwrap();

        let (eigs, eigvs) = eigen_decompose_sparse_symmetric_matrix(&mat, 8);

        let test = mat * eigvs.col(1);
        let expected = eigvs.col(1) * eigs[1];

        for i in 0..test.nrows() {
            assert!((test[i] - expected[i]).abs() < 0.0001);
        }
    }
}
