// //! Theory: https://slepc.upv.es/documentation/reports/

// use faer::Col;

// use crate::sparse_vector::orthogonalize;
// use crate::sparse_vector::stringify_mat;

// pub fn arnoldi_method(
//     mat: &faer::sparse::SparseColMat<usize, f64>,
//     guess: faer::Col<f64>,
//     vec_num: usize,
//     eps: f64,
// ) -> (faer::Mat<f64>, faer::Mat<f64>) {
//     debug_assert!(mat.nrows() == mat.ncols());
//     let mut h = faer::Mat::zeros(vec_num + 1, vec_num);
//     let mut q = faer::Mat::zeros(mat.nrows(), vec_num + 1);

//     let norm = guess.norm_l2();
//     let normalized = guess * (1.0 / norm);
//     let dim = mat.nrows();
//     q.get_mut(0..dim, 0).copy_from(normalized);

//     for k in 1..=dim + 1 {
//         let mut v = mat * q.col(k);

//         // Orhtonogonalize v relative to all priorly computed values.
//         for j in 0..=k {
//             h[(j, k - 1)] = q.col(j).transpose() * v.clone();
//             v = v - h[(j, k - 1)] * q.col(j);
//         }
//         h[(k, k - 1)] = v.norm_l2();
//         if h[(k, k - 1)] > eps {
//             let denom = h[(k, k - 1)];
//             q.col_mut(k).copy_from(v * (1.0 / denom));
//         } else {
//             break;
//         }
//     }

//     return (q, h);
// }

// pub fn arnoldi_method_with_deflation_in_place(
//     mat: &faer::sparse::SparseColMat<usize, f64>,
//     step_count: usize,
//     v: &mut faer::Mat<f64>,
//     h: &mut faer::Mat<f64>,
//     locked_indices: usize,
// ) {
//     debug_assert!(locked_indices < step_count);

//     for i in locked_indices + 1..=step_count {
//         let mut w = mat * v.col(i - 1);

//         for j in 0..i {
//             h[(j, i - 1)] = v.col(j).transpose() * w.as_ref();
//             w = w - h[(j, i - 1)] * v.col(j);
//         }

//         if i == step_count {
//             break;
//         }

//         h[(i, i - 1)] = w.norm_l2();
//         if h[(i, i - 1)].abs() <= 0.0000001 {
//             find_orhtogonal_vector_in_place(v, &mut w, i);
//             w = mat * w;
//             h[(i, i - 1)] = w.norm_l2();
//         }
//         // Normalize the vector.
//         w.iter_mut().for_each(|e| *e /= h[(i, i - 1)]);

//         v.col_mut(i).copy_from(w);
//     }
// }

// pub fn qr_iterations_with_shift_in_place(
//     h: &mut faer::Mat<f64>,
//     q_res: &mut faer::Mat<f64>,
//     tolerance: f64,
// ) {
//     fn has_converged(matrix: &faer::Mat<f64>, tolerance: f64) -> usize {
//         for i in 1..matrix.nrows() {
//             // Check only elements below the diagonal
//             if matrix[(i, i - 1)].abs() > tolerance {
//                 println!("diag error {}", matrix[(i, i - 1)]);
//                 return i; // Found an element below the diagonal larger
//                           // than tolerance
//             }
//         }

//         matrix.nrows() - 1 // All elements below the diagonal are within tolerance
//     }

//     *q_res = faer::Mat::identity(h.nrows(), h.ncols());

//     let mut dbg = 0;
//     let n = h.nrows();
//     let mut converged_count = 0;
//     loop {
//         let mut active_h =
//             h.submatrix_mut(0, 0, n - converged_count, n - converged_count);

//         zero_sub_diagonal(&mut active_h, tolerance);

//         dbg += 1;
//         let alpha = wilkinson_shift_coeff(&active_h.as_ref());
//         let id = faer::Mat::<f64>::identity(active_h.nrows(), active_h.ncols());

//         // Don't know why I need to negate it. It doesn't converge otherwise.
//         let shift = alpha * id;
//         let qr = (&active_h - &shift).qr();
//         let q = qr.compute_q();
//         let r = qr.compute_r();

//         active_h.as_mut().copy_from((r * q.as_ref()) + shift);

//         let mut active_q_res = q_res.submatrix_mut(
//             0,
//             converged_count,
//             q_res.nrows(),
//             q_res.nrows() - converged_count,
//         );
//         active_q_res.copy_from(&active_q_res * q);

//         converged_count = has_converged(h, tolerance);
//         println!("converged {}", converged_count);
//         if converged_count == h.nrows() - 1 {
//             break;
//         }
//     }

//     println!("qr iterations {}", dbg);
// }
// // page 6:
// // https://dspace.mit.edu/bitstream/handle/1721.1/75282/18-335j-fall-2006/contents/lecture-notes/lec16.pdf
// fn wilkinson_shift_coeff(mat: &faer::MatRef<f64>) -> f64 {
//     debug_assert!(mat.ncols() == mat.nrows());
//     let n = mat.nrows() - 1;
//     let a = mat[(n - 1, n - 1)];
//     let b = mat[(n, n - 1)];
//     let c = mat[(n, n)];
//     let delta = (a - c) / 2.;

//     let mu = c - b * b * delta.signum() / (delta.abs() + (b * b + delta * delta).sqrt());

//     mu
// }

// pub fn find_orhtogonal_vector_in_place(
//     set: &mut faer::Mat<f64>,
//     vec: &mut faer::Col<f64>,
//     subset: usize,
// ) {
//     let mut active_index = 0;
//     loop {
//         let block = set.submatrix(0, 0, set.nrows(), subset);
//         vec[active_index] = 1.0;

//         orthogonalize(&block.as_ref(), subset, &mut vec.as_mut());

//         let norm = vec.norm_l2();
//         if norm > 0.000001 {
//             vec.iter_mut().for_each(|e| *e /= norm);
//             break;
//         }

//         vec[active_index] -= 1.0;
//         active_index += 1;
//     }
// }

// pub fn krylov_schur_method(
//     mat: &faer::sparse::SparseColMat<usize, f64>,
//     mut guess: Col<f64>,
//     eigen_value_count: usize,
//     tolerance: f64,
// ) -> (Vec<f64>, faer::Mat<f64>) {
//     let k = eigen_value_count + 1;
//     let mut v = faer::Mat::zeros(mat.nrows(), k);
//     let mut h = faer::Mat::zeros(k, k);
//     v.col_mut(0).copy_from(guess);

//     let mut p = 0;

//     let mut u1 = faer::Mat::zeros(k, k);
//     let mut u2 = faer::Mat::zeros(k, k);

//     let mut dbg = 0;
//     loop {
//         dbg += 1;
//         arnoldi_method_with_deflation_in_place(mat, k, &mut v, &mut h, p);

//         println!("====");
//         println!("{} {}", dbg, p);
//         println!("{}", stringify_mat(&h, 3));
//         qr_iterations_with_shift_in_place(&mut h, &mut u1, tolerance);

//         bubble_sort_diag_desc(&h, &mut u2);

//         let u = &u1 * &u2;
//         v = v * u;

//         let error = |i| {
//             let tentative = mat * v.col(i);
//             let approx_lambda = h[(i, i)];
//             let residual = tentative - approx_lambda * v.col(i);
//             let error = residual.norm_l2();

//             error
//         };

//         for i in p..=eigen_value_count {
//             let err = error(i);
//             println!("{}", err);
//             if err > tolerance {
//                 p = i;
//                 break;
//             }
//         }

//         if p == eigen_value_count {
//             break;
//         }

//         // Add all krylov vectors together.
//         guess = v.col(0).to_owned();
//         for i in 1..p {
//             guess += v.col(i).to_owned().clone();
//         }
//         let norm = guess.norm_l2();
//         guess.iter_mut().for_each(|e| *e /= norm);
//     }

//     let res = h.split_at(h.nrows() - 1, eigen_value_count).0;
//     let eigs: Vec<_> = (0..eigen_value_count).map(|i| res[(i, i)]).collect();

//     // Normalize eigenvectors.
//     let mut eigvs = v.submatrix(0, 0, v.nrows(), eigen_value_count).to_owned();
//     for i in 0..eigvs.ncols() {
//         let col = eigvs.col_mut(i);
//         let norm = col.norm_l2();
//         col.iter_mut().for_each(|e| *e /= norm);
//     }

//     (eigs, eigvs)
// }

// fn zero_sub_diagonal(mat: &mut faer::MatMut<f64>, tolerance: f64) {
//     for i in 1..mat.nrows() {
//         for j in 0..i {
//             if mat[(i, j)] <= tolerance {
//                 mat[(i, j)] = 0.0;
//             }
//         }
//     }
// }

// fn householder_reflection(x: &faer::Col<f64>) -> (f64, Col<f64>) {
//     let m = x.nrows();
//     let t = x.subrows(1, m - 1);

//     let sigma = t.transpose() * t;
//     let mut v = x.clone();
//     v[0] = 1.;

//     let beta = if sigma.abs() < f64::EPSILON && x[0] >= 0. {
//         0.
//     } else if sigma.abs() < f64::EPSILON && x[0] < 0. {
//         -2.
//     } else {
//         let mu = (x[0] * x[0] + sigma).sqrt();
//         if x[0] <= 0. {
//             v[0] = x[0] - mu;
//         } else {
//             v[0] = -sigma / (x[0] + mu);
//         }

//         let denom = v[0];
//         v = v / denom;

//         2. * v[0] * v[0] / (sigma + v[0] * v[0])
//     };

//     (beta, v)
// }

// fn francis_qr_step(h: &mut faer::Mat<f64>) {
//     let n = h.nrows() - 1;
//     let m = n - 1;
//     let s = h[(m, m)] + h[(n, n)];
//     let t = h[(m, m)] * h[(n, n)] - h[(m, n)] * h[(n, m)];
//     let mut x = h[(0, 0)] * h[(0, 0)] + h[(0, 1)] * h[(1, 0)] - s * h[(0, 0)] + t;
//     let mut y = h[(1, 0)] * (h[(0, 0)] + h[(1, 1)] - s);
//     let mut z = h[(1, 0)] * h[(2, 1)];

//     for k in -1..=(n as isize) - 3 {
//         let tmp = vec![x, y, z];
//         let (beta, v) = householder_reflection(&Col::<f64>::from_fn(3, |i| tmp[i]));
//         let q = k.max(0) as usize;

//         let i = faer::Mat::<f64>::identity(n - q, n - q);
//         let mut sub_h = h.submatrix_mut((k + 1) as usize, q, (k + 3) as usize, n);
//         sub_h
//             .copy_from((i.clone() - beta * v.clone() * v.transpose()) * sub_h.to_owned());

//         let r = n.min((k + 4) as usize);

//         let mut sub_h = h.submatrix_mut(0, (k + 1) as usize, r, (k + 3) as usize);
//         x = h[((k + 2) as usize, (k + 1) as usize)];
//         y = h[((k + 3) as usize, (k + 1) as usize)];

//         if (k as usize) < n - 3 {
//             z = h[((k + 4) as usize, (k + 1) as usize)];
//         }
//     }

//     let tmp = vec![x, y];
//     let (beta, v) = householder_reflection(&Col::<f64>::from_fn(2, |i| tmp[i]));

//     let i = faer::Mat::<f64>::identity(2, 2);
//     let mat = i.clone() - beta * v.clone() * v.transpose();

//     let mut sub_h = h.submatrix_mut(n - 1, n - 2, n, n);
//     sub_h.copy_from(mat.clone() * sub_h.to_owned());

//     let mut sub_h = h.submatrix_mut(0, n - 1, n, n);
//     sub_h.copy_from(sub_h.to_owned() * mat);
// }

// fn qr_iterations(h: &mut faer::Mat<f64>, q_res: &mut faer::Mat<f64>, tolerance: f64) {
//     let mut q = 0;
//     let n = h.nrows() - 1;
//     loop {
//         if q == n {
//             break;
//         }
//     }
// }

// fn zero_qr(mat: &mut faer::MatMut<f64>, tolerance: f64) {
//     for i in 1..mat.nrows() {
//         // mat[(i, i - 1)] = tolerance * (mat[(i, i)] + h[(i - 1, i - 1)]);
//     }
// }

// fn bubble_sort_diag_desc(mat: &faer::Mat<f64>, u: &mut faer::Mat<f64>) {
//     debug_assert!(mat.nrows() == mat.ncols());
//     let mut n = mat.nrows();
//     let mut swapped = true;
//     *u = faer::Mat::<f64>::identity(mat.nrows(), mat.ncols());
//     let mut eigs = mat
//         .diagonal()
//         .column_vector()
//         .to_owned()
//         .as_slice()
//         .to_vec();

//     while swapped {
//         swapped = false;
//         for i in 1..n {
//             if eigs[i - 1] < eigs[i] {
//                 let (c1, c2) = u.two_cols_mut(i - 1, i);
//                 faer::perm::swap_cols(c1, c2);
//                 eigs.swap(i - 1, i);
//                 swapped = true;
//             }
//         }
//         n -= 1;
//     }
// }

// // +| Tests |+ =======================================================
// #[cfg(test)]
// mod tests {
//     use core::f64::consts::PI;

//     use faer::sparse::SparseColMat;

//     use super::*;

//     #[test]
//     fn test_eigen_decomposition() {
//         let dim = 400;
//         let triplets: Vec<_> = (0..dim).map(|i| (i, i, (i + 1) as f64)).collect();
//         let mat = SparseColMat::try_new_from_triplets(dim, dim, &triplets).unwrap();

//         let guess = faer::Col::<f64>::ones(dim) * (1.0 / (dim as f64).sqrt());

//         let (vals, vecs) = krylov_schur_method(&mat, guess, (dim / 2).min(50), 0.001);

//         // for i in 0..vals.len() {
//         //     let lambda = (&mat * vecs.col(i)).norm_l2();
//         //     assert!((lambda - vals[i]).abs() < 0.1, "{} {}", lambda, vals[i]);
//         // }

//         // ===
//         // let a = 6.;
//         // let b = 3.;
//         // let n = 40;
//         // let mut triplets = Vec::new();
//         // for i in 0..n {
//         //     triplets.push((i, i, a));
//         //     if i < n - 1 {
//         //         triplets.push((i, i + 1, b));
//         //     }
//         //     if i > 0 {
//         //         triplets.push((i, i - 1, b));
//         //     }
//         // }

//         // use crate::sparse_vector::stringify_sparse_mat;
//         // let mat = SparseColMat::try_new_from_triplets(n, n, &triplets).unwrap();

//         // let guess = faer::Col::<f64>::ones(n) * (1.0 / (n as f64).sqrt());
//         // let (vals, vecs) = krylov_schur_method(&mat, guess.clone(), dim / 2, 0.001);

//         // for i in (0..n / 2).rev() {
//         //     let j = n - i;
//         //     let eig = a - 2. * b * (j as f64 * PI / (n + 1) as f64).cos();
//         //     assert!((eig - vals[i]).abs() < 0.0001);
//         //     let lambda = (&mat * vecs.col(i)).norm_l2();
//         //     assert!((lambda - vals[i]).abs() < 0.00001);
//         // }

//         // let mat = mat.to_dense();
//         // /*let (eigs, eigvs) =*/
//         // faer::krylov_schur_prototype::partial_schur(
//         //     mat.as_ref(),
//         //     guess.as_ref(),
//         //     20,
//         //     35,
//         //     5,
//         //     0.000001,
//         //     20,
//         // );

//         // let eigs = eigs.col(0);
//         // println!("aaaaaaaaaa=\n{}", stringify_mat(&eigs, 3));

//         // for i in (0..5).rev() {
//         //     let j = n - i;
//         //     let eig = a - 2. * b * (j as f64 * PI / (n + 1) as f64).cos();
//         //     assert!((eig - eigs[i]).abs() < 0.0001, "{} {}", eig, eigs[i]);
//         // }
//     }
// }
