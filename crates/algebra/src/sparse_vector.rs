use core::fmt::Display;
use core::fmt::Formatter;
use core::ops::Add;
use core::ops::Mul;
use core::ops::Sub;
use std::collections::BTreeMap;

use faer::unzipped;
use faer::zipped;
use faer::{sparse::*, SimpleEntity};
use linear_isomorphic::{InnerSpace, RealField};

#[derive(Clone, Default)]
pub struct SpEntry<S: RealField> {
    pub index: usize,
    pub value: S,
}

#[derive(Clone, Default)]
pub struct SparseRealVector<S: RealField> {
    data: Vec<SpEntry<S>>,
    dim: usize,
}

impl<S: RealField> SparseRealVector<S> {
    pub fn from_iter(iter: impl Iterator<Item = (usize, S)>, dim: usize) -> Self {
        let mut current = 0;
        let res = Self {
            data: iter
                .map(|(i, v)| {
                    debug_assert!(i >= current);
                    current = i;
                    SpEntry { index: i, value: v }
                })
                .collect(),
            dim,
        };

        debug_assert!(res.data.windows(2).all(|w| w[0].index <= w[1].index));
        debug_assert!(res.data.iter().all(|w| w.index < dim));

        res
    }

    pub fn dot(&self, other: &Self) -> S {
        debug_assert!(self.dim == other.dim);

        let mut res = S::from(0.0).unwrap();
        if self.data.is_empty() || other.data.is_empty() {
            return res;
        }

        let mut i = 0;
        let mut j = 0;
        while i < self.data.len() && j < other.data.len() {
            let SpEntry {
                index: si,
                value: sv,
            } = self.data[i];
            let SpEntry {
                index: oi,
                value: ov,
            } = other.data[j];

            if si == oi {
                res += sv * ov;
            }

            i += (si <= oi) as usize;
            j += (oi <= si) as usize;
        }

        res
    }

    pub fn norm_squared(&self) -> S {
        let mut res = S::from(0.0).unwrap();
        for v in self.data.iter() {
            res += v.value * v.value;
        }

        res
    }

    pub fn norm(&self) -> S {
        self.norm_squared().sqrt()
    }

    pub fn normalized(&self) -> Self {
        let norm = self.norm();
        debug_assert!(norm > S::from(0.0).unwrap());

        let res = self
            .data
            .iter()
            .cloned()
            .map(|e| SpEntry {
                index: e.index,
                value: e.value / norm,
            })
            .collect();

        Self {
            data: res,
            dim: self.dim,
        }
    }

    pub fn normalize(&mut self) {
        let norm = self.norm();
        debug_assert!(norm > S::from(0.0).unwrap());

        for SpEntry { value, .. } in self.data.iter_mut() {
            *value /= norm;
        }
    }

    pub fn index(&self, i: usize) -> S {
        let entry = self
            .data
            .binary_search_by(|entry: &SpEntry<S>| entry.index.cmp(&i));

        match entry {
            Err(_) => S::from(0.).unwrap(),
            Ok(x) => self.data[x].value,
        }
    }
}

impl<S: RealField> Display for SparseRealVector<S> {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        let decimals = 3;
        let y = S::from(10i32.pow(decimals as u32) as f64).unwrap();
        let round = |x: S| (x * y).round() / y;

        let mut longest_string_len = 0;
        for val in self.data.iter() {
            let len = round(val.value).to_string().len();
            longest_string_len = longest_string_len.max(len);
        }

        let mut res = String::from("");
        let mut index = 0;
        for i in 0..self.dim {
            let val = if i > self.data.len() {
                S::from(0.).unwrap()
            } else if i == self.data[index].index {
                let v = self.data[index].value;
                index += 1;

                v
            } else {
                S::from(0.).unwrap()
            };

            let val = round(val);
            let val_len = val.to_string().len();

            let mut padding = String::new();
            padding.extend((0..longest_string_len - val_len).map(|_| ' '));

            res.push_str(&"| ");
            res.push_str(&padding);
            res.push_str(&val.to_string());
            res.push_str(", |\n");
        }

        write!(formatter, "{}", res)
    }
}

impl<I, S> Mul<&SparseColMat<I, S>> for &SparseRealVector<S>
where
    I: Index + TryFrom<usize> + Copy,
    S: SimpleEntity + Copy + RealField,
    usize: From<I>,
    <I as TryFrom<usize>>::Error: std::fmt::Debug,
{
    type Output = SparseRealVector<S>;

    fn mul(self, rhs: &SparseColMat<I, S>) -> Self::Output {
        debug_assert!(rhs.nrows() == self.dim);
        if self.data.is_empty() {
            return SparseRealVector {
                data: Vec::new(),
                dim: self.dim,
            };
        }

        let mut result = BTreeMap::new();
        for (row, col, mat_val) in to_triplets_iter(rhs) {
            let self_val = self.index(usize::from(col));
            if self_val > S::epsilon() {
                let entry = result.entry(row).or_insert(S::from(0.).unwrap());
                *entry += self_val * mat_val;
            }
        }

        SparseRealVector::from_iter(
            result
                .iter()
                .map(|(index, val)| (usize::from(*index), *val)),
            self.dim,
        )
    }
}

impl<S> Mul<S> for &SparseRealVector<S>
where
    S: SimpleEntity + Copy + RealField,
{
    type Output = SparseRealVector<S>;

    fn mul(self, rhs: S) -> Self::Output {
        SparseRealVector::from_iter(
            self.data
                .iter()
                .map(|entry| (entry.index, entry.value * rhs)),
            self.dim,
        )
    }
}

impl<S> Add<&SparseRealVector<S>> for &SparseRealVector<S>
where
    S: SimpleEntity + Copy + RealField,
{
    type Output = SparseRealVector<S>;

    fn add(self, other: &SparseRealVector<S>) -> Self::Output {
        debug_assert!(other.dim == self.dim);
        let mut vals = Vec::with_capacity(other.data.len() + self.data.len());

        let mut i = 0;
        let mut j = 0;
        while i < self.data.len() && j < other.data.len() {
            let SpEntry {
                index: si,
                value: sv,
            } = self.data[i];

            let SpEntry {
                index: oi,
                value: ov,
            } = other.data[j];

            if si == oi {
                vals.push((i, sv + ov));
                i += 1;
                j += 1;
            } else if si < oi {
                vals.push((i, sv));
                i += 1;
            } else {
                vals.push((j, ov));
                j += 1;
            }
        }

        Self::Output::from_iter(vals.into_iter(), self.dim)
    }
}

impl<S> Sub<&SparseRealVector<S>> for &SparseRealVector<S>
where
    S: SimpleEntity + Copy + RealField,
{
    type Output = SparseRealVector<S>;

    fn sub(self, other: &SparseRealVector<S>) -> Self::Output {
        debug_assert!(other.dim == self.dim);
        let mut vals = Vec::with_capacity(other.data.len() + self.data.len());

        let mut i = 0;
        let mut j = 0;
        while i < self.data.len() && j < other.data.len() {
            let SpEntry {
                index: si,
                value: sv,
            } = self.data[i];
            let SpEntry {
                index: oi,
                value: ov,
            } = other.data[j];

            if si == oi {
                vals.push((i, sv - ov));
                i += 1;
                j += 1;
            } else if si < oi {
                vals.push((i, sv));
                i += 1;
            } else {
                vals.push((j, -ov));
                j += 1;
            }
        }

        Self::Output::from_iter(vals.into_iter(), self.dim)
    }
}

pub fn in_place_sparse_gram_schmidt<E>(basis: &mut Vec<SparseRealVector<E>>)
where
    E: SimpleEntity + Copy + RealField,
{
    let project = |v: &SparseRealVector<_>, u: &SparseRealVector<_>| u * v.dot(u);
    basis[0].normalize();
    for i in 1..basis.len() {
        let mut res = basis[i].clone();
        for j in 0..i {
            res = &res - &project(&basis[i], &basis[j]);
        }

        basis[i] = res.normalized();
    }
}

pub fn gram_schmidt<'a, V, S>(
    mut set: impl Iterator<Item = &'a V> + ExactSizeIterator,
) -> Vec<V>
where
    S: Copy + RealField + Mul<V, Output = V>,
    V: InnerSpace<S> + 'static + std::fmt::Debug,
{
    let mut basis = Vec::with_capacity(set.len());
    basis.push(set.find(|v| v.norm() > S::epsilon()).unwrap().clone());
    let project = |v: &V, u: &V| u.clone() * v.dot(u);
    for (i, v) in set.enumerate() {
        let mut res = v.clone();
        for j in 0..i {
            res = res - project(v, &basis[j]);
        }

        basis.push(res.normalized());
    }

    basis
}

pub fn in_place_gram_schmidt_cols<S>(set: &mut faer::Mat<S>)
where
    S: SimpleEntity
        + Copy
        + RealField
        + faer::ComplexField
        + Mul<faer::Col<S>, Output = faer::Col<S>>,
{
    for i in 0..set.ncols() {
        for j in 0..i {
            // Project col_i onto col_j.
            let alpha = set.col(j).transpose() * set.col(i).as_ref();
            let (coli, colj) = set.two_cols_mut(i, j);

            // This does `col(i) -= col(j) * alpha` in place.
            zipped!(coli, colj).for_each(|unzipped!(mut a, b)| {
                let c = a.read();
                a.write(c - b.read() * alpha);
            });
        }
        // Normalize the column.
        let norm = S::faer_from_real(set.col(i).norm_l2());
        if norm < S::epsilon() {
            set.col_mut(i)
                .iter_mut()
                .for_each(|e| *e = S::from(0.0).unwrap());
            continue;
        }
        set.col_mut(i).iter_mut().for_each(|e| *e /= norm);
    }
}

/// Orhtogonalize a vector `v` in place, relative to the `0..subset` columns of
/// set.
pub fn orthogonalize<S>(set: &faer::MatRef<S>, subset: usize, v: &mut faer::ColMut<S>)
where
    S: SimpleEntity
        + Copy
        + RealField
        + faer::ComplexField
        + Mul<faer::Col<S>, Output = faer::Col<S>>,
{
    for j in 0..subset {
        // Project v onto col_j.
        let alpha = set.col(j).transpose() * v.as_ref();

        // This does `v -= col(j) * alpha` in place.
        zipped!(v.as_mut(), set.col(j)).for_each(|unzipped!(mut a, b)| {
            let c = a.read();
            a.write(c - b.read() * alpha);
        });
    }
}

pub fn qr_factorization_sparse<I, E>(mat: &SparseColMat<I, E>)
where
    I: Index + TryFrom<usize> + Copy,
    E: SimpleEntity + Copy + RealField,
    usize: From<I>,
    <I as TryFrom<usize>>::Error: std::fmt::Debug,
{
    debug_assert!(mat.nrows() == mat.ncols());
    let mut basis_map = BTreeMap::new();
    for (row, col, val) in to_triplets_iter(mat) {
        let col_entry = basis_map.entry(col).or_insert(BTreeMap::new());
        let entry = col_entry.entry(row).or_insert(E::from(0.0).unwrap());
        *entry += val;
    }

    let mut basis = vec![SparseRealVector::default(); mat.ncols()];
    for (col, map) in basis_map {
        basis[usize::from(col)] = SparseRealVector::from_iter(
            map.iter().map(|(i, v)| (usize::from(*i), *v)),
            mat.nrows(),
        );
    }

    let prior_basis = basis.clone();
    in_place_sparse_gram_schmidt(&mut basis);

    let mut res = Vec::new();
    for row in 0..prior_basis.len() {
        for col in 0..=row {
            res.push((row, col, (&prior_basis[col]).dot(&basis[row])));
        }
    }
}

pub fn stringify_sparse_mat(mat: &SparseColMat<usize, f64>, decimals: usize) -> String {
    let y = 10i32.pow(decimals as u32) as f64;
    let round = |x: f64| (x * y).round() / y;

    let rows = mat.nrows();
    let cols = mat.ncols();

    let mut longest_string_len = 0;
    for val in mat.values() {
        let len = round(*val).to_string().len();
        longest_string_len = longest_string_len.max(len);
    }

    let mut res = String::from("");
    for row in 0..rows {
        for col in 0..cols {
            let vals = mat.get_all(row, col);
            let val = round(if vals.is_empty() { 0. } else { vals[0] });

            let val_len = val.to_string().len();
            let mut padding = String::new();
            padding.extend((0..longest_string_len - val_len).map(|_| ' '));

            res.push_str(&padding);
            res.push_str(format!("{}, ", val).as_str());
        }
        res.pop();
        res.pop();
        res.push_str("\n");
    }

    res
}

pub fn stringify_mat(mat: &faer::Mat<f64>, decimals: usize) -> String {
    let y = 10i32.pow(decimals as u32) as f64;
    let round = |x: f64| (x * y).round() / y;

    let rows = mat.nrows();
    let cols = mat.ncols();

    let mut longest_string_len = 0;
    for col in mat.col_iter() {
        for val in col.iter() {
            let len = round(*val).to_string().len();
            longest_string_len = longest_string_len.max(len);
        }
    }

    let mut res = String::from("");
    for row in 0..rows {
        for col in 0..cols {
            let val = mat[(row, col)];
            let val = round(val);

            let val_len = val.to_string().len();
            let mut padding = String::new();
            padding.extend((0..longest_string_len - val_len).map(|_| ' '));

            res.push_str(&padding);
            res.push_str(format!("{}, ", val).as_str());
        }
        res.pop();
        res.pop();
        res.push_str("\n");
    }

    res
}

// TODO(ultra low): delete this when faer has this logic implemented in the main
// project.
pub fn to_triplets_iter<I, E>(
    mat: &SparseColMat<I, E>,
) -> impl Iterator<Item = (I, I, E)> + '_
where
    I: Index + TryFrom<usize> + Copy,
    E: SimpleEntity + Copy,
    <I as TryFrom<usize>>::Error: std::fmt::Debug,
{
    (0..mat.ncols()).flat_map(move |j| {
        let col_index = j.try_into().unwrap();
        mat.row_indices_of_col(j)
            .zip(mat.values_of_col(j))
            .map(move |(i, &v)| (i.try_into().unwrap(), col_index, v))
    })
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sparse_dot_product() {
        let spv1 = SparseRealVector::<f64>::from_iter((0..10).map(|i| (i, 1.0)), 10);
        let norm_squared = spv1.dot(&spv1);
        assert!((norm_squared - 10.0).abs() < f64::EPSILON);

        let v1 = vec![1., 2., 3., 4., 5., 6., 7., 8., 9., 10.];
        let v2 = vec![
            726. / 77.,
            605. / 77.,
            484. / 77.,
            363. / 77.,
            242. / 77.,
            121. / 77.,
            0.,
            -121. / 77.,
            -242. / 77.,
            -363. / 77.,
        ];

        type Spv = SparseRealVector<f64>;
        let v1 = Spv::from_iter(v1.iter().copied().enumerate(), 10);
        let v2 = Spv::from_iter(v2.iter().copied().enumerate(), 10);

        let dot = v1.dot(&v2);
        assert!(dot.abs() < f64::EPSILON);
    }

    #[test]
    fn test_sparse_addition_and_subtraction() {
        let spv1 = SparseRealVector::<f64>::from_iter((0..10).map(|i| (i, 1.0)), 10);
        let norm_squared = spv1.dot(&spv1);
        assert!((norm_squared - 10.0).abs() < f64::EPSILON);

        let v = vec![1., 2., 3., 4., 5., 6., 7., 8., 9., 10.];

        type Spv = SparseRealVector<f64>;
        let v1 = Spv::from_iter(v.iter().copied().enumerate(), 10);
        let v2 = Spv::from_iter(v.iter().rev().copied().enumerate(), 10);

        let v3 = &v1 + &v2;
        debug_assert!(v3.data.iter().all(|v| v.value == 11.0));

        let v3 = &v1 - &v2;

        let expected = vec![-9., -7., -5., -3., -1., 1., 3., 5., 7., 9.];
        debug_assert!(v3
            .data
            .iter()
            .enumerate()
            .all(|(i, v)| v.value == expected[i]));
    }

    #[test]
    fn test_in_place_gram_schmidt() {
        let mut id = faer::Mat::<f64>::identity(10, 10);
        in_place_gram_schmidt_cols(&mut id);

        assert_eq!(id, faer::Mat::<f64>::identity(10, 10));

        let mut mat = faer::Mat::<f64>::from_fn(10, 10, |i, j| {
            if i == j {
                return 1.0;
            } else if j == (i + 1) {
                return 1.0;
            } else {
                0.0
            }
        });

        in_place_gram_schmidt_cols(&mut mat);

        assert!(mat == faer::Mat::<f64>::identity(10, 10));

        let vals = vec![
            0.22071751, 0.98385283, 0.26286167, 0.54438886, 0.24495643, 0.47732295,
            0.30220845, 0.71559385, 0.95284662, 0.35611669, 0.57841467, 0.70153062,
            0.7636522, 0.71093363, 0.08964144, 0.28216578, 0.03727329, 0.94636624,
            0.37873543, 0.46478876, 0.0153165, 0.58502921, 0.18614217, 0.24454767,
            0.77572478, 0.33411306, 0.83554303, 0.16901703, 0.06835714, 0.32185485,
            0.18111677, 0.76209253, 0.89085403, 0.4111057, 0.0813274, 0.78844727,
            0.5109696, 0.88248113, 0.47015618, 0.24866142, 0.22692629, 0.61891753,
            0.94364658, 0.75482812, 0.03223807, 0.89739618, 0.0181181, 0.79407004,
            0.06810709, 0.50420521, 0.16351863, 0.86029844, 0.83463673, 0.28957868,
            0.42404003, 0.34391943, 0.48866651, 0.15591374, 0.2229894, 0.08822313,
            0.0656585, 0.42138762, 0.9269305, 0.05802121, 0.66196771, 0.71128645,
            0.30547097, 0.19631256, 0.75986285, 0.58264672, 0.25134602, 0.56815589,
            0.80314659, 0.22454245, 0.66663353, 0.99350991, 0.51195731, 0.86654788,
            0.4145873, 0.54578121, 0.4526697, 0.80164276, 0.83043729, 0.23640752,
            0.22904584, 0.38765076, 0.93915574, 0.21522521, 0.03797434, 0.96034224,
            0.74729766, 0.31205163, 0.47070171, 0.01476088, 0.53774256, 0.21113166,
            0.75762295, 0.15488187, 0.8838923, 0.86582809,
        ];
        let mut count = 0;
        let mut mat = faer::Mat::from_fn(10, 10, |_, _| {
            count += 1;
            vals[count - 1]
        });

        in_place_gram_schmidt_cols(&mut mat);

        for i in 0..mat.nrows() {
            for j in 0..mat.nrows() {
                let res: f64 = mat.col(i).transpose().to_owned() * mat.col(j).to_owned();

                if i == j {
                    assert!((res - 1.0).abs() < 0.000001);
                } else {
                    assert!((res).abs() < 0.000001);
                }
            }
        }

        let mut mat = faer::Mat::from_fn(10, 30, |i, j| {
            if j <= i {
                return 1.0;
            } else {
                return 0.0;
            }
        });

        in_place_gram_schmidt_cols(&mut mat);

        for i in 0..mat.ncols() {
            for j in 0..mat.ncols() {
                let res: f64 = mat.col(i).transpose().to_owned() * mat.col(j).to_owned();

                if i == j && j < mat.nrows() {
                    assert!((res - 1.0).abs() < 0.000001);
                } else {
                    assert!((res).abs() < 0.000001);
                }
            }
        }
    }
}
