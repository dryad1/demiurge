use std::fmt::Debug;

use linear_isomorphic::{InnerSpace, VectorSpace};

/// Find the index of a vector to use for a basis of a set of points.
/// The first vector is assumed to be $(p_1 - p_0)$, the second is $(p_i - p_0)$
/// where $p_i$ is the most orthoganal vector relative to $(p_1 - p_0)$.
pub fn find_ortho_point<V, S>(points: &dyn Fn(usize) -> V, point_count: usize) -> usize
where
    V: InnerSpace<S> + VectorSpace<Scalar = S> + Debug,
    S: linear_isomorphic::RealField,
{
    // Find a direction that is numerically good to orthonormalize.
    let dir = (points(1) - points(0)).normalized();
    let mut dot = S::from(-1.0).unwrap();
    let mut selected_i = 0;
    for i in 2..point_count
    {
        let other_dir = (points(i) - points(0)).normalized();
        let test = other_dir.dot(&dir);
        if test.abs() < S::abs(dot)
        {
            dot = test;
            selected_i = i;
        }
    }
    debug_assert!(
        S::from(1.0).unwrap() - dot.abs() > S::from(1e-3).unwrap(),
        "Point set is too colinear, cannot define plane."
    );

    selected_i
}
