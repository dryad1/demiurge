use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DataStruct, DeriveInput, Expr, Field, Fields};

fn should_skip(field: &Field) -> bool
{
    for attr in &field.attrs
    {
        if attr.path().is_ident("skip")
        {
            return true;
        }
    }

    return false;
}

fn has_optional_attr(field: &Field) -> bool
{
    for attr in &field.attrs
    {
        if attr.path().is_ident("optional")
        {
            return true;
        }
    }

    return false;
}

fn get_optional_attr(field: &Field) -> syn::Expr
{
    for attr in &field.attrs
    {
        if attr.path().is_ident("optional")
        {
            return attr.parse_args::<Expr>().unwrap();
        }
    }
    panic!("{:#?}", field)
}

#[proc_macro_derive(Builder, attributes(optional, skip, builder))]
pub fn builder_derive(input: TokenStream) -> TokenStream
{
    let input = parse_macro_input!(input as DeriveInput);
    let st_name = input.ident;
    let struct_visibility = input.vis;
    let builder_name =
        syn::Ident::new(format!("{}Builder", st_name).as_str(), st_name.span());

    let fields = match input.data
    {
        Data::Struct(DataStruct {
            fields: Fields::Named(fields),
            ..
        }) => fields.named,
        _ => panic!("This derive macro only works on structs with named fields."),
    };

    let mandatory_fields = fields
        .clone()
        .into_iter()
        .filter(|x| !has_optional_attr(x) && !should_skip(x))
        .map(|f| {
            let field_name = f.ident;
            let field_ty = f.ty;

            quote! {
                #field_name : #field_ty,
            }
        });

    let mandatory_fields_init = fields
        .clone()
        .into_iter()
        .filter(|x| !has_optional_attr(x) && !should_skip(x))
        .map(|f| {
            let field_name = f.ident;

            quote! {
                #field_name,
            }
        });

    let optional_field_setters = fields
        .clone()
        .into_iter()
        .filter(|x| has_optional_attr(x))
        .map(|f| {
            let field_name = f.ident;
            let field_ty = f.ty;

            quote! {
                pub fn #field_name(mut self, #field_name : #field_ty) -> Self {
                    self.#field_name = #field_name;
                    self
                }
            }
        });

    let optional_field_defaults = fields
        .clone()
        .into_iter()
        .filter(|x| has_optional_attr(x))
        .map(|f| {
            let val = get_optional_attr(&f);
            let field_name = f.ident;
            quote! {
                #field_name : #val,
            }
        });

    let skip_field_defaults =
        fields
            .clone()
            .into_iter()
            .filter(|x| should_skip(x))
            .map(|f| {
                let field_name = f.ident;
                let field_type = f.ty;
                quote! {
                    #field_name : #field_type::default(),
                }
            });

    let builder_field_setters = fields.clone().into_iter().map(|f| {
        let field_name = f.ident;

        quote! {
            #field_name: self.#field_name,
        }
    });

    let builder_field_defines = fields.clone().into_iter().map(|f| {
        let field_name = f.ident;
        let field_ty = f.ty;

        quote! {
            #field_name: #field_ty,
        }
    });

    let builder = input
        .attrs
        .iter()
        .find(|x| x.path().is_ident("builder"))
        .map(|attr| attr.parse_args::<Expr>().unwrap())
        .unwrap_or_else(|| syn::parse_str::<Expr>("build").unwrap());

    let build_method_name = builder;

    let output = quote! {
        #[automatically_derived]
        impl #st_name {
            #struct_visibility fn new(#(#mandatory_fields)*) -> #builder_name {
                #builder_name {
                    #(#mandatory_fields_init)*
                    #(#optional_field_defaults)*
                    #(#skip_field_defaults)*
                }
            }
        }

        #[automatically_derived]
        #[derive(Debug, Clone)]
        #struct_visibility struct #builder_name
        {
            #(#builder_field_defines)*
        }

        #[automatically_derived]
        impl #builder_name {
            #(#optional_field_setters)*

            #struct_visibility fn #build_method_name (mut self) -> #st_name
            {
                #st_name{
                    #(#builder_field_setters)*
                }
            }
        }
    };

    output.into()
}
