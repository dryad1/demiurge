pub use nalgebra as na;
pub use nalgebra_glm as glm;

pub type Float = f32;
pub type Vec2 = na::Vector2<Float>;
pub type Vec3 = na::Vector3<Float>;
pub type Mat4 = na::Matrix4<Float>;

pub trait CameraProperties {
    fn view_matrix(&self) -> na::Matrix4<Float>;
    fn proj_matrix(&self) -> na::Matrix4<Float>;
}

#[derive(Default)]
pub struct BaseCamera {
    pub position: na::Vector3<Float>,
    pub up: na::Vector3<Float>,
    pub focus_point: na::Vector3<Float>,
    pub fov: Float,
    pub width: u32,
    pub height: u32,
    pub z_near: Float,
    pub z_far: Float,
}

// Perspective projection, `aspect` is `width / height`.
// See naglebra ticket: https://github.com/dimforge/nalgebra/issues/1152
fn perspective(fov: Float, aspect: Float, z_near: Float, z_far: Float) -> Mat4 {
    debug_assert!(aspect > Float::MIN_POSITIVE, "");

    let tan_half_fovy = Float::tan(fov / 2.0);

    let mut result = Mat4::zeros();
    result[(0, 0)] = 1.0 / (aspect * tan_half_fovy);
    result[(1, 1)] = 1.0 / (tan_half_fovy);
    result[(2, 2)] = -(z_far + z_near) / (z_far - z_near);
    result[(2, 3)] = -(2.0 * z_far * z_near) / (z_far - z_near);
    result[(3, 2)] = -1.0;

    return result;
}

impl CameraProperties for BaseCamera {
    fn view_matrix(&self) -> Mat4 {
        return glm::look_at(&self.position, &self.focus_point, &self.up);
    }

    fn proj_matrix(&self) -> Mat4 {
        let tmp = perspective(
            Float::to_radians(self.fov),
            (self.width / self.height) as Float,
            self.z_near,
            self.z_far,
        );

        return tmp;
    }
}

impl BaseCamera {
    pub fn new() -> BaseCamera {
        BaseCamera {
            position: Vec3::new(0.0, -5.0, 0.0),
            up: Vec3::new(0.0, 0.0, 1.0),
            focus_point: Vec3::new(0.0, 0.0, 0.0),
            width: 512,
            height: 512,
            z_near: 0.1,
            z_far: 20_000.0,
            fov: 45.0,
        }
    }
}

pub struct FlyCamera {
    pub base_camera: BaseCamera,
    pub theta: f32,
    pub phi: f32,
    pub speed: Float,
    pub radius: Float,
    pub forward: Vec3,
    pub should_update: bool,
}

impl FlyCamera {
    pub fn new(width: u32, height: u32, radius: Float) -> FlyCamera {
        let mut camera = FlyCamera {
            base_camera: BaseCamera::new(),
            theta: 0.0,
            phi: 0.0,
            speed: 1.0,
            radius: radius,
            forward: Vec3::new(0.0, 1.0, 0.0),
            should_update: true,
        };

        camera.base_camera.position =
            camera.forward * camera.radius + camera.base_camera.focus_point;
        camera.base_camera.width = width;
        camera.base_camera.height = height;
        camera.base_camera.up = -camera.base_camera.up;
        camera
    }

    pub fn update_camera_angles(
        camera: &mut Self,
        _position_x: Float,
        _position_y: Float,
        offset_x: Float,
        offset_y: Float,
    ) {
        let offset = Vec2::new(offset_x, offset_y);

        camera.theta = camera.theta - offset.x * 80.0;
        camera.phi = camera.phi - offset.y * 80.0;

        camera.phi = camera.phi.clamp(-(89.0), 89.0);
    }

    pub fn update_camera_position(
        camera: &mut Self,
        offset_x: Float,
        offset_y: Float,
        offset_z: Float,
    ) {
        let forward = na::Vector3::new(
            camera.theta.to_radians().cos() * camera.phi.to_radians().cos(),
            camera.phi.to_radians().sin(),
            camera.theta.to_radians().sin() * camera.phi.to_radians().cos(),
        )
        .normalize();

        let side = forward.cross(&na::Vector3::y()).normalize();
        let up = forward.cross(&side).normalize();

        let offset_3d =
            (up * offset_y + side * offset_x + forward * offset_z) * camera.speed;

        camera.base_camera.position += offset_3d;
        camera.base_camera.focus_point += offset_3d;
    }

    pub fn update_camera_zoom(camera: &mut Self, _offset_x: Float, offset_y: Float) {
        camera.radius -= offset_y * 50.0;
        camera.radius = camera.radius.max(1.0e-6);
    }

    pub fn front(&self) -> Vec3 {
        na::Vector3::new(
            self.theta.to_radians().cos() * self.phi.to_radians().cos(),
            self.phi.to_radians().sin(),
            self.theta.to_radians().sin() * self.phi.to_radians().cos(),
        )
        .normalize()
    }
}

impl CameraProperties for FlyCamera {
    fn proj_matrix(&self) -> na::Matrix4<Float> {
        return self.base_camera.proj_matrix();
    }

    fn view_matrix(&self) -> na::Matrix4<Float> {
        let front = na::Vector3::new(
            self.theta.to_radians().cos() * self.phi.to_radians().cos(),
            self.phi.to_radians().sin(),
            self.theta.to_radians().sin() * self.phi.to_radians().cos(),
        )
        .normalize();

        let right = front.cross(&na::Vector3::y()).normalize();
        let up = front.cross(&right).normalize();

        let view_matrix = glm::look_at(
            &(self.base_camera.position - front * self.radius),
            &(self.base_camera.position + front), // trick to avoid numeric issues
            &up,
        );

        view_matrix
    }
}

pub struct ArcballCamera {
    pub base_camera: BaseCamera,
    pub rotation: na::UnitQuaternion<Float>,
    pub speed: Float,
    pub radius: Float,
    pub forward: Vec3,
    pub should_update: bool,
}

impl ArcballCamera {
    pub fn new(width: u32, height: u32, radius: Float) -> ArcballCamera {
        let mut camera = ArcballCamera {
            base_camera: BaseCamera::new(),
            rotation: na::UnitQuaternion::<Float>::identity(),
            speed: 10.0,
            radius: radius,
            forward: Vec3::new(0.0, 1.0, 0.0),
            should_update: true,
        };

        camera.base_camera.position =
            camera.forward * camera.radius + camera.base_camera.focus_point;
        camera.base_camera.width = width;
        camera.base_camera.height = height;

        camera
    }

    pub fn update_camera_angles(
        camera: &mut Self,
        position_x: Float,
        position_y: Float,
        offset_x: Float,
        offset_y: Float,
    ) {
        let position = Vec2::new(position_x, -position_y);
        let offset = Vec2::new(offset_x, -offset_y);

        let vb = screen_to_arc_surface(&Vec2::new(position.x, position.y)).normalize();
        let centered_pos = position - offset;
        let va =
            screen_to_arc_surface(&Vec2::new(centered_pos.x, centered_pos.y)).normalize();

        let speed = 1.1;
        let angle = Float::acos(Float::min(1.0, vb.dot(&va))) * speed;
        let angle = Float::min(angle, Float::to_radians(60.0));
        let axis = if Float::abs(angle) > 0.001 {
            vb.cross(&va)
        } else {
            Vec3::new(1.0, 0.0, 0.0)
        };

        let axis_angle: Vec3 = axis.normalize() * angle;
        camera.rotation *= na::UnitQuaternion::<Float>::new(axis_angle);

        let rot = camera.rotation;
        camera.base_camera.position =
            camera.base_camera.focus_point + rot * (camera.forward * camera.radius);
    }

    pub fn update_camera_position(
        camera: &mut Self,
        _pos_x: Float,
        _pos_y: Float,
        offset_x: Float,
        offset_y: Float,
    ) {
        let offset = Vec2::new(offset_x, offset_y);

        let up = camera.rotation * camera.base_camera.up;
        let side = (camera.rotation * (camera.forward.cross(&camera.base_camera.up)))
            .normalize();

        let offset_3d = (up * -offset.y + side * offset.x) * camera.speed;
        camera.base_camera.focus_point += offset_3d;
        let rot = camera.rotation;
        camera.base_camera.position =
            camera.base_camera.focus_point + rot * (camera.forward * camera.radius);
    }

    pub fn update_camera_zoom(&mut self, offset_x: Float, offset_y: Float) {
        let offset = Vec2::new(offset_x, offset_y);

        let speed = 0.06 * self.speed;
        self.radius -= speed * offset.y;
        self.radius = Float::max(0.0001, self.radius);

        self.base_camera.position = self.base_camera.focus_point
            + (self.base_camera.position - self.base_camera.focus_point).normalize()
                * self.radius;
    }
}

impl CameraProperties for ArcballCamera {
    fn proj_matrix(&self) -> na::Matrix4<Float> {
        return self.base_camera.proj_matrix();
    }

    fn view_matrix(&self) -> na::Matrix4<Float> {
        return glm::look_at(
            &self.base_camera.position,
            &self.base_camera.focus_point,
            &(self.rotation * self.base_camera.up),
        );
    }
}

// +| Internal |+ ==============================================================
fn screen_to_arc_surface(pos: &na::Vector2<Float>) -> na::Vector3<Float> {
    let radius: Float = 1.0; // Controls the speed
    if pos.x * pos.x + pos.y * pos.y >= (radius * radius) / 2.0 - 0.00001 {
        // This is equal to (r^2 / 2) / (sqrt(x^2 + y^2)) since the magnitude of
        // the vector is sqrt(x^2 + y^2).
        return na::Vector3::<Float>::new(
            pos.x,
            -(radius * radius / 2.0) / (pos.norm()),
            pos.y,
        );
    }

    return na::Vector3::<Float>::new(
        pos.x,
        -Float::sqrt(radius * radius - (pos.x * pos.x + pos.y * pos.y)),
        pos.y,
    );
}
