import os
import subprocess
import argparse

def process_gltf_files(input_directory, output_directory):
    # Get a list of all files in the specified input directory
    files = os.listdir(input_directory)

    # Filter only GLTF files
    gltf_files = [file for file in files if file.lower().endswith('.gltf')]

    # Run the command for each GLTF file
    for gltf_file in gltf_files:
        input_path = os.path.join(input_directory, gltf_file)
        output_path = output_directory
        command = f'cargo run --release -- -i {input_path} -o {output_path} -n {gltf_file[:-5]}'
        subprocess.run(command, shell=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process GLTF files and convert them to OBJ using a specified command.')
    parser.add_argument('-i', '--input', required=True, help='Input directory containing GLTF files with one level of nesting.')
    parser.add_argument('-o', '--output', required=True, help='Output directory for OBJ files.')
    args = parser.parse_args()

    process_gltf_files(args.input, args.output)
