#![allow(dead_code)]

use std::fs;

use clap::Parser;
use gltf_loader::*;

fn set_skin_to_default(skin: &Skin, gltf: &Gltf)
{
    let mat_data = gltf
        .data_from_accessor::<Mat4>(skin.inverse_bind_matrices)
        .unwrap();

    for mat in mat_data
    {
        *mat = Mat4::identity();
    }
}

fn scale_skin(scale: f32, skin: &Skin, gltf: &Gltf)
{
    let mat_data = gltf
        .data_from_accessor::<Mat4>(skin.inverse_bind_matrices)
        .unwrap();

    for mat in mat_data
    {
        let mut slice = mat.slice_mut((0, 3), (3, 1));

        slice *= scale;
    }
}

fn translate_skin(offset: &Vec3, skin: &Skin, gltf: &Gltf)
{
    let mat_data = gltf
        .data_from_accessor::<Mat4>(skin.inverse_bind_matrices)
        .unwrap();

    for mat in mat_data
    {
        let mut slice = mat.slice_mut((0, 3), (3, 1));
        slice += offset;
    }
}

fn set_animation_to_default(animation: &Animation, gltf: &Gltf)
{
    for channel in &animation.channels
    {
        match channel.target.path
        {
            ChannelPath::TRANSLATION =>
            {
                let sampler = &animation.samplers[channel.sampler];
                let accessor_id = sampler.output;
                let data = gltf.data_from_accessor::<Vec3>(accessor_id).unwrap();

                for datum in data
                {
                    *datum = Vec3::new(0.0, 0.0, 0.0);
                }
            }
            ChannelPath::ROTATION =>
            {
                let sampler = &animation.samplers[channel.sampler];
                let accessor_id = sampler.output;
                let data = gltf.data_from_accessor::<Quatf>(accessor_id).unwrap();

                for datum in data
                {
                    *datum = Quatf::default();
                }
            }
            ChannelPath::SCALE =>
            {
                let sampler = &animation.samplers[channel.sampler];
                let accessor_id = sampler.output;
                let data = gltf.data_from_accessor::<Vec3>(accessor_id).unwrap();

                for datum in data
                {
                    *datum = Vec3::new(1.0, 1.0, 1.0);
                }
            }
        }
    }
}

fn scale_animation(scale: f32, animation: &Animation, gltf: &Gltf)
{
    for channel in &animation.channels
    {
        match channel.target.path
        {
            ChannelPath::TRANSLATION =>
            {
                let sampler = &animation.samplers[channel.sampler];

                let accessor_id = sampler.output;

                let data = gltf.data_from_accessor::<Vec3>(accessor_id).unwrap();
                for datum in data
                {
                    *datum *= scale;
                }
            }
            ChannelPath::ROTATION | ChannelPath::SCALE =>
            {
                continue;
            }
        }
    }
}

fn scale_nodes(scale: f32, gltf: &mut Gltf)
{
    for node in &mut gltf.nodes
    {
        node.translation *= scale;
    }
}

fn set_nodes_to_default(gltf: &mut Gltf)
{
    for node in &mut gltf.nodes
    {
        node.translation = Vec3::new(0.0, 0.0, 0.0);
        node.rotation = Quatf::default();
        node.scale = Vec3::new(1.0, 1.0, 1.0);
    }
}

fn scale_mesh(scale: f32, mesh: &Mesh, gltf: &Gltf)
{
    for primitive in &mesh.primitives
    {
        let vert_data = gltf
            .data_from_accessor::<Vec3>(primitive.attributes.position)
            .unwrap();

        for position in vert_data
        {
            *position *= scale;
        }
    }
}

fn from_vec3(vec3: Vec3) -> serde_json::Value
{
    type Val = serde_json::Value;

    let x = serde_json::Value::from(vec3.x);
    let y = serde_json::Value::from(vec3.y);
    let z = serde_json::Value::from(vec3.z);

    return serde_json::Value::Array([x, y, z].to_vec());
}

fn from_quatf(quat: Quatf) -> serde_json::Value
{
    type Val = serde_json::Value;

    let w = serde_json::Value::from(quat.w);
    let x = serde_json::Value::from(quat.i);
    let y = serde_json::Value::from(quat.j);
    let z = serde_json::Value::from(quat.k);

    return serde_json::Value::Array([x, y, z, w].to_vec());
}

fn from_vec4(vec4: Vec4) -> serde_json::Value
{
    type Val = serde_json::Value;

    let x = serde_json::Value::from(vec4.x);
    let y = serde_json::Value::from(vec4.y);
    let z = serde_json::Value::from(vec4.z);
    let w = serde_json::Value::from(vec4.z);

    return serde_json::Value::Array([x, y, z, w].to_vec());
}

fn scale_data(args: &Arguments)
{
    let data = fs::read_to_string(&args.path).expect("Unable to read file");

    let mut gltf = Gltf::from_string(&data);

    for animation in &gltf.animations
    {
        scale_animation(args.scale.unwrap(), &animation, &gltf);
    }

    for skin in &gltf.skins
    {
        scale_skin(args.scale.unwrap(), &skin, &gltf);
    }

    scale_nodes(args.scale.unwrap(), &mut gltf);

    for mesh in &gltf.meshes
    {
        scale_mesh(args.scale.unwrap(), &mesh, &gltf);
    }

    let mut json: serde_json::Value =
        serde_json::from_str(&data).expect("JSON was not well-formatted");
    let encoded = format!(
        "data:application/octet-stream;base64,{}",
        base64::encode(&gltf.buffers[0])
    );

    for (i, node) in &mut gltf.nodes.iter().enumerate()
    {
        let jnode = &mut json["nodes"].as_array_mut().unwrap()[i];
        jnode["translation"] = from_vec3(node.translation);
        jnode["rotation"] = from_quatf(node.rotation);
        jnode["scale"] = from_vec3(node.scale);
    }

    json["buffers"].as_array_mut().unwrap()[0]["uri"] =
        serde_json::Value::String(encoded);

    fs::write(&args.out, serde_json::to_string_pretty(&json).unwrap()).unwrap();
}

fn append_animations(args: &Arguments)
{
    let path1 = args.path.clone();
    let path2 = args.append_from.as_ref().unwrap().clone();
    let data = fs::read_to_string(&path1)
        .expect(format!("Unable to read file {}.", &path1).as_str());
    let mut gltf1 = Gltf::from_string(&data);

    let data = fs::read_to_string(&path2)
        .expect(format!("Unable to read file {}.", &path2).as_str());
    let gltf2 = Gltf::from_string(&data);

    Gltf::append_animations(&mut gltf1, &gltf2);
    let mut json_string = serde_json::to_string_pretty(&gltf1).unwrap();
    json_string = json_string.replace("kind", "type");

    fs::write(&args.out, json_string).unwrap();
}

fn rename_animations(args: &Arguments)
{
    let path1 = args.path.clone();
    let data = fs::read_to_string(&path1)
        .expect(format!("Unable to read file {}.", &path1).as_str());
    let mut gltf = Gltf::from_string(&data);

    gltf.rename_animation(
        args.animation_id.unwrap(),
        args.rename_animation.as_ref().unwrap(),
    );
    let mut json_string = serde_json::to_string_pretty(&gltf).unwrap();
    json_string = json_string.replace("kind", "type");

    fs::write(&args.out, json_string).unwrap();
}

fn delete_animations(args: &Arguments)
{
    let path1 = args.path.clone();
    let data = fs::read_to_string(&path1)
        .expect(format!("Unable to read file {}.", &path1).as_str());
    let mut gltf = Gltf::from_string(&data);

    gltf.remove_animation(args.animation_id.unwrap());
    let mut json_string = serde_json::to_string_pretty(&gltf).unwrap();
    json_string = json_string.replace("kind", "type");

    fs::write(&args.out, json_string).unwrap();
}

fn report_stats(args: &Arguments)
{
    let path1 = args.path.clone();
    let data = fs::read_to_string(&path1)
        .expect(format!("Unable to read file {}.", &path1).as_str());
    let gltf = Gltf::from_string(&data);

    println!("{:?}", gltf.aabb_dims());
}

#[derive(Parser, Debug)]
#[clap(author, version, about="Command line utility for editing gltf files.", long_about = None)]
struct Arguments
{
    /// Path to input gltf file.
    #[clap(short, long, value_parser)]
    path: String,
    /// Path to output gltf file.
    #[clap(short, long, value_parser)]
    out: String,
    /// If present, the file will be scaled by the specified amount.
    #[clap(short, long, value_parser)]
    scale: Option<f32>,
    /// If present, the animations from this file will be added to the input.
    #[clap(short, long, value_parser)]
    append_from: Option<String>,
    /// If present, renames the animation with "new name".
    #[clap(short, long, value_parser)]
    rename_animation: Option<String>,
    /// If present, deletes the animation.
    #[clap(short = 'd', long, value_parser, takes_value(false))]
    delete_animation: bool,
    /// If present, specifies the animation at animation_id.
    #[clap(short = 'i', long, value_parser)]
    animation_id: Option<usize>,
    /// If present, specifies the animation at animation_id.
    #[clap(long, value_parser, takes_value(false))]
    stats: bool,
}


fn main()
{
    let args = Arguments::parse();

    if args.scale.is_some()
    {
        scale_data(&args);
    }

    if args.append_from.is_some()
    {
        append_animations(&args);
    }

    if args.rename_animation.is_some()
    {
        rename_animations(&args);
    }

    if args.delete_animation
    {
        delete_animations(&args);
    }

    if args.stats
    {
        report_stats(&args);
    }
}
