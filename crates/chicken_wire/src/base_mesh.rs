use std::{fs, mem::size_of};

use gltf_loader::*;

#[repr(C)]
#[derive(Default, Debug, Clone)]
pub struct BaseMesh
{
    pub positions: Vec<Vec3>,
    pub normals: Vec<Vec3>,
    pub uvs: Vec<Vec3>,
    pub indices: Vec<u32>,
}

impl BaseMesh
{
    pub fn serialize(&self) -> (Vec<*const u8>, Vec<usize>, *const u32, usize)
    {
        let mut buffers = Vec::<*const u8>::new();
        let mut sizes = Vec::<usize>::new();

        buffers.push(self.positions.as_ptr() as *const u8);
        sizes.push(self.positions.len() * size_of::<Vec3>());

        if !self.normals.is_empty()
        {
            buffers.push(self.normals.as_ptr() as *const u8);
            sizes.push(self.normals.len() * size_of::<Vec3>());
            debug_assert!(
                *sizes.last().unwrap() == sizes[0],
                "Are {} and {}.",
                *sizes.last().unwrap(),
                sizes[0]
            )
        }
        if !self.uvs.is_empty()
        {
            buffers.push(self.uvs.as_ptr() as *const u8);
            sizes.push(self.uvs.len() * size_of::<Vec3>());
        }

        return (buffers, sizes, self.indices.as_ptr(), self.indices.len());
    }

    pub fn base_meshes_from_path(path: &str) -> Vec<BaseMesh>
    {
        let data = fs::read_to_string(path)
            .expect(format!("Unable to read file {}.", path).as_str());

        let gltf = Gltf::from_string(&data, path);

        return Self::from_gltf(&gltf);
    }

    pub fn from_gltf(gltf: &Gltf) -> Vec<BaseMesh>
    {
        let mut meshes = Vec::<BaseMesh>::new();

        for mesh in &gltf.meshes
        {
            meshes.push(merge_primitives(&mesh.primitives, &gltf));
            meshes.last_mut().unwrap().fill_data_with_defaults();
        }

        meshes
    }

    pub fn fill_data_with_defaults(&mut self)
    {
        if self.uvs.len() < self.positions.len()
        {
            for _ in self.uvs.len()..self.positions.len()
            {
                self.uvs.push(Vec3::new(0.0, 0.0, 0.0));
            }
        }

        if self.normals.len() < self.positions.len()
        {
            for _ in self.normals.len()..self.positions.len()
            {
                self.normals.push(Vec3::new(0.0, 0.0, 1.0));
            }
        }
    }
}

// +| Internal |+ ==============================================================
fn merge_primitives(primitives: &Vec<Primitive>, gltf: &Gltf) -> BaseMesh
{
    let mut mesh = BaseMesh::default();

    let mut current_offset = 0;
    for primitive in primitives
    {
        let vert_data: &mut [Vec3] = gltf
            .data_from_accessor::<Vec3>(primitive.attributes.position)
            .unwrap_or_default();

        let normal_data: &mut [Vec3] = gltf
            .data_from_accessor::<Vec3>(primitive.attributes.normal)
            .unwrap_or_default();

        let uv_data: &mut [Vec2] = gltf
            .data_from_accessor::<Vec2>(primitive.attributes.tex_coords)
            .unwrap_or_default();

        let index_data = load_indices(primitive.indices, gltf);

        mesh.positions.extend_from_slice(vert_data);

        if !normal_data.is_empty()
        {
            mesh.normals.extend_from_slice(&normal_data);
        }

        // Store the texture id on the uvs. This way we can properly texture the model.
        if !uv_data.is_empty()
        {
            for uv in uv_data
            {
                let material_id = primitive.material.unwrap();
                let base_color_texture = gltf.materials[material_id]
                    .pbr_metallic_roughness
                    .base_color_texture
                    .as_ref();
                let im_id = match base_color_texture
                {
                    None => usize::MAX,
                    Some(id) => gltf.textures[id.index].source,
                };

                mesh.uvs.push(Vec3::new(uv.x, uv.y, im_id as f32));
            }
        }

        for index in index_data.iter()
        {
            assert!(*index < vert_data.len() as u32);
            mesh.indices.push(*index + current_offset as u32);
        }
        current_offset += vert_data.len();
    }

    mesh
}

fn load_indices(accessor_id: usize, gltf: &Gltf) -> Vec<u32>
{
    let accessor = &gltf.accessors[accessor_id];
    let mut result = Vec::<u32>::new();

    match accessor.component_type
    {
        ComponentType::SBYTE =>
        {
            for datum in gltf
                .data_from_accessor::<i8>(accessor_id)
                .unwrap_or_default()
            {
                result.push(*datum as u32);
            }
        }
        ComponentType::USHORT =>
        {
            for datum in gltf
                .data_from_accessor::<u16>(accessor_id)
                .unwrap_or_default()
            {
                result.push(*datum as u32);
            }
        }
        ComponentType::UINT =>
        {
            for datum in gltf
                .data_from_accessor::<u32>(accessor_id)
                .unwrap_or_default()
            {
                result.push(*datum as u32);
            }
        }
        _ =>
        {
            panic!("Not implemented.")
        }
    }

    return result;
}
