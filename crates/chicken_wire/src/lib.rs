use algebra::*;
pub use base_mesh::*;
pub use linear_isomorphic::*;
use pga::Motor;
use skeleton::Skeleton;
pub use skin::*;
pub use texture::*;

pub mod base_mesh;
mod skeletal_animation;
pub mod skeleton;
pub mod skin;
pub mod texture;


use std::fs;

pub use gltf_loader::Gltf;
pub use wavefront_loader;

pub struct AnimatedDataAssociation
{
    pub mesh_id: usize,
    pub skin_id: usize,
    pub skeleton_id: usize,
    pub animation_set_id: usize,
}
#[derive(Debug)]
pub struct AnimationSet
{
    pub animations: Vec<skeletal_animation::SkeletalAnimation>,
}

impl AnimationSet
{
    pub fn sample(&self, time: f32, skeleton: &Skeleton, anim: usize) -> Vec<Motor>
    {
        let mut local_motors =
            Vec::<Motor>::with_capacity(self.animations[anim].joint_animations.len());

        for joint_animation in &self.animations[anim].joint_animations
        {
            local_motors.push(joint_animation.sample(time));
        }

        return get_global_joint_motors(&local_motors, skeleton);
    }

    fn from_multi_vec(
        animations_sets: &Vec<Vec<skeletal_animation::SkeletalAnimation>>,
    ) -> Vec<AnimationSet>
    {
        let mut sets = Vec::with_capacity(animations_sets.len());
        for set in animations_sets
        {
            sets.push(AnimationSet {
                animations: set.clone(),
            });
        }
        sets
    }
}

pub fn load_static_meshes_from_path(path: &str) -> Vec<BaseMesh>
{
    let data = fs::read_to_string(&path)
        .expect(format!("Unable to read file {}.", &path).as_str());

    let gltf = Gltf::from_string(&data, path);

    load_static_meshes_from_gltf(&gltf)
}

pub fn load_static_meshes_from_gltf(gltf: &Gltf) -> Vec<BaseMesh>
{
    BaseMesh::from_gltf(gltf)
}

pub fn load_skinned_data_from_path(
    path: &str,
) -> (
    Vec<BaseMesh>,
    Vec<skin::Skin>,
    Vec<skeleton::Skeleton>,
    Vec<AnimationSet>,
    Vec<AnimatedDataAssociation>,
)
{
    let data = fs::read_to_string(&path)
        .expect(format!("Unable to read file {}.", &path).as_str());

    let gltf = Gltf::from_string(&data, path);

    return load_skinned_data_from_gltf(&gltf);
}

pub fn load_skinned_data_from_gltf(
    gltf: &Gltf,
) -> (
    Vec<BaseMesh>,
    Vec<skin::Skin>,
    Vec<skeleton::Skeleton>,
    Vec<AnimationSet>,
    Vec<AnimatedDataAssociation>,
)
{
    let meshes = BaseMesh::from_gltf(gltf);
    let skins = skin::Skin::from_gltf(gltf);
    let skeletons = skeleton::Skeleton::from_gltf(gltf);
    let animations = skeletal_animation::SkeletalAnimation::from_gltf(gltf);

    assert!(skeletons.len() == animations.len());
    assert!(meshes.len() == skins.len());

    let mut animation_associations = Vec::new();
    for node in &gltf.nodes
    {
        let mesh = node.mesh;
        let skin = node.skin;
        match (mesh, skin)
        {
            (Some(mesh), Some(skin)) =>
            {
                // Mesh id, skin id, skeleton id,
                animation_associations.push(AnimatedDataAssociation {
                    mesh_id: mesh,
                    skin_id: mesh,
                    skeleton_id: skin,
                    animation_set_id: skin,
                });
            }
            _ =>
            {}
        }
    }

    (
        meshes,
        skins,
        skeletons,
        AnimationSet::from_multi_vec(&animations),
        animation_associations,
    )
}

pub fn skeleton_to_graph(skeleton: &Skeleton) -> (Vec<Vec3>, Vec<[usize; 2]>)
{
    // Extract local coordiantes for each joint by inverting the inverse pose
    // data.
    let mut points = Vec::new();
    for mv in &skeleton.inverse_poses
    {
        let tmp = Motor::from_multi_vec(&mv.reverse());
        let p = tmp.sandwich(&Vec3::new(0.0, 0.0, 0.0));

        points.push(p);
    }

    // Extract line topology for the skeleton by finding the parent of each node.
    let mut indices = Vec::<[usize; 2]>::with_capacity(skeleton.inverse_poses.len() * 2);
    for (id, parent_id) in skeleton.joints.iter().enumerate()
    {
        indices.push([id, *parent_id]);
    }

    (points, indices)
}

fn get_global_joint_motors(
    animation_motors: &Vec<Motor>,
    skeleton: &Skeleton,
) -> Vec<Motor>
{
    let skeleton = &skeleton.joints;
    let mut skeleton_motors = Vec::<Motor>::with_capacity(skeleton.len());
    unsafe {
        skeleton_motors.set_len(skeleton.len());
        skeleton_motors.as_mut_ptr().add(0).write(Motor::identity());

        for child in 0..skeleton.len()
        {
            let parent = skeleton[child];
            skeleton_motors
                .as_mut_ptr()
                .add(child)
                .write(skeleton_motors[parent] * animation_motors[child]);
        }
    }

    return skeleton_motors;
}
