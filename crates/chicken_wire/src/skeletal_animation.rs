use gltf_loader::*;
use pga::*;

use crate::skeleton::skin_to_permutation_map;

#[derive(Clone, Default, Debug)]
pub struct SkeletalAnimation
{
    pub joint_animations: Vec<JointAnimation>,
}

impl SkeletalAnimation
{
    /// Return is nested as Animation sets per skin, then an animation set
    /// (e.g. {Walking, Running, Idle}), then a single animation for a mesh.
    pub fn from_gltf(gltf: &Gltf) -> Vec<Vec<SkeletalAnimation>>
    {
        let rest_transforms = load_rest_transforms_from_gltf(gltf);
        let node_animations = load_animations_from_gltf(gltf);

        // For each mesh there may a skin, each skin needs its own animation set.
        let mut skin_animation_sets = Vec::new();
        for skin in &gltf.skins
        {
            let mut skeletal_animation_sets = Vec::new();
            let sparse_to_compact_map = skin_to_permutation_map(skin);

            for animation_set in &node_animations
            {
                let mut skeletal_animation = SkeletalAnimation {
                    joint_animations: vec![
                        JointAnimation::default();
                        sparse_to_compact_map.len()
                    ],
                };
                // Initialize animations to rest pose in case no animation targets the
                // node.
                for (&sparse_id, &compact_id) in &sparse_to_compact_map
                {
                    skeletal_animation.joint_animations[compact_id] =
                        rest_transforms[sparse_id].clone();
                }

                for (&sparse_id, &compact_id) in &sparse_to_compact_map
                {
                    skeletal_animation.joint_animations[compact_id]
                        .update_from(&animation_set[sparse_id]);
                }
                skeletal_animation_sets.push(skeletal_animation);
            }
            skin_animation_sets.push(skeletal_animation_sets);
        }

        skin_animation_sets
    }
}

#[derive(Clone, Default, Debug)]
pub struct JointAnimation
{
    pub rotations: Vec<Rotor>,
    pub translations: Vec<Translator>,
    pub rotation_times: Vec<f32>,
    pub translation_times: Vec<f32>,
}

impl JointAnimation
{
    pub fn sample(&self, time: f32) -> Motor
    {
        let (index, t1, t2, t) = find_interval(time, &self.rotation_times);

        let r1 = self.rotations[index];
        let r2 = self.rotations[(index + 1) % self.rotations.len()];
        let t = (t - t1) / (t2 - t1);

        let rot = Rotor::slerp(&r1, &r2, t);

        let (index, t1, t2, t) = find_interval(time, &self.translation_times);
        let u1 = &self.translations[index];
        let u2 = &self.translations[(index + 1) % self.translations.len()];
        let t = (t - t1) / (t2 - t1);
        let trans = (1.0 - t) * *u1 + t * *u2;

        trans * rot
    }

    fn update_from(&mut self, other: &JointAnimation)
    {
        if !other.translations.is_empty()
        {
            self.translations = other.translations.clone();
            self.translation_times = other.translation_times.clone();
        }

        if !other.rotations.is_empty()
        {
            self.rotations = other.rotations.clone();
            self.rotation_times = other.rotation_times.clone();
        }
    }
}

// +| Internal |+ ==============================================================
fn find_interval(time: f32, times: &Vec<f32>) -> (usize, f32, f32, f32)
{
    let t = f32::rem_euclid(time, *times.last().unwrap());
    let mut index = 0;
    let mut key_time1 = times[0];
    let mut key_time2 = times[1];
    let key_time_num = times.len();

    while !(t >= key_time1 && t < key_time2)
    {
        index = (index + 1) % key_time_num;
        key_time1 = if times[index] >= *times.last().unwrap()
        {
            0.0
        }
        else
        {
            times[index]
        };

        key_time2 = times[(index + 1) % key_time_num];
    }

    return (index, key_time1, key_time2, t);
}

fn load_rest_transforms_from_gltf(gltf: &Gltf) -> Vec<JointAnimation>
{
    let mut rest_transforms = Vec::new();
    for node in &gltf.nodes
    {
        let pos = node.translation;
        let rot = node.rotation;
        let scale = node.scale;

        // Due to PGA, we can't handle scalings, we would not want to anyway.
        debug_assert!((scale.x - 1.0).abs() < 0.0001, "x is {}!=1", scale.x);
        debug_assert!((scale.y - 1.0).abs() < 0.0001, "y is {}!=1", scale.y);
        debug_assert!((scale.z - 1.0).abs() < 0.0001, "z is {}!=1", scale.z);

        let rot = quaternion_to_rotor(&rot);
        let pos = vector_to_translator(&pos);

        rest_transforms.push(JointAnimation {
            rotations: vec![rot; 2],
            translations: vec![pos; 2],
            translation_times: vec![0.0, 1.0],
            rotation_times: vec![0.0, 1.0],
        });
    }

    rest_transforms
}

fn load_animations_from_gltf(gltf: &Gltf) -> Vec<Vec<JointAnimation>>
{
    let mut skeletal_animations = Vec::new();
    for anim in &gltf.animations
    {
        let mut animations = vec![JointAnimation::default(); gltf.nodes.len()];
        for channel_id in 0..anim.channels.len()
        {
            let target_node = anim.channels[channel_id].target.node;
            load_animation(&mut animations[target_node], channel_id, anim, gltf);
        }

        skeletal_animations.push(animations);
    }

    skeletal_animations
}

fn load_animation(
    j_animation: &mut JointAnimation,
    channel_id: usize,
    animation: &Animation,
    gltf: &Gltf,
)
{
    let channel = &animation.channels[channel_id];
    let sampler = &animation.samplers[channel.sampler];

    match channel.target.path
    {
        ChannelPath::ROTATION =>
        {
            debug_assert!(j_animation.rotations.len() == 0);
            for rot in gltf.data_from_accessor::<Quatf>(sampler.output).unwrap()
            {
                j_animation
                    .rotations
                    .push(quaternion_to_rotor(&Quatf::from_quaternion(
                        rot.normalize(),
                    )));
            }

            for time in gltf.data_from_accessor::<f32>(sampler.input).unwrap()
            {
                j_animation.rotation_times.push(*time);
            }

            if j_animation.rotation_times.len() == 1
            {
                j_animation.rotations.push(j_animation.rotations[0]);
                j_animation
                    .rotation_times
                    .push(j_animation.rotation_times[0] + 1.0);
            }
        }
        ChannelPath::TRANSLATION =>
        {
            debug_assert!(j_animation.translations.len() == 0);
            for trans in gltf.data_from_accessor::<Vec3>(sampler.output).unwrap()
            {
                j_animation.translations.push(vector_to_translator(trans));
            }
            for time in gltf.data_from_accessor::<f32>(sampler.input).unwrap()
            {
                j_animation.translation_times.push(*time);
            }

            if j_animation.translation_times.len() == 1
            {
                j_animation.translations.push(j_animation.translations[0]);
                j_animation
                    .translation_times
                    .push(j_animation.translation_times[0] + 1.0);
            }
        }

        _ =>
        {}
    }
}

fn quaternion_to_rotor(rotation: &Quatf) -> Rotor
{
    let mut rot = Rotor::zero();
    *rot.s_mut() = rotation.coords.w;
    *rot.e23_mut() = -rotation.coords.x;
    *rot.e31_mut() = -rotation.coords.y;
    *rot.e12_mut() = -rotation.coords.z;

    rot
}

fn vector_to_translator(trans: &Vec3) -> Translator
{
    let mut translator = Translator::zero();
    *translator.e01_mut() = -0.5 * trans.x;
    *translator.e02_mut() = -0.5 * trans.y;
    *translator.e03_mut() = -0.5 * trans.z;
    *translator.s_mut() = 1.0;

    translator
}
