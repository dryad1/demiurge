use std::collections::HashMap;

use gltf_loader::*;
use pga::*;

#[derive(Clone, Default, Debug)]
pub struct Skeleton
{
    pub joints: Vec<usize>,
    pub inverse_poses: Vec<MultiVec3D>,
}

impl Skeleton
{
    pub fn from_gltf(gltf: &Gltf) -> Vec<Skeleton>
    {
        let mut skeletons = Vec::new();

        for skin in &gltf.skins
        {
            skeletons.push(Skeleton {
                joints: build_skeleton_from_skin(&skin, gltf),
                inverse_poses: Vec::new(),
            });

            for datum in gltf
                .data_from_accessor::<Mat4>(skin.inverse_bind_matrices)
                .unwrap_or_default()
            {
                skeletons
                    .last_mut()
                    .unwrap()
                    .inverse_poses
                    .push(inverse_homogeneous_to_multivec(datum));
            }
        }

        skeletons
    }
}

pub(crate) fn skin_to_permutation_map(skin: &Skin) -> HashMap<usize, usize>
{
    let mut map = HashMap::new();

    // This computes where in a compact array for the skin the node will end up.
    for (count, &node_id) in skin.joints.iter().enumerate()
    {
        map.insert(node_id, count);
    }

    map
}

fn build_skeleton_from_skin(skin: &Skin, gltf: &Gltf) -> Vec<usize>
{
    let mut skeleton = vec![0; skin.joints.len()];

    let root = match skin.root
    {
        None => skin.joints[0],
        Some(x) => x,
    };

    let sparse_to_compact_map = skin_to_permutation_map(skin);
    let mut stack = vec![(root, root)];
    while !stack.is_empty()
    {
        let (parent, child) = stack.pop().unwrap();
        let c_parent = *sparse_to_compact_map.get(&parent).unwrap();
        let c_child = *sparse_to_compact_map.get(&child).unwrap();

        skeleton[c_child] = c_parent;

        for grand_child in &gltf.nodes[child].children
        {
            // Sometimes the nodes have children not in the skin.
            if skin.joints.contains(grand_child)
            {
                stack.push((child, *grand_child));
            }
        }
    }

    return skeleton;
}

// https://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/
fn inverse_homogeneous_to_multivec(mat: &Mat4) -> MultiVec3D
{
    let m00 = mat[(0, 0)];
    let m01 = mat[(1, 0)];
    let m02 = mat[(2, 0)];
    let m10 = mat[(0, 1)];
    let m11 = mat[(1, 1)];
    let m12 = mat[(2, 1)];
    let m20 = mat[(0, 2)];
    let m21 = mat[(1, 2)];
    let m22 = mat[(2, 2)];
    let m30 = mat[(0, 3)];
    let m31 = mat[(1, 3)];
    let m32 = mat[(2, 3)];

    let trace = m00 + m11 + m22;

    let mut tran = MultiVec3D::zero();
    *tran.e01_mut() = -0.5 * m30;
    *tran.e02_mut() = -0.5 * m31;
    *tran.e03_mut() = -0.5 * m32;
    *tran.s_mut() = 1.0;

    if trace > 0.0
    {
        let s = (trace + 1.0).sqrt() * 2.0;
        let mut rot = MultiVec3D::identity();
        *rot.s_mut() = s / 4.0;
        *rot.e23_mut() = (m21 - m12) / s;
        *rot.e31_mut() = (m02 - m20) / s;
        *rot.e12_mut() = (m10 - m01) / s;

        return tran * rot;
    }
    else if m00 > m11 && m00 > m22
    {
        let s = (1.0 + m00 - m11 - m22).sqrt() * 2.0;
        let mut rot = MultiVec3D::identity();
        *rot.s_mut() = (m21 - m12) / s;
        *rot.e23_mut() = s / 4.0;
        *rot.e31_mut() = (m01 + m10) / s;
        *rot.e12_mut() = (m02 + m20) / s;

        return tran * rot;
    }
    else if m11 > m22
    {
        let s = (1.0 + m11 - m00 - m22).sqrt() * 2.0;
        let mut rot = MultiVec3D::identity();
        *rot.s_mut() = (m02 - m20) / s;
        *rot.e23_mut() = (m01 + m10) / s;
        *rot.e31_mut() = s / 4.0;
        *rot.e12_mut() = (m12 + m21) / s;

        return tran * rot;
    }
    else
    {
        let s = (1.0 + m22 - m00 - m11).sqrt() * 2.0;
        let mut rot = MultiVec3D::identity();
        *rot.s_mut() = (m10 - m01) / s;
        *rot.e23_mut() = (m02 + m20) / s;
        *rot.e31_mut() = (m12 + m21) / s;
        *rot.e12_mut() = s / 4.0;

        return tran * rot;
    }
}
