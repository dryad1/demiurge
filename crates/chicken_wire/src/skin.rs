use gltf_loader::*;

#[derive(Default, Debug)]
pub struct Skin
{
    pub joints: Vec<IVec4>,
    pub weights: Vec<Vec4>,
}

impl Skin
{
    pub fn from_gltf(gltf: &Gltf) -> Vec<Skin>
    {
        let mut skins = Vec::new();
        for mesh in &gltf.meshes
        {
            let mut skin = Skin::default();
            for primitive in &mesh.primitives
            {
                let weight_attribute_id = primitive.attributes.weights0;
                let joint_attribute_id = primitive.attributes.joint0;

                if primitive.attributes.weights0 == !0
                    || primitive.attributes.joint0 == !0
                {
                    continue;
                }

                let w = gltf
                    .data_from_accessor::<Vec4>(weight_attribute_id)
                    .unwrap();

                skin.weights.extend_from_slice(w);

                match gltf.accessors[joint_attribute_id].component_type
                {
                    ComponentType::UBYTE =>
                    {
                        let j = gltf
                            .data_from_accessor::<BVec4>(joint_attribute_id)
                            .unwrap();
                        for joint in j
                        {
                            skin.joints.push(IVec4::new(
                                joint[0] as i32,
                                joint[1] as i32,
                                joint[2] as i32,
                                joint[3] as i32,
                            ));
                        }
                    }
                    ComponentType::USHORT =>
                    {
                        let j = gltf
                            .data_from_accessor::<USVec4>(joint_attribute_id)
                            .unwrap();
                        for joint in j
                        {
                            skin.joints.push(IVec4::new(
                                joint[0] as i32,
                                joint[1] as i32,
                                joint[2] as i32,
                                joint[3] as i32,
                            ));
                        }
                    }
                    _ =>
                    {
                        todo!()
                    }
                }
            }
            skins.push(skin);
        }
        skins
    }
}
