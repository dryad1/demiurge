use std::fs;
use std::path::Path;

use dem_core::rendering::*;
use gltf_loader::*;

#[derive(Debug)]
pub struct Texture
{
    pub width: usize,
    pub height: usize,
    pub channel_num: u8,
    pub format: ImageFormat,
    pub data: Vec<u8>,
}

impl Texture
{
    pub fn from_path(path: &str) -> Vec<Texture>
    {
        let data = fs::read_to_string(&path)
            .expect(format!("Unable to read file {}.", &path).as_str());

        let gltf = Gltf::from_string(&data, path);

        Self::from_gltf(&gltf)
    }

    pub fn from_gltf(gltf: &Gltf) -> Vec<Texture>
    {
        let mut textures = Vec::<Texture>::with_capacity(gltf.images.len());
        for image in &gltf.images
        {
            let byte_offset = gltf.buffer_views[image.buffer_view].byte_offset;
            let byte_length = gltf.buffer_views[image.buffer_view].byte_length;
            let buffer_id = gltf.buffer_views[image.buffer_view].buffer;

            let bytes = if !image.uri.is_empty()
            {
                let path = Path::new(&gltf.data_path)
                    .parent()
                    .unwrap()
                    .join(&image.uri);
                fs::read(path).unwrap()
            }
            else
            {
                gltf.buffers[buffer_id][byte_offset..(byte_offset + byte_length)].to_vec()
            };


            let image_data = image::load_from_memory(&bytes)
                .unwrap_or_default()
                .to_rgba8();

            let mut raw_data = Vec::<u8>::new();

            let width = image_data.width() as usize;
            let height = image_data.height() as usize;

            raw_data.extend_from_slice(image_data.as_raw());

            textures.push(Texture {
                width,
                height,
                channel_num: 4,
                format: ImageFormat::RGBA8,
                data: raw_data,
            });
        }

        textures
    }
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use std::fs;

    use gltf_loader::*;

    #[test]
    fn test_textures_from_gltf()
    {
        let path = "../../Assets/werewolf_animated.gltf".to_string();
        let data = fs::read_to_string(&path)
            .expect(format!("Unable to read file {}.", &path).as_str());

        let gltf = Gltf::from_string(&data, &path);

        let textures = crate::texture::Texture::from_gltf(&gltf);

        for texture in &textures
        {
            assert_eq!(texture.width * texture.height * 4, texture.data.len());
        }
    }
}
