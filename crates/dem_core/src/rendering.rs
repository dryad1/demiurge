use std::fmt::Debug;

use builder::Builder;
use dem_log::{dem_debug_assert, error};

pub use crate::gpu_api::{GpuApi, GraphicsInput, GraphicsInputModifiers};

/// Message object to issue a compute task to the gpu.
#[derive(Debug, Default, Clone, Builder)]
pub struct ComputeRequest
{
    pub shader: ShaderHandle,
    /// The amount of work per dimension. For example (250, 90, 1) will issue
    /// 250x90 work groups as an array spanning a grid of [0-249, 0,89].
    pub work_groups: (u32, u32, u32),
    /// Images that will be available either as samplers or image storage based
    /// on the shader declaration.
    #[optional(Vec::new())]
    pub image_inputs: Vec<GpuDataDescriptor>,
    /// (RWStructuredBuffer, StructuredBuffer, etc...) inputs.
    #[optional(Vec::new())]
    pub buffer_inputs: Vec<GpuDataDescriptor>,

    pub uniforms: Vec<UboData>,
}

impl ComputeRequestBuilder
{
    pub fn add_ubo<T: Pod>(mut self, binding: u32, val: T) -> Self
    {
        self.uniforms.push(UboData::new(binding, val));

        self
    }

    pub fn set_ubo<T: Pod>(&mut self, binding: u32, val: T)
    {
        let uniform_index = self.uniforms.iter().position(|x| x.binding == binding);

        dem_debug_assert!(uniform_index.is_some(), "");
        if let Some(index) = uniform_index
        {
            self.uniforms[index] = UboData::new(binding, val);
        }
    }
}

#[derive(Debug, Default, Clone, Builder)]
#[builder(inner_build)]
pub struct RenderRequest
{
    pub shader: ShaderHandle,
    /// Attribute inputs such as vertices and normals.
    #[optional(GraphicsInput::default())]
    pub vertex_input: GraphicsInput,
    /// Modifiers to the attributes, for example instancing information, or
    /// offsets into the geometry arrays, e.g. if you want to start
    /// rendering from a given index.
    #[optional(GraphicsInputModifiers::default())]
    pub vertex_modifiers: GraphicsInputModifiers,

    /// `Samplers` or `ImageStorage` variables.
    #[optional(Vec::new())]
    pub image_inputs: Vec<GpuDataDescriptor>,
    /// RWStructuredBuffers and simlar buffer objects.
    #[optional(Vec::new())]
    pub buffer_inputs: Vec<GpuDataDescriptor>,
    #[optional(true)]
    pub should_clear: bool,
    #[optional([0.0, 0.0, 0.0, 0.0])]
    pub color_clear: [f32; 4],
    #[optional(1.0)]
    pub depth_clear: f32,
    #[optional(0)]
    pub stencil_clear: u32,
    /// Portion of the window that will actually be rendered to (cropping).
    #[optional(Scissor::default())]
    pub scissor: Scissor,
    /// Images that will be rendered to, if empty the screen buffer will be
    /// assumed. The `usize` parameter denotes the binding to which to attach
    /// each output.
    #[optional(RenderTargets::default())]
    pub render_target: RenderTargets,

    #[optional(Vec::new())]
    pub uniforms: Vec<UboData>,
}

impl RenderRequestBuilder
{
    pub fn build(mut self) -> RenderRequest
    {
        if self.vertex_modifiers.element_count == 0
        {
            self.vertex_modifiers.element_count = self.vertex_input.index_count;
        }

        self.inner_build()
    }

    pub fn add_ubo<T: Pod>(mut self, binding: u32, val: T) -> Self
    {
        self.uniforms.push(UboData::new(binding, val));

        self
    }

    /// Add an additional storage buffer to this request.
    /// The index denotes the starting index in the shader array from which the
    /// writes start, often it should be 0.
    /// ALWAYS call this *after* calls to `buffer_inputs()`
    pub fn add_storage_buffer(
        mut self,
        binding: usize,
        index: usize,
        buffer: GpuMemoryHandle,
    ) -> Self
    {
        self.buffer_inputs.push(GpuDataDescriptor {
            binding,
            index,
            handle: buffer,
        });

        self
    }

    pub fn add_image(
        mut self,
        binding: usize,
        index: usize,
        image: GpuMemoryHandle,
    ) -> Self
    {
        self.image_inputs.push(GpuDataDescriptor {
            binding,
            index,
            handle: image,
        });

        self
    }

    pub fn add_images_from_iterator<I>(mut self, binding: usize, images: I) -> Self
    where
        I: Iterator<Item = GpuMemoryHandle>,
    {
        for (index, image) in images.enumerate()
        {
            self.image_inputs.push(GpuDataDescriptor {
                binding,
                index,
                handle: image,
            });
        }

        self
    }

    pub fn set_ubo<T: Pod>(&mut self, binding: u32, val: T)
    {
        let uniform_index = self.uniforms.iter().position(|x| x.binding == binding);

        dem_debug_assert!(uniform_index.is_some(), "");
        if let Some(index) = uniform_index
        {
            self.uniforms[index] = UboData::new(binding, val);
        }
    }
}
// TODO(low): avoid repeating the ubo logic
impl RenderRequest
{
    pub fn set_ubo<T: Pod>(&mut self, binding: u32, val: T)
    {
        let uniform_index = self.uniforms.iter().position(|x| x.binding == binding);

        dem_debug_assert!(
            uniform_index.is_some(),
            "Trying to set uniform binding {}, but it could not be found.",
            binding
        );
        if let Some(index) = uniform_index
        {
            self.uniforms[index] = UboData::new(binding, val);
        }
    }
}

#[derive(Debug, Default, Clone)]
pub enum RenderTargets
{
    /// Width and height resolution at which to rasterize.
    None(u32, u32),
    #[default]
    ScreenBuffer,
    OffScreenBuffers(Vec<(ImageHandle, usize)>),
}

pub trait AbstractRenderWindow
{
    fn get_required_vulkan_extensions(&self) -> Vec<String>;
    fn create_surface(&self, instance_handle: u64) -> u64;
    fn was_window_resized(&self) -> bool;
    fn get_framebuffer_dimensions(&self) -> (u32, u32);
}

#[derive(Debug, Default, Clone, Copy)]
pub struct ShaderHandle(u64);

impl ShaderHandle
{
    pub fn from_raw(i: u64) -> ShaderHandle { return ShaderHandle(i); }

    pub fn to_raw(&self) -> u64 { return self.0; }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct ImageHandle(u64);

impl ImageHandle
{
    pub fn from_raw(i: u64) -> ImageHandle { return ImageHandle(i); }

    pub fn to_raw(&self) -> u64 { return self.0; }
}

#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct BufferHandle(pub(crate) u64);

impl BufferHandle
{
    pub fn from_raw(i: u64) -> BufferHandle { return BufferHandle(i); }

    pub fn to_raw(&self) -> u64 { return self.0; }
}

impl From<ShaderHandle> for u64
{
    fn from(val: ShaderHandle) -> Self { val.to_raw() }
}

impl From<BufferHandle> for u64
{
    fn from(val: BufferHandle) -> Self { val.to_raw() }
}

impl From<ImageHandle> for u64
{
    fn from(val: ImageHandle) -> Self { val.to_raw() }
}

impl From<u64> for ShaderHandle
{
    fn from(val: u64) -> Self { Self::from_raw(val) }
}

impl From<u64> for BufferHandle
{
    fn from(val: u64) -> Self { Self::from_raw(val) }
}

impl From<u64> for ImageHandle
{
    fn from(val: u64) -> Self { Self::from_raw(val) }
}

#[derive(PartialEq, Clone, Copy)]
pub enum VSync
{
    SYNCHRONOUS,
    ASYNCHRONOUS,
}

#[derive(Debug)]
pub enum ImageFormat
{
    R8,
    RGBA8,
    BGRA8,
    R32,
    RGBA32,
    D32SfloatS8uint,
}

#[derive(Copy, Clone, Debug)]
pub enum GpuMemoryHandle
{
    Buffer(BufferHandle),
    Image(ImageHandle),
}

impl Default for GpuMemoryHandle
{
    fn default() -> Self { Self::Buffer(BufferHandle::from_raw(0)) }
}

impl GpuMemoryHandle
{
    pub fn to_buffer_handle(&self) -> BufferHandle
    {
        match self
        {
            Self::Buffer(handle) => *handle,
            Self::Image(_) => panic!(),
        }
    }

    pub fn to_image_handle(&self) -> ImageHandle
    {
        match self
        {
            Self::Image(handle) => *handle,
            Self::Buffer(_) => panic!(),
        }
    }

    pub fn to_u64(&self) -> u64
    {
        match self
        {
            Self::Image(handle) => handle.to_raw(),
            Self::Buffer(handle) => handle.to_raw(),
        }
    }
}

pub struct RawImageData
{
    pub width: u32,
    pub height: u32,
    pub depth: u32,
    pub format: ImageFormat,
    pub data: Option<*const u8>,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum ShaderLanguage
{
    GLSL,
    HLSL,
    RUST,
    SPIRV,
}

#[derive(Debug, Default, Clone)]
pub struct GpuDataDescriptor
{
    pub handle: GpuMemoryHandle,
    pub binding: usize,
    pub index: usize,
}

impl From<(GpuMemoryHandle, usize)> for GpuDataDescriptor
{
    fn from((handle, binding): (GpuMemoryHandle, usize)) -> Self
    {
        return Self {
            handle: handle,
            binding: binding,
            index: 0,
        };
    }
}

#[derive(Debug, Clone)]
pub struct Scissor
{
    pub offset: (u32, u32),
    pub extent: (u32, u32),
}

impl Default for Scissor
{
    fn default() -> Self
    {
        return Self {
            offset: (0, 0),
            extent: (!0, !0),
        };
    }
}

#[derive(Debug, Clone)]
pub struct UboData
{
    binding: u32,
    data: Vec<u8>,
}

pub trait Pod: Copy + Sized + 'static {}
impl<T: Copy + Sized + 'static> Pod for T {}

impl UboData
{
    pub fn new<T: Pod>(binding: u32, val: T) -> Self
    {
        let pod = &val;
        let pod_size = std::mem::size_of::<T>();
        let pod_ptr = pod as *const T as *const u8;

        let data = unsafe { std::slice::from_raw_parts(pod_ptr, pod_size).to_vec() };
        Self { binding, data }
    }

    pub fn get_raw_data(&self) -> (u32, *const u8, usize)
    {
        (self.binding, self.data.as_ptr(), self.data.len())
    }
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use std::*;

    use super::serialize_uniform;

    #[warn(dead_code)]
    #[derive(Debug, Default)]
    struct Dummy
    {
        pub f1: f32,
        pub f2: i32,
    }

    #[derive(Debug, Default)]
    struct SecondDummy
    {
        pub f1: f32,
        pub f2: f32,
        pub f3: f32,
    }

    #[derive(Debug, Default)]
    struct ThirdDummy(f32, f32, f32, f32);

    #[test]
    fn make_uniform_macro()
    {
        let dummy = Dummy::default();
        let dummy2 = SecondDummy::default();
        let dummy3 = ThirdDummy::default();
        let t = UBO!((dummy2, 0), (dummy, 1), (dummy3, 3));
        assert_eq!(t.len(), 3);
    }
}
