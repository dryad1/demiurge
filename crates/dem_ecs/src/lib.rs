use std::any::*;
use std::cell::{Ref, RefCell, RefMut, UnsafeCell};
use std::collections::{BTreeMap, HashSet};
use std::fmt::Debug;
use std::marker::PhantomData;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub struct Entity(u64);

impl Entity
{
    pub(crate) fn next(&self) -> Entity { return Entity(self.0 + 1); }
}

pub(crate) struct ComponentStorage<T>
{
    entity_component_map: BTreeMap<Entity, usize>,
    components: Vec<T>,
}

impl<T> ComponentStorage<T>
{
    pub(crate) fn new() -> Self
    {
        Self {
            entity_component_map: BTreeMap::new(),
            components: Vec::new(),
        }
    }

    pub(crate) fn insert(&mut self, entity: Entity, component: T)
    {
        let val = self
            .entity_component_map
            .insert(entity, self.components.len());
        assert!(val == None);
        self.components.push(component);
    }

    pub(crate) fn get_by_entity(&mut self, entity: Entity) -> &mut T
    {
        let component_index = self.entity_component_map.get(&entity).unwrap();
        return &mut self.components[*component_index];
    }

    pub(crate) fn check_by_entity(&self, entity: Entity) -> &T
    {
        let component_index = self.entity_component_map.get(&entity).unwrap();
        return &self.components[*component_index];
    }
}

pub struct ECSHandle(RefCell<ECStorage>);

impl ECSHandle
{
    pub fn new() -> ECSHandle { ECSHandle(RefCell::new(ECStorage::new())) }

    pub fn as_ecs_mut(&self) -> RefMut<ECStorage> { self.0.borrow_mut() }

    pub fn as_ecs(&self) -> Ref<ECStorage> { self.0.borrow() }
}


pub struct ECStorage
{
    pub(crate) entity_count: usize,
    pub(crate) entity_components_map: BTreeMap<Entity, HashSet<TypeId>>,
    pub(crate) components: BTreeMap<TypeId, UnsafeCell<Box<dyn Any>>>,
}

impl ECStorage
{
    pub fn new() -> ECStorage
    {
        ECStorage {
            entity_count: 0,
            entity_components_map: BTreeMap::new(),
            components: BTreeMap::new(),
        }
    }

    pub fn spawn_entity(&mut self) -> Entity
    {
        self.entity_count += 1;
        let entity = Entity(self.entity_count as u64);
        self.entity_components_map.insert(entity, HashSet::new());

        return entity;
    }

    pub fn insert_component<T>(&mut self, entity: Entity, component: T)
    where
        T: 'static,
    {
        let type_id = TypeId::of::<T>();
        let was_new = self
            .entity_components_map
            .get_mut(&entity)
            .unwrap()
            .insert(type_id);
        assert!(was_new);

        let storage = self
            .components
            .entry(type_id)
            .or_insert(UnsafeCell::new(Box::new(ComponentStorage::<T>::new())))
            .get_mut()
            .downcast_mut::<ComponentStorage<T>>()
            .unwrap();

        storage.insert(entity, component);
    }

    pub fn get_component<T>(&mut self, entity: Entity) -> Option<&mut T>
    where
        T: 'static,
    {
        let type_id = TypeId::of::<T>();

        let component = match self.components.get_mut(&type_id)
        {
            Some(x) => x,
            None => return None,
        };

        let storage: &mut ComponentStorage<T> =
            component.get_mut().downcast_mut().unwrap();

        return Some(storage.get_by_entity(entity));
    }

    pub fn entity_query<'s, 'q, T>(&'s mut self) -> EntityQueryIterator<'q, T>
    where
        T: 'static + ComponentQuery<'q>,
        's: 'q,
    {
        EntityQueryIterator {
            ecs: self,
            current_entity: Entity(0),
            _phantom: PhantomData::default(),
        }
    }

    pub fn component_query<T>(&mut self) -> std::slice::IterMut<T>
    where
        T: 'static,
    {
        let type_id = TypeId::of::<T>();
        assert!(self.components.contains_key(&type_id));

        let storage: &mut ComponentStorage<T> = self
            .components
            .get_mut(&type_id)
            .unwrap()
            .get_mut()
            .downcast_mut()
            .unwrap();

        storage.components.iter_mut()
    }

    pub(crate) fn has_component_id(&self, entity: Entity, type_id: TypeId) -> bool
    {
        let component_set = self.entity_components_map.get(&entity).unwrap();

        return component_set.contains(&type_id);
    }
}

pub struct EntityQueryIterator<'ecs, T>
where
    T: 'static + ComponentQuery<'ecs>,
{
    ecs: &'ecs mut ECStorage,
    current_entity: Entity,

    _phantom: PhantomData<T>,
}

impl<'ecs, T> Iterator for EntityQueryIterator<'ecs, T>
where
    T: 'static + ComponentQuery<'ecs>,
{
    type Item = (Entity, T::AccessType);

    fn next(&mut self) -> Option<(Entity, T::AccessType)>
    {
        'outer: loop
        {
            self.current_entity = self.current_entity.next();
            if self.current_entity > Entity(self.ecs.entity_count as u64)
            {
                return None;
            }

            let mut kind_containers = Vec::<&mut Box<dyn Any>>::new();
            for kind in &T::type_ids()
            {
                if !self.ecs.has_component_id(self.current_entity, *kind)
                {
                    continue 'outer;
                }
                unsafe {
                    kind_containers
                        .push(&mut *self.ecs.components.get(kind).unwrap().get());
                }
            }

            let tuple = T::from_boxes(&self.current_entity, &mut kind_containers);
            return Some((self.current_entity, tuple));
        }
    }
}

// +| Macros |+ ================================================================
pub trait ComponentQuery<'query>
{
    type AccessType;
    fn type_ids() -> Vec<TypeId>;
    fn from_boxes(entity: &Entity, data: &Vec<&mut Box<dyn Any>>) -> Self::AccessType;
}

fn increment(var: &mut usize) -> usize
{
    let prior = *var;
    *var += 1;

    prior
}
macro_rules! tuple_query {
    ($($kind : ident),*) => {
        impl<'query, $($kind),*> ComponentQuery<'query> for ($($kind,)*)
        where $($kind : 'static),*
        {
            type AccessType = ($(&'query mut $kind),*,);
            fn type_ids() -> Vec::<TypeId>
            {
                return vec![$(TypeId::of::<$kind>()),*];
            }

            fn from_boxes(entity: &Entity, data : &Vec<&mut Box<dyn Any>>) -> Self::AccessType
            {
                let mut index = 0 as usize;

                ($(
                    fill_query_data!(&mut index, entity, $kind, @source data)
                ),*,)
            }
        }
    };
}

macro_rules! fill_query_data {
    ($_idx : expr, @source $data : ident) => {};
    ($idx : expr, $entity : ident, $kind : ident, @source $data : expr) => {{
        let storage: &ComponentStorage<$kind> =
            $data[increment($idx)].as_ref().downcast_ref().unwrap();
        // WARNING: This is UB, beware bugs
        unsafe { &mut *(storage.check_by_entity(*$entity) as *const $kind as *mut $kind) }
    }};
}

macro_rules! define_tuple_queries {
    () => {};
    ($kind : ident) => {
        tuple_query!($kind);
    };
    ($kind : ident, $($tail : ident),+) => {
        tuple_query!($kind, $($tail),+);
        define_tuple_queries!($($tail),+);
    };
}

define_tuple_queries!(
    A, B, D, E, F, G, H, I, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
);

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use crate::*;

    #[derive(Debug, PartialEq)]
    struct DummyComponent(u64);
    #[derive(Debug, PartialEq)]
    struct OtherDummyComponent(f64);
    #[derive(Debug, PartialEq)]
    struct NamedDummyComponent(String);
    #[test]
    fn test_ecs()
    {
        let mut ecs = ECStorage::new();
        let entity = ecs.spawn_entity();
        ecs.insert_component(entity, DummyComponent(64));

        {
            let mut query = ecs.entity_query::<(DummyComponent,)>();
            let (entity, (dummy_component,)) = query.next().unwrap();
            assert_eq!(entity, Entity(1));
            assert_eq!(dummy_component.0, 64);
        }

        let entity2 = ecs.spawn_entity();
        ecs.insert_component(entity2, DummyComponent(42));
        ecs.insert_component(entity2, OtherDummyComponent(69.0));

        {
            let mut query = ecs.entity_query::<(DummyComponent,)>();
            let (entity, (dummy_component,)) = query.next().unwrap();
            assert_eq!(entity, Entity(1));
            assert_eq!(dummy_component.0, 64);

            let (entity, (dummy_component,)) = query.next().unwrap();
            assert_eq!(entity, Entity(2));
            assert_eq!(dummy_component.0, 42);

            let entity = query.next();
            assert_eq!(entity, None);

            let mut query = ecs.entity_query::<(DummyComponent, OtherDummyComponent)>();
            let (entity, (dummy_component, other_dummy)) = query.next().unwrap();
            assert_eq!(entity, Entity(2));
            assert_eq!(dummy_component.0, 42);
            assert_eq!(other_dummy.0, 69.0);

            let entity = query.next();
            assert_eq!(entity, None);
        }

        let mut query = ecs.entity_query::<(NamedDummyComponent, OtherDummyComponent)>();
        let entity = query.next();
        assert_eq!(entity, None);
    }
}
