use std::path::Path;

use vulkan_bindings::shader_parsing::compile_shader_crate;

fn main()
{
    let crate_path = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    let debug_gui_path = Path::new(&crate_path).join("debug_gui_shader");
    let debug_gui_lib_path = Path::new(&crate_path)
        .join("debug_gui_shader")
        .join("src")
        .join("lib.rs");

    println!(
        "{}",
        format!(
            "cargo:rustc-env=DEM_GUI_CONFIG_PATH={}",
            debug_gui_lib_path.display()
        )
    );

    let spirv_paths = compile_shader_crate(
        &(debug_gui_path.clone())
            .into_os_string()
            .into_string()
            .unwrap(),
    );

    println!(
        "{}",
        format!(
            "cargo:rustc-env=DEM_GUI_FRAG_PATH={}",
            spirv_paths[0].display()
        )
    );
    println!(
        "{}",
        format!(
            "cargo:rustc-env=DEM_GUI_VERT_PATH={}",
            spirv_paths[1].display()
        )
    );
}
