/*
#ifdef never
[[ne::depth_test(false)]]
[[ne::cull_mode(none)]]
#endif
*/

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use spirv_std::{arch::IndexUnchecked, glam::*, image::*, num_traits::Float, spirv};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(
    v_rgba: Vec4,
    v_tc: Vec2,
    #[spirv(descriptor_set = 1, binding = 1)] u_texture: &SampledTexture2D,
    output: &mut Vec4,
)
{
    *output = v_rgba * u_texture.sample(v_tc);
}

// Vertex shader
struct ScreenUBO
{
    u_screen_size: Vec2,
}

#[spirv(vertex)]
pub fn main_vs(
    a_pos_bind0: Vec2,
    a_tc_bind0: Vec2,
    // [[ne::vertex_format(R8G8B8A8Unorm)]]
    a_srgba_bind0_FORMAT_R8G8B8A8Unorm: Vec4,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] screen_ubo: &ScreenUBO,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    v_rgba: &mut Vec4,
    v_tc: &mut Vec2,
)
{
    *screen_pos = Vec4::new(
        2.0 * a_pos_bind0.x / screen_ubo.u_screen_size.x - 1.0,
        2.0 * a_pos_bind0.y / screen_ubo.u_screen_size.y - 1.0,
        0.0,
        1.0,
    );

    *v_rgba = a_srgba_bind0_FORMAT_R8G8B8A8Unorm; //linear_from_srgba(a_srgba);
    *v_tc = a_tc_bind0;
}
