#version 460
#extension GL_EXT_scalar_block_layout : enable

layout(location = 0) in vec4 v_rgba;
layout(location = 1) in vec2 v_tc;

layout(location = 0) out vec4 f_color;

layout(binding = 1) uniform sampler2D u_sampler;

void main() {
    f_color = (v_rgba) * texture(u_sampler, v_tc);
}