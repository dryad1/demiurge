use dem_core::{gpu_api::*, rendering::*};
pub use egui;
pub use egui::{
    epaint,
    epaint::Primitive,
    pos2,
    vec2,
    Context,
    Event,
    ImageData,
    Modifiers,
    Pos2,
    RawInput,
    Rect,
    Slider,
    TextureId,
    Window,
};
use glfw::WindowEvent::*;

const IMGUI_VERT_SHADER: &[u8] = include_bytes!(env!("DEM_GUI_VERT_PATH"));
const IMGUI_FRAG_SHADER: &[u8] = include_bytes!(env!("DEM_GUI_FRAG_PATH"));
const IMGUI_CONFIG: &str = include_str!(env!("DEM_GUI_CONFIG_PATH"));

pub struct DebugGui
{
    pub ctx: egui::Context,
    textures: Vec<GpuDataDescriptor>,

    pub pointer_pos: Pos2,
    pub modifiers: egui::Modifiers,

    raw_input: RawInput,
    window_size: (u32, u32),

    shader: ShaderHandle,
    attribute_handle: BufferHandle,
    index_handle: BufferHandle,
}

impl DebugGui
{
    pub fn init<RenderContext>(
        render_context: &mut RenderContext,
        window_size: (u32, u32),
    ) -> DebugGui
    where
        RenderContext: GpuApi,
    {
        let ctx = egui::Context::default();

        let (width, height) = window_size;
        let raw_input = egui::RawInput {
            screen_rect: Some(egui::Rect::from_points(&[
                Default::default(),
                [width as f32, height as f32].into(),
            ])),
            pixels_per_point: Some(1.0),
            ..Default::default()
        };

        DebugGui {
            ctx,
            shader: render_context.add_shader((
                vec!["debug_ui_vert".to_string(), "debug_ui_frag".to_string()],
                vec![IMGUI_VERT_SHADER.to_vec(), IMGUI_FRAG_SHADER.to_vec()],
                IMGUI_CONFIG.to_string(),
            )),
            textures: Vec::new(),

            pointer_pos: Pos2::default(),
            modifiers: egui::Modifiers::default(),

            raw_input,
            window_size,
            attribute_handle: BufferHandle::from_raw(!0),
            index_handle: BufferHandle::from_raw(!0),
        }
    }

    pub fn handle_event(&mut self, event: &glfw::WindowEvent)
    {
        let (width, height) = self.window_size;
        self.raw_input = egui::RawInput {
            screen_rect: Some(egui::Rect::from_points(&[
                Default::default(),
                [width as f32, height as f32].into(),
            ])),
            pixels_per_point: Some(1.0),
            ..Default::default()
        };

        match event
        {
            FramebufferSize(width, height) =>
            {
                self.raw_input.screen_rect = Some(Rect::from_min_size(
                    Pos2::new(0f32, 0f32),
                    egui::vec2(*width as f32, *height as f32)
                        / self.raw_input.pixels_per_point.unwrap(),
                ))
            }

            MouseButton(mouse_btn, glfw::Action::Press, _) =>
            {
                self.raw_input.events.push(egui::Event::PointerButton {
                    pos: self.pointer_pos,
                    button: match mouse_btn
                    {
                        glfw::MouseButtonLeft => egui::PointerButton::Primary,
                        glfw::MouseButtonRight => egui::PointerButton::Secondary,
                        glfw::MouseButtonMiddle => egui::PointerButton::Middle,
                        _ => unreachable!(),
                    },
                    pressed: true,
                    modifiers: self.modifiers,
                })
            }

            MouseButton(mouse_btn, glfw::Action::Release, _) =>
            {
                self.raw_input.events.push(egui::Event::PointerButton {
                    pos: self.pointer_pos,
                    button: match mouse_btn
                    {
                        glfw::MouseButtonLeft => egui::PointerButton::Primary,
                        glfw::MouseButtonRight => egui::PointerButton::Secondary,
                        glfw::MouseButtonMiddle => egui::PointerButton::Middle,
                        _ => unreachable!(),
                    },
                    pressed: false,
                    modifiers: self.modifiers,
                })
            }

            CursorPos(x, y) =>
            {
                self.pointer_pos = pos2(
                    *x as f32 / self.raw_input.pixels_per_point.unwrap(),
                    *y as f32 / self.raw_input.pixels_per_point.unwrap(),
                );
                self.raw_input
                    .events
                    .push(egui::Event::PointerMoved(self.pointer_pos))
            }

            Key(keycode, _scancode, glfw::Action::Release, keymod) =>
            {
                use glfw::Modifiers as Mod;
                if let Some(key) = translate_virtual_key_code(*keycode)
                {
                    self.modifiers = Modifiers {
                        alt: (*keymod & Mod::Alt == Mod::Alt),
                        ctrl: (*keymod & Mod::Control == Mod::Control),
                        shift: (*keymod & Mod::Shift == Mod::Shift),

                        // TODO: GLFW doesn't seem to support the mac command key
                        // mac_cmd: keymod & Mod::LGUIMOD == Mod::LGUIMOD,
                        command: (*keymod & Mod::Control == Mod::Control),

                        ..Default::default()
                    };

                    self.raw_input.events.push(Event::Key {
                        key,
                        pressed: false,
                        modifiers: self.modifiers,
                    });
                }
            }

            Key(
                keycode,
                _scancode,
                glfw::Action::Press | glfw::Action::Repeat,
                keymod,
            ) =>
            {
                use glfw::Modifiers as Mod;
                if let Some(key) = translate_virtual_key_code(*keycode)
                {
                    self.modifiers = Modifiers {
                        alt: (*keymod & Mod::Alt == Mod::Alt),
                        ctrl: (*keymod & Mod::Control == Mod::Control),
                        shift: (*keymod & Mod::Shift == Mod::Shift),

                        command: (*keymod & Mod::Control == Mod::Control),

                        ..Default::default()
                    };

                    if self.modifiers.command && key == egui::Key::X
                    {
                        self.raw_input.events.push(egui::Event::Cut);
                    }
                    else if self.modifiers.command && key == egui::Key::C
                    {
                        self.raw_input.events.push(egui::Event::Copy);
                    }
                    else if self.modifiers.command && key == egui::Key::V
                    {
                        todo!()
                    }
                    else
                    {
                        self.raw_input.events.push(Event::Key {
                            key,
                            pressed: true,
                            modifiers: self.modifiers,
                        });
                    }
                }
            }

            Char(c) =>
            {
                self.raw_input.events.push(Event::Text(c.to_string()));
            }

            Scroll(x, y) =>
            {
                self.raw_input
                    .events
                    .push(egui::Event::Scroll(vec2(*x as f32, *y as f32)));
            }

            _ =>
            {}
        }
    }

    pub fn draw<RenderContext, F>(
        &mut self,
        render_context: &mut RenderContext,
        ui_calls: &mut F,
    ) where
        RenderContext: GpuApi,
        F: FnMut(&Context),
    {
        let output = self.ctx.run(self.raw_input.clone(), ui_calls);

        let texture_changes = output.textures_delta;
        debug_assert!(texture_changes.free.len() == 0);
        for (texture_id, image_delta) in texture_changes.set
        {
            debug_assert!(texture_id == TextureId::Managed(0));
            debug_assert!(image_delta.pos.is_none());

            let bytes_per_px = image_delta.image.bytes_per_pixel();
            debug_assert!(bytes_per_px == 4);

            let width = image_delta.image.width() as u32;
            let height = image_delta.image.height() as u32;

            let image_data = match image_delta.image
            {
                ImageData::Font(font_data) =>
                {
                    let mut data = Vec::<epaint::Color32>::new();
                    for pixel in font_data.srgba_pixels(1.0)
                    {
                        data.push(pixel);
                    }
                    data
                }
                _ =>
                {
                    todo!()
                }
            };

            let gpu_image = RawImageData {
                width,
                height,
                depth: 0,
                data: Some(image_data.as_ptr() as *const u8),
                format: ImageFormat::RGBA8,
            };
            let image_handle = render_context.allocate_image(&gpu_image);
            self.textures.push(GpuDataDescriptor {
                handle: image_handle,
                binding: 1,
                index: 0,
            });
        }

        let clipped_primitives = self.ctx.tessellate(output.shapes);
        for primitive in clipped_primitives
        {
            let (vbuffer, ibuffer, icount, _) = match primitive.primitive
            {
                Primitive::Mesh(mesh) =>
                {
                    if self.attribute_handle != BufferHandle::from_raw(!0)
                    {
                        render_context.destroy_buffer(GpuMemoryHandle::Buffer(
                            self.attribute_handle,
                        ))
                    }
                    self.attribute_handle = render_context
                        .allocate_buffer(&mesh.vertices)
                        .to_buffer_handle();

                    if self.index_handle != BufferHandle::from_raw(!0)
                    {
                        render_context
                            .destroy_buffer(GpuMemoryHandle::Buffer(self.index_handle))
                    }

                    self.index_handle = render_context
                        .allocate_buffer(&mesh.indices)
                        .to_buffer_handle();

                    let tex_id = mesh.texture_id;

                    (
                        self.attribute_handle,
                        self.index_handle,
                        mesh.indices.len(),
                        tex_id,
                    )
                }
                _ => todo!(),
            };

            let gpu_input = GraphicsInput {
                buffers: vec![vbuffer],
                index_buffer: ibuffer,
                index_count: icount,
            };
            let mut vertex_modifiers = GraphicsInputModifiers::default();
            vertex_modifiers.element_count = gpu_input.index_count;

            let scissor = Scissor {
                offset: (
                    primitive.clip_rect.min.x as u32,
                    primitive.clip_rect.min.y as u32,
                ),
                extent: (
                    (primitive.clip_rect.max.x - primitive.clip_rect.min.x) as u32,
                    (primitive.clip_rect.max.y - primitive.clip_rect.min.y) as u32,
                ),
            };

            let dims = self.raw_input.screen_rect.unwrap().max;
            let render_request = RenderRequest::new(self.shader)
                .vertex_input(gpu_input.clone())
                .vertex_modifiers(vertex_modifiers)
                .image_inputs(self.textures.clone())
                .should_clear(false)
                .scissor(scissor)
                .add_ubo(0, (dims.x, dims.y))
                .build();

            render_context.draw(&render_request);

            self.attribute_handle = vbuffer;
            self.index_handle = ibuffer;
        }
    }
}

fn translate_virtual_key_code(key: glfw::Key) -> Option<egui::Key>
{
    use glfw::Key::*;

    Some(match key
    {
        Left => egui::Key::ArrowLeft,
        Up => egui::Key::ArrowUp,
        Right => egui::Key::ArrowRight,
        Down => egui::Key::ArrowDown,

        Escape => egui::Key::Escape,
        Tab => egui::Key::Tab,
        Backspace => egui::Key::Backspace,
        Space => egui::Key::Space,

        Enter => egui::Key::Enter,

        Insert => egui::Key::Insert,
        Home => egui::Key::Home,
        Delete => egui::Key::Delete,
        End => egui::Key::End,
        PageDown => egui::Key::PageDown,
        PageUp => egui::Key::PageUp,

        A => egui::Key::A,
        B => egui::Key::B,
        C => egui::Key::C,
        D => egui::Key::D,
        E => egui::Key::E,
        F => egui::Key::F,
        G => egui::Key::G,
        H => egui::Key::H,
        I => egui::Key::I,
        J => egui::Key::J,
        K => egui::Key::K,
        L => egui::Key::L,
        M => egui::Key::M,
        N => egui::Key::N,
        O => egui::Key::O,
        P => egui::Key::P,
        Q => egui::Key::Q,
        R => egui::Key::R,
        S => egui::Key::S,
        T => egui::Key::T,
        U => egui::Key::U,
        V => egui::Key::V,
        W => egui::Key::W,
        X => egui::Key::X,
        Y => egui::Key::Y,
        Z => egui::Key::Z,

        _ =>
        {
            return None;
        }
    })
}
