pub use log::{error, warn};

#[macro_export]
macro_rules! dem_debug_assert {
    ($condition:expr, $($arg:tt)+) => {
        if !$condition {
            error!($($arg)+);
            #[cfg(debug_assertions)]
            panic!();
        }
    };
}

#[macro_export]
macro_rules! dem_warning {
    ($condition:expr, $($arg:tt)+) => {
        if !$condition {
            warn!($($arg)+);
        }
    };
}
