use std::{collections::BTreeMap, sync::mpsc::Receiver};

pub use dem_core::{io::*, rendering::*};
use dem_gui::DebugGui;
use glfw::*;

#[derive(Debug)]
enum MouseButtonState
{
    None,
    Left,
    Right,
}

const GLFW_KEY_COUNT: usize = 121;

type CursorEvent<'io> = Box<dyn FnMut(f64, f64, f64, f64) + 'io>;
type MouseEvent<'io> = Box<dyn FnMut() + 'io>;
type ScrollEvent<'io> = Box<dyn FnMut(f64, f64) + 'io>;
type KeyEvent<'io> = Box<dyn FnMut(&[KeyActionState]) + 'io>;

unsafe impl<'io> Send for Window<'io> {}

unsafe impl<'io> Sync for Window<'io> {}

pub struct Window<'io>
{
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) glfw_window_handle: glfw::Window,
    pub(crate) cursor_callbacks: BTreeMap<MouseState, Vec<CursorEvent<'io>>>,
    pub(crate) scroll_callbacks: Vec<ScrollEvent<'io>>,
    pub(crate) mouse_callbacks: Vec<MouseEvent<'io>>,
    pub(crate) key_callbacks: Vec<KeyEvent<'io>>,

    was_window_resized: bool,
    mouse_position: (f64, f64),
    mouse_offset: (f64, f64),
    mouse_state: MouseState,

    events: Receiver<(f64, WindowEvent)>,

    key_states: [KeyActionState; GLFW_KEY_COUNT],
}

impl<'io> Window<'io>
{
    pub(crate) fn update_frame(&mut self, debug_gui: &mut Option<DebugGui>)
    {
        self.was_window_resized = false;
        let mut left_button_pressed = false;
        let mut right_button_pressed = false;
        let mut action = glfw::Action::Repeat;
        let mut modifiers = glfw::Modifiers::Alt;
        let mut new_position = self.mouse_position;
        let mut scroll_offsets = (0.0, 0.0);

        for (_, event) in glfw::flush_messages(&self.events)
        {
            if debug_gui.is_some()
            {
                debug_gui.as_mut().unwrap().handle_event(&event);
            }

            match event
            {
                WindowEvent::CursorPos(x, y) =>
                {
                    let x = 2.0 * (x / self.width as f64) - 1.0;
                    let y = 2.0 * (y / self.height as f64) - 1.0;
                    new_position = (x, y);
                }
                WindowEvent::MouseButton(button, glfw_action, glfw_modifiers) =>
                {
                    right_button_pressed = button == glfw::MouseButtonRight;
                    left_button_pressed = button == glfw::MouseButtonLeft;
                    action = glfw_action;
                    modifiers = glfw_modifiers;
                }
                WindowEvent::Scroll(x, y) =>
                {
                    scroll_offsets = (x, y);
                }
                WindowEvent::Key(key, _scancode, action, _modifiers) =>
                {
                    let index = glfw_key_to_index(key);
                    self.key_states[index] =
                        key_state_transition(&self.key_states[index], action);
                }
                WindowEvent::FramebufferSize(w, h) =>
                {
                    self.width = w as u32;
                    self.height = h as u32;
                    self.was_window_resized = true;
                }
                _ =>
                {}
            }
        }

        self.mouse_offset = (
            new_position.0 - self.mouse_position.0,
            new_position.1 - self.mouse_position.1,
        );
        self.mouse_position = new_position;

        let button_state = if left_button_pressed
        {
            MouseButtonState::Left
        }
        else if right_button_pressed
        {
            MouseButtonState::Right
        }
        else
        {
            MouseButtonState::None
        };

        self.mouse_state = mouse_state_transition(
            &self.mouse_state,
            &button_state,
            &action,
            &modifiers,
            self.mouse_offset,
        );

        let mouse_state = self.mouse_state;
        for callback in self
            .cursor_callbacks
            .entry(mouse_state)
            .or_insert_with(|| Vec::new())
        {
            callback(
                self.mouse_position.0,
                self.mouse_position.1,
                self.mouse_offset.0,
                self.mouse_offset.1,
            );
        }

        for callback in &mut self.scroll_callbacks
        {
            callback(scroll_offsets.0, scroll_offsets.1);
        }

        for callback in &mut self.key_callbacks
        {
            callback(&self.key_states);
        }

        for key in &mut self.key_states
        {
            if *key == KeyActionState::Release
            {
                *key = KeyActionState::Idle
            }
            else if *key == KeyActionState::Press
            {
                *key = KeyActionState::Held
            };
        }
    }

    pub fn glfw_window_handle(&self) -> &glfw::Window { &self.glfw_window_handle }

    pub fn new(
        width: u32,
        height: u32,
        window: glfw::Window,
        events: Receiver<(f64, glfw::WindowEvent)>,
    ) -> Window<'io>
    {
        Window {
            width,
            height,
            glfw_window_handle: window,
            mouse_position: (0.0, 0.0),
            mouse_offset: (0.0, 0.0),
            cursor_callbacks: BTreeMap::new(),
            was_window_resized: false,
            mouse_callbacks: Vec::new(),
            scroll_callbacks: Vec::new(),
            key_callbacks: Vec::new(),
            mouse_state: MouseState::NoAction,
            events,
            key_states: [KeyActionState::Idle; GLFW_KEY_COUNT],
        }
    }
}

impl<'io> IoEventHandling<'io> for Window<'io>
{
    fn add_cursor_callback(&mut self, state: MouseState, callback: CursorEvent<'io>)
    {
        let callbacks_ref = self
            .cursor_callbacks
            .entry(state)
            .or_insert_with(|| Vec::new());
        callbacks_ref.push(callback);
    }

    fn add_mouse_callback(&mut self, callback: MouseEvent<'io>)
    {
        self.mouse_callbacks.push(callback);
    }

    fn add_scroll_callback(&mut self, callback: ScrollEvent<'io>)
    {
        self.scroll_callbacks.push(callback);
    }

    fn add_key_callback(&mut self, callback: KeyEvent<'io>)
    {
        self.key_callbacks.push(callback);
    }

    fn get_window_size(&self) -> (u32, u32) { return (self.width, self.height); }
}

pub struct IoContext<'io>
{
    pub(crate) glfw: Glfw,
    pub(crate) window: Window<'io>,
}

impl<'io> AbstractRenderWindow for IoContext<'io>
{
    fn get_required_vulkan_extensions(&self) -> Vec<String>
    {
        match self.glfw.get_required_instance_extensions()
        {
            Some(i) => return i,
            None => panic!("Glfw returned an empty list of required extensions."),
        }
    }

    fn get_framebuffer_dimensions(&self) -> (u32, u32)
    {
        (self.window.width, self.window.height)
    }

    fn was_window_resized(&self) -> bool { self.window.was_window_resized }

    fn create_surface(&self, instance: u64) -> u64
    {
        let mut surface_handle: u64 = 0;
        debug_assert!(instance != 0);

        self.window.glfw_window_handle.create_window_surface(
            instance as usize,
            ::std::ptr::null(),
            &mut surface_handle as *mut u64,
        );

        debug_assert!(surface_handle != 0);

        surface_handle
    }
}

impl<'io> IoContext<'io>
{
    pub fn new(
        window_name: &str,
        dimensions: (u32, u32),
        decorated: bool,
    ) -> IoContext<'io>
    {
        use env_logger::*;
        // TODO: strongly consider requiring the user to set this up.
        Builder::from_default_env()
            .filter_level(log::LevelFilter::Debug)
            .init();

        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

        glfw.window_hint(glfw::WindowHint::Visible(true));
        glfw.window_hint(glfw::WindowHint::Decorated(decorated));
        glfw.window_hint(glfw::WindowHint::ClientApi(ClientApiHint::NoApi));

        debug_assert!(glfw.vulkan_supported());

        let (mut window, events) = glfw
            .create_window(
                dimensions.0,
                dimensions.1,
                window_name,
                glfw::WindowMode::Windowed,
            )
            .expect("Failed to create GLFW window.");

        window.set_key_polling(true);
        window.set_mouse_button_polling(true);
        window.set_cursor_pos_polling(true);
        window.set_scroll_polling(true);
        window.set_framebuffer_size_polling(true);

        let window = Window::new(800, 800, window, events);
        IoContext { glfw, window }
    }

    pub fn poll_io(&mut self, debug_gui: &mut Option<DebugGui>) -> bool
    {
        self.glfw.poll_events();

        self.window.update_frame(debug_gui);

        !self.window.glfw_window_handle.should_close()
    }

    pub fn window(&mut self) -> &mut Window<'io> { return &mut self.window; }
}

fn mouse_state_transition(
    state: &MouseState,
    button: &MouseButtonState,
    action: &glfw::Action,
    _modifiers: &glfw::Modifiers,
    offset: (f64, f64),
) -> MouseState
{
    use glfw::Action as A;
    use MouseButtonState::Left;
    use MouseButtonState::None;
    use MouseButtonState::Right;
    use MouseState as S;
    let moves = offset.0 != 0.0 || offset.1 != 0.0;
    match (state, button, action, moves)
    {
        (S::NoAction, Left, A::Press, _) =>
        {
            return S::LeftDown;
        }
        (S::NoAction, Right, A::Press, _) =>
        {
            return S::RightDown;
        }
        (S::NoAction, _, _, _) =>
        {
            return S::NoAction;
        }

        (S::LeftDown, Left | None, A::Press | A::Repeat, _) =>
        {
            return S::LeftDrag;
        }
        (S::LeftDown, Left | None, A::Release, _) =>
        {
            return S::LeftUp;
        }

        (S::LeftDrag, _, A::Press | A::Repeat, _) =>
        {
            return S::LeftDrag;
        }
        (S::LeftDrag, Right, A::Release, _) =>
        {
            return S::LeftDrag;
        }
        (S::LeftDrag, Left | None, A::Release, _) =>
        {
            return S::LeftUp;
        }

        (S::LeftUp, _, _, _) =>
        {
            return S::NoAction;
        }

        (S::RightDown, Right | None, A::Press | A::Repeat, _) =>
        {
            return S::RightDrag;
        }
        (S::RightDown, Right | None, A::Release, _) =>
        {
            return S::RightUp;
        }

        (S::RightDrag, _, A::Press | A::Repeat, _) =>
        {
            return S::RightDrag;
        }
        (S::RightDrag, Left, A::Release, _) =>
        {
            return S::RightDrag;
        }
        (S::RightDrag, Right | None, A::Release, _) =>
        {
            return S::RightUp;
        }

        (S::RightUp, _, _, _) =>
        {
            return S::NoAction;
        }
        _ => todo!("{:?} {:?} {:?} {:?}", state, button, action, moves),
    }
}

fn key_state_transition(
    key_state: &KeyActionState,
    action: glfw::Action,
) -> KeyActionState
{
    match (key_state, action)
    {
        (KeyActionState::Idle, Action::Press | Action::Repeat) => KeyActionState::Press,
        (KeyActionState::Idle, _) => todo!(),

        (KeyActionState::Press, Action::Press | Action::Repeat) => KeyActionState::Held,
        (KeyActionState::Press, Action::Release) => KeyActionState::Release,

        (KeyActionState::Held, Action::Release) => KeyActionState::Release,
        (KeyActionState::Held, _) => KeyActionState::Held,

        (KeyActionState::Release, Action::Press | Action::Repeat) =>
        {
            KeyActionState::Press
        }
        (KeyActionState::Release, Action::Release) => KeyActionState::Idle,
    }
}

pub fn glfw_key_to_index(key: glfw::Key) -> usize
{
    match key
    {
        glfw::Key::Space => 0,
        glfw::Key::Apostrophe => 1,
        glfw::Key::Comma => 2,
        glfw::Key::Minus => 3,
        glfw::Key::Period => 4,
        glfw::Key::Slash => 5,
        glfw::Key::Num0 => 6,
        glfw::Key::Num1 => 7,
        glfw::Key::Num2 => 8,
        glfw::Key::Num3 => 9,
        glfw::Key::Num4 => 10,
        glfw::Key::Num5 => 11,
        glfw::Key::Num6 => 12,
        glfw::Key::Num7 => 13,
        glfw::Key::Num8 => 14,
        glfw::Key::Num9 => 15,
        glfw::Key::Semicolon => 16,
        glfw::Key::Equal => 17,
        glfw::Key::A => 18,
        glfw::Key::B => 19,
        glfw::Key::C => 20,
        glfw::Key::D => 21,
        glfw::Key::E => 22,
        glfw::Key::F => 23,
        glfw::Key::G => 24,
        glfw::Key::H => 25,
        glfw::Key::I => 26,
        glfw::Key::J => 27,
        glfw::Key::K => 28,
        glfw::Key::L => 29,
        glfw::Key::M => 30,
        glfw::Key::N => 31,
        glfw::Key::O => 32,
        glfw::Key::P => 33,
        glfw::Key::Q => 34,
        glfw::Key::R => 35,
        glfw::Key::S => 36,
        glfw::Key::T => 37,
        glfw::Key::U => 38,
        glfw::Key::V => 39,
        glfw::Key::W => 40,
        glfw::Key::X => 41,
        glfw::Key::Y => 42,
        glfw::Key::Z => 43,
        glfw::Key::LeftBracket => 44,
        glfw::Key::Backslash => 45,
        glfw::Key::RightBracket => 46,
        glfw::Key::GraveAccent => 47,
        glfw::Key::World1 => 48,
        glfw::Key::World2 => 49,
        glfw::Key::Escape => 50,
        glfw::Key::Enter => 51,
        glfw::Key::Tab => 52,
        glfw::Key::Backspace => 53,
        glfw::Key::Insert => 54,
        glfw::Key::Delete => 55,
        glfw::Key::Right => 56,
        glfw::Key::Left => 57,
        glfw::Key::Down => 58,
        glfw::Key::Up => 59,
        glfw::Key::PageUp => 60,
        glfw::Key::PageDown => 61,
        glfw::Key::Home => 62,
        glfw::Key::End => 63,
        glfw::Key::CapsLock => 64,
        glfw::Key::ScrollLock => 65,
        glfw::Key::NumLock => 66,
        glfw::Key::PrintScreen => 67,
        glfw::Key::Pause => 68,
        glfw::Key::F1 => 69,
        glfw::Key::F2 => 70,
        glfw::Key::F3 => 71,
        glfw::Key::F4 => 72,
        glfw::Key::F5 => 73,
        glfw::Key::F6 => 74,
        glfw::Key::F7 => 75,
        glfw::Key::F8 => 76,
        glfw::Key::F9 => 77,
        glfw::Key::F10 => 78,
        glfw::Key::F11 => 79,
        glfw::Key::F12 => 80,
        glfw::Key::F13 => 81,
        glfw::Key::F14 => 82,
        glfw::Key::F15 => 83,
        glfw::Key::F16 => 84,
        glfw::Key::F17 => 85,
        glfw::Key::F18 => 86,
        glfw::Key::F19 => 87,
        glfw::Key::F20 => 88,
        glfw::Key::F21 => 89,
        glfw::Key::F22 => 90,
        glfw::Key::F23 => 91,
        glfw::Key::F24 => 92,
        glfw::Key::F25 => 93,
        glfw::Key::Kp0 => 94,
        glfw::Key::Kp1 => 95,
        glfw::Key::Kp2 => 96,
        glfw::Key::Kp3 => 97,
        glfw::Key::Kp4 => 98,
        glfw::Key::Kp5 => 99,
        glfw::Key::Kp6 => 100,
        glfw::Key::Kp7 => 101,
        glfw::Key::Kp8 => 102,
        glfw::Key::Kp9 => 103,
        glfw::Key::KpDecimal => 104,
        glfw::Key::KpDivide => 105,
        glfw::Key::KpMultiply => 106,
        glfw::Key::KpSubtract => 107,
        glfw::Key::KpAdd => 108,
        glfw::Key::KpEnter => 109,
        glfw::Key::KpEqual => 110,
        glfw::Key::LeftShift => 111,
        glfw::Key::LeftControl => 112,
        glfw::Key::LeftAlt => 113,
        glfw::Key::LeftSuper => 114,
        glfw::Key::RightShift => 115,
        glfw::Key::RightControl => 116,
        glfw::Key::RightAlt => 117,
        glfw::Key::RightSuper => 118,
        glfw::Key::Menu => 119,
        glfw::Key::Unknown => 120,
    }
}
