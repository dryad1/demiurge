use std::cell::Cell;

use algebra::*;
/// Functor wrapper to represent a BSpline of some order. Care should be taken
/// that the order is low enough to actually handle all control points.
///
/// An order 1 B_spline is just the points, order 2 is piecewise linear, order 3
/// is a cubic spline (i.e. G2 continuity)...
#[derive(Clone, Debug)]
pub struct BSpline
{
    knots: Vec<f32>,
    control_points: Vec<Vec3>,
    /// Internal helper variable. Used to optimize repetetitive samplings on
    /// close $t$ values.
    last_t_index: Cell<usize>,
    order: usize,
}

impl BSpline
{
    /// Create a new spline.
    ///
    /// # Arguments
    ///
    /// `control_points` the points that influence the spline.
    ///
    /// `knots` ordered list of $|control_points| + order$ knot values. Care
    /// should be taken to guarantee boundary conditions (repeated knot
    /// values).
    ///
    /// `order` order fo the spline, make sure it is lower than the number of
    /// control points.
    ///
    /// Returns spline object
    pub fn new(control_points: Vec<Vec3>, knots: Vec<f32>, order: usize) -> Self
    {
        // Assert the knots obey the expected properties of a spline knot vector.
        debug_assert!(knots[order..knots.len() - order]
            .windows(2)
            .all(|w| w[0] < w[1]));
        debug_assert!(knots[0..order].windows(2).all(|w| w[0] == w[1]));
        debug_assert!(knots[control_points.len()..control_points.len() + order]
            .windows(2)
            .all(|w| w[0] == w[1]));

        Self {
            knots,
            control_points,
            last_t_index: Cell::new(order - 1),
            order,
        }
    }

    /// Create a new spline with uniform knot sequence.
    ///
    /// # Arguments
    ///
    /// `control_points` the points that influence the spline.
    ///
    /// `order` order fo the spline, make sure it is lower than the number of
    /// control points.
    ///
    /// Returns spline object
    pub fn new_uniform(control_points: Vec<Vec3>, order: usize) -> Self
    {
        let mut knots = Vec::with_capacity(order + control_points.len());
        for _ in 0..order
        {
            knots.push(0.0);
        }

        let interior = control_points.len();
        for i in order - 1..interior - 1
        {
            knots.push(i as f32 / interior as f32);
        }

        for _ in 0..order
        {
            knots.push(interior as f32 / interior as f32);
        }

        Self::new(control_points, knots, order)
    }

    /// Sample a spline *within* its knot range.
    ///
    /// # Arguments
    ///
    /// `t` parameter at which the spline will be sampled. Must be in the domain
    /// of the spline, else it will be clamped.
    ///
    /// Returns spline evaluated at $t$.
    pub fn sample(&self, t: f32) -> Vec3
    {
        debug_assert!(self.control_points.len() >= self.order);

        // Select the index of the interval containing t.
        let t_index =
        // larger than the last knot
        if t >= *self.knots.last().unwrap()
        {
            self.knots.len() - 1 - self.order
        }
        // smaller than the first knot
        else if t <= *self.knots.first().unwrap()
        {
            self.order - 1
        }
        // inside the knot interval
        else
        {
            let mut index = self.last_t_index.get();
            while !(t >= self.knots[index] && t <= self.knots[index + 1])
            {
                index = (index + 1) % self.knots.len();
            }
            index
        };

        self.last_t_index.set(usize::min(t_index, self.order - 1));
        // Create the cache to compute the pyramid of intermediate evaluations.
        let order = self.order;
        let degree = order - 1;
        let mut evaluation = Vec::new();

        for i in 0..order
        {
            evaluation.push(self.control_points[t_index - degree + i]);
        }

        for l in 0..degree
        {
            for j in ((l + 1)..=degree).rev()
            {
                let alpha = (t - self.knots[t_index - degree + j])
                    / (self.knots[t_index - l + j] - self.knots[t_index - degree + j]);
                evaluation[j] = (1.0 - alpha) * evaluation[j - 1] + alpha * evaluation[j];
            }
        }

        evaluation[degree]
    }

    pub fn control_points(&self) -> &Vec<Vec3> { &self.control_points }

    pub fn knots(&self) -> &Vec<f32> { &self.knots }

    pub fn order(&self) -> usize { self.order }
}
