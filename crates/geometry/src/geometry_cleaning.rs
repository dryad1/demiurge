use std::collections::BTreeMap;

use linear_isomorphic::*;

use crate::euclidean::point_segment_distance;

pub fn find_duplicate_points<T>(points: &Vec<T>, epsilon: f32) -> Vec<T>
where
    T: InnerSpace<f32>,
{
    // Improve performance with a query data structure, e.g. a KD-tree
    let mut duplicates = Vec::new();
    for i in 0..points.len()
    {
        for j in 0..i
        {
            if (points[i].clone() - points[j].clone()).norm() <= epsilon
            {
                duplicates.push(points[i].clone());
                break;
            }
        }
    }

    duplicates
}

pub fn delete_duplicates<T>(points: &mut Vec<T>, epsilon: f32) -> BTreeMap<usize, usize>
where
    T: InnerSpace<f32>,
{
    let mut map = BTreeMap::new();
    let mut kept: Vec<T> = Vec::with_capacity(points.len());
    // Improve performance with a query data structure, e.g. a KD-tree
    for (count, p) in points.iter().enumerate()
    {
        // Keep only points with no duplicate.
        let index = kept
            .iter()
            .enumerate()
            .position(|(_, o)| (p.clone() - o.clone()).norm() < epsilon);

        let is_unique = index.is_none();

        if is_unique
        {
            map.insert(count, kept.len());
            kept.push(p.clone());
        }
        else
        {
            let index = index.unwrap();
            map.insert(count, index);
        }
    }

    *points = kept;

    map
}

pub fn merge_overlapping_vertices<T>(
    points: &mut Vec<T>,
    edges: &mut Vec<[usize; 2]>,
    epsilon: f32,
) where
    T: InnerSpace<f32>,
{
    let permutation = delete_duplicates(points, epsilon);

    edges.iter_mut().for_each(|[i, j]| {
        *i = *permutation.get(i).unwrap();
        *j = *permutation.get(j).unwrap();
    });
}

pub fn merge_overlapping_vertices_and_edges<T>(
    points: &mut Vec<T>,
    edges: &mut Vec<[usize; 2]>,
    epsilon: f32,
) where
    T: InnerSpace<f32>,
{
    merge_overlapping_vertices(points, edges, epsilon);

    for (pid, point) in points.iter().enumerate()
    {
        let overlapping = edges.iter_mut().find(|[i, j]| {
            let p1 = points[*i].clone();
            let p2 = points[*j].clone();

            let distance = point_segment_distance(&p1, &p2, point);

            distance < epsilon && *i != pid && *j != pid
        });

        if overlapping.is_none()
        {
            continue;
        }

        let [_i, j] = overlapping.unwrap();

        let endpoint = *j;
        *j = pid;

        edges.push([pid, endpoint]);
    }
}

#[cfg(test)]
mod tests
{
    use algebra::*;

    use crate::merge_overlapping_vertices_and_edges;

    #[test]
    fn test_merge_overlapping_vertices_and_edges()
    {
        let mut points = vec![
            Vec3::new(0.0, -1.0, 0.0),
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(-1.0, 0.0, 0.0),
            Vec3::new(1.0, 0.0, 0.0),
        ];

        let mut edges = vec![[0, 1], [2, 3]];

        merge_overlapping_vertices_and_edges(&mut points, &mut edges, 0.01);

        assert_eq!(edges[0], [0, 1]);
        assert_eq!(edges[1], [2, 1]);
        assert_eq!(edges[2], [1, 3]);

        let mut points = vec![
            Vec3::new(0.0, -1.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            Vec3::new(0.0, 1.0, 0.0),
            Vec3::new(1.0, 0.0, 0.0),
            Vec3::new(-1.0, 0.0, 0.0),
        ];

        let mut edges = vec![[0, 1], [2, 3], [2, 4]];

        merge_overlapping_vertices_and_edges(&mut points, &mut edges, 0.01);

        assert_eq!(edges[0], [0, 1]);
        assert_eq!(edges[1], [1, 2]);
        assert_eq!(edges[2], [1, 3]);
    }
}
