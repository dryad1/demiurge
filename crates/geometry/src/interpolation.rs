use crate::point_line_distance;
use crate::Vec2;
use crate::{sample_discrete_bitangents, sample_discrete_curve};
use core::f64::consts::PI;
use spade::{DelaunayTriangulation, Triangulation};

pub fn lerp<V, S>(v1: &V, v2: &V, t: S) -> V
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    v1.clone() * (S::from(1.).unwrap() - t) + v2.clone() * t
}

pub fn cubic_hermite<V, S>(v1: &V, m1: &V, v2: &V, m2: &V, t: S) -> V
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    let one = S::from(1.).unwrap();
    let two = S::from(2.).unwrap();
    let three = S::from(3.).unwrap();

    let t2 = t * t;
    let t3 = t * t * t;

    v1.clone() * (two * t3 - three * t2 + one)
        + m1.clone() * (t3 - two * t2 + t)
        + v2.clone() * (-two * t3 + three * t2)
        + m2.clone() * (t3 - t2)
}

pub struct CharrogGregoryPatch<V, S>
where
    V: linear_isomorphic::InnerSpace<S> + std::iter::Sum + std::fmt::Debug,
    S: linear_isomorphic::RealField,
{
    polygon: Vec<V>,
    curves: Vec<Vec<V>>,
    bitangents: Option<Vec<Vec<V>>>,
    bitangent_scale: S,
}

impl<V, S> CharrogGregoryPatch<V, S>
where
    V: linear_isomorphic::InnerSpace<S> + std::iter::Sum + std::fmt::Debug,
    S: linear_isomorphic::RealField,
{
    pub fn new(
        curves: Vec<Vec<V>>,
        bitangents: Option<Vec<Vec<V>>>,
        bitangent_scale: S,
    ) -> Self {
        let polygon = generate_regular_polygon(curves.len());

        Self {
            polygon,
            curves,
            bitangents,
            bitangent_scale,
        }
    }

    pub fn sample(&self, p: &V) -> V {
        let res = map_polygon_to_patch(
            p,
            &self.polygon,
            &self.curves,
            &self.bitangents,
            self.bitangent_scale,
        );

        res
    }

    pub fn mesh(&self, resolution: usize) -> (Vec<V>, Vec<usize>) {
        self.mesh_with_ring_coeff(resolution, S::from(1.0).unwrap())
    }

    pub fn mesh_with_per_curve_coeffs(
        &self,
        resolutions: &[usize],
        starting_ring_coeff: S,
    ) -> (Vec<V>, Vec<usize>) {
        assert!(resolutions.len() == self.curves.len());

        let mut triangulation = DelaunayTriangulation::<spade::Point2<_>>::new();
        let starting_ring_coeff = S::from(1.).unwrap() - starting_ring_coeff;
        let sides = resolutions.len();
        let mut patch = Vec::new();
        let ring_res = *resolutions.iter().min().unwrap();
        let ring_denom = S::from(ring_res as f32).unwrap();

        for i in 0..ring_res {
            let ring_coeff = S::from(i).unwrap() / ring_denom;
            for k in 0..sides {
                let curve_res = (resolutions[k] as isize - i as isize).max(1) as usize;
                let curve_denom = S::from(curve_res).unwrap();
                for j in 0..curve_res {
                    let t = S::from(j).unwrap() / curve_denom;

                    let p: V = lerp(
                        &self.polygon[k],
                        &self.polygon[(k + 1) % self.polygon.len()],
                        t,
                    );

                    let t = (ring_coeff + starting_ring_coeff)
                        / (S::from(1.).unwrap() + starting_ring_coeff);
                    let p = p * (S::from(1.).unwrap() - t);

                    let x = p[0].to_f32().unwrap();
                    let y = p[1].to_f32().unwrap();
                    triangulation.insert(spade::Point2::new(x, y)).unwrap();

                    let res = self.sample(&p);

                    patch.push(res);
                }
            }
        }

        patch.push(self.sample(&V::default()));

        let mut tris = Vec::<usize>::new();
        let vertices: Vec<_> = triangulation.vertices().collect();

        for face in triangulation.inner_faces() {
            let p1 = vertices[face.vertices()[0].index() as usize].position();
            let p2 = vertices[face.vertices()[1].index() as usize].position();
            let p3 = vertices[face.vertices()[2].index() as usize].position();

            let p1 = Vec2::new(p1.x, p1.y);
            let p2 = Vec2::new(p2.x, p2.y);
            let p3 = Vec2::new(p3.x, p3.y);

            let d1 = (p2 - p1).normalize();
            let d2 = (p3 - p1).normalize();

            let dot = d1.dot(&d2).abs();

            let threshold = f32::EPSILON * 10.0;
            if !dot.is_finite() || (dot - 1.0).abs() < threshold {
                continue;
            }

            tris.push(face.vertices()[0].index() as usize);
            tris.push(face.vertices()[1].index() as usize);
            tris.push(face.vertices()[2].index() as usize);
        }

        (patch, tris)
    }

    pub fn mesh_with_ring_coeff(
        &self,
        resolution: usize,
        starting_ring_coeff: S,
    ) -> (Vec<V>, Vec<usize>) {
        let starting_ring_coeff = S::from(1.).unwrap() - starting_ring_coeff;
        let sides = self.curves.len();
        let mut patch = Vec::new();
        let ring_res = resolution;
        let ring_denom = S::from(ring_res as f32).unwrap();
        for i in 0..ring_res {
            let ring_coeff = S::from(i).unwrap() / ring_denom;
            for k in 0..sides {
                let curve_res = resolution - i;
                let curve_denom = S::from(curve_res).unwrap();
                for j in 0..curve_res {
                    let t = S::from(j).unwrap() / curve_denom;
                    let p: V = lerp(
                        &self.polygon[k],
                        &self.polygon[(k + 1) % self.polygon.len()],
                        t,
                    );

                    let t = (ring_coeff + starting_ring_coeff)
                        / (S::from(1.).unwrap() + starting_ring_coeff);
                    let p = p * (S::from(1.).unwrap() - t);

                    let res = self.sample(&p);

                    patch.push(res);
                }
            }
        }

        patch.push(self.sample(&V::default()));

        let mut tris = Vec::new();
        let total_ring_res = (ring_res * (ring_res + 1)) / 2;
        for i in 0..ring_res - 1 {
            let curve_res = resolution - i;
            let next_res = curve_res - 1;
            let next_points = sides * next_res;
            let next_row = curve_res * sides;
            let n = ring_res - i;
            // How many points we have added before hitting this row.
            let offset = (total_ring_res - (n * (n + 1)) / 2) * sides;
            for k in 0..sides {
                for j in 0..curve_res {
                    let l = k * curve_res + j;
                    let next_l = k * next_res + j;
                    tris.push(offset + l);
                    tris.push(offset + (l + 1) % (curve_res * sides));
                    tris.push(offset + next_row + (next_l % next_points));

                    // The next row has on less point and thus one less triangle.
                    if j < curve_res - 1 {
                        tris.push(offset + (l + 1) % (curve_res * sides));
                        tris.push(offset + next_row + ((next_l + 1) % next_points));
                        tris.push(offset + next_row + next_l);
                    }
                }
            }
        }

        let start = patch.len() - sides - 1;
        for i in 0..sides {
            tris.push(start + i);
            tris.push(start + ((i + 1) % sides));
            tris.push(patch.len() - 1);
        }

        (patch, tris)
    }
}

pub struct CharrotGregoryParameters<V, S>
where
    V: std::fmt::Debug,
{
    pub resolution: usize,
    pub curves: Vec<Vec<V>>,
    pub bitangent_scale: S,
    pub bitangents: Option<Vec<Vec<V>>>,
}

fn map_polygon_to_patch<V, S>(
    p: &V,
    polygon: &[V],
    curves: &[Vec<V>],
    bitangents: &Option<Vec<Vec<V>>>,
    bitangent_scale: S,
) -> V
where
    V: linear_isomorphic::InnerSpace<S> + std::iter::Sum + std::fmt::Debug,
    S: linear_isomorphic::RealField,
{
    let (weights, sampling_weights) = angle_barycentric(p, polygon);

    let n = polygon.len();
    let sampled1: Vec<_> = (0..sampling_weights.len())
        .map(|i| {
            sample_discrete_curve(
                S::from(1.).unwrap() - sampling_weights[i][0],
                &curves[(i + n - 1) % n],
            )
        })
        .collect();

    let sampled2: Vec<_> = (0..sampling_weights.len())
        .map(|i| sample_discrete_curve(sampling_weights[i][1], &curves[i]))
        .collect();

    let (sampled_bitangent1, sampled_bitangent2) = match bitangents {
        Some(bitangents) => {
            let sampled_bitangent1: Vec<_> = (0..sampling_weights.len())
                .map(|i| {
                    sample_discrete_bitangents(
                        S::from(1.).unwrap() - sampling_weights[i][0],
                        &curves[(i + n - 1) % n],
                        &bitangents[(i + n - 1) % n],
                    )
                })
                .collect();

            let sampled_bitangent2: Vec<_> = (0..sampling_weights.len())
                .map(|i| {
                    sample_discrete_bitangents(
                        sampling_weights[i][1],
                        &curves[i],
                        &bitangents[i],
                    )
                })
                .collect();

            (sampled_bitangent1, sampled_bitangent2)
        }

        None => (Vec::new(), Vec::new()),
    };

    let mut res = V::default();
    for i in 0..sampled1.len() {
        let pi = sampled1[i].clone();
        let qi = sampled2[i].clone();
        let vi = curves[i][0].clone();

        match bitangents {
            Some(bitangents) => {
                let ui = -sampled_bitangent1[(i + n - 0) % n].clone();
                let wi = -sampled_bitangent2[i].clone();
                let tan1 = bitangents[(i + n - 1) % n].last().unwrap().clone();
                let tan2 = bitangents[i][0].clone();
                let twist = (tan1.clone() + tan2.clone()) * S::from(0.5).unwrap();
                let [ti, si] = sampling_weights[i];

                let scale = bitangent_scale;
                let ri = // This comment is for formatting
                        pi +
                        qi -
                        vi +
                        ui * si * scale +
                        wi * ti * scale
                        + tan1 * si * scale
                        + tan2 * ti * scale
                        - twist * (ti * si) * scale;

                res += ri * weights[i];
            }

            None => {
                let ri = pi + qi - vi;
                res += ri * weights[i];
            }
        }
    }

    res
}

pub fn generate_regular_polygon<V, S>(sides: usize) -> Vec<V>
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    let mut vertices = Vec::with_capacity(sides);
    for i in 0..sides {
        let t = S::from(2.).unwrap() * S::from(PI).unwrap() * S::from(i).unwrap()
            / S::from(sides).unwrap();
        let mut new = V::default();
        new[0] = t.cos();
        new[1] = t.sin();
        vertices.push(new);
    }

    vertices
}

pub fn angle_barycentric<V, S>(p: &V, polygon: &[V]) -> (Vec<S>, Vec<[S; 2]>)
where
    V: linear_isomorphic::InnerSpace<S> + std::iter::Sum,
    S: linear_isomorphic::RealField,
{
    let n = polygon.len();

    let distances: Vec<_> = (0..n)
        .map(|i| {
            let p1 = &polygon[i];
            let p2 = &polygon[(i + 1) % n];
            let h = point_line_distance(p1, &(p2.clone() - p1.clone()).normalized(), p);
            h
        })
        .collect();

    let mut barycentric = Vec::with_capacity(polygon.len());
    let mut sampling_weights = Vec::with_capacity(polygon.len());
    let mut total = S::from(0.).unwrap();
    for i in 0..n {
        let mut prod = S::from(1.0).unwrap();
        // Garb every distance not adjacent to the  active polygon corner.
        for j in 0..n - 2 {
            let k = (i + j + 1) % n;

            let h = distances[k];
            debug_assert!(h >= S::from(0.).unwrap());

            prod = prod * h;
        }

        prod = prod * prod;

        let h1 = distances[(i + n - 1) % n];
        let h2 = distances[(i + n + 1) % n];
        let si = h1 / (h1 + h2);

        let h3 = distances[(i) % n];
        let h4 = distances[(i + n - 2) % n];
        let ti = h3 / (h3 + h4);
        sampling_weights.push([ti, si]);

        barycentric.push(prod);
        total += prod;
    }

    for w in &mut barycentric {
        *w = *w / total;
        debug_assert!(*w >= S::from(0.).unwrap(), "{}", w);
    }

    (barycentric, sampling_weights)
}

pub fn barycentric<V, S>(p: &V, tri: &[V; 3]) -> [S; 3]
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    let area_abc = area(&tri[0], &tri[1], &tri[2]);
    let area_bcp = area(&tri[2], &tri[0], p);
    let area_cap = area(&tri[1], &tri[2], p);
    let area_abp = area(&tri[0], &tri[1], p);

    let alpha = area_bcp / area_abc;
    let beta = area_cap / area_abc;
    let gamma = area_abp / area_abc;

    [alpha, beta, gamma]
}

fn area<V, S>(a: &V, b: &V, c: &V) -> S
where
    V: linear_isomorphic::InnerSpace<S>,
    S: linear_isomorphic::RealField,
{
    S::from(0.5).unwrap()
        * ((b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])).abs()
}

// +| Tests |+ =======================================================
#[cfg(test)]
mod tests {
    use super::*;
    use algebra::Vec3;

    #[test]
    fn test_charrot_gregory_patch() {
        let polygon = generate_regular_polygon::<Vec3, f32>(3);

        let n = polygon.len();

        let res = 40;
        let mut curves = Vec::new();
        let mut curve_bitangents = Vec::new();
        for i in 0..n {
            let p1 = polygon[i];
            let p2 = polygon[(i + 1) % n];

            let curve: Vec<_> = (0..=res)
                .map(|i| {
                    let t = i as f32 / res as f32;
                    let f = 2. * t - 1.;
                    lerp(&p1, &p2, t) + Vec3::new(0., 0., 0.3) * (-f * f + 1.)
                })
                .collect();

            let bitangents: Vec<_> = (0..=res)
                .map(|_i| {
                    ((p2 - p1).cross(&Vec3::new(1., 1., 1.)) + Vec3::new(0., 1., 0.))
                        .normalize()
                        * 0.5
                })
                .collect();

            curve_bitangents.push(bitangents);
            curves.push(curve);
        }

        use chicken_wire::wavefront_loader::ObjData;
        let c: Vec<_> = curves.iter().cloned().flatten().collect();
        let charrot_gregory_patch = CharrogGregoryPatch::new(curves, None, 0.3);
        let (patch, tris) = charrot_gregory_patch.mesh(60);
        ObjData::export(&(&patch, &tris), "charrot_gregory_patch.obj");
        ObjData::export(&c, "curves.obj");
    }

    #[test]
    fn test_bezier() {
        let verts = vec![
            Vec3::new(0.000000, 0.000000, -1.000000),
            Vec3::new(-0.951057, 0.000000, -0.309017),
            Vec3::new(-0.587785, 0.000000, 0.809017),
            Vec3::new(0.587785, 0.000000, 0.809017),
            Vec3::new(0.951057, 0.000000, -0.309017),
            Vec3::new(-0.634038, -0.500000, -0.539345),
            Vec3::new(-0.317019, 0.500000, -0.769672),
            Vec3::new(-0.708876, -0.500000, 0.436339),
            Vec3::new(-0.829966, 0.500000, 0.063661),
            Vec3::new(0.195928, -0.500000, 0.809017),
            Vec3::new(-0.195928, 0.500000, 0.809017),
            Vec3::new(0.829966, -0.500000, 0.063661),
            Vec3::new(0.708876, 0.500000, 0.436339),
            Vec3::new(0.317019, -0.500000, -0.769672),
            Vec3::new(0.634038, 0.500000, -0.539345),
        ];

        // Cubic bezier curves. In order, corners match up consecutively.
        let bezier_curves = vec![
            vec![verts[1], verts[6], verts[5], verts[0]],
            vec![verts[0], verts[14], verts[13], verts[4]],
            vec![verts[4], verts[12], verts[11], verts[3]],
            vec![verts[3], verts[10], verts[9], verts[2]],
            vec![verts[2], verts[8], verts[7], verts[1]],
        ];

        ObjData::export(&bezier_curves[0], "controls_1.obj");

        let n = bezier_curves.len();

        let res = 40;
        let mut curves = Vec::new();
        let mut curve_bitangents = Vec::new();
        for i in 0..n {
            let bezier = &bezier_curves[i];

            let curve: Vec<_> = (0..=res)
                .map(|i| {
                    let t = i as f32 / res as f32;
                    let mut inner_bezier = bezier.clone();
                    while inner_bezier.len() > 1 {
                        inner_bezier = inner_bezier
                            .windows(2)
                            .map(|a| lerp(&a[0], &a[1], t))
                            .collect();
                    }
                    inner_bezier[0]
                })
                .collect();
            let prev = &bezier_curves[(i + n - 1) % n];
            let next = &bezier_curves[(i + 1) % n];
            let bt1 = prev[prev.len() - 2] - prev[prev.len() - 1];
            let bt2 = next[1] - next[0];
            let mut bitangents: Vec<_> = curve
                .windows(2)
                .enumerate()
                .map(|(i, p)| {
                    let t = i as f32 / res as f32;
                    let tangent = p[1] - p[0];
                    lerp(&bt1, &bt2, 0.5 - (t * PI as f32).cos() * 0.5)
                        .cross(&tangent)
                        .cross(&tangent) // double cross to perpendicularize
                        .normalize()
                })
                .collect();
            let tangent = curve[curve.len() - 1] - curve[curve.len() - 2];
            bitangents.push(bt2.cross(&tangent).cross(&tangent).normalize());

            curve_bitangents.push(bitangents);
            curves.push(curve);
        }

        use chicken_wire::wavefront_loader::ObjData;
        let c: Vec<_> = curves.iter().cloned().flatten().collect();
        let mut b = Vec::new();
        for i in 0..curves.len() {
            for j in 0..curves[i].len() {
                for k in 0..50 {
                    let t = k as f32 / 49.;
                    b.push(curves[i][j] + curve_bitangents[i][j] * t * 0.1);
                }
            }
        }

        let charrot_gregory_patch =
            CharrogGregoryPatch::new(curves, Some(curve_bitangents), 0.3);
        let (patch, tris) = charrot_gregory_patch.mesh(60);
        ObjData::export(&patch, "charrot_gregory_patch_bezier_dots.obj");
        ObjData::export(&(&patch, &tris), "charrot_gregory_patch_bezier.obj");
        ObjData::export(&c, "bezier_curves.obj");
        ObjData::export(&b, "bitangents.obj");
    }

    #[test]
    fn test_bezier_heterogeneous() {
        let verts = vec![
            Vec3::new(0.000000, 0.000000, -1.000000),
            Vec3::new(-0.951057, 0.000000, -0.309017),
            Vec3::new(-0.587785, 0.000000, 0.809017),
            Vec3::new(0.587785, 0.000000, 0.809017),
            Vec3::new(0.951057, 0.000000, -0.309017),
            Vec3::new(-0.634038, -0.500000, -0.539345),
            Vec3::new(-0.317019, 0.500000, -0.769672),
            Vec3::new(-0.708876, -0.500000, 0.436339),
            Vec3::new(-0.829966, 0.500000, 0.063661),
            Vec3::new(0.195928, -0.500000, 0.809017),
            Vec3::new(-0.195928, 0.500000, 0.809017),
            Vec3::new(0.829966, -0.500000, 0.063661),
            Vec3::new(0.708876, 0.500000, 0.436339),
            Vec3::new(0.317019, -0.500000, -0.769672),
            Vec3::new(0.634038, 0.500000, -0.539345),
        ];

        // Cubic bezier curves. In order, corners match up consecutively.
        let bezier_curves = vec![
            vec![verts[1], verts[6], verts[5], verts[0]],
            vec![verts[0], verts[14], verts[13], verts[4]],
            vec![verts[4], verts[12], verts[11], verts[3]],
            vec![verts[3], verts[10], verts[9], verts[2]],
            vec![verts[2], verts[8], verts[7], verts[1]],
        ];

        ObjData::export(&bezier_curves[0], "controls_1.obj");

        let n = bezier_curves.len();

        let res = 40;
        let mut curves = Vec::new();
        let mut curve_bitangents = Vec::new();
        for i in 0..n {
            let bezier = &bezier_curves[i];

            let curve: Vec<_> = (0..=res)
                .map(|i| {
                    let t = i as f32 / res as f32;
                    let mut inner_bezier = bezier.clone();
                    while inner_bezier.len() > 1 {
                        inner_bezier = inner_bezier
                            .windows(2)
                            .map(|a| lerp(&a[0], &a[1], t))
                            .collect();
                    }
                    inner_bezier[0]
                })
                .collect();
            let prev = &bezier_curves[(i + n - 1) % n];
            let next = &bezier_curves[(i + 1) % n];
            let bt1 = prev[prev.len() - 2] - prev[prev.len() - 1];
            let bt2 = next[1] - next[0];
            let mut bitangents: Vec<_> = curve
                .windows(2)
                .enumerate()
                .map(|(i, p)| {
                    let t = i as f32 / res as f32;
                    let tangent = p[1] - p[0];
                    lerp(&bt1, &bt2, 0.5 - (t * PI as f32).cos() * 0.5)
                        .cross(&tangent)
                        .cross(&tangent) // double cross to perpendicularize
                        .normalize()
                })
                .collect();
            let tangent = curve[curve.len() - 1] - curve[curve.len() - 2];
            bitangents.push(bt2.cross(&tangent).cross(&tangent).normalize());

            curve_bitangents.push(bitangents);
            curves.push(curve);
        }

        use chicken_wire::wavefront_loader::ObjData;
        let mut b = Vec::new();
        for i in 0..curves.len() {
            for j in 0..curves[i].len() {
                for k in 0..50 {
                    let t = k as f32 / 49.;
                    b.push(curves[i][j] + curve_bitangents[i][j] * t * 0.1);
                }
            }
        }

        let charrot_gregory_patch =
            CharrogGregoryPatch::new(curves, Some(curve_bitangents), 0.3);
        let (patch, tris) =
            charrot_gregory_patch.mesh_with_per_curve_coeffs(&[10, 20, 10, 20, 30], 1.0);
        ObjData::export(&patch, "charrot_gregory_patch_heterogeneous_dots.obj");
        ObjData::export(&(&patch, &tris), "charrot_gregory_patch_heterogeneous.obj");
    }
}
