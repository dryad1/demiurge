use algebra::*;

#[derive(Debug, PartialEq, Eq)]
pub enum TopologyKind
{
    Cylinder,
    CappedVMin,
    CappedVMax,
    Sphere,
}

pub struct Parameters
{
    pub u_resolution: usize,
    pub v_resolution: usize,
    pub u_range: [f32; 2],
    pub v_range: [f32; 2],
    pub topology: TopologyKind,
}

impl Default for Parameters
{
    fn default() -> Self
    {
        Parameters {
            u_resolution: 10,
            v_resolution: 10,
            u_range: [0., 1.],
            v_range: [0., 1.],
            topology: TopologyKind::Cylinder,
        }
    }
}

/// Compute the geometry and topology of an arbitrary parametric surface that is
/// isomorphic to an open cylinder (i.e. no caps) or a sphere (if capped). This
/// function assumes the boundaries will occur at the iso lines $f(u, v_a)$ and
/// $f(u, v_b)$ where $v_a, v_b$ are the constant values of the endpoints of the
/// range of $v$.
///
/// The first `u_resolution` points correspond to the first boundary, the last
/// `u_resolution` points correspond to the second boundary.
///
/// # Arguments
///
/// * `f` the parametric function.
/// * `u_resolution` number of samples to make along $u$.
/// * `v_resolution` number of samples to make along $v$.
/// * `u_range` interval along which `u` will be sampled.
/// * `v_range` interval along which `v` will be sampled.
///
/// Returns tuple of the sampled vertices and their connectivity
/// indices/topology.
pub fn mesh_surface_triangle<F>(f: F, parameters: Parameters) -> (Vec<Vec3>, Vec<usize>)
where
    F: Fn(f32, f32) -> Vec3,
{
    let Parameters {
        u_resolution,
        v_resolution,
        u_range,
        mut v_range,
        topology,
    } = parameters;

    let delta = 1. / (v_resolution as f32 - 1.);
    // Depending on the topology kind, lead a small gap at the end of the ranges to
    // create a cap and close the mesh.

    if topology == TopologyKind::CappedVMin || topology == TopologyKind::Sphere
    {
        v_range[0] += delta;
    }
    if topology == TopologyKind::CappedVMax || topology == TopologyKind::Sphere
    {
        v_range[1] -= delta;
    }

    let u_span = u_range[1] - u_range[0];
    let v_span = v_range[1] - v_range[0];

    debug_assert!(u_span > 0.0);
    debug_assert!(v_span > 0.0);

    let mut points = Vec::new();
    for j in 0..v_resolution
    {
        // Unlike for u, *do* sample up to the end point for $v$.
        let v = (j as f32 * v_span / (v_resolution - 1) as f32) + v_range[0];
        for i in 0..u_resolution
        {
            // u should *not* span the full range. It is assumed to be cyclical so
            // $f(u_a, v) = f(u_b, v)$. Leave a small gap to avoid duplicate vertices.
            let u = (i as f32 * u_span / (u_resolution + 1) as f32) + u_range[0];

            points.push(f(u, v));
        }
    }

    // Construct a triangular mesh topology for the points.
    let mut indices = Vec::new();
    for i in 0..v_resolution - 1
    {
        for j in 0..u_resolution
        {
            let p00 = j + i * u_resolution;
            let p01 = (i + 1) * u_resolution + j;
            let p10 = i * u_resolution + ((j + 1) % u_resolution);
            let p11 = (i + 1) * u_resolution + ((j + 1) % u_resolution);
            let corners = [p00, p01, p10, p11];

            // This is to shift the diagonal every other quad to avoid biasing the mesh.
            // i.e. it creats a zigzaging pattern along the parallel isolines.
            let offset = j % 2;
            // First triangle.
            indices.push(corners[1]);
            indices.push(corners[0]);
            indices.push(corners[3 - offset]);
            // Second triangle.
            indices.push(corners[offset]);
            indices.push(corners[2]);
            indices.push(corners[3]);
        }
    }

    // Depending on the chosen topology kind we need to add a cap at the ends of
    // the "cylinder" we just made.

    // Add a cap at the "botom".
    if topology == TopologyKind::CappedVMin || topology == TopologyKind::Sphere
    {
        let n = points.len();
        points.push(f(u_range[0], v_range[0] - delta));
        for i in 0..u_resolution
        {
            indices.push(i);
            indices.push(n);
            indices.push((i + 1) % u_resolution);
        }
    }
    // Add a cap at the "top".
    if topology == TopologyKind::CappedVMax || topology == TopologyKind::Sphere
    {
        let n = points.len();
        points.push(f(u_range[0], v_range[1] + delta));
        for i in 0..u_resolution
        {
            let offset = n - u_resolution;
            indices.push(n);
            indices.push(i + offset);
            indices.push(((i + 1) % u_resolution) + offset);
        }
    }

    (points, indices)
}

/// Like `mesh_surface_triangle` but returns quads for the
/// topology.
pub fn mesh_surface_quad<F>(f: F, parameters: Parameters) -> (Vec<Vec3>, Vec<Vec<usize>>)
where
    F: Fn(f32, f32) -> Vec3,
{
    let Parameters {
        u_resolution,
        v_resolution,
        u_range,
        mut v_range,
        topology,
    } = parameters;

    let delta = 1. / (v_resolution as f32 - 1.);
    // Depending on the topology kind, lead a small gap at the end of the ranges to
    // create a cap and close the mesh.

    if topology == TopologyKind::CappedVMin || topology == TopologyKind::Sphere
    {
        v_range[0] += delta;
    }
    if topology == TopologyKind::CappedVMax || topology == TopologyKind::Sphere
    {
        v_range[1] -= delta;
    }

    let u_span = u_range[1] - u_range[0];
    let v_span = v_range[1] - v_range[0];

    debug_assert!(u_span > 0.0);
    debug_assert!(v_span > 0.0);

    let mut points = Vec::new();
    for j in 0..v_resolution
    {
        // Unlike for u, *do* sample up to the end point for $v$.
        let v = (j as f32 * v_span / (v_resolution - 1) as f32) + v_range[0];
        for i in 0..u_resolution
        {
            // u should *not* span the full range. It is assumed to be cyclical so
            // $f(u_a, v) = f(u_b, v)$. Leave a small gap to avoid duplicate vertices.
            let u = (i as f32 * u_span / (u_resolution + 1) as f32) + u_range[0];

            points.push(f(u, v));
        }
    }

    // Construct a mesh topology for the points.
    let mut indices = Vec::new();
    for i in 0..v_resolution - 1
    {
        for j in 0..u_resolution
        {
            let p00 = j + i * u_resolution;
            let p01 = (i + 1) * u_resolution + j;
            let p10 = i * u_resolution + ((j + 1) % u_resolution);
            let p11 = (i + 1) * u_resolution + ((j + 1) % u_resolution);

            indices.push(vec![p00, p10, p11, p01]);
        }
    }

    // Depending on the chosen topology kind we need to add a cap at the ends of
    // the "cylinder" we just made.

    // Add a cap at the "botom".
    if topology == TopologyKind::CappedVMin || topology == TopologyKind::Sphere
    {
        let n = points.len();
        points.push(f(u_range[0], v_range[0] - delta));
        for i in 0..u_resolution
        {
            indices.push(vec![i, n, (i + 1) % u_resolution]);
        }
    }

    // Add a cap at the "top".
    if topology == TopologyKind::CappedVMax || topology == TopologyKind::Sphere
    {
        let n = points.len();
        points.push(f(u_range[0], v_range[1] + delta));
        for i in 0..u_resolution
        {
            let test = topology == TopologyKind::Sphere;
            let offset = n - u_resolution - (test as usize);
            indices.push(vec![n, i + offset, ((i + 1) % u_resolution) + offset]);
        }
    }

    (points, indices)
}
