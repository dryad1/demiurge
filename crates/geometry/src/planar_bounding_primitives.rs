use std::{f32::consts::PI, fmt::Debug, ops::Mul};

use algebra::utils::*;
use linear_isomorphic::*;

/// Find an orthonormal basis for a planar set of points in some dimension.
/// The order of the return elements is, origin, first basis, second basis.
pub fn find_planar_orthonormal_basis<V, S>(
    points: &dyn Fn(usize) -> V,
    point_count: usize,
) -> (V, V, V)
where
    V: InnerSpace<S> + Debug,
    S: RealField + Mul<V, Output = V>,
{
    let pi = find_ortho_point(points, point_count);
    let e1 = (points(1) - points(0)).normalized();
    let e2 = (points(pi) - points(0)).normalized();
    let e2 = orthogonal_vec_from_basis(&e1, &e2);

    (points(0), e1, e2)
}

pub struct BoundingRectangle<V>
{
    pub corner: V,
    pub d1: V,
    pub d2: V,
}

/// Finds the tightest bounding rectangle of a set of points.
pub fn bounding_rectangle<V>(
    points: &dyn Fn(usize) -> V,
    point_count: usize,
) -> BoundingRectangle<V>
where
    V: InnerSpace<f32> + Debug + Copy,
{
    let pi = find_ortho_point(points, point_count);
    let e1 = (points(1) - points(0)).normalized();
    let e2 = (points(pi) - points(0)).normalized();
    let e2 = orthogonal_vec_from_basis(&e1, &e2);

    let x_coord = |point: &V| (point.clone() - points(0)).dot(&e1);
    let y_coord = |point: &V| (point.clone() - points(0)).dot(&e2);

    let x_min = (0..point_count)
        .map(|id| x_coord(&points(id)))
        .min_by(|a, b| a.total_cmp(b))
        .unwrap();
    let x_max = (0..point_count)
        .map(|id| x_coord(&points(id)))
        .max_by(|a, b| a.total_cmp(b))
        .unwrap();
    let y_min = (0..point_count)
        .map(|id| y_coord(&points(id)))
        .min_by(|a, b| a.total_cmp(b))
        .unwrap();
    let y_max = (0..point_count)
        .map(|id| y_coord(&points(id)))
        .max_by(|a, b| a.total_cmp(b))
        .unwrap();

    BoundingRectangle {
        corner: points(0) + e1 * x_min + e2.clone() * y_min,
        d1: e1 * (x_max - x_min),
        d2: e2 * (y_max - y_min),
    }
}

pub struct BoundingCircle<V>
{
    pub center: V,
    pub radius: f32,
    pub basis_1: V,
    pub basis_2: V,
}

/// Returns *a* bounding circle guaranteed to contain all points, not
/// necessarily a tightest one.
pub fn bounding_circle<V>(
    points: &dyn Fn(usize) -> V,
    point_count: usize,
) -> BoundingCircle<V>
where
    V: InnerSpace<f32> + Debug + Copy,
{
    let BoundingRectangle { corner, d1, d2 } = bounding_rectangle(points, point_count);

    let w = d1.norm();
    let h = d2.norm();
    let r = ((w * w + h * h) / 4.0).sqrt();

    BoundingCircle {
        center: (corner * 2.0 + d1 + d2.clone()) * 0.5,
        radius: r,
        basis_1: d1.normalized(),
        basis_2: d2.normalized(),
    }
}

pub struct BoundingTriangle<V>
{
    pub p1: V,
    pub p2: V,
    pub p3: V,
}

/// Returns *a* bounding circle guaranteed to contain all points, not
/// necessarily a tightest one.
pub fn bounding_triangle<V>(
    points: &dyn Fn(usize) -> V,
    point_count: usize,
) -> BoundingTriangle<V>
where
    V: InnerSpace<f32> + Debug + Copy,
{
    let BoundingCircle {
        center,
        radius,
        basis_1: e1,
        basis_2: e2,
    } = bounding_circle(points, point_count);

    let l = radius * (60.0 * PI / 180.0).tan();

    let p1 = center - e2.clone() * radius - e1.clone() * l;
    let p2 = center.clone() - e2.clone() * radius + e1.clone() * l;
    let p3 = center + e2.clone() * ((radius * radius + l * l).sqrt());

    BoundingTriangle { p1, p2, p3 }
}

// The returned vector will have a positive dot product with v.
// https://math.stackexchange.com/q/4746227/460810
pub fn orthogonal_vec_from_basis<V, S>(u: &V, v: &V) -> V
where
    V: InnerSpace<S> + Debug,
    S: RealField + Debug,
{
    let a = -u.dot(v) / u.dot(u);

    let w = u.clone() * a + v.clone();

    // The result better be orthogonal.
    debug_assert!(
        w.normalized().dot(&u.normalized()).abs() <= S::from(1e5).unwrap(),
        "{:?}",
        w.dot(u).abs()
    );

    w.normalized()
}
