use crate::point_segment_distance;
use crate::project_onto_open_poly_line;

use linear_isomorphic::{InnerSpace, RealField};

pub fn polyline_sdf<V, S>(p: &V, curve: &[V], radius: S, decay: S) -> S
where
    V: InnerSpace<S>,
    S: RealField + std::iter::Sum,
{
    let (proj, param, _) = project_onto_open_poly_line(p, curve);

    let d = (p.clone() - proj.clone()).norm()
        - radius * (param * S::from(2).unwrap().ln() * decay).exp();

    d
}

pub fn capsule<V, S>(p: &V, a: &V, b: &V, radius: S) -> S
where
    V: InnerSpace<S>,
    S: RealField + std::iter::Sum,
{
    point_segment_distance(a, b, p) - radius
}
