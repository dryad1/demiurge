use std::collections::{BTreeMap, HashMap, HashSet};
use std::fmt::Debug;

use algebra::{na::DMatrix, *};
use graphs::depth_first_search::DFSIterator;
use hedge::mesh::HalfMesh;
use hedge::mesh::{verify_geometry, verify_topology};
use itertools::Itertools;
use linear_isomorphic::*;
use topology::concatenate_meshes;

use crate::{
    constrained_delaunay_triangulation_hollow, interior_polygon_point,
    polytope_mesh_from_directions, project_onto_plane,
};

const PERCENTAGE_OF_EDGE: f32 = 0.2;

pub struct MeshSkeletonParameters {
    pub corner_sharpness: f32,
}

impl Default for MeshSkeletonParameters {
    fn default() -> Self {
        Self {
            corner_sharpness: 0.9,
        }
    }
}

type FaceList = Vec<Vec<usize>>;
type VertList = Vec<Vec3>;
// TODO(low): to finish this properly, implement catmull-clark subdivision
// then proceed to do, one subdivision, some average smoothing, a couple
// subdivisions.
pub fn mesh_skeleton<
    PositionGetter,
    DirectionGetter,
    EdgeGetter,
    EdgeIter,
    EdgeMesh,
    LeafMesh,
    JointSize,
>(
    root: usize,
    get_position: PositionGetter,
    get_direction: DirectionGetter,
    neighbours: EdgeGetter,
    edge_mesher: EdgeMesh,
    leaf_mesher: LeafMesh,
    joint_size: JointSize,
    parameters: MeshSkeletonParameters,
) -> (HalfMesh<Vec<Vec3>, (), ()>, usize)
where
    PositionGetter: Fn(usize) -> Vec3,
    DirectionGetter: Fn(usize, usize) -> Vec3,
    EdgeGetter: Fn(&usize, usize, &usize) -> EdgeIter + Clone,
    EdgeIter: Iterator<Item = usize>,
    // (Vertices, Faces, First boundary, second boundary).
    EdgeMesh: Fn(&[Vec3; 2], &[usize; 2]) -> (VertList, FaceList, [usize; 2], [usize; 2]),
    LeafMesh: Fn(&[Vec3; 2], &[usize; 2]) -> (VertList, FaceList, [usize; 2]),
    JointSize: Fn(usize) -> f32,
{
    // Create a polytope per joint.
    let JointPolytopeData {
        verts: mut vs,
        topology: mut fs,
        polytope_vertex_ranges,
        edge_face_map,
        joint_shortest_distance,
    } = make_joint_polytopes(root, &get_position, &get_direction, &neighbours);

    // ObjData::export(&(&vs, &fs), "joint_polytopes.obj");
    debug_assert!(
        verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
    );
    debug_assert!(verify_geometry(&HalfMesh::new(
        vs.clone(),
        (),
        (),
        fs.clone()
    )));

    let polytope_vertex_count = vs.len();
    let mut flags = vec![VertexType::Polytope; vs.len()];

    // Create generalized cylinders connecting the polytope.
    let mut joint_boundary_vertex_map = BTreeMap::new();
    let mut edge_boundaries_edges = HashSet::new();
    let mut seen_edges = HashSet::new();

    for (&[j1, j2], &fid1) in edge_face_map.iter() {
        let opposite = [j2, j1];
        let mut key = opposite;
        key.sort();

        if !seen_edges.insert(key) {
            continue;
        }

        // ObjData::export(&(&vs, &fs), format!("stitching_{}.obj",
        // dbg_count).as_str());
        debug_assert!(
            verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
        );

        let source = get_position(j1);
        let dest = get_position(j2);
        let dir1 = get_direction(j1, j2).normalize();
        let dir2 = get_direction(j2, j1).normalize();

        let is_leaf_edge = !edge_face_map.contains_key(&opposite);

        let cverts;
        let cfaces;
        let boundary1;
        let mut boundary2 = [0; 2];

        let polytope_size1 =
            *joint_shortest_distance.get(&j1).unwrap() * PERCENTAGE_OF_EDGE;
        let polytope_size2 =
            *joint_shortest_distance.get(&j2).unwrap() * PERCENTAGE_OF_EDGE;
        if is_leaf_edge {
            (cverts, cfaces, boundary1) =
                leaf_mesher(&[(source + dir1 * polytope_size1), (dest)], &[j1, j2]);
        } else {
            (cverts, cfaces, boundary1, boundary2) = edge_mesher(
                &[
                    (source + dir1 * polytope_size1),
                    (dest + dir2 * polytope_size2),
                ],
                &[j1, j2],
            );
        }

        // crate::export(&cverts, &vec![], &cfaces, "tube.obj");

        let n = vs.len();
        (vs, fs) = concatenate_meshes(vs, fs, cverts, cfaces);

        // crate::export(&vs, &vec![], &fs, "concatenated.obj");

        flags.extend((n..vs.len()).map(|_| VertexType::Edge));

        // Connect both ends of the cylinder to the faces of the polytopes.
        connect_cylinder_to_face(
            n,
            boundary1,
            false,
            j1,
            fid1,
            &mut vs,
            &mut fs,
            &mut joint_boundary_vertex_map,
            &mut edge_boundaries_edges,
            &mut flags,
        );

        // crate::export(&vs, &vec![], &fs, "stitching_b1.obj");
        debug_assert!(
            verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
        );

        // If the edge is a leaf then there is no polytope on the other endpoint.
        if is_leaf_edge {
            continue;
        }
        let fid2 = *(edge_face_map.get(&opposite).unwrap());

        connect_cylinder_to_face(
            n,
            boundary2,
            true,
            j2,
            fid2,
            &mut vs,
            &mut fs,
            &mut joint_boundary_vertex_map,
            &mut edge_boundaries_edges,
            &mut flags,
        );

        // crate::export(&vs, &vec![], &fs, "stitching_b2.obj");
        debug_assert!(
            verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
        );
    }

    // ObjData::export(&(&vs, &fs), "topology1.obj");
    debug_assert!(
        verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
    );

    // Project polytope corners into a sphere around a point near the joint.
    for (joint_id, vert_range) in polytope_vertex_ranges {
        let vids = joint_boundary_vertex_map.get(&joint_id);
        // This means the joint is a leaf joint.
        if vids.is_none() {
            continue;
        }
        let vids = vids.unwrap();
        let boundary_average: Vec3 =
            vids.iter().map(|id| vs[*id]).sum::<Vec3>() / vids.len() as f32;

        let joint_position = get_position(joint_id);

        let vert_indices: Vec<_> = (vert_range[0]..vert_range[1]).collect();

        let distance = joint_size(joint_id);
        // Compute an average between the joint position and the centroid of the tube
        // boundaries.
        let t = parameters.corner_sharpness;
        let c = (1. - t) * boundary_average + t * joint_position;
        for &i in vert_indices.iter() {
            vs[i] = c + (vs[i] - c).normalized() * distance
        }
    }

    let mesh = HalfMesh::new(vs, (), (), fs);

    debug_assert!(verify_topology(&mesh).is_none());
    debug_assert!(verify_geometry(&mesh));

    (mesh, polytope_vertex_count)
}

// +| Internal |+ ==============================================================
fn connect_tube_end_to_face<I>(
    curve_range: I,
    face_index: usize,
    verts: &mut Vec<Vec3>,
    topology: &mut Vec<Vec<usize>>,
) where
    I: Iterator<Item = usize> + ExactSizeIterator,
{
    let face = &mut topology[face_index];

    let v0 = verts[face[0]];
    let d1 = (verts[face[1]] - v0).normalize();
    let orthogonal_id = (2..face.len())
        .min_by(|i, j| {
            let u = (verts[face[*i]] - v0).normalize().dot(&d1).abs();
            let v = (verts[face[*j]] - v0).normalize().dot(&d1).abs();

            u.partial_cmp(&v).unwrap()
        })
        .unwrap();
    let d2 = (verts[face[orthogonal_id]] - v0).normalize();

    let normal = d1.cross(&d2).normalize();
    debug_assert!(!normal[0].is_nan());
    let mut center: Vec3 = (0..face.len()).map(|i| verts[face[i]]).sum();
    center = center / face.len() as f32;

    let n = face.len();
    let curve_point_count = curve_range.len();
    face.extend(curve_range);

    let mut projected_points = face
        .iter()
        .map(|i| project_onto_plane(&center, &normal, &verts[*i]))
        .collect_vec();

    let max_distance = face
        .iter()
        .map(|i| (project_onto_plane(&center, &normal, &verts[*i]) - center).norm())
        .max_by(|a, b| a.partial_cmp(&b).unwrap())
        .unwrap();

    for i in 0..n {
        let dir = (projected_points[i] - center).normalize();
        // All we truly need is for the convex face to fully contain the points.
        projected_points[i] += dir * max_distance * 5.0;
    }

    let interior_point =
        interior_polygon_point(&projected_points.iter().skip(n).copied().collect_vec());

    projected_points.push(interior_point);

    let triangulation = {
        constrained_delaunay_triangulation_hollow(
            &|i| projected_points[i],
            projected_points.len(),
            &|i| [n + i, n + ((i + 1) % curve_point_count)],
            curve_point_count,
            &|_| projected_points.len() - 1,
            1,
        )
    };

    // Add all triangles except the first to the global mesh.
    for i in (3..triangulation.len()).step_by(3) {
        let p1 = triangulation[i + 0];
        let p2 = triangulation[i + 1];
        let p3 = triangulation[i + 2];

        topology.push(vec![
            topology[face_index][p1],
            topology[face_index][p2],
            topology[face_index][p3],
        ]);
    }

    // The face at `face_index` is the original polygonal face we just triangulated.
    // Copy the first triangle to the first 3 indices of that polygonal face.
    let p1 = topology[face_index][triangulation[0]];
    let p2 = topology[face_index][triangulation[1]];
    let p3 = topology[face_index][triangulation[2]];

    topology[face_index][0] = p1;
    topology[face_index][1] = p2;
    topology[face_index][2] = p3;

    // Truncate the array to have 3 elements, effectively deleting the original face
    // and replacing it with the first triangle in the triangulation.
    topology[face_index].truncate(3);
}

struct JointPolytopeData {
    verts: Vec<Vec3>,
    topology: Vec<Vec<usize>>,
    polytope_vertex_ranges: Vec<(usize, [usize; 2])>,
    edge_face_map: BTreeMap<[usize; 2], usize>,
    joint_shortest_distance: HashMap<usize, f32>,
}
/// Creates polytope meshes for all the joints in the specified graph.
///
/// Also constructs a map from each edge to the two faces in the
/// polytope that correspond to it. Faces are guaranteed to appear in the
/// same order as the edges of the graph that generate them.
fn make_joint_polytopes<E, I, P, D>(
    start: usize,
    position: P,
    direction: D,
    neighbours: E,
) -> JointPolytopeData
where
    E: Fn(&usize, usize, &usize) -> I + Clone,
    I: Iterator<Item = usize>,
    P: Fn(usize) -> Vec3,
    D: Fn(usize, usize) -> Vec3,
{
    let mut verts = Vec::<Vec3>::new();
    let mut topology = Vec::new();
    let mut polytope_vertex_ranges = Vec::new();

    let mut face_count = 0;
    let mut edge_face_map = BTreeMap::new();
    let mut joint_shortest_distance = HashMap::new();

    for (vert, depth, parent) in DFSIterator::new(start, neighbours.clone()) {
        let center = position(vert);
        let mut directions = DMatrix::<f32>::from_vec(0, 3, vec![]);

        let min_distance = neighbours(&vert, depth, &parent)
            .map(|n| (position(n) - position(vert)).norm())
            .min_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();
        joint_shortest_distance.insert(vert, min_distance);

        let valence = neighbours(&vert, depth, &parent).count();
        if valence <= 1 {
            polytope_vertex_ranges.push((vert, [verts.len(), verts.len()]));
            continue;
        }

        for neighbour in neighbours(&vert, depth, &parent) {
            let dir = direction(vert, neighbour).normalize();

            directions = directions.clone().insert_row(directions.nrows(), 0.0);
            let n = directions.nrows();
            directions[(n - 1, 0)] = dir[0];
            directions[(n - 1, 1)] = dir[1];
            directions[(n - 1, 2)] = dir[2];

            let key = [vert, neighbour];
            edge_face_map.insert(key, face_count);
            face_count += 1;
        }

        let distance = |normal: &Vec3| {
            (center + normal * min_distance * PERCENTAGE_OF_EDGE).dot(normal)
        };
        let (vs, fs) = polytope_mesh_from_directions(&mut directions, distance, true);

        // crate::export(&vs, &vec![], &fs, "polytope.obj");
        debug_assert!(
            verify_topology(&HalfMesh::new(vs.clone(), (), (), fs.clone())).is_none()
        );

        let prior_vert_count = verts.len();
        (verts, topology) = concatenate_meshes(verts, topology, vs, fs);

        face_count = topology.len();
        polytope_vertex_ranges.push((vert, [prior_vert_count, verts.len()]))
    }

    JointPolytopeData {
        verts,
        topology,
        polytope_vertex_ranges,
        edge_face_map,
        joint_shortest_distance,
    }
}

#[derive(Debug, Clone)]
enum VertexType {
    Polytope,
    Edge,
    EdgeBoundary,
}

impl PartialEq for VertexType {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

impl Eq for VertexType {}

fn connect_cylinder_to_face(
    start_index: usize,
    boundary: [usize; 2],
    reverse_boundary_orientation: bool,
    joint_id: usize,
    face_id: usize,
    vs: &mut Vec<Vec3>,
    fs: &mut Vec<Vec<usize>>,
    joint_boundary_vertex_map: &mut BTreeMap<usize, Vec<usize>>,
    edge_boundaries_edges: &mut HashSet<[usize; 2]>,
    flags: &mut Vec<VertexType>,
) {
    use itertools::*;
    let range = if reverse_boundary_orientation {
        Either::Left((start_index + boundary[0]..start_index + boundary[1]).rev())
    } else {
        Either::Right(start_index + boundary[0]..start_index + boundary[1])
    };

    // Connect both ends of the cylinder to the faces of the
    // polytopes.
    let pn = vs.len();
    connect_tube_end_to_face(range, face_id, vs, fs);
    flags.extend((0..vs.len() - pn).map(|_| VertexType::Polytope));

    let list: &mut Vec<_> = joint_boundary_vertex_map.entry(joint_id).or_default();
    list.extend(start_index + boundary[0]..start_index + boundary[1]);

    for i in start_index + boundary[0]..start_index + boundary[1] {
        flags[i] = VertexType::EdgeBoundary;
        let o = (i - start_index + 1) % boundary[1];
        edge_boundaries_edges.insert([i, o + start_index]);
    }
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests {
    use std::f32::consts::PI;

    use algebra::*;
    use chicken_wire::wavefront_loader::ObjData;
    use graphs::line_soup_to_graph;
    use hedge::mesh::GeomId;
    use hedge::mesh::HalfMesh;
    use hedge::HalfMeshLike;
    use linear_isomorphic::*;
    use pga::MultiVec3D;

    use crate::mesh_skeleton;

    fn sweep_2d_curve<F>(
        source: &Vec3,
        dest: &Vec3,
        resolution: usize,
        rotation: f32,
        curve: F,
        close_cap: bool,
    ) -> (Vec<Vec3>, Vec<Vec<usize>>, [usize; 2], [usize; 2])
    where
        F: Fn(f32) -> Vec2,
    {
        let start = MultiVec3D::from_vec3(source);
        let end = MultiVec3D::from_vec3(dest);
        let l = start & end;

        let circle_radius = 0.05;
        // Find a vector that is orthogonal with the line.
        let l1 = l
            * MultiVec3D::new(1.0, 14)
            * (MultiVec3D::new(1.0, 2)
                + MultiVec3D::new(1.0, 3)
                + MultiVec3D::new(1.0, 4));
        let x_axis = l1.get_line_dir().normalized() * circle_radius;

        let l2 = (l * l1 - l1 * l) / 2.0;
        let y_axis = l2.get_line_dir().normalized() * circle_radius;

        use crate::mesh_surface_quad;
        use crate::TopologyKind;
        let (verts, faces) = mesh_surface_quad(
            |u, v| {
                let c = (1.0 - v) * source + v * dest;

                let coords = curve(u);
                let coords = Vec2::new(
                    (v * rotation).cos() * coords.x - (v * rotation).sin() * coords.y,
                    (v * rotation).sin() * coords.x + (v * rotation).cos() * coords.y,
                );

                c + (x_axis * coords.x - y_axis * coords.y)
            },
            crate::Parameters {
                u_resolution: resolution,
                v_resolution: 24,
                u_range: [0.0, 2.0 * PI],
                v_range: [0.0, 1.0],
                topology: if close_cap {
                    TopologyKind::CappedVMax
                } else {
                    TopologyKind::Cylinder
                },
                ..Default::default()
            },
        );

        let n = verts.len();
        (
            verts,
            // triangle_mesh_to_poly_mesh(faces),
            faces,
            [0, resolution],
            [n - resolution, n],
        )
    }

    #[test]
    fn test_mesh_skeleton_screw_edges_cube() {
        let ObjData {
            vertices,
            vertex_face_indices,
            ..
        } = ObjData::from_disk_file("test_data/cube.obj");
        let cube_mesh = HalfMesh::new(vertices, (), (), vertex_face_indices);

        let mesh = mesh_skeleton(
            0,
            |i: usize| cube_mesh.vert_handle(GeomId(i as u64)).data(),
            |i: usize, j: usize| {
                cube_mesh.vert_handle(GeomId(j as u64)).data()
                    - cube_mesh.vert_handle(GeomId(i as u64)).data()
            },
            |&i: &usize, _, _| {
                cube_mesh
                    .vert_handle(GeomId(i as u64))
                    .iter_verts()
                    .map(|v| v.id().0 as usize)
            },
            |&[v1, v2], _| {
                let res = 20;
                sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    7.1,
                    |u| {
                        let r = 0.5 + 0.3 * (5. * (u + 0.95)).cos();
                        let x = r * u.cos();
                        let y = r * u.sin();

                        Vec2::new(x, y)
                    },
                    false,
                )
            },
            |&[v1, v2], _| {
                let res = 20;

                let (vs, fs, b1, _) = sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    7.1,
                    |u| {
                        let r = 0.5 + 0.3 * (5. * (u + 0.95)).cos();
                        let x = r * u.cos();
                        let y = r * u.sin();

                        Vec2::new(x, y)
                    },
                    true,
                );

                (vs, fs, b1)
            },
            |_| 0.2,
            crate::MeshSkeletonParameters {
                ..Default::default()
            },
        );

        ObjData::export(&mesh.0, "tmp/screw_cube.obj");
    }

    #[test]
    fn test_mesh_skeleton_screw_edges() {
        let ObjData {
            vertices, lines, ..
        } = ObjData::from_disk_file("test_data/arrow_skeleton.obj");
        let graph = line_soup_to_graph(&lines);

        let mesh = mesh_skeleton(
            0,
            |i: usize| vertices[i],
            |i: usize, j: usize| vertices[j] - vertices[i],
            |&i: &usize, _, _| graph[i].iter().map(|n| *n as usize),
            |&[v1, v2], _| {
                let res = 30;
                sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    0.,
                    |u| {
                        let x = 0.8 * u.cos();
                        let y = 0.8 * u.sin();

                        Vec2::new(x, y)
                    },
                    false,
                )
            },
            |&[v1, v2], _| {
                let res = 20;

                let (vs, fs, b1, _) = sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    7.1,
                    |u| {
                        let r = 0.5 + 0.3 * (5. * (u + 0.95)).cos();
                        let x = r * u.cos();
                        let y = r * u.sin();

                        Vec2::new(x, y)
                    },
                    true,
                );

                (vs, fs, b1)
            },
            |_| 0.2,
            crate::MeshSkeletonParameters {
                ..Default::default()
            },
        );

        ObjData::export(&mesh.0, "tmp/screw_arrow.obj");
    }

    #[test]
    fn test_mesh_skeleton_circular_edges() {
        let ObjData {
            vertices, lines, ..
        } = ObjData::from_disk_file("test_data/arrow_skeleton.obj");
        let graph = line_soup_to_graph(&lines);

        let mesh = mesh_skeleton(
            0,
            |i: usize| vertices[i],
            |i: usize, j: usize| vertices[j] - vertices[i],
            |&i: &usize, _, _| graph[i].iter().map(|n| *n as usize),
            |&[v1, v2], _| {
                let res = 30;
                sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    0.,
                    |u| {
                        let x = 0.8 * u.cos();
                        let y = 0.8 * u.sin();

                        Vec2::new(x, y)
                    },
                    false,
                )
            },
            |&[v1, v2], _| {
                let res = 4;
                let (vs, fs, b1, _) = sweep_2d_curve(
                    &v1,
                    &v2,
                    res,
                    0.,
                    |u| {
                        let x = 0.8 * u.cos();
                        let y = 0.8 * u.sin();

                        Vec2::new(x, y)
                    },
                    true,
                );

                (vs, fs, b1)
            },
            |_| 0.4,
            crate::MeshSkeletonParameters {
                ..Default::default()
            },
        );

        ObjData::export(&mesh.0, "tmp/meshing_circular_arrow.obj");
    }
}
