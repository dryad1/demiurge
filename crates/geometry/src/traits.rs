use std::ops::Mul;

pub(crate) use linear_isomorphic::*;

pub fn norm_3d<T>(v: &T) -> f32
where
    T: VectorSpace<Scalar = f32>,
    f32: Mul<T, Output = T>,
{
    let mul = <f32 as Mul<f32>>::mul;
    f32::sqrt(mul(v[0], v[0]) + mul(v[1], v[1]) + mul(v[2], v[2]))
}

pub fn cross<T>(a: &T, b: &T) -> T
where
    T: VectorSpace<Scalar = f32>,
{
    let mul = <f32 as Mul<f32>>::mul;

    let mut res: T = a.clone() * 0.0;
    res[0] = mul(a[1], b[2]) - mul(a[2], b[1]);
    res[1] = mul(a[2], b[0]) - mul(a[0], b[2]);
    res[2] = mul(a[0], b[1]) - mul(a[1], b[0]);

    res
}
