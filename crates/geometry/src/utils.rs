use std::collections::{HashMap, HashSet};

use algebra::*;

/// Returns a new set of points from a list of indices and remaps the indices.
/// Useful if, for example, there are points with no edge connections and one
/// wants the main meshes without isolated points. The permutation map of old to
/// new vertices is also returned.
pub fn compress_sparse_triangle_mesh(
    points: &Vec<Vec3>,
    indices: &Vec<usize>,
) -> (Vec<Vec3>, Vec<usize>, HashMap<usize, usize>)
{
    let mut new_points = Vec::new();
    let valid_points: HashSet<usize> = HashSet::from_iter(indices.iter().copied());
    let mut new_id_map: HashMap<usize, usize> = HashMap::new();
    let mut old_id_map: HashMap<usize, usize> = HashMap::new();
    for (id, point) in points.iter().enumerate()
    {
        if valid_points.contains(&id)
        {
            new_id_map.insert(id, new_points.len());
            old_id_map.insert(new_points.len(), id);
            new_points.push(*point);
        }
    }

    let mut new_indices = Vec::new();
    for index in indices
    {
        new_indices.push(*new_id_map.get(index).unwrap());
    }

    (new_points, new_indices, old_id_map)
}
