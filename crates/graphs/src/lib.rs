pub mod breadth_first_search;
pub mod depth_first_search;

use std::{
    collections::{BTreeMap, BTreeSet},
    fmt::Debug,
};

pub fn find_root_of_tree<T>(graph: &Vec<Vec<T>>) -> usize {
    graph
        .iter()
        .enumerate()
        .find(|(_i, l)| l.len() == 1)
        .unwrap()
        .0
}

pub fn line_soup_to_graph<T>(list: &Vec<[T; 2]>) -> Vec<Vec<T>>
where
    T: Ord + Clone,
{
    let mut map = BTreeMap::new();
    for [n1, n2] in list.into_iter() {
        map.entry(n1.clone()).or_insert(Vec::new()).push(n2.clone());
        map.entry(n2.clone()).or_insert(Vec::new()).push(n1.clone());
    }

    let mut graph = Vec::new();
    for (_, neighbours) in map.into_iter() {
        graph.push(neighbours)
    }

    graph
}

fn convert_to_usize<T>(value: &T) -> usize
where
    T: TryInto<usize> + Clone,
{
    value.clone().try_into().unwrap_or_else(|_| {
        panic!("Conversion to usize failed.");
    })
}

pub fn skeleton_hierarchy_to_graph<T>(skeleton: &Vec<T>) -> Vec<Vec<T>>
where
    T: Ord + Clone + TryInto<usize> + Debug + Eq + TryFrom<usize>,
{
    let mut segments = Vec::new();
    for i in 0..skeleton.len() {
        let val: T = T::try_from(i).unwrap_or_else(|_| panic!());
        segments.push([val, skeleton[i].clone()]);
    }

    line_soup_to_graph(&segments)
}

/// Take a graph with many polyline subgraphs and construct
/// a new graph that treats polyline subgraphs as edges.
/// The first returned value is the grouped sets of polylines making
/// the edges of the compacted graph.
/// The second returned value is the compacted graph in adjacency list representation.
/// The third returned value is a map between edge sof the original graph and the chain
/// they belong to.
pub fn compact_line_graph<T>(
    root: T,
    list: &Vec<Vec<T>>,
    // Chains, sparse graph, edge-chain map
) -> (Vec<Vec<T>>, BTreeMap<T, Vec<T>>, BTreeMap<[T; 2], usize>)
where
    T: Ord + Clone + TryInto<usize> + Debug + Eq,
{
    let current = root.clone();

    let mut visited = vec![false; list.len()];
    let mut queue = vec![(current.clone(), vec![current])];

    let mut chains = Vec::new();
    while !queue.is_empty() {
        let (mut current, mut chain) = queue.pop().unwrap();
        let mut prior = chain[0].clone();
        if visited[convert_to_usize(&current)] {
            continue;
        }
        while list[convert_to_usize(&current)].len() <= 2 {
            let index = convert_to_usize(&current);
            visited[index] = true;

            // Make sure there's only one neighbour next in the chain.
            let n_count = list[index].iter().filter(|i| **i != prior).count();
            debug_assert!(n_count <= 1, "{:?}", list[index]);

            // We ran into a leaf node.
            if n_count == 0 {
                break;
            }

            let next = list[index].iter().find(|i| **i != prior).unwrap();

            chain.push(next.clone());
            prior = current;
            current = next.clone();
        }
        chains.push(chain);

        let c_index = convert_to_usize(&current);
        visited[c_index] = true;
        for n in list[c_index]
            .iter()
            .filter(|i| !visited[convert_to_usize(*i)])
        {
            queue.push((n.clone(), vec![current.clone(), n.clone()]));
        }
    }

    let mut sparse_graph = BTreeMap::new();
    let mut edge_chain_map = BTreeMap::new();
    for (i, chain) in chains.iter().enumerate() {
        let neighbours1 = sparse_graph
            .entry(chain[0].clone())
            .or_insert(BTreeSet::new());
        neighbours1.insert(chain.last().unwrap().clone());

        let neighbours2 = sparse_graph
            .entry(chain.last().unwrap().clone())
            .or_insert(BTreeSet::new());
        neighbours2.insert(chain[0].clone());

        let mut key = [chain[0].clone(), chain.last().unwrap().clone()];
        key.sort();

        edge_chain_map.insert(key, i);
    }

    let sparse_graph = sparse_graph
        .iter()
        .map(|(key, set)| (key.clone(), set.into_iter().map(|n| n.clone()).collect()))
        .collect();

    (chains, sparse_graph, edge_chain_map)
}

/// Apply a permutation to a list.
/// The permutation list is assumed to be ordered as (source, target) so
/// for example:
///
/// [a, b, c], [1, 0, 2] -> [b, a, c]
/// [a, b, c, d], [3, 1, 2, 0] -> [d, b, c, a]
///
/// Beware that online resources usually use the opposite ordering (target,
/// source) for permutations.
// TODO(low): This can be done in place in O(1) space and O(n) steps.
pub fn permute<T>(list: &mut Vec<T>, permutation: Vec<usize>)
where
    T: Clone + Copy,
{
    let mut result = list.clone();

    for i in 0..permutation.len() {
        result[i] = list[permutation[i]];
    }

    *list = result
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests {
    use std::collections::BTreeMap;

    use crate::compact_line_graph;

    #[test]
    fn test_compact_line_graph() {
        let list = vec![
            vec![1],          // 0
            vec![0, 2],       // 1
            vec![1, 3],       // 2
            vec![2, 4],       // 3
            vec![3, 5, 7, 6], // 4
            vec![4, 10],      // 5
            vec![4, 8],       // 6
            vec![4, 9],       // 7
            vec![6, 13],      // 8
            vec![12, 7],      // 9
            vec![5, 11],      // 10
            vec![10, 12],     // 11
            vec![11, 9],      // 12
            vec![8],          // 13
        ];
        let (chains, sparse_graph, edge_chain_map) = compact_line_graph(0, &list);
        assert!(chains.len() == 3);
        assert!(sparse_graph.len() == 3);
        assert!(edge_chain_map.len() == 3);

        let expected_chains = [
            vec![0, 1, 2, 3, 4],
            vec![4, 6, 8, 13],
            vec![4, 7, 9, 12, 11, 10, 5, 4],
        ];
        assert_eq!(chains, expected_chains);

        let expected_sparse_graph: BTreeMap<i32, Vec<i32>> =
            [(0, vec![4]), (4, vec![0, 4, 13]), (13, vec![4])]
                .iter()
                .cloned()
                .collect();
        assert_eq!(sparse_graph, expected_sparse_graph);

        let expected_edge_chain_map: BTreeMap<[i32; 2], usize> =
            [([0, 4], 0), ([4, 13], 1), ([4, 4], 2)]
                .iter()
                .cloned()
                .collect();
        assert_eq!(edge_chain_map, expected_edge_chain_map);
    }
}
