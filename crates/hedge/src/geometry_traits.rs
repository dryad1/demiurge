use std::ops::Mul;

use algebra::{na, Vec2, Vec3};
use linear_isomorphic::*;

pub trait FaceDataGetters
{
    type Normal;
    type Uv;
    fn normal_count(&self) -> usize;
    fn uv_count(&self) -> usize;
    fn normal(&self, id: usize) -> Self::Normal;
    fn uv(&self, id: usize) -> Self::Uv;
}

impl FaceDataGetters for ()
{
    type Normal = Vec3;
    type Uv = Vec2;

    fn normal_count(&self) -> usize { 0 }

    fn normal(&self, _id: usize) -> Self::Normal { Vec3::zeros() }

    fn uv_count(&self) -> usize { 0 }

    fn uv(&self, _id: usize) -> Self::Uv { Vec2::zeros() }
}
/// Since it is more common to have just normals than just uvs, this will
/// assume that the vector stores normal information.
impl<V> FaceDataGetters for Vec<V>
where
    V: VectorSpace + Default,
{
    type Normal = V;
    type Uv = Vec2;

    fn normal_count(&self) -> usize { self.len() }

    fn normal(&self, id: usize) -> Self::Normal { self[id].clone() }

    fn uv_count(&self) -> usize { 0 }

    fn uv(&self, _id: usize) -> Self::Uv { Vec2::zeros() }
}

pub trait VertDataGetters
{
    type Scalar: RealField;
    type V2: VectorSpace<Scalar = Self::Scalar>;
    type V3: VectorSpace<Scalar = Self::Scalar>;
    fn position(&self) -> Self::V3;
    fn normal(&self) -> Self::V3;
    fn uv(&self) -> Self::V2;
}

impl<T> VertDataGetters for na::Vector3<T>
where
    T: RealField,
    T: Mul<na::Vector3<T>, Output = na::Vector3<T>>,
    T: Mul<na::Vector2<T>, Output = na::Vector2<T>>,
{
    type Scalar = T;
    type V2 = na::Vector2<T>;
    type V3 = na::Vector3<T>;

    fn position(&self) -> Self::V3 { *self }

    fn normal(&self) -> Self::V3 { Self::zeros() }

    fn uv(&self) -> Self::V2 { Self::V2::zeros() }
}
