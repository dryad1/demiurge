use crate::mesh::*;

/// Find the hedge ids of all boundaries of the mesh.
///
/// Returns A list of lists of hedge ids, each inner list corresponds to the
/// list of half edge ids in one boundary.
pub fn find_boundaries<V, E, F>(mesh: &HalfMesh<V, E, F>) -> Vec<Vec<GeomId<1>>>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    let mut visited = vec![false; mesh.hedge_count()];
    let mut boundaries = Vec::new();
    for hedge_handle in mesh.iter_edges()
    {
        if visited[hedge_handle.id.0 as usize]
            || visited[hedge_handle.pair().id.0 as usize]
        {
            visited[hedge_handle.id.0 as usize] = true;
            visited[hedge_handle.pair().id.0 as usize as usize] = true;
            continue;
        }

        visited[hedge_handle.id().0 as usize] = true;
        visited[hedge_handle.pair().id.0 as usize] = true;

        if hedge_handle.is_in_boundary_edge()
        {
            let hedge = hedge_handle.get_boundary_hedge().unwrap();
            let boundary: Vec<_> = mesh
                .hedge_handle(hedge.id)
                .iter_loop()
                .map(|x| x.id)
                .collect();
            for id in &boundary
            {
                visited[id.0 as usize] = true;
            }

            boundaries.push(boundary);
        }
    }

    boundaries
}
