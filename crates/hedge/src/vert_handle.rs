use std::fmt::Debug;

use graphs::breadth_first_search::BFSIterator;

use crate::facets::*;
use crate::hedge_handle::*;
use crate::mesh::*;

/// As a best practice don't store this handle, prefer using its id as  returned
/// by the `id()` method.
#[derive(Clone, Copy)]
pub struct VertHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) id: GeomId<0>,
}

impl<V, E, F> VertHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn data(&self) -> V::PrimitiveData {
        unsafe { (*self.mesh).verts.borrow().get(self.id.0).clone() }
    }

    pub fn mutate_data(&self, data: &V::PrimitiveData) {
        unsafe { (*self.mesh).verts.borrow_mut().set(self.id.0, data.clone()) }
    }

    pub fn id(&self) -> GeomId<0> {
        self.id
    }

    pub fn disable(&self) {
        unsafe {
            (*self.mesh).verts_meta.borrow_mut()[self.id.0 as usize].enabled = false;
        }
    }

    pub fn is_disabled(&self) -> bool {
        unsafe {
            (*self.mesh).verts_meta.borrow_mut()[self.id.0 as usize].enabled == false
        }
    }

    pub fn hedge(&self) -> Option<HEdgeHandle<V, E, F>> {
        unsafe {
            let vert = &(*self.mesh).verts_meta.borrow()[self.id.0 as usize];
            if vert.hedge_id == ABSENT() {
                return None;
            }

            return Some((*self.mesh).hedge_handle(vert.hedge_id));
        }
    }

    /// Return the number of edges having this vertex as an endpoint.
    pub fn valence(&self) -> usize {
        let mut count = 0;
        for _ in self.iter_verts() {
            count += 1;
        }
        count
    }

    /// If this vertex is at the boundary, get a hedge in the boundary.
    pub fn get_boundary_hedge(&self) -> Option<HEdgeHandle<V, E, F>> {
        unsafe {
            if (*self.mesh).vert_handle(self.id).hedge().is_none() {
                return None;
            }

            for hedge in (*self.mesh).vert_handle(self.id).iter_hedges() {
                if hedge.is_boundary_hedge() {
                    return Some(HEdgeHandle {
                        mesh: self.mesh,
                        id: hedge.id(),
                    });
                }
            }
        }

        None
    }

    pub fn is_in_boundary(&self) -> bool {
        unsafe {
            for hedge in (*self.mesh).vert_handle(self.id).iter_hedges() {
                if hedge.is_boundary_hedge() {
                    return true;
                }
            }
        }

        return false;
    }

    /// Returns an iterator over the edges connected to the current vertex.
    pub fn iter_hedges(&self) -> VertEdgeStarIter<V, E, F>
    where
        V: Clone + Debug,
        E: Clone + Debug,
        F: Clone + Debug,
    {
        let hedge_id =
            unsafe { (*self.mesh).verts_meta.borrow_mut()[self.id.0 as usize].hedge_id };
        let handle = if hedge_id == ABSENT() {
            None
        } else {
            unsafe { Some((*self.mesh).hedge_handle(hedge_id)) }
        };

        VertEdgeStarIter {
            current_edge: handle,
            start_id: ABSENT(),
        }
    }

    /// Iterate over all edges connected to this vertex starting at the
    /// specified one. The input MUST have the active vertex  as its source.
    pub fn iter_hedges_starting_at(
        &self,
        hedge: HEdgeHandle<V, E, F>,
    ) -> VertEdgeStarIter<V, E, F>
    where
        V: Clone + Debug,
        E: Clone + Debug,
        F: Clone + Debug,
    {
        assert!(self.id() == hedge.source().id());
        VertEdgeStarIter {
            current_edge: Some(hedge),
            start_id: ABSENT(),
        }
    }

    /// Returns an iterator over the vertices sharing an edge with the current
    /// vertex.
    pub fn iter_verts(&self) -> VertRingIter<V, E, F>
    where
        V: Clone + Debug,
        E: Clone + Debug,
        F: Clone + Debug,
    {
        VertRingIter {
            star_iter: self.iter_hedges(),
        }
    }

    pub fn iter_concentric_rings<'a>(
        &'a self,
        max_depth: usize,
    ) -> impl Iterator<Item = VertHandle<V, E, F>> + 'a
    where
        V: Clone + Debug,
        E: Clone + Debug,
        F: Clone + Debug,
    {
        BFSIterator::new(self.id(), move |&id, depth, _| {
            let v = unsafe { (*self.mesh).vert_handle(id) };

            if depth < max_depth {
                VertRingIdIter {
                    star_ring: v.iter_verts(),
                }
            } else {
                // TODO(low): figure out a safer way to accomplish this.
                VertRingIdIter {
                    star_ring: VertRingIter {
                        star_iter: VertEdgeStarIter {
                            current_edge: Some(HEdgeHandle {
                                mesh: std::ptr::null::<HalfMesh<V, E, F>>() as *mut _,
                                id: ABSENT(),
                            }),
                            start_id: ABSENT(),
                        },
                    },
                }
            }
        })
        .map(|(vid, _, _)| unsafe { (*self.mesh).vert_handle(vid) })
    }

    /// Tests if this vertex is connected to another vertex.
    pub fn is_connected_to(&self, other: GeomId<0>) -> bool {
        for neighbour in self.iter_verts() {
            if neighbour.id() == other {
                return true;
            }
        }

        false
    }

    /// If it exists, returns the hedge starting at this vertex connecting it
    /// with the input.
    pub fn shared_hedge(&self, other: GeomId<0>) -> Option<HEdgeHandle<V, E, F>> {
        for hedge in self.iter_hedges() {
            if hedge.dest().id() == other {
                return Some(hedge);
            }
        }

        None
    }
}

/// Iterates over the immediate neighbours of a vertex by id.
pub struct VertRingIdIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) star_ring: VertRingIter<V, E, F>,
}

impl<V, E, F> Iterator for VertRingIdIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = GeomId<0>;

    fn next(&mut self) -> Option<Self::Item> {
        let edge = self.star_ring.next();
        match edge {
            None => None,
            Some(handle) => Some(handle.id()),
        }
    }
}

/// Iterates over the incident edges of a vertex.
pub struct VertEdgeStarIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) current_edge: Option<HEdgeHandle<V, E, F>>,
    pub(crate) start_id: GeomId<1>,
}

impl<V, E, F> Iterator for VertEdgeStarIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = HEdgeHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current_edge.is_none() {
            return None;
        }

        let current_edge = self.current_edge.clone().unwrap();
        if current_edge.id == self.start_id {
            return None;
        }

        // Assert that all edges we are iterating over are valid.
        debug_assert!(unsafe {
            (*current_edge.mesh).hedges_meta.borrow()[current_edge.id.0 as usize].enabled
        });

        let val = Some(current_edge.clone());
        if self.start_id == ABSENT() {
            self.start_id = current_edge.id;
        }

        debug_assert!(unsafe {
            (*current_edge.mesh).hedges_meta.borrow()[current_edge.pair().id.0 as usize]
                .enabled
        });
        self.current_edge = Some(current_edge.pair().next());

        val
    }
}

/// Iterates over the immediate neighbours of a vertex.
pub struct VertRingIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) star_iter: VertEdgeStarIter<V, E, F>,
}

impl<V, E, F> Iterator for VertRingIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = VertHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item> {
        let edge = self.star_iter.next();
        match edge {
            None => None,
            Some(handle) => Some(handle.dest()),
        }
    }
}

#[derive(Clone)]
pub struct FullVertIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub(crate) mesh: *mut HalfMesh<V, E, F>,
    pub(crate) current_id: GeomId<0>,
    pub(crate) end_id: GeomId<0>,
}

impl<V, E, F> Iterator for FullVertIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    type Item = VertHandle<V, E, F>;

    fn next(&mut self) -> Option<Self::Item> {
        let val = if self.current_id.0 < self.end_id.0 {
            unsafe { Some((*self.mesh).vert_handle(self.current_id)) }
        } else {
            None
        };

        self.current_id.0 += 1;

        val
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        debug_assert!(n < self.end_id.0 as usize);
        self.current_id = GeomId::from(n);
        unsafe { Some((*self.mesh).vert_handle(self.current_id)) }
    }
}

impl<V, E, F> ExactSizeIterator for FullVertIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    fn len(&self) -> usize {
        unsafe { (*self.mesh).vert_count() }
    }
}

impl<V, E, F> FullVertIter<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
{
    pub fn skip_to(&mut self, v_id: GeomId<0>) -> FullVertIter<V, E, F> {
        self.current_id = v_id;
        self.clone()
    }
}

use core::ops::Neg;
use linear_isomorphic::InnerSpace;
use linear_isomorphic::RealField;
use linear_isomorphic::VectorSpace;

pub trait VertMetrics<V, N, S>
where
    V: Clone + Debug,
    S: RealField,
{
    fn normal(&self) -> N;
}

impl<V, E, F, S> VertMetrics<V, V::PrimitiveData, S> for VertHandle<V, E, F>
where
    V: PrimitiveContainer,
    E: PrimitiveContainer,
    F: PrimitiveContainer,
    V::PrimitiveData: InnerSpace<S> + Neg<Output = V::PrimitiveData>,
    S: RealField,
{
    fn normal(&self) -> V::PrimitiveData {
        let mut normal = V::PrimitiveData::default();
        let mut total_weight = S::from(0.0).unwrap();
        for h in self.iter_hedges() {
            let d1 = (h.dest().data().clone() - h.source().data().clone()).normalized();
            let d2 = (h.prev().pair().dest().data().clone()
                - h.prev().pair().source().data().clone())
            .normalized();

            let n = d1.cross(&d2);

            let w = (d1.dot(&d2)).acos();

            normal += n * w;
            total_weight += w;
        }

        (normal * (S::from(1.).unwrap() / total_weight)).normalized()
    }
}
