// Theory: http://eastfarthing.com/blog/2015-04-21-noise/
fn fract(v: f32) -> f32 { v - v.floor() }

// Change for PCG in this:
// https://jcgt.org/published/0009/03/02/
fn hash<const N: usize>(input: &[i32; N], seed: i32) -> [f32; N]
{
    let mut result = [0.0; N];
    let mut pattern = 12289;
    for n in 0..N
    {
        for i in input
        {
            pattern = pattern ^ (i << (13 * n));
            pattern = (pattern ^ seed) >> n;
        }

        pattern = pattern ^ seed;

        result[n] = (157431.243435 * ((pattern - n as i32) as f32).sin()).sin();
    }

    result
}

fn fade(t: f32) -> f32
{
    let t = t.abs();
    return 1.0 - t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
}

pub fn perlin<const N: usize>(input: &[f32; N], seed: i32) -> f32
{
    let mut fract_parts = [input[0]; N];
    let mut int_parts = [0; N];

    // Extract integer and decimal portions of the coordinates.
    for i in 0..N
    {
        fract_parts[i] = fract(input[i]);
        int_parts[i] = (input[i] - fract_parts[i]).round() as i32;
    }

    let corner_total = 1 << N;

    // Generate a random vector at each corner of the cell.
    let mut result = 0.0;
    for corner in 0..corner_total
    {
        // For each corner, initialize all components of it to a random number.
        let mut corner_coords = int_parts;
        let mut relative_coords = *input;
        for bit_index in 0..N
        {
            // Extract the N bit of the binary sequence.
            corner_coords[bit_index] += ((corner >> (N - 1 - bit_index)) & 1) as i32;
            relative_coords[bit_index] -= corner_coords[bit_index] as f32;
        }

        let corner_dir = hash(&corner_coords, seed);
        let val = relative_coords.iter().fold(1.0, |acc, x| acc * fade(*x));

        // Dot product between the corner vector and the local input coords.
        let dot: f32 = relative_coords
            .iter()
            .zip(corner_dir.iter())
            .map(|(x, y)| x * y)
            .sum();
        result += val * dot;
    }

    return result;
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use image::{ImageBuffer, Rgba, RgbaImage};
    use iterators::IterDimensions;

    use crate::*;

    #[test]
    fn test_2d_perlin_noise()
    {
        let resolution = 512;
        let mut img: RgbaImage = ImageBuffer::new(resolution as u32, resolution as u32);

        let function_sampling = |x: f32, y: f32| {
            let mut value = 0.0;

            let persistence = 0.5;
            let mut amplitude = 1.0;
            let lacunarity = 2.0;
            let mut frequency = 1.0;
            for _ in 0..4
            {
                value += amplitude * perlin(&[x * frequency, y * frequency], 1);
                amplitude *= persistence;
                frequency *= lacunarity;
            }

            value
        };

        for [i, j] in IterDimensions::new([resolution, resolution])
        {
            let value = function_sampling(i as f32 / 100.0, j as f32 / 100.0);
            let pixel = (((value + 1.0) / 2.0) * 255.0) as u8;
            img.put_pixel(i as u32, j as u32, Rgba::<u8>([pixel, pixel, pixel, 255]));
        }

        img.save("perlin_test.png").unwrap();
    }
}
