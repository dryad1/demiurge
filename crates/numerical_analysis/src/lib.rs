pub use crate::fast_sweeping::fast_sweeping;

pub mod differential_operators;
pub mod fast_sweeping;
pub mod multilinear;
pub mod sampler;

pub fn factorial(n: usize) -> usize
{
    match n
    {
        0 => 1,
        1 => 1,
        2 => 2,
        3 => 6,
        4 => 24,
        5 => 120,
        6 => 720,
        7 => 5_040,
        8 => 40_320,
        9 => 362_880,
        10 => 3_628_800,
        11 => 39_916_800,
        12 => 479_001_600,
        13 => 6_227_020_800,
        14 => 87_178_291_200,
        15 => 1_307_674_368_00,
        16 => 20_922_789_888_000,
        17 => 355_687_428_096_000,
        18 => 6_402_373_705_728_000,
        19 => 121_645_100_408_832_000,
        20 => 2_432_902_008_176_640_000,
        _ => panic!("Integer {}! is too large for 64 bit representations.", n),
    }
}
