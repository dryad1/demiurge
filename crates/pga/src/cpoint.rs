use std::ops::{BitAnd, BitXor};

use algebra::*;
use derive_more::{Add, AddAssign, Div, Index, Mul, Neg, Sub};

use crate::*;

#[derive(
    Default, Debug, Clone, Copy, PartialEq, Mul, Add, Sub, Div, Index, AddAssign, Neg,
)]
pub struct CPoint(pub Vec3);

impl CPoint
{
    pub fn new(x: f32, y: f32, z: f32) -> Self { Self(Vec3::new(x, y, z)) }

    pub fn zero() -> Self { Self(Vec3::new(0.0, 0.0, 0.0)) }

    pub fn from_vector(v: &Vec3) -> Self { Self(Vec3::new(v.x, v.y, v.z)) }

    #[inline]
    pub const fn e123(&self) -> f32 { 1.0 }

    pub fn to_vec(&self) -> Vec3 { self.0 }

    pub fn dual(self: Self) -> MultiVec3D
    {
        let mut res = MultiVec3D::zero();
        let a = self;
        *res.e0_mut() = 1.0;
        *res.e1_mut() = a.e032();
        *res.e2_mut() = a.e013();
        *res.e3_mut() = a.e021();
        res
    }
}

impl CPointLike for CPoint
{
    fn e032(&self) -> f32 { self[0] }

    fn e013(&self) -> f32 { self[1] }

    fn e021(&self) -> f32 { self[2] }

    fn e032_mut(&mut self) -> &mut f32 { &mut self.0[0] }

    fn e013_mut(&mut self) -> &mut f32 { &mut self.0[1] }

    fn e021_mut(&mut self) -> &mut f32 { &mut self.0[2] }
}

// smul
// scalar/multivector multiplication
impl Mul<CPoint> for f32
{
    type Output = CPoint;

    fn mul(self: f32, b: CPoint) -> CPoint
    {
        let mut res = CPoint::zero();
        let a = self;
        *res.e021_mut() = a * b.e021();
        *res.e013_mut() = a * b.e013();
        *res.e032_mut() = a * b.e032();
        res
    }
}

// Vee
// The regressive product. (JOIN)
impl BitAnd for CPoint
{
    type Output = MultiVec3D;

    fn bitand(self: CPoint, o: CPoint) -> MultiVec3D
    {
        let mut a = MultiVec3D::zero();
        let mut b = MultiVec3D::zero();
        *a.e021_mut() = self.e021();
        *a.e013_mut() = self.e013();
        *a.e032_mut() = self.e032();
        *a.e123_mut() = 1.0;

        *b.e021_mut() = o.e021();
        *b.e013_mut() = o.e013();
        *b.e032_mut() = o.e032();
        *b.e123_mut() = 1.0;

        (a.dual() ^ b.dual()).dual()
    }
}

// Vee
// The regressive product. (JOIN)
impl BitAnd<MultiVec3D> for CPoint
{
    type Output = MultiVec3D;

    fn bitand(self: CPoint, b: MultiVec3D) -> MultiVec3D
    {
        (self.dual() ^ b.dual()).dual()
    }
}

// Wedge
// The outer product. (MEET)
impl BitXor for CPoint
{
    type Output = MultiVec3D;

    fn bitxor(self: CPoint, o: CPoint) -> MultiVec3D
    {
        let mut a = MultiVec3D::zero();
        *a.e021_mut() = self.e021();
        *a.e013_mut() = self.e013();
        *a.e032_mut() = self.e032();
        *a.e123_mut() = 1.0;

        let mut b = MultiVec3D::zero();
        *b.e021_mut() = o.e021();
        *b.e013_mut() = o.e013();
        *b.e032_mut() = o.e032();
        *b.e123_mut() = 1.0;

        a ^ b
    }
}

// Wedge
// The outer product. (MEET)
impl BitXor<MultiVec3D> for CPoint
{
    type Output = MultiVec3D;

    fn bitxor(self: CPoint, b: MultiVec3D) -> MultiVec3D
    {
        let mut a = MultiVec3D::zero();
        *a.e021_mut() = self.e021();
        *a.e013_mut() = self.e013();
        *a.e032_mut() = self.e032();
        *a.e123_mut() = 1.0;

        a ^ b
    }
}

#[derive(Default, Debug, Clone, Copy, PartialEq, Mul, Add, Sub, Div, Index)]
pub struct Point(pub Vec4);

impl Point
{
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Self { Self(Vec4::new(x, y, z, w)) }

    pub fn from_vec_non_homogenous(v: &Vec3) -> Self
    {
        Self(Vec4::new(v.x, v.y, v.z, 0.0))
    }

    pub fn to_vec(&self) -> Vec4 { self.0 }

    pub fn e032(&self) -> f32 { self[0] }

    pub fn e013(&self) -> f32 { self[1] }

    pub fn e021(&self) -> f32 { self[2] }

    pub fn e123(&self) -> f32 { self[3] }

    pub fn e032_mut(&mut self) -> &mut f32 { &mut self.0[0] }

    pub fn e013_mut(&mut self) -> &mut f32 { &mut self.0[1] }

    pub fn e021_mut(&mut self) -> &mut f32 { &mut self.0[2] }

    pub fn e123_mut(&mut self) -> &mut f32 { &mut self.0[3] }
}
