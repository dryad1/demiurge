use std::ops::{Index, IndexMut, Neg};

use algebra::*;

use crate::*;

#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct Rotor
{
    pub coeffs: [f32; 4],
}

impl Index<usize> for Rotor
{
    type Output = f32;

    fn index<'a>(&'a self, index: usize) -> &'a Self::Output { &self.coeffs[index] }
}

impl IndexMut<usize> for Rotor
{
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut Self::Output
    {
        &mut self.coeffs[index]
    }
}

impl Neg for Rotor
{
    type Output = Rotor;

    fn neg(self) -> Self::Output
    {
        let mut res = Rotor::zero();
        let b = self;
        *res.s_mut() = -b.s();
        *res.e12_mut() = -b.e12();
        *res.e31_mut() = -b.e31();
        *res.e23_mut() = -b.e23();
        res
    }
}

impl Mul<f32> for Rotor
{
    type Output = Rotor;

    fn mul(self: Rotor, b: f32) -> Rotor
    {
        let mut res = Rotor::zero();
        let a = self;
        *res.s_mut() = a.s() * b;
        *res.e12_mut() = a.e12() * b;
        *res.e31_mut() = a.e31() * b;
        *res.e23_mut() = a.e23() * b;

        res
    }
}

impl Mul for Rotor
{
    type Output = Rotor;

    fn mul(self, b: Self) -> Self::Output
    {
        let mut ret = Self::default();
        let a = self;

        *ret.s_mut() =
            b.s() * a.s() - b.e12() * a.e12() - b.e31() * a.e31() - b.e23() * a.e23();
        *ret.e12_mut() =
            b.e12() * a.s() + b.s() * a.e12() + b.e23() * a.e31() - b.e31() * a.e23();
        *ret.e31_mut() =
            b.e31() * a.s() - b.e23() * a.e12() + b.s() * a.e31() + b.e12() * a.e23();
        *ret.e23_mut() =
            b.e23() * a.s() + b.e31() * a.e12() - b.e12() * a.e31() + b.s() * a.e23();

        ret
    }
}

impl Rotor
{
    pub fn from_axis_angle(angle: f32, axis: &Vec3) -> Rotor
    {
        Rotor {
            coeffs: [
                f32::cos(angle / 2.0),
                -axis.z * f32::sin(angle / 2.0),
                -axis.y * f32::sin(angle / 2.0),
                -axis.x * f32::sin(angle / 2.0),
            ],
        }
    }

    pub fn zero() -> Rotor
    {
        Rotor {
            coeffs: [0.0, 0.0, 0.0, 0.0],
        }
    }

    pub fn identity() -> Rotor
    {
        Rotor {
            coeffs: [1.0, 0.0, 0.0, 0.0],
        }
    }

    pub fn sandwich<T>(&self, p: &T) -> T
    where
        T: CPointLike + Default,
    {
        let r = self;
        let mut ret = T::default();

        let loc_e123 =
            r.e23() * r.e23() + r.e31() * r.e31() + r.e12() * r.e12() + r.s() * r.s();

        *ret.e032_mut() = p.e032() * r.e23() * r.e23()
            - p.e032() * r.e31() * r.e31()
            - p.e032() * r.e12() * r.e12()
            + p.e032() * r.s() * r.s()
            + 2.0 * p.e013() * r.e23() * r.e31()
            + 2.0 * p.e013() * r.e12() * r.s()
            + 2.0 * p.e021() * r.e23() * r.e12()
            - 2.0 * p.e021() * r.e31() * r.s();
        *ret.e013_mut() = 2.0 * p.e032() * r.e23() * r.e31()
            - 2.0 * p.e032() * r.e12() * r.s()
            - p.e013() * r.e23() * r.e23()
            + p.e013() * r.e31() * r.e31()
            - p.e013() * r.e12() * r.e12()
            + p.e013() * r.s() * r.s()
            + 2.0 * p.e021() * r.e23() * r.s()
            + 2.0 * p.e021() * r.e31() * r.e12();
        *ret.e021_mut() = 2.0 * p.e032() * r.e23() * r.e12()
            + 2.0 * p.e032() * r.e31() * r.s()
            - 2.0 * p.e013() * r.e23() * r.s()
            + 2.0 * p.e013() * r.e31() * r.e12()
            - p.e021() * r.e23() * r.e23()
            - p.e021() * r.e31() * r.e31()
            + p.e021() * r.e12() * r.e12()
            + p.e021() * r.s() * r.s();

        *ret.e032_mut() /= loc_e123;
        *ret.e013_mut() /= loc_e123;
        *ret.e021_mut() /= loc_e123;

        ret
    }

    // https://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/index.htm
    pub fn slerp(qa: &Rotor, qb: &Rotor, t: f32) -> Rotor
    {
        let qb = if (*qa * qb.conjugate()).s() < 0.0
        {
            -*qb
        }
        else
        {
            *qb
        };

        // quaternion to return
        let mut qm = Rotor::identity();
        // Calculate angle between them.
        let cos_half_theta = qa.s() * qb.s()
            + qa.e12() * qb.e12()
            + qa.e31() * qb.e31()
            + qa.e23() * qb.e23();
        // if qa=qb or qa=-qb then theta = 0 and we can return qa
        if f32::abs(cos_half_theta) >= 1.0
        {
            *qm.s_mut() = qa.s();
            *qm.e12_mut() = qa.e12();
            *qm.e31_mut() = qa.e31();
            *qm.e23_mut() = qa.e23();
            return qm;
        }
        // Calculate temporary values.
        let half_theta = f32::acos(cos_half_theta);
        let sin_half_theta = f32::sqrt(1.0 - cos_half_theta * cos_half_theta);
        // if theta = 180 degrees then result is not fully defined
        // we could rotate around any axis normal to qa or qb
        if f32::abs(sin_half_theta) < 0.001
        // fabs is floating point absolute
        {
            *qm.s_mut() = qa.s() * 0.5 + qb.s() * 0.5;
            *qm.e12_mut() = qa.e12() * 0.5 + qb.e12() * 0.5;
            *qm.e31_mut() = qa.e31() * 0.5 + qb.e31() * 0.5;
            *qm.e23_mut() = qa.e23() * 0.5 + qb.e23() * 0.5;
            return qm;
        }
        let ratio_a = f32::sin((1.0 - t) * half_theta) / sin_half_theta;
        let ratio_b = f32::sin(t * half_theta) / sin_half_theta;
        //calculate Quaternion.
        *qm.s_mut() = qa.s() * ratio_a + qb.s() * ratio_b;
        *qm.e12_mut() = qa.e12() * ratio_a + qb.e12() * ratio_b;
        *qm.e31_mut() = qa.e31() * ratio_a + qb.e31() * ratio_b;
        *qm.e23_mut() = qa.e23() * ratio_a + qb.e23() * ratio_b;
        return qm;
    }

    pub fn conjugate(self: Self) -> Rotor
    {
        let mut res = Rotor::zero();
        let a = self;
        *res.s_mut() = a.s();
        *res.e12_mut() = -a.e12();
        *res.e31_mut() = -a.e31();
        *res.e23_mut() = -a.e23();
        res
    }

    pub fn s(&self) -> f32 { self[0] }

    pub fn e12(&self) -> f32 { self[1] }

    pub fn e31(&self) -> f32 { self[2] }

    pub fn e23(&self) -> f32 { self[3] }

    pub fn s_mut(&mut self) -> &mut f32 { &mut self[0] }

    pub fn e12_mut(&mut self) -> &mut f32 { &mut self[1] }

    pub fn e31_mut(&mut self) -> &mut f32 { &mut self[2] }

    pub fn e23_mut(&mut self) -> &mut f32 { &mut self[3] }
}
