use algebra::*;

pub trait CPointLike
{
    fn e032(&self) -> f32;
    fn e013(&self) -> f32;
    fn e021(&self) -> f32;

    fn e032_mut(&mut self) -> &mut f32;
    fn e013_mut(&mut self) -> &mut f32;
    fn e021_mut(&mut self) -> &mut f32;
}

impl CPointLike for Vec3
{
    fn e032(&self) -> f32 { self[0] }

    fn e013(&self) -> f32 { self[1] }

    fn e021(&self) -> f32 { self[2] }

    fn e032_mut(&mut self) -> &mut f32 { &mut self[0] }

    fn e013_mut(&mut self) -> &mut f32 { &mut self[1] }

    fn e021_mut(&mut self) -> &mut f32 { &mut self[2] }
}
