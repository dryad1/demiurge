use std::fmt;
use std::ops::{Add, Index, IndexMut, Mul, Neg};

use linear_isomorphic::VectorSpace;

use crate::{CPointLike, Motor, MultiVec3D, Rotor};

const BASIS: &'static [&'static str] = &[
    "1",   // Scalar
    "e01", // Bivectors / Lines
    "e02", //
    "e03", //
];

#[derive(Default, Debug, Clone, Copy, PartialEq)]
pub struct Translator
{
    pub coeffs: [f32; 4],
}

impl Index<usize> for Translator
{
    type Output = f32;

    fn index<'a>(&'a self, index: usize) -> &'a Self::Output { &self.coeffs[index] }
}

impl IndexMut<usize> for Translator
{
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut Self::Output
    {
        &mut self.coeffs[index]
    }
}

impl Neg for Translator
{
    type Output = Translator;

    fn neg(self) -> Self::Output
    {
        let mut res = Translator::zero();
        let b = self;
        *res.s_mut() = -b.s();
        *res.e01_mut() = -b.e01();
        *res.e02_mut() = -b.e02();
        *res.e01_mut() = -b.e01();
        res
    }
}

impl Mul<f32> for Translator
{
    type Output = Translator;

    fn mul(self: Translator, b: f32) -> Translator
    {
        let mut res = Translator::zero();
        let a = self;
        *res.s_mut() = a.s() * b;
        *res.e01_mut() = a.e01() * b;
        *res.e02_mut() = a.e02() * b;
        *res.e03_mut() = a.e03() * b;

        res
    }
}

impl Mul<Translator> for f32
{
    type Output = Translator;

    fn mul(self, b: Translator) -> Translator
    {
        let mut res = Translator::zero();
        let a = b;
        *res.s_mut() = a.s() * self;
        *res.e01_mut() = a.e01() * self;
        *res.e02_mut() = a.e02() * self;
        *res.e03_mut() = a.e03() * self;

        res
    }
}

impl Add for Translator
{
    type Output = Translator;

    fn add(self, b: Translator) -> Translator
    {
        let mut res = Translator::zero();
        let a = self;
        *res.s_mut() = a.s() + b.s();
        *res.e01_mut() = a.e01() + b.e01();
        *res.e02_mut() = a.e02() + b.e02();
        *res.e03_mut() = a.e03() + b.e03();

        res
    }
}

impl Mul<Translator> for Rotor
{
    type Output = Motor;

    fn mul(self, t: Translator) -> Self::Output
    {
        let mut res = Motor::default();
        let r = self;

        *res.s_mut() = t.s() * r.s();
        *res.e01_mut() = t.e01() * r.s() + t.e02() * r.e12() - t.e03() * r.e31();
        *res.e02_mut() = -t.e01() * r.e12() + t.e02() * r.s() + t.e03() * r.e23();
        *res.e03_mut() = t.e01() * r.e31() - t.e02() * r.e23() + t.e03() * r.s();
        *res.e12_mut() = r.e12();
        *res.e31_mut() = r.e31();
        *res.e23_mut() = r.e23();
        *res.e0123_mut() = t.e01() * r.e23() + t.e02() * r.e31() + t.e03() * r.e12();

        res
    }
}

impl Mul<Rotor> for Translator
{
    type Output = Motor;

    fn mul(self, r: Rotor) -> Self::Output
    {
        let mut res = Motor::default();
        let t = self;

        *res.s_mut() = t.s() * r.s();
        *res.e01_mut() = t.e01() * r.s() - r.e12() * t.e02() + t.e03() * r.e31();
        *res.e02_mut() = r.e12() * t.e01() + t.e02() * r.s() - t.e03() * r.e23();
        *res.e03_mut() = -t.e01() * r.e31() + t.e02() * r.e23() + t.e03() * r.s();
        *res.e12_mut() = r.e12() * t.s();
        *res.e31_mut() = r.e31() * t.s();
        *res.e23_mut() = r.e23() * t.s();
        *res.e0123_mut() = t.e01() * r.e23() + t.e02() * r.e31() + t.e03() * r.e12();

        res
    }
}

impl Mul for Translator
{
    type Output = Translator;

    fn mul(self, b: Self) -> Self::Output
    {
        let mut ret = Self::zero();

        *ret.s_mut() = 1.0;
        *ret.e01_mut() = self.e01() + b.e01();
        *ret.e02_mut() = self.e02() + b.e02();
        *ret.e03_mut() = self.e03() + b.e03();
        ret
    }
}


impl fmt::Display for Translator
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        let mut n = 0;
        let ret = self
            .coeffs
            .iter()
            .enumerate()
            .filter_map(|(i, &coeff)| {
                if coeff.abs() != 0.000000
                {
                    n = 1;
                    Some(format!(
                        "{}{}",
                        format!("{:.*}", 16, coeff)
                            .trim_end_matches('0')
                            .trim_end_matches('.'),
                        if i > 0 { BASIS[i] } else { "" }
                    ))
                }
                else
                {
                    None
                }
            })
            .collect::<Vec<String>>()
            .join(" + ");
        if n == 0
        {
            write!(f, "0")
        }
        else
        {
            write!(f, "{}", ret)
        }
    }
}


impl Translator
{
    pub fn zero() -> Translator
    {
        Translator {
            coeffs: [0.0, 0.0, 0.0, 0.0],
        }
    }

    pub fn identity() -> Translator
    {
        Translator {
            coeffs: [1.0, 0.0, 0.0, 0.0],
        }
    }

    pub fn sandwich<T>(&self, other: &T) -> T
    where
        T: CPointLike + VectorSpace<Scalar = f32>,
        f32: Mul<T, Output = T>,
    {
        let mut res = other.clone();
        res[0] += -self.e01() - self.e01();
        res[1] += -self.e02() - self.e02();
        res[2] += -self.e03() - self.e03();
        res
    }

    pub fn from_multivec(other: &MultiVec3D) -> Translator
    {
        Translator {
            coeffs: [other.s(), other.e01(), other.e02(), other.e03()],
        }
    }

    pub fn s(&self) -> f32 { self[0] }

    pub fn e01(&self) -> f32 { self[1] }

    pub fn e02(&self) -> f32 { self[2] }

    pub fn e03(&self) -> f32 { self[3] }

    pub fn s_mut(&mut self) -> &mut f32 { &mut self[0] }

    pub fn e01_mut(&mut self) -> &mut f32 { &mut self[1] }

    pub fn e02_mut(&mut self) -> &mut f32 { &mut self[2] }

    pub fn e03_mut(&mut self) -> &mut f32 { &mut self[3] }
}
