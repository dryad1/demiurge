#![no_std]
pub mod pga;
pub mod ray_tracing;
pub mod voxel_oct_tree;

use spirv_std::{
    arch::IndexUnchecked,
    memory::{Scope, Semantics},
};

pub trait ShaderIndexing<T>
{
    fn index_mut(&mut self, i: usize) -> &mut T;
    fn index(&self, i: usize) -> &T;
    fn val(&self, i: usize) -> T;
}

impl<T: Clone> ShaderIndexing<T> for &mut [T]
{
    fn index_mut(&mut self, i: usize) -> &mut T { unsafe { self.index_unchecked_mut(i) } }

    fn index(&self, i: usize) -> &T { unsafe { self.index_unchecked(i) } }

    fn val(&self, i: usize) -> T { unsafe { self.index_unchecked(i).clone() } }
}

impl<T: Clone, const N: usize> ShaderIndexing<T> for [T; N]
{
    fn index_mut(&mut self, i: usize) -> &mut T { unsafe { self.index_unchecked_mut(i) } }

    fn index(&self, i: usize) -> &T { unsafe { self.index_unchecked(i) } }

    fn val(&self, i: usize) -> T { unsafe { self.index_unchecked(i).clone() } }
}

pub fn atomic_inc_u32(ptr: &mut u32) -> u32
{
    let old = unsafe {
        spirv_std::arch::atomic_i_add::<
            _,
            { Scope::Invocation as u32 },
            { Semantics::UNIFORM_MEMORY.bits() },
        >(ptr, 1)
    };

    old
}
