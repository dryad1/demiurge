use spirv_std::{glam::*, image::*, num_traits::Float};

use crate::ShaderIndexing;

pub type Texture3D = Image!(3D, type=f32, sampled);
pub type SampledTexture3D = SampledImage<Texture3D>;
pub type Storage3D = Image!(3D, sampled = false, format = rgba32f);

#[cfg(feature = "rustgpu")]
use spirv_std::num_traits::Float;
use voctree::*;

/// Shader safe for loop
macro_rules! For {
    ($var:ident, $start:expr, $end:expr, $body:block) => {{
        let mut $var = $start;
        loop
        {
            if $var >= $end
            {
                break;
            }
            $body;
            $var += 1;
        }
    }};
}

#[repr(C, align(16))]
pub struct VoxelVolumeMeta
{
    pub box_corner: Vec4,
    pub box_extents: Vec4,
    pub voxel_size: f32,
    _p1: f32,
    _p2: f32,
    _p3: f32,
}

#[repr(C, align(16))]
#[derive(Clone, Copy)]
pub struct WindowData
{
    pub dimensions: Vec4,
    pub view_matrix: Mat4,
}

pub fn trace_svo<T: Clone>(
    cam_pos: Vec3,
    ray: Vec3,
    svo: &GpuVoctree<T>,
) -> (bool, u32, u32)
{
    let WalkResult {
        was_hit,
        hit_node,
        hit_corner,
        iter_count,
    } = unsafe {
        svo.walk_tree(
            move |box_query| {
                let collision = ray_box_intersection(
                    cam_pos,
                    ray,
                    &box_query.box_position,
                    &Vec3::splat(box_query.box_dimension),
                    f32::EPSILON * 10000.,
                );

                collision.is_finite()
            },
            direction_permutation(&ray),
            svo.meta_data.level_count,
        )
    };

    (was_hit, hit_node, iter_count)
}

pub fn trace_voxel_grid(
    cam_pos: Vec3,
    ray: Vec3,
    volume_meta_data: &VoxelVolumeMeta,
    volume_data: &Storage3D,
) -> (bool, Vec4)
{
    let collision = ray_box_intersection(
        cam_pos,
        ray,
        &volume_meta_data.box_corner.xyz(),
        &volume_meta_data.box_extents.xyz(),
        0.001,
    );

    if collision.is_infinite()
    {
        return (false, Vec4::new(0.2, 0.2, 0.2, 1.0));
    }

    let start = cam_pos + collision * ray * 1.0001;

    let coords = ((start - volume_meta_data.box_corner.xyz())
        / volume_meta_data.voxel_size)
        .floor();

    let index = IVec3::new(coords.x as i32, coords.y as i32, coords.z as i32);
    let box_corner = volume_meta_data.box_corner;

    let mut ray_iter = DDA::new(
        [start.x, start.y, start.z],
        [box_corner.x, box_corner.y, box_corner.z],
        [ray.x, ray.y, ray.z],
        [index.x as isize, index.y as isize, index.z as isize],
        volume_meta_data.voxel_size,
    );

    let mut iter_count = 0;
    loop
    {
        let (t, coords) = ray_iter.next();
        iter_count += 1;
        if iter_count > 1000
        {
            return (false, Vec4::ZERO);
        }

        let sample = volume_data.read(IVec3::new(
            coords.val(0) as i32,
            coords.val(1) as i32,
            coords.val(2) as i32,
        ));

        if sample.w > 0.
        {
            return (true, sample);
        }
    }

    (true, Vec4::default())
}

/// Returns the camera position followed by the ray direction for the input
/// pixel with screen coordinates `screen_position`.
pub fn create_ray_from_camera_data(
    screen_position: &Vec2,
    window_dimensions: &Vec2,
    view_matrix: &Mat4,
) -> (Vec3, Vec3)
{
    let display_width = window_dimensions.x;
    let display_height = window_dimensions.y;
    let aspect_ratio = display_width / display_height;

    let x = (screen_position.x * 2.0 / display_width) - 1.0;
    let y = (screen_position.y * 2.0 / display_height) - 1.0;

    let screen_ray = generate_ray(&Vec2::new(x, y), aspect_ratio, 45.);
    let ray = (view_matrix.inverse() * screen_ray.extend(0.)).xyz();
    let cam_pos = (view_matrix.inverse() * Vec4::new(0., 0., 0., 1.)).xyz();

    (cam_pos, ray)
}

pub fn generate_ray(screen_position: &Vec2, aspect_ratio: f32, fov: f32) -> Vec3
{
    let mut r = screen_position.extend(-1. / (fov / 2.).to_radians().tan());
    r.y /= aspect_ratio;

    return r.normalize();
}

pub fn point_in_box_bounds(
    corner: &Vec3,
    size_vec: &Vec3,
    position: &Vec3,
    epsilon: f32,
) -> bool
{
    let mut inside = true;
    // Put the position in the coordinate frame of the box.
    let pos = *position - *corner;

    let position = [pos.x, pos.y, pos.z];
    let size = [size_vec.x, size_vec.y, size_vec.z];
    // The point is inside only if all of its components are inside.

    For!(i, 0, 3, {
        let pos_coord = *position.index(i);
        let size_comp = *size.index(i);

        inside = inside && (pos_coord >= -epsilon);
        inside = inside && (pos_coord <= size_comp + epsilon);
    });

    return inside;
}

pub fn ray_box_intersection(
    origin: Vec3,
    dir: Vec3,
    corner0: &Vec3,
    extents: &Vec3,
    epsilon: f32,
) -> f32
{
    if point_in_box_bounds(corner0, extents, &origin, epsilon)
    {
        return 0.;
    }

    // Calculate opposite corner.
    let corner1 = *corner0 + *extents;

    // Set the ray plane intersections.
    let coeffs = [
        (corner0.x - origin.x) / (dir.x),
        (corner0.y - origin.y) / (dir.y),
        (corner0.z - origin.z) / (dir.z),
        (corner1.x - origin.x) / (dir.x),
        (corner1.y - origin.y) / (dir.y),
        (corner1.z - origin.z) / (dir.z),
    ];

    let mut t = f32::INFINITY;
    // Check for the smallest valid intersection distance.
    For!(i, 0, 6, {
        if *coeffs.index(i) >= epsilon
            && point_in_box_bounds(
                corner0,
                extents,
                &(origin + dir * *coeffs.index(i)),
                epsilon,
            )
        {
            t = coeffs.index(i).min(t);
        }
    });

    t
}

/// WARNING: This class is designed to not terminate when iterating on purpose.
/// Check that the returned values are within valid bounds for your problem.
#[derive(Debug)]
pub struct DDA<const N: usize>
{
    index: [isize; N],
    index_updates: [isize; N],
    step_size: [f32; N],
    cumulative_distances: [f32; N],
    distance: f32,
}

impl<const N: usize> DDA<N>
{
    pub fn new(
        position: [f32; N],
        box_corner_position: [f32; N],
        direction: [f32; N],
        index: [isize; N],
        cell_width: f32,
    ) -> Self
    {
        // Initialize the step size for each dimension from the direction vector.
        // Moving by `direction[i] * step_size[i]` is guaranteed to move the point by
        // exactly one cell width.
        let mut step_size = [0.0f32; N];
        For!(i, 0, N, {
            *step_size.index_mut(i) = cell_width / direction.index(i).abs()
        });

        // Start axial distances by computing the first intersection point for
        // each dimension.
        let mut cumulative_parameter = [0.0; N];
        For!(i, 0, N, {
            let centered_position = *position.index(i) - *box_corner_position.index(i);
            let cell_relative_position =
                centered_position - *index.index(i) as f32 * cell_width;

            *cumulative_parameter.index_mut(i) = if *direction.index(i) < 0.0
            {
                (cell_relative_position / *direction.index(i)).abs()
            }
            else
            {
                (cell_width - cell_relative_position) / direction.index(i).abs()
            };
        });

        // For each direction, whether the cell index will increase or decrease when
        // moving along that direction.
        let mut index_updates = [isize::default(); N];
        For!(i, 0, N, {
            *index_updates.index_mut(i) = direction.val(i).signum() as isize;
        });

        let ret = DDA::<N> {
            distance: 0.0,
            step_size,
            index,
            cumulative_distances: cumulative_parameter,
            index_updates,
        };

        ret
    }

    fn next(&mut self) -> (f32, [isize; N])
    {
        let ret = (self.distance, self.index);

        let mut t_next = *self.cumulative_distances.index(0);
        let mut distance_index = 0;
        // Find the axial distance that would move the current point the least.
        // store the correpsonding index.
        For!(i, 0, N, {
            if *self.cumulative_distances.index(i) < t_next
                && !self.cumulative_distances.index(i).is_infinite()
            {
                distance_index = i;
                t_next = *self.cumulative_distances.index(i);
            }
        });

        // Update the corresponding index and move the point by a distance
        // matching the selected dimension.
        let index = self.index.val(distance_index);
        *self.index.index_mut(distance_index) =
            index + *self.index_updates.index(distance_index);

        let cum_dist = self.cumulative_distances.val(distance_index);
        *self.cumulative_distances.index_mut(distance_index) =
            cum_dist + *self.step_size.index(distance_index);
        self.distance = t_next;

        ret
    }
}
