#[cfg(not(feature = "rustgpu"))]
use core::cell::UnsafeCell;
#[cfg(not(feature = "rustgpu"))]
use std::sync::atomic::{AtomicU32, Ordering};

#[cfg(not(feature = "rustgpu"))]
use crate::*;

#[cfg(not(feature = "rustgpu"))]
#[derive(Debug)]
pub struct CpuVoctree<T>
where
    T: FeatureDependentTraits,
{
    pub meta_data: VoctreeMetadata,
    pub(crate) node_count: AtomicU32,
    pub(crate) nodes: UnsafeCell<Vec<Node<T>>>,
}

#[cfg(not(feature = "rustgpu"))]
impl<T> VoctreeLike<T> for CpuVoctree<T>
where
    T: FeatureDependentTraits,
{
    fn node_at_mut(&mut self, index: usize) -> &mut Node<T>
    {
        unsafe { &mut (*self.nodes.get())[index] }
    }

    fn node_at(&self, index: usize) -> &Node<T> { unsafe { &(*self.nodes.get())[index] } }

    fn corner_position(&self) -> Vec3 { self.meta_data.position }

    fn box_width(&self) -> f32 { self.meta_data.side_length }

    fn tree_depth(&self) -> u32 { self.meta_data.level_count }

    fn add_node(&mut self) -> u32 { self.node_count.fetch_add(1, Ordering::Acquire) }

    fn maximum_nodes(&self) -> u32 { self.meta_data.capacity }

    fn current_nodes(&self) -> u32 { self.node_count.load(Ordering::Relaxed) }
}

#[cfg(not(feature = "rustgpu"))]
impl<T> CpuVoctree<T>
where
    T: FeatureDependentTraits,
{
    pub fn new(data: T, position: Vec3, box_width: f32, level_count: u32) -> Self
    {
        assert!(level_count < 32);

        // TODO: find a better formula for the capacity.
        let node_capacity = 2_f32.powf(level_count as f32 + 3.) as u32;
        let myself = Self {
            meta_data: VoctreeMetadata {
                position,
                level_count,
                side_length: box_width,
                capacity: node_capacity,
                ..Default::default()
            },
            node_count: AtomicU32::new(1),
            nodes: UnsafeCell::new(vec![Node::new(T::default()); node_capacity as usize]),
        };

        unsafe { (*myself.nodes.get())[0] = Node::new(data) };
        myself
    }

    pub fn from_meta_data(
        meta_data: VoctreeMetadata,
        node_count: u32,
        nodes: Vec<Node<T>>,
    ) -> Self
    {
        Self {
            meta_data,
            node_count: AtomicU32::new(node_count),
            nodes: UnsafeCell::new(nodes),
        }
    }

    pub fn from_points(
        points: &Vec<Vec3>,
        corner: Vec3,
        side_length: f32,
        level_count: u32,
        node_count: u32,
        data: Vec<T>,
    ) -> Self
    where
        T: Clone,
    {
        debug_assert!(points.len() == data.len());

        let nodes = vec![Node::new(data[0].clone()); data.len() * 2];
        let mut voctree = Self {
            meta_data: VoctreeMetadata {
                capacity: nodes.len() as u32,
                level_count,
                side_length,
                position: corner,
                _pad1: 0.,
                _pad2: 0.,
            },
            node_count: AtomicU32::new(1),
            nodes: UnsafeCell::new(nodes),
        };

        for (i, node) in data.into_iter().enumerate().take(node_count as usize)
        {
            unsafe { voctree.add_node_recursively(points[i], node) };
        }

        voctree
    }

    pub fn meta_data(&self) -> VoctreeMetadata { self.meta_data.clone() }

    pub fn nodes(&self) -> &[Node<T>] { unsafe { (*self.nodes.get()).as_slice() } }

    pub fn node_count(&self) -> u32 { self.node_count.load(Ordering::Relaxed) }

    pub fn export_as_obj(&self, path: &str)
    {
        let mut stack = vec![(self.meta_data.position, 0, self.node_at(0))];
        let mut levels = vec![Vec::new(); self.meta_data.level_count as usize];

        while let Some((corner, level, node)) = stack.pop()
        {
            if level == self.meta_data.level_count
            {
                continue;
            }
            let size = 2_f32.powf(level as f32);
            levels[level as usize].push((corner, self.meta_data.side_length / size));

            for index in 0..8
            {
                let child_id = node.child(index);

                if child_id == u32::MAX
                {
                    continue;
                }

                let logical_offset = index_to_vec(index as u32);

                stack.push((
                    corner + (logical_offset * self.meta_data.side_length / (size * 2.)),
                    level + 1,
                    self.node_at(child_id as usize),
                ));
            }
        }

        for (i, level) in levels.iter().enumerate()
        {
            let (verts, faces) = blocks_to_mesh(level);
            let faces: Vec<_> = faces
                .iter()
                .map(|slice| slice.iter().copied().collect::<Vec<usize>>())
                .collect();

            chicken_wire::wavefront_loader::ObjData::export(
                &(&verts, &faces),
                format!("{}_{}.obj", path, i).as_str(),
            );
        }
    }
}

#[cfg(not(feature = "rustgpu"))]
pub fn export_node_list_as_obj(nodes: &Vec<Vec3>, size: f32, path: &str)
{
    let cubes: Vec<_> = nodes.iter().cloned().map(|node| (node, size)).collect();

    let (verts, faces) = crate::blocks_to_mesh(&cubes);
    let faces: Vec<_> = faces
        .iter()
        .map(|slice| slice.iter().copied().collect::<Vec<usize>>())
        .collect();

    chicken_wire::wavefront_loader::ObjData::export(
        &(&verts, &faces),
        format!("{}.obj", path).as_str(),
    );
}

// +| Tests |+ ================================================================
#[cfg(test)]
#[cfg(not(feature = "rustgpu"))]
mod tests
{
    use std::f32::consts::PI;

    use super::*;

    #[test]
    fn test_node_addition()
    {
        let mut oct_tree = CpuVoctree::new(42., Vec3::new(-1., -1., -1.), 2., 3);
        unsafe { oct_tree.add_node_recursively(Vec3::new(-1., -1., -1.), 32.) };
        unsafe {
            let res = oct_tree.walk_tree(
                |box_query| is_inside_box(&Vec3::new(-1., -1., -1.), &box_query),
                0,
                oct_tree.meta_data.level_count,
            );
            assert!(res.was_hit);

            let res = oct_tree.walk_tree(
                |box_query| is_inside_box(&Vec3::new(1., 1., 1.), &box_query),
                0,
                oct_tree.meta_data.level_count,
            );
            assert!(!res.was_hit);

            let res = oct_tree.walk_tree(
                |box_query| is_inside_box(&Vec3::new(-0.9, -0.9, -0.9), &box_query),
                0,
                oct_tree.meta_data.level_count,
            );
            assert!(res.was_hit);

            let res = oct_tree.walk_tree(
                |box_query| is_inside_box(&Vec3::new(0., 0., 0.), &box_query),
                0,
                oct_tree.meta_data.level_count,
            );
            assert!(!res.was_hit);
        }

        oct_tree.export_as_obj("node_add_test");
    }

    #[test]
    fn test_voctree_circle()
    {
        let mut oct_tree = CpuVoctree::new(42., Vec3::new(-1., -1., -1.), 2., 8);

        let resolution = 500;
        for i in 0..resolution
        {
            let t = i as f32 / (resolution - 1) as f32;
            let t = t * 2. * PI;
            let x = t.cos() * 0.5;
            let y = t.sin() * 0.5;

            unsafe {
                oct_tree.add_node_recursively(Vec3::new(x, y, 0.01), 32.);
            }
        }

        oct_tree.export_as_obj("circle_test");
    }
}
