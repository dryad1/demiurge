#[cfg(feature = "rustgpu")]
use spirv_std::{
    memory::{Scope, Semantics},
    RuntimeArray,
};

#[cfg(feature = "rustgpu")]
use crate::*;

#[cfg(feature = "rustgpu")]
pub struct GpuVoctree<'a, T>
where
    T: FeatureDependentTraits,
{
    pub meta_data: VoctreeMetadata,
    pub node_count: &'a mut u32,
    pub nodes: &'a mut RuntimeArray<Node<T>>,
}

#[cfg(feature = "rustgpu")]
impl<'a, T> VoctreeLike<T> for GpuVoctree<'a, T>
where
    T: FeatureDependentTraits,
{
    fn node_at_mut(&mut self, index: usize) -> &mut Node<T>
    {
        unsafe { self.nodes.index_mut(index) }
    }

    fn node_at(&self, index: usize) -> &Node<T> { unsafe { self.nodes.index(index) } }

    fn corner_position(&self) -> Vec3 { self.meta_data.position }

    fn box_width(&self) -> f32 { self.meta_data.side_length }

    fn tree_depth(&self) -> u32 { self.meta_data.level_count }

    fn add_node(&mut self) -> u32
    {
        let old = unsafe {
            spirv_std::arch::atomic_i_add::<
                _,
                { Scope::Invocation as u32 },
                { Semantics::UNIFORM_MEMORY.bits() },
            >(self.node_count, 1)
        };

        old
    }

    fn current_nodes(&self) -> u32
    {
        unsafe {
            spirv_std::arch::atomic_load::<
                _,
                { Scope::Invocation as u32 },
                { Semantics::UNIFORM_MEMORY.bits() },
            >(self.node_count)
        }
    }

    fn maximum_nodes(&self) -> u32 { self.meta_data.capacity }
}

#[cfg(feature = "rustgpu")]
impl<'a, T> GpuVoctree<'a, T>
where
    T: Clone + Default + FeatureDependentTraits + FeatureDependentTraits,
{
    pub fn new(
        meta_data: VoctreeMetadata,
        node_count: &'a mut u32,
        nodes: &'a mut RuntimeArray<Node<T>>,
    ) -> Self
    {
        Self {
            meta_data,
            node_count,
            nodes,
        }
    }
}
