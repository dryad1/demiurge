#![cfg_attr(feature = "rustgpu", no_std)]
const TREE_WALK_STACK_SIZE: usize = 7 * 8;

// Normal compilation
#[cfg(not(feature = "rustgpu"))]
mod not_rustgpu
{
    pub use algebra::*;
    pub use dem_log::*;
    pub use itertools::*;
}

#[cfg(not(feature = "rustgpu"))]
use not_rustgpu::*;
// Rustgpu
#[cfg(feature = "rustgpu")]
use spirv_std::{
    arch::IndexUnchecked,
    glam::*,
    memory::{Scope, Semantics},
    num_traits::Float,
};

pub mod traits;
pub use traits::*;
pub mod cpu_voctree;
pub mod gpu_voctree;
#[cfg(not(feature = "rustgpu"))]
pub use cpu_voctree::*;
#[cfg(feature = "rustgpu")]
pub use gpu_voctree::*;

#[repr(C, align(4))]
#[derive(Copy, Clone, Default)]
#[cfg_attr(not(feature = "rustgpu"), derive(Debug))]
pub struct VoctreeMetadata
{
    /// Number of levels in the SVO, e.g. `2^level_count` yields the resolution
    /// of the leaf level.
    pub level_count: u32,
    // Maximum possible of nodes that can be stored in this SVO.
    pub capacity: u32,
    /// Side length on all directions for the AABB of the SVO.
    pub side_length: f32,
    _pad1: f32,
    /// Position of the min coordinates of the AABB of the SVO.
    pub position: Vec3,
    _pad2: f32,
}

#[cfg_attr(not(feature = "rustgpu"), derive(Default, Copy, Clone, Debug))]
#[repr(C)]
pub struct Node<T>
where
    T: FeatureDependentTraits,
{
    pub data: T,
    pub children: [u32; 8],
}

impl<T> Node<T>
where
    T: FeatureDependentTraits,
{
    pub fn new(data: T) -> Self
    {
        Self {
            data,
            children: [u32::MAX; 8],
        }
    }

    #[cfg(not(feature = "rustgpu"))]
    pub fn set_child(&mut self, id: usize, val: u32) -> u32
    {
        let prior = self.children[id];
        self.children[id] = val;
        prior
    }

    #[cfg(feature = "rustgpu")]
    pub fn set_child(&mut self, id: usize, val: u32) -> u32
    {
        let reference = unsafe { self.children.index_unchecked_mut(id) };

        let old = unsafe {
            spirv_std::arch::atomic_exchange::<
                _,
                { Scope::Invocation as u32 },
                { Semantics::UNIFORM_MEMORY.bits() },
            >(reference, val)
        };

        old
    }

    #[cfg(not(feature = "rustgpu"))]
    pub fn reserve_child(&mut self, id: usize, val: u32) -> u32
    {
        let prior = self.children[id];
        self.children[id] = val;
        prior
    }

    #[cfg(feature = "rustgpu")]
    pub fn reserve_child(&mut self, id: usize, val: u32) -> u32
    {
        let reference = unsafe { self.children.index_unchecked_mut(id) };

        let old = unsafe {
            spirv_std::arch::atomic_compare_exchange::<
                _,
                { Scope::Invocation as u32 },
                { Semantics::UNIFORM_MEMORY.bits() },
                { Semantics::UNIFORM_MEMORY.bits() },
            >(reference, u32::MAX - 1, u32::MAX)
        };

        old
    }

    #[cfg(not(feature = "rustgpu"))]
    pub fn child(&self, id: usize) -> u32 { self.children[id] }

    #[cfg(feature = "rustgpu")]
    pub fn child(&self, id: usize) -> u32
    {
        let reference = unsafe { self.children.index_unchecked(id) };
        unsafe {
            spirv_std::arch::atomic_load::<
                _,
                { Scope::Invocation as u32 },
                { Semantics::UNIFORM_MEMORY.bits() },
            >(reference)
        }
    }
}

#[cfg_attr(not(feature = "rustgpu"), derive(Debug))]
#[derive(Default, Clone)]
pub struct SimpleNodeData
{
    pub color: Vec4,
    pub normal: Vec4,
}

#[inline]
pub fn is_inside_box(point: &Vec3, box_query: &BoxQuery) -> bool
{
    let relative_pos = *point - box_query.box_position;

    relative_pos.x >= 0.
        && relative_pos.x <= box_query.box_dimension
        && relative_pos.y >= 0.
        && relative_pos.y <= box_query.box_dimension
        && relative_pos.z >= 0.
        && relative_pos.z <= box_query.box_dimension
}

#[cfg(not(feature = "rustgpu"))]
pub fn blocks_to_mesh(blocks: &[(Vec3, f32)]) -> (Vec<Vec3>, Vec<[usize; 4]>)
{
    let mut verts = Vec::new();
    let mut faces = Vec::new();
    for (position, size) in blocks
    {
        let mut block_verts = [*position; 8];

        // The following code generates vertices in this order
        //        P4___________P5
        //        /|          /|
        //       / |         / |
        //      /  |        /  |
        //   P6/___|______ /P7 |
        //     |   |       |   |
        //     |  P0_______|___| P1
        //     |  /        |  /
        //     | /         | /
        //     |/__________|/
        //    P2           P3
        //
        for i in 0..8
        {
            let x_bit = (i & 1) >> 0;
            let y_bit = (i & 2) >> 1;
            let z_bit = (i & 4) >> 2;

            let mut corner = *position;
            corner[0] += x_bit as f32 * size;
            corner[1] += y_bit as f32 * size;
            corner[2] += z_bit as f32 * size;

            block_verts[i] = corner;
        }

        let block_faces = [
            [1, 0, 2, 3], // Bottom face
            [4, 6, 2, 0], // Left face
            [6, 7, 3, 2], // Front face
            [3, 7, 5, 1], // Right face
            [5, 7, 6, 4], // Top face
            [0, 1, 5, 4], // Back face
        ];

        // Append this geometry to the global list of blocks.
        let offset = verts.len();
        verts.extend(block_verts);
        faces.extend(block_faces.iter().map(|&face| {
            let transformed_face: [usize; 4] = face
                .iter()
                .map(|&i| i + offset)
                .collect_vec()
                .try_into()
                .unwrap();
            transformed_face
        }));
    }

    (verts, faces)
}

#[inline]
fn index_to_vec(index: u32) -> Vec3
{
    let x = 1 & index;
    let y = 1 & (index >> 1);
    let z = 1 & (index >> 2);

    Vec3::new(x as f32, y as f32, z as f32)
}

#[inline]
pub fn direction_to_mask(dir: &Vec3) -> u32
{
    let mut mask = 0;

    mask = mask & (((dir.x < 0.) as u32) << 0);
    mask = mask & (((dir.y < 0.) as u32) << 1);
    mask = mask & (((dir.z < 0.) as u32) << 2);

    mask
}

#[inline]
pub fn position_to_child_index(pos: &Vec3, box_query: BoxQuery) -> u32
{
    let relative_pos = *pos - box_query.box_position;

    #[cfg(not(feature = "rustgpu"))]
    dem_warning!(
        is_inside_box(pos, &box_query),
        "Point is {:?} dimension is {}",
        pos,
        box_query.box_dimension
    );

    let half_size = box_query.box_dimension / 2.0;
    let x_bit = (relative_pos.x > half_size) as u32;
    let y_bit = (relative_pos.y > half_size) as u32;
    let z_bit = (relative_pos.z > half_size) as u32;

    x_bit | (y_bit << 1) | (z_bit << 2)
}

#[inline]
fn permute_index(index: u32, mask: u32) -> u32 { index ^ mask }

pub struct BoxQuery
{
    pub box_position: Vec3,
    pub box_dimension: f32,
}

#[derive(Default, Clone, Copy)]
struct StackValue
{
    corner: Vec3,
    node_id: u32,
    level: u32,
}

impl StackValue
{
    fn new(node_id: u32, level: u32, corner: &Vec3) -> Self
    {
        Self {
            node_id,
            level,
            corner: *corner,
        }
    }
}
