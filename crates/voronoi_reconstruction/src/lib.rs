use core::f32::consts::PI;
use std::collections::{BTreeMap, BTreeSet, VecDeque};

use algebra::*;
use chicken_wire::wavefront_loader::{ColoredVerts, ObjData};
use geometry::implicit_curvature_features;
use geometry::project_onto_plane;
use numerical_analysis::differential_operators;
use numerical_analysis::differential_operators::mean_curvature;
use rstar::Point;
use rstar::RTree;
use spade::{DelaunayTriangulation, Triangulation};

#[derive(Clone, PartialEq, Debug, Copy)]
pub struct TreePoint(pub Vec3, pub usize);

impl Point for TreePoint
{
    type Scalar = f32;

    const DIMENSIONS: usize = 3;

    fn generate(mut generator: impl FnMut(usize) -> Self::Scalar) -> Self
    {
        TreePoint(
            Vec3::new(generator(0), generator(1), generator(2)),
            usize::MAX,
        )
    }

    fn nth(&self, index: usize) -> Self::Scalar { self.0[index] }

    fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar { &mut self.0[index] }
}

fn seek_surface<F>(mut point: Vec3, sdf: &F, reach: f32) -> Option<Vec3>
where
    F: Fn(&Vec3) -> f32,
{
    let mut found = false;
    for _ in 0..30
    {
        let d = sdf(&point);
        let mut g = differential_operators::gradient(&point, &sdf, 0.00001 * reach);
        if d.abs() < 0.001
        {
            found = true;
            break;
        }
        if g.norm() == 0.0
        {
            g = Vec3::new(0., -1.0, 0.);
        }
        point -= g * (d + 0.0001 * reach);
    }

    if found
    {
        Some(point)
    }
    else
    {
        None
    }
}

pub fn sample_sdf<F, R>(sdf: F, reach: R) -> RTree<TreePoint>
where
    F: Fn(&Vec3) -> f32 + Clone,
    R: Fn(&Vec3) -> f32,
{
    // TODO: Try to figure out with the anisotropic sampling is not working.
    let point = Vec3::default();
    let point = seek_surface(point, &sdf, 0.01).unwrap();

    let mut seeds = VecDeque::new();
    seeds.push_back(point);

    let mut tree = RTree::new();
    tree.insert(TreePoint(*seeds.front().unwrap(), tree.size()));

    let mut iterations = 0;
    while let Some(point) = seeds.pop_front()
    {
        iterations += 1;

        // if iterations % 1 == 0
        // {
        //     let mut points: Vec<_> = tree.iter().cloned().collect();
        //     points.sort_by(|a, b| a.1.cmp(&b.1));
        //     let points: Vec<_> = points.into_iter().map(|p| p.0).collect();

        //     ObjData::export(&points, format!("samples_{}.obj", iterations).as_str());
        // }

        let g = differential_operators::gradient(&point, &sdf, 0.0001).normalize();
        let u = orthogonal_vector(&g).normalize();
        let v = g.cross(&u).normalize();

        let sample_count = 6;

        // Create a series of samples on the planar neighbourhood of the current sample.
        // Then project them to the surface.
        let radius = reach(&point);

        // let (kmin, kmax, [t1, t2, n]) = implicit_curvature_features(&point, &sdf, radius);
        // println!("{} {} {} {} {}", t1, t2, radius, kmin, kmax);
        // let t1 = t1.normalize();
        // let t2 = t2.normalize();
        //
        // let s1 = (kmin).clamp(1.0, 20.0);
        // let s2 = (kmax).clamp(1.0, 20.0);

        // let rot_mat =
        //     Mat3::from_columns(&[t1.normalize(), t2.normalize(), n.normalize()]);
        // let diag_values = Vec3::new(s1, s2, 1.);
        // let diag = Mat3::from_diagonal(&diag_values);
        // let dist_mat = rot_mat.transpose() * diag * rot_mat;

        // println!("{} {}", s1, s2);

        for i in 0..sample_count
        {
            let t = (i as f32 / sample_count as f32) * 2. * PI;

            let candidate =
                (t.cos() * u + t.sin() * v) * radius * 1.25 + point;
            let candidate = seek_surface(candidate, &sdf, radius);
            if candidate.is_none()
            {
                continue;
            }
            let candidate = candidate.unwrap();
            let mut candidate = TreePoint(candidate, usize::MAX);
            // If the projected neighbouring sample is too close to any existing sample,
            // discard it. This creates a minimum gap between samples that
            // must be respected.
            if let Some(nearest) = tree.nearest_neighbor(&candidate)
            {
                if (nearest.0 - candidate.0).norm() <= radius * 0.95
                {
                    continue;
                }

                // let d = nearest.0 - candidate.0;
                // let distance = (d.transpose() * dist_mat * d)[(0, 0)].sqrt();
                // let dist = (nearest.0 - candidate.0).norm();

                // println!("euclid {} spetial {}", dist, distance);
                // if distance <= radius * 0.95
                // {
                //     continue;
                // }
            };

            seeds.push_back(candidate.0);
            candidate.1 = tree.size();
            tree.insert(candidate.clone());
        }
    }

    tree
}

struct Star
{
    center: usize,
    edges: BTreeSet<[usize; 2]>,
    faces: Vec<[usize; 3]>,
}

impl Star
{
    fn find_inconsistencies(&self, other: &Star) -> Vec<[usize; 3]>
    {
        let mut key = [self.center, other.center];
        key.sort();

        let relevant_faces: Vec<_> = other
            .faces
            .iter()
            .chain(self.faces.iter())
            .filter(|face| {
                let mut keep = false;
                for i in 0..3
                {
                    let u = face[i];
                    let v = face[(i + 1) % 3];
                    let mut k = [u, v];
                    k.sort();

                    keep = keep || (k == key);
                }
                keep
            })
            .map(|l| l.clone())
            .collect();
        let mut relevant_vertices: BTreeSet<_> = relevant_faces
            .iter()
            .flat_map(|f| f.iter().copied())
            .collect();

        let relevant_vertices_count = relevant_vertices.len();

        relevant_vertices.remove(&self.center);
        relevant_vertices.remove(&other.center);

        let mut inconsistencies = Vec::new();
        if relevant_vertices_count != 4
        {
            inconsistencies = other
                .faces
                .iter()
                .chain(self.faces.iter())
                .filter(|f| {
                    let mut count = 0;
                    for v in f.iter()
                    {
                        count += relevant_vertices.contains(v) as usize;
                    }

                    count == 1
                })
                .map(|f| f.clone())
                .collect();
        }

        inconsistencies
    }

    fn neighbours(&self) -> Vec<usize>
    {
        let v = self.center;
        self.edges
            .iter()
            .map(|e| {
                if v == e[0]
                {
                    e[1]
                }
                else
                {
                    e[0]
                }
            })
            .collect()
    }
}

pub fn topologize_sdf_stars<F, R>(
    sdf: F,
    tree: &RTree<TreePoint>,
    reach: R,
) -> Vec<[usize; 3]>
where
    F: Fn(&Vec3) -> f32,
    R: Fn(&Vec3) -> f32,
{
    let mut points: Vec<_> = tree.into_iter().collect();
    points.sort_by(|a, b| a.1.cmp(&b.1));
    let points: Vec<_> = points.into_iter().map(|p| p.0).collect();

    let mut faces = Vec::new();
    let mut stars = BTreeMap::new();
    for point in tree.iter()
    {
        let mut n_iter = tree.nearest_neighbor_iter(point);
        let _ = n_iter.next().unwrap(); // skip the active point.

        let g = differential_operators::gradient(&point.0, &sdf, 0.00001).normalize();
        let x = orthogonal_vector(&g).normalize();
        let y = g.cross(&x).normalize();

        let get_2d_coords = |p: &Vec3| spade::Point2::new(x.dot(&p), y.dot(&p));

        let mut local_points = vec![point];
        let mut triangulation = DelaunayTriangulation::<spade::Point2<_>>::new();
        let _ = triangulation.insert(get_2d_coords(&point.0));

        // Find all neighbours within a radius and project them to the tangent plane of
        // the active point. This yields  aset of potential triangle candidates.
        for neighbour in n_iter
        {
            if (neighbour.0 - point.0).norm() >= reach(&point.0) * 5.
            {
                break;
            }

            let q = neighbour.0;
            let proj = project_onto_plane(&point.0, &g, &q);
            let d = (proj - point.0).normalize();

            let a = (q - point.0).norm() / 2.0;
            let theta = d.dot(&(q - point.0).normalize());
            let l = a / theta;

            let res = 2. * l * d + point.0;

            let _ = triangulation.insert(get_2d_coords(&res));
            local_points.push(neighbour);
        }

        // Add the faces in the triangulation to a set of potential candidates
        // if they meet certain criteria.
        let mut face_map = BTreeMap::new();
        let mut triangulation_faces = Vec::new();
        for (i, face) in triangulation.inner_faces().enumerate()
        {
            if !face.vertices().iter().any(|v| v.index() == 0)
            {
                continue;
            }

            triangulation_faces.push([
                face.vertices()[0].index(),
                face.vertices()[1].index(),
                face.vertices()[2].index(),
            ]);

            let index = face.vertices().iter().position(|v| v.index() == 0).unwrap();
            // Associate this face with the star points that connect it to the active
            // point.
            let next = face.vertices()[(index + 1) % 3];
            let prev = face.vertices()[(index + 2) % 3];
            let list = face_map.entry(next.index()).or_insert(Vec::new());
            list.push(i);
            let list = face_map.entry(prev.index()).or_insert(Vec::new());
            list.push(i);
        }

        // Update all seen edges with those connecting the current face.
        let mut edges = BTreeSet::new();
        let faces: Vec<_> = triangulation_faces
            .into_iter()
            .map(|f| {
                [
                    local_points[f[0]].1,
                    local_points[f[1]].1,
                    local_points[f[2]].1,
                ]
            })
            .collect();
        for face in &faces
        {
            for i in 0..3
            {
                let mut key = [face[i], face[(i + 1) % 3]];
                key.sort();
                edges.insert(key);
            }
        }

        let star = Star {
            center: point.1,
            edges,
            faces,
        };

        stars.insert(point.1, star);
    }

    // Enforce manifold properties as we add the local faces ot the global set.
    let mut inconsistencies = Vec::new();
    let mut seen_faces = BTreeSet::new();
    let mut seen_edges = BTreeSet::new();
    let mut edge_counts = BTreeMap::new();
    let mut full_edge_counts = BTreeMap::new();

    // Add all sane faces to the global set and mark non-manifold faces for further
    // processing.
    for (_, star) in &stars
    {
        let mut is_inconsistent = false;
        for neighbour in star.neighbours()
        {
            let other_star = stars.get(&neighbour).unwrap();
            let incon = star.find_inconsistencies(other_star);
            if !incon.is_empty()
            {
                inconsistencies
                    .push(incon.into_iter().map(|l| l.to_vec()).collect::<Vec<_>>());
                is_inconsistent = true;

                break;
            }
        }

        for face in &star.faces
        {
            update_edge_counts(face, &mut full_edge_counts);
        }

        for face in &star.faces
        {
            faces.push(*face);
            let mut key = *face;
            key.sort();
            seen_faces.insert(key);

            update_edge_counts(face, &mut edge_counts);
            update_seen_edges(face, &mut seen_edges);
        }
    }

    // TODO: needed?
    let mut seen_faces = BTreeSet::new();
    let mut result = Vec::new();
    for f in faces
    {
        let mut key = f.clone();
        key.sort();
        if seen_faces.insert(key)
        {
            result.push(f);
        }
    }

    let mut edge_faces = BTreeMap::new();
    for (i, face) in result.iter().enumerate()
    {
        for j in 0..3
        {
            let mut edge = [face[j], face[(j + 1) % 3]];
            edge.sort();

            let edge_faces = edge_faces.entry(edge).or_insert(BTreeSet::new());
            edge_faces.insert(i);
        }
    }

    let mut boundary_faces = edge_faces
        .iter()
        .filter(|(_, fs)| fs.len() == 1)
        .map(|(_, fs)| *fs.first().unwrap())
        .collect::<Vec<_>>();

    // // Remove triangles with a boundary as a flood fill.
    // let mut seen_faces = BTreeSet::new();
    // while let Some(face_id) = boundary_faces.pop()
    // {
    //     seen_faces.insert(face_id);
    //     for j in 0..3
    //     {
    //         let face = result[face_id];
    //         let mut edge = [face[j], face[(j + 1) % 3]];
    //         edge.sort();

    //         let edge_faces = edge_faces.get_mut(&edge).unwrap();
    //         edge_faces.remove(&face_id);
    //     }

    //     for j in 0..3
    //     {
    //         let face = result[face_id];
    //         let mut edge = [face[j], face[(j + 1) % 3]];
    //         edge.sort();

    //         let local_faces = edge_faces.get(&edge).unwrap();
    //         if local_faces.len() == 1
    //         {
    //             boundary_faces.push(*local_faces.first().unwrap());
    //         }
    //     }
    // }

    // let mut faces = seen_faces.into_iter().collect::<Vec<_>>();
    // faces.sort();

    // for fid in faces.into_iter().rev()
    // {
    //     result.remove(fid);
    // }

    let mut seen_edges = BTreeSet::new();
    let mut res = Vec::new();
    for f in result
    {
        let mut saw_edge = false;
        for j in 0..3
        {
            let edge = [f[j], f[(j + 1) % 3]];
            saw_edge = saw_edge || seen_edges.contains(&edge);
        }

        // if !saw_edge
        {
            res.push(f);

            for j in 0..3
            {
                let edge = [f[j], f[(j + 1) % 3]];
                seen_edges.insert(edge);
            }
        }
    }

    res
}

pub fn voronoi_meshing<F, R>(sdf: F, reach: R) -> (Vec<Vec3>, Vec<[usize; 3]>)
where
    F: Fn(&Vec3) -> f32,
    R: Fn(&Vec3) -> f32,
{
    let points = sample_sdf(&sdf, &reach);

    let faces = topologize_sdf_stars(&sdf, &points, &reach);

    let mut points: Vec<_> = points.into_iter().collect();
    points.sort_by(|a, b| a.1.cmp(&b.1));
    let points: Vec<_> = points.into_iter().map(|p| p.0).collect();

    (points, faces)
}

fn topology_score(edge_counts: &[usize; 3]) -> usize
{
    if edge_counts.iter().all(|i| *i == 1)
    {
        return 0;
    }


    let zeros_count = edge_counts.iter().map(|i| (*i == 0) as usize).sum();
    if edge_counts.iter().all(|i| *i <= 1)
    {
        return zeros_count;
    }

    let bad_count: usize = edge_counts.iter().map(|i| (*i > 1) as usize).sum();
    return bad_count + zeros_count;
}

fn update_edge_counts(f: &[usize], edge_counts: &mut BTreeMap<[usize; 2], usize>)
{
    for i in 0..3
    {
        let u = f[i];
        let v = f[(i + 1) % 3];
        let mut key = [u, v];
        key.sort();

        let count = edge_counts.entry(key).or_insert(0);
        *count += 1;
    }
}

fn update_seen_edges(f: &[usize], edge_counts: &mut BTreeSet<[usize; 2]>)
{
    for i in 0..3
    {
        let u = f[i];
        let v = f[(i + 1) % 3];

        edge_counts.insert([u, v]);
    }
}

fn saw_an_edge(f: &[usize], seen_edges: &BTreeSet<[usize; 2]>) -> bool
{
    let mut saw = false;
    for i in 0..3
    {
        let u = f[i];
        let v = f[(i + 1) % 3];

        saw = saw || seen_edges.contains(&[u, v]);
    }
    saw
}

fn find_edge_counts(f: &[usize], edge_counts: &BTreeMap<[usize; 2], usize>)
    -> [usize; 3]
{
    let mut counts = [0; 3];
    for i in 0..3
    {
        let u = f[i];
        let v = f[(i + 1) % 3];
        let mut key = [u, v];
        key.sort();

        let count = *edge_counts.get(&key).unwrap_or(&0);
        counts[i] = count;
    }

    counts
}
