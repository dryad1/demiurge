#![allow(dead_code)]

mod hardware_interface;
mod image;
mod memory;
mod pipeline;
mod shader_program;
mod swapchain;
mod tools;

pub mod shader_parsing;

use std::path::Path;
use std::time::Instant;

use ash::vk::RenderingAttachmentInfoKHR;
use ash::{vk::Handle, *};
use dem_core::{gpu_api::*, rendering::*};
use log::*;

use crate::hardware_interface::*;
use crate::image::*;
use crate::memory::*;
use crate::pipeline::*;
use crate::shader_parsing::*;
pub use crate::shader_parsing::{rust_to_spirv, spirv_parsing::parse_spirv};
use crate::shader_program::*;
use crate::swapchain::*;
use crate::tools::*;

const FRAME_DATA_COLLECTION_COUNT: usize = 1;

unsafe impl Send for RenderContext {}

unsafe impl Sync for RenderContext {}

pub struct RenderContext {
    buffers: Vec<vk::Buffer>,
    images: Vec<VulkanImage>,
    pipelines: Vec<Pipeline>,

    swapchain: Box<Swapchain>,
    memory: Box<Memory>,
    hardware_interface: Box<HardwareInterface>,

    frame_id: usize,
    time_since_last_frame_ms: f32,
    frame_time_stamp: Instant,
}

impl GpuApi for RenderContext {
    fn allocate_buffer<T: IntoPtrSize>(&mut self, input: &T) -> GpuMemoryHandle {
        let usage: vk::BufferUsageFlags = vk::BufferUsageFlags::VERTEX_BUFFER
            | vk::BufferUsageFlags::INDEX_BUFFER
            | vk::BufferUsageFlags::STORAGE_BUFFER;

        let (data, size) = input.into_ptr_size();
        let buffer = self.memory.create_filled_buffer(data, size, usage);

        self.buffers.push(buffer);

        return GpuMemoryHandle::Buffer(BufferHandle::from_raw(buffer.as_raw()));
    }

    fn update_buffer(&mut self, buffer: &GpuMemoryHandle, data: *const u8, size: usize) {
        let buffer = vk::Buffer::from_raw(buffer.to_buffer_handle().to_raw());
        unsafe {
            self.memory.update_buffer(buffer, data, size);
        };
    }

    fn allocate_image(&mut self, image: &RawImageData) -> GpuMemoryHandle {
        let format = match image.format {
            ImageFormat::R8 => vk::Format::R8_UNORM,
            ImageFormat::RGBA8 => vk::Format::R8G8B8A8_UNORM,
            ImageFormat::BGRA8 => vk::Format::B8G8R8A8_UNORM,
            ImageFormat::R32 => vk::Format::R32_SFLOAT,
            ImageFormat::RGBA32 => vk::Format::R32G32B32A32_SFLOAT,
            ImageFormat::D32SfloatS8uint => vk::Format::D32_SFLOAT_S8_UINT,
        };

        let channel_num = match image.format {
            ImageFormat::R8 => 1,
            ImageFormat::RGBA8 => 4,
            ImageFormat::BGRA8 => 4,
            ImageFormat::R32 => 1,
            ImageFormat::RGBA32 => 4,
            ImageFormat::D32SfloatS8uint => 4,
        };

        let image = VulkanImage::new(
            &mut *self.hardware_interface,
            &mut *self.memory,
            image.data,
            vk::Extent3D {
                width: image.width,
                height: image.height,
                depth: image.depth,
            },
            channel_num,
            format,
            vk::SamplerAddressMode::REPEAT,
        );

        let id = self.images.len();
        self.images.push(image);

        return GpuMemoryHandle::Image(ImageHandle::from_raw(id as u64));
    }

    fn destroy_image(&mut self, image: GpuMemoryHandle) {
        let image = image.to_image_handle();
        let index = self
            .images
            .iter()
            .position(|x| x.image.as_raw() == image.to_raw())
            .unwrap();
        self.images.remove(index);
        self.memory
            .destroy_image(vk::Image::from_raw(image.to_raw()));
    }

    fn destroy_buffer(&mut self, buffer: GpuMemoryHandle) {
        let buffer = buffer.to_buffer_handle();
        // TODO: This is inefficient at least use a hash table to keep track of buffer
        // indices.
        let index = self
            .buffers
            .iter()
            .position(|x| x.as_raw() == buffer.to_raw())
            .unwrap();
        self.buffers.remove(index);
        self.memory
            .destroy_buffer(vk::Buffer::from_raw(buffer.to_raw()));
    }

    fn add_shader<T>(&mut self, shader_data: T) -> ShaderHandle
    where
        ShaderRepresentation: From<T>,
    {
        let shader_data = ShaderRepresentation::from(shader_data);

        let shader_program = match shader_data {
            ShaderRepresentation::Paths { data_paths: paths } => {
                if let &[path] = &paths.as_slice() {
                    let lib_path = Path::new(&path).join("src").join("lib.rs");
                    let config = parse_shader_configurations(
                        &std::fs::read_to_string(&lib_path).expect(
                            format!("Failed to read `{}`", lib_path.display()).as_str(),
                        ),
                    );

                    let spirv_data = rust_gpu_parsing::compile_shader(&path);

                    ShaderProgram::new(
                        &self.hardware_interface.device,
                        &self.hardware_interface.instance,
                        &paths,
                        &spirv_data,
                        config,
                    )
                } else if paths.iter().find(|x| x.contains("hlsl")).is_some() {
                    let sources = paths
                        .iter()
                        .map(|x| std::fs::read_to_string(x).unwrap())
                        .collect::<Vec<_>>();

                    let (i, _) = paths
                        .iter()
                        .enumerate()
                        .find(|(_i, x)| x.contains("vert"))
                        .unwrap();
                    let config = parse_shader_configurations(&sources[i]);

                    let sources = hlsl_parsing::compile_shader(&paths, &sources);

                    ShaderProgram::new(
                        &self.hardware_interface.device,
                        &self.hardware_interface.instance,
                        &paths,
                        &sources,
                        config,
                    )
                } else {
                    todo!("no GLSL support")
                }
            }
            ShaderRepresentation::Sources { names, sources } => {
                if names.iter().find(|x| x.contains("hlsl")).is_some() {
                    let (i, _) = names
                        .iter()
                        .enumerate()
                        .find(|(_i, x)| x.contains("vert"))
                        .unwrap();
                    let config = parse_shader_configurations(&sources[i]);

                    let sources = hlsl_parsing::compile_shader(&names, &sources);

                    ShaderProgram::new(
                        &self.hardware_interface.device,
                        &self.hardware_interface.instance,
                        &names,
                        &sources,
                        config,
                    )
                } else {
                    todo!("no GLSL support")
                }
            }
            ShaderRepresentation::Raw {
                names,
                raw_sources,
                metadata,
            } => {
                let config = parse_shader_configurations(&metadata);
                ShaderProgram::new(
                    &self.hardware_interface.device,
                    &self.hardware_interface.instance,
                    &names,
                    &raw_sources,
                    config,
                )
            }
        };

        let id = self.pipelines.len();
        self.pipelines.push(Pipeline::new(
            &mut *self.hardware_interface,
            &mut *self.memory,
            &mut *self.swapchain,
            shader_program,
        ));

        return ShaderHandle::from_raw(id as u64);
    }

    fn draw(&mut self, render_request: &RenderRequest) {
        match render_request.render_target {
            RenderTargets::ScreenBuffer => self.draw_to_screen(render_request),
            _ => self.draw_off_screen(render_request),
        }
    }

    fn compute(&mut self, compute_request: &ComputeRequest) {
        let pipeline = &self.pipelines[compute_request.shader.to_raw() as usize];

        let cmd = &self.hardware_interface.start_cmd_update();

        type ImageBindingIndex = (*mut VulkanImage, usize, usize);
        let input_count = compute_request.image_inputs.len();
        let mut shader_images = Vec::<ImageBindingIndex>::with_capacity(input_count);

        for GpuDataDescriptor {
            handle,
            binding,
            index,
        } in &compute_request.image_inputs
        {
            shader_images.push((
                &mut self.images[handle.to_buffer_handle().to_raw() as usize],
                *binding,
                *index,
            ));
        }

        pipeline.update_uniforms(
            &cmd,
            &compute_request.uniforms,
            shader_images,
            &compute_request.buffer_inputs,
            vk::PipelineBindPoint::COMPUTE,
        );

        unsafe {
            self.hardware_interface.device.cmd_dispatch(
                *cmd,
                compute_request.work_groups.0,
                compute_request.work_groups.1,
                compute_request.work_groups.2,
            );
        }
        if !compute_request.work_groups.0 == 0
            || compute_request.work_groups.1 == 0
            || compute_request.work_groups.2 == 0
        {
            warn!(
                "{}",
                format!(
                    "One of the work group dimensions is 0, the shader will do no work.\
            They are X {}, Y {}, Z {}",
                    compute_request.work_groups.0,
                    compute_request.work_groups.1,
                    compute_request.work_groups.2
                )
            )
        };

        self.hardware_interface.end_cmd_update(*cmd);

        let compute_queue = self.hardware_interface.compute_queue;

        let submit_info = vk::SubmitInfo {
            command_buffer_count: 1,
            p_command_buffers: cmd,
            ..Default::default()
        };

        let fence_create_info = vk::FenceCreateInfo::default();

        unsafe {
            let fence = self
                .hardware_interface
                .device
                .create_fence(&fence_create_info, None)
                .expect("Failed to create compute fence.");

            self.hardware_interface
                .device
                .queue_submit(compute_queue, &[submit_info], fence)
                .expect("Submiting to compute queue failed.");

            self.hardware_interface
                .device
                .wait_for_fences(&[fence], true, u64::MAX)
                .expect("Waiting for compute fence failed.");

            self.hardware_interface.device.destroy_fence(fence, None);
        };
    }

    fn read_memory(&mut self, resource: &GpuMemoryHandle) -> Vec<u8> {
        match *resource {
            GpuMemoryHandle::Image(image) => self.read_image(&image),
            GpuMemoryHandle::Buffer(buffer) => self.read_into_buffer(&buffer),
        }
    }
}

impl RenderContext {
    pub fn init_rendering<T>(monitor: &T, vsync: VSync) -> RenderContext
    where
        T: AbstractRenderWindow,
    {
        let mut hi = Box::new(HardwareInterface::new(monitor));
        let mut memory = Box::new(Memory::new(&mut hi));
        let swapchain = Box::new(Swapchain::new(&mut *hi, &mut *memory, vsync));

        return RenderContext {
            hardware_interface: hi,
            memory,

            swapchain,
            pipelines: Vec::new(),
            images: Vec::new(),
            buffers: Vec::new(),

            frame_id: 0,
            frame_time_stamp: Instant::now(),
            time_since_last_frame_ms: 0.0,
        };
    }

    pub fn start_frame<T>(&mut self, monitor: &T)
    where
        T: AbstractRenderWindow,
    {
        unsafe {
            self.hardware_interface
                .device
                .queue_wait_idle(self.hardware_interface.graphics_queue)
                .expect("Failed to wait for graphics queue in 'to screen' draw.");
        }

        let should_remake_swapchain = monitor.was_window_resized();
        if should_remake_swapchain {
            self.swapchain.update(self.swapchain.vsync);
        }
        self.swapchain.sync();
        self.time_since_last_frame_ms =
            self.frame_time_stamp.elapsed().as_micros() as f32 / 1000.0;
        self.frame_time_stamp = Instant::now();
    }

    pub fn frame_time(&self) -> f32 {
        self.time_since_last_frame_ms
    }

    // +| Internal |+ ==========================================================
    fn draw_to_screen(&mut self, render_request: &RenderRequest) {
        debug_assert!(render_request.shader.to_raw() < self.pipelines.len() as u64);
        let command_buffer = self.hardware_interface.start_cmd_update();
        let pipeline = &self.pipelines[render_request.shader.to_raw() as usize];

        let mut shader_images = Vec::<(*mut VulkanImage, usize, usize)>::new();
        for GpuDataDescriptor {
            handle,
            binding,
            index,
        } in &render_request.image_inputs
        {
            shader_images.push((
                &mut self.images[handle.to_image_handle().to_raw() as usize],
                *binding,
                *index,
            ));
        }

        pipeline.update_uniforms(
            &command_buffer,
            &render_request.uniforms,
            shader_images,
            &render_request.buffer_inputs,
            vk::PipelineBindPoint::GRAPHICS,
        );

        self.swapchain
            .draw_to_screen(&command_buffer, render_request);
        self.hardware_interface.end_cmd_update(command_buffer);

        let submit_info = vk::SubmitInfo {
            command_buffer_count: 1,
            p_command_buffers: &command_buffer,
            ..Default::default()
        };

        let fence_create_info = vk::FenceCreateInfo::default();
        unsafe {
            let device = &self.hardware_interface.device;
            let fence = device
                .create_fence(&fence_create_info, None)
                .expect("Failed to create draw to screen fence.");

            let graphics_queue = &self.hardware_interface.graphics_queue;
            let result = device.queue_submit(*graphics_queue, &[submit_info], fence);

            if result.is_err() {
                eprintln!("Failed to submit render command");
            }

            device
                .wait_for_fences(&[fence], true, u64::MAX)
                .expect("Waiting for screen drawing fence failed.");

            device
                .queue_wait_idle(self.hardware_interface.graphics_queue)
                .expect("Failed to wait for graphics queue in to screen draw.");
            device.destroy_fence(fence, None);
        }
    }

    fn draw_off_screen(&mut self, render_request: &RenderRequest) {
        let pipeline = &self.pipelines[render_request.shader.to_raw() as usize];

        let cmd = &self.hardware_interface.start_cmd_update();

        type ImageBindingIndex = (*mut VulkanImage, usize, usize);
        let input_count = render_request.image_inputs.len();
        let mut shader_images = Vec::<ImageBindingIndex>::with_capacity(input_count);

        for GpuDataDescriptor {
            handle,
            binding,
            index,
        } in &render_request.image_inputs
        {
            shader_images.push((
                &mut self.images[handle.to_image_handle().to_raw() as usize],
                *binding,
                *index,
            ));
        }

        pipeline.update_uniforms(
            &cmd,
            &render_request.uniforms,
            shader_images,
            &render_request.buffer_inputs,
            vk::PipelineBindPoint::GRAPHICS,
        );

        let empty_outputs = Vec::new();
        let image_outputs = match &render_request.render_target {
            RenderTargets::ScreenBuffer => {
                panic!("Calling off screen rendering on screen buffer")
            }

            RenderTargets::None(_, _) => &empty_outputs,

            RenderTargets::OffScreenBuffers(image_outputs) => image_outputs,
        };

        let (width, height) = match &render_request.render_target {
            RenderTargets::ScreenBuffer => {
                panic!("Calling off screen rendering on screen buffer")
            }

            RenderTargets::None(w, h) => (*w, *h),

            RenderTargets::OffScreenBuffers(image_outputs) => {
                debug_assert!(
                    !image_outputs.is_empty(),
                    "Tried to do an off screen render but the list of targets is empty."
                );
                let first_handle = image_outputs[0].0;
                let image = &self.images[first_handle.to_raw() as usize];

                let width = image.width;
                let height = image.height;

                (width, height)
            }
        };

        let image_count = image_outputs.len();

        let (depth_image_handle, shader_image_outputs) = unsafe {
            let mut shader_image_outputs =
                Vec::<vk::RenderingAttachmentInfoKHR>::with_capacity(image_count);

            let mut depth_image_handle = ImageHandle::from_raw(!0);
            let mut output_count = 0;
            for (handle, binding) in image_outputs {
                if *binding == !0 {
                    depth_image_handle = *handle;
                    continue;
                }

                output_count += 1;

                if !*binding < shader_image_outputs.len() {
                    error!(
                        "{}",
                        format!(
                            "Touching binding {} out of {} image outputs.",
                            *binding,
                            shader_image_outputs.len()
                        )
                    );
                }
                let image = &mut self.images[handle.to_raw() as usize];

                debug_assert!(image.width == width);
                debug_assert!(image.height == height);

                image.transition_layout(
                    vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                    vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                );
                shader_image_outputs.as_mut_ptr().add(*binding).write(
                    RenderingAttachmentInfoKHR {
                        image_view: image.image_view,
                        image_layout: image.layout,
                        load_op: if render_request.should_clear {
                            vk::AttachmentLoadOp::CLEAR
                        } else {
                            vk::AttachmentLoadOp::LOAD
                        },
                        store_op: vk::AttachmentStoreOp::STORE,
                        clear_value: vk::ClearValue {
                            color: vk::ClearColorValue {
                                float32: render_request.color_clear,
                            },
                        },
                        ..Default::default()
                    },
                );
            }
            shader_image_outputs.set_len(output_count);

            (depth_image_handle, shader_image_outputs)
        };

        let mut depth_stencil_attachment = vk::RenderingAttachmentInfoKHR::default();
        let mut rendering_info = vk::RenderingInfoKHR::default();

        if depth_image_handle.to_raw() != !0 {
            let depth_image = &self.images[depth_image_handle.to_raw() as usize];

            depth_stencil_attachment.image_view = depth_image.image_view;
            depth_stencil_attachment.image_layout =
                vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL_KHR;

            depth_stencil_attachment.load_op = if render_request.should_clear {
                vk::AttachmentLoadOp::CLEAR
            } else {
                vk::AttachmentLoadOp::LOAD
            };

            depth_stencil_attachment.store_op = vk::AttachmentStoreOp::STORE;
            depth_stencil_attachment.clear_value.depth_stencil =
                vk::ClearDepthStencilValue {
                    depth: render_request.depth_clear,
                    stencil: render_request.stencil_clear,
                };

            rendering_info.p_depth_attachment = &depth_stencil_attachment;
            rendering_info.p_stencil_attachment = &depth_stencil_attachment;
        }

        rendering_info.render_area = vk::Rect2D {
            extent: vk::Extent2D { width, height },
            offset: vk::Offset2D { x: 0, y: 0 },
        };
        rendering_info.layer_count = 1;
        rendering_info.color_attachment_count = shader_image_outputs.len() as u32;
        rendering_info.p_color_attachments = shader_image_outputs.as_ptr();

        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: width as f32,
            height: height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
            ..Default::default()
        };

        let render_scissor = &render_request.scissor;
        let scissor = vk::Rect2D {
            offset: vk::Offset2D {
                x: render_request.scissor.offset.0 as i32,
                y: render_request.scissor.offset.1 as i32,
            },
            extent: vk::Extent2D {
                width: width.min(render_scissor.extent.0),
                height: height.min(render_scissor.extent.1),
            },
        };

        unsafe {
            self.hardware_interface
                .device
                .cmd_set_scissor(*cmd, 0, &[scissor]);
            self.hardware_interface
                .device
                .cmd_set_viewport(*cmd, 0, &[viewport]);
            self.hardware_interface
                .device
                .cmd_begin_rendering(*cmd, &rendering_info);
            self.hardware_interface.draw(
                &render_request.vertex_input,
                &render_request.vertex_modifiers,
            );
            self.hardware_interface.device.cmd_end_rendering(*cmd);
        }

        self.hardware_interface.end_cmd_update(*cmd);
        let submit_info = vk::SubmitInfo {
            command_buffer_count: 1,
            p_command_buffers: cmd,
            ..Default::default()
        };

        let fence_create_info = vk::FenceCreateInfo::default();
        let fence = unsafe {
            self.hardware_interface
                .device
                .create_fence(&fence_create_info, None)
                .expect("Failed to create draw off screen fence.")
        };

        unsafe {
            let graphic_queue = &self.hardware_interface.graphics_queue;
            self.hardware_interface
                .device
                .queue_submit(*graphic_queue, &[submit_info], fence)
                .expect("Failed to submit off screen command.");

            self.hardware_interface
                .device
                .wait_for_fences(&[fence], true, u64::MAX)
                .expect("Failed to wait for fence.");
            self.hardware_interface
                .device
                .queue_wait_idle(*graphic_queue)
                .expect("Failed to wait off screen draw.");

            self.hardware_interface.device.destroy_fence(fence, None);
        }
    }

    fn read_image(&mut self, image: &ImageHandle) -> Vec<u8> {
        let image = &mut self.images[image.to_raw() as usize];

        image.transition_layout(
            vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            vk::PipelineStageFlags::BOTTOM_OF_PIPE,
        );

        let total_size = (image.width
            * image.height
            * u32::max(1, image.depth)
            * image.channel_num as u32
            * format_to_channel_pixel_size(image.format))
            as usize;

        let mut data: Vec<u8> = vec![0; total_size];

        self.memory.read_image(
            image.image,
            image.width,
            image.height,
            image.depth,
            data.as_mut_slice(),
        );

        data
    }

    fn read_into_buffer(&mut self, buffer: &BufferHandle) -> Vec<u8> {
        debug_assert!(self
            .buffers
            .iter()
            .any(|buf| buf.as_raw() == buffer.to_raw()));

        let buffer = vk::Buffer::from_raw(buffer.to_raw());

        self.memory.read_buffer(buffer)
    }
}
