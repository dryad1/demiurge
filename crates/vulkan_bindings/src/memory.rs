#![allow(dead_code)]

use std::collections::BTreeMap;
use std::ptr::copy_nonoverlapping;

use ash::{vk::Handle, *};
use gpu_allocator::{vulkan::*, *};

use crate::hardware_interface::*;

// +| Public |+ ================================================================
pub(crate) struct Memory
{
    hardware_interface: *mut HardwareInterface,

    pub(crate) vk_allocator: vulkan::Allocator,
    pub(crate) image_allocations: BTreeMap<u64, vulkan::Allocation>,
    pub(crate) buffer_allocations: BTreeMap<u64, vulkan::Allocation>,
}

impl Memory
{
    pub(crate) fn create_buffer(
        &mut self,
        size: usize,
        kind: vk::BufferUsageFlags,
    ) -> vk::Buffer
    {
        let create_info = vk::BufferCreateInfo {
            size: size as u64,
            usage: kind,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };

        let buffer = unsafe {
            self.hardware_interface()
                .device
                .create_buffer(&create_info, None)
                .expect("Could not create vulkan image.")
        };
        let requirements = unsafe {
            self.hardware_interface()
                .device
                .get_buffer_memory_requirements(buffer)
        };

        let allocation = self
            .vk_allocator
            .allocate(&AllocationCreateDesc {
                name: "Buffer allocation",
                requirements,
                location: MemoryLocation::CpuToGpu,
                linear: false,
                allocation_scheme: AllocationScheme::GpuAllocatorManaged,
            })
            .expect("Could not create allocation for vulkan buffer.");

        unsafe {
            self.hardware_interface()
                .device
                .bind_buffer_memory(buffer, allocation.memory(), allocation.offset())
                .expect("Faild to bind vulkan buffer.");
        };

        self.buffer_allocations.insert(buffer.as_raw(), allocation);

        return buffer;
    }

    pub(crate) fn create_filled_buffer(
        &mut self,
        data_src: *const u8,
        size: usize,
        usage: vk::BufferUsageFlags,
    ) -> vk::Buffer
    {
        let buffer_size = vk::DeviceSize::from(size as u64);
        // Create buffer optimized for data copying
        let staging_buffer: vk::Buffer =
            self.create_buffer(size, vk::BufferUsageFlags::TRANSFER_SRC);

        let staging_buffer_allocation = self
            .buffer_allocations
            .get(&staging_buffer.as_raw())
            .unwrap();

        // Copy data from pointer onto vulkan buffer
        unsafe {
            let data_dst = staging_buffer_allocation.mapped_ptr().unwrap();

            copy_nonoverlapping(
                data_src as *const u8,
                data_dst.as_ptr().cast::<u8>(),
                size,
            );
        };

        // Create a buffer optimized for selected usage (e.g uniform, vertex...).
        let buffer = self.create_buffer(size, vk::BufferUsageFlags::TRANSFER_DST | usage);
        // Copy data from staging buffer onto final buffer.
        let c_buffer = self.hardware_interface().begin_single_time_command();
        let copy_region = vk::BufferCopy {
            src_offset: 0,
            dst_offset: 0,
            size: buffer_size,
        };

        unsafe {
            let device = (self.hardware_interface()).device.clone();
            device.cmd_copy_buffer(c_buffer, staging_buffer, buffer, &[copy_region]);

            self.hardware_interface().end_single_time_command(c_buffer);
            self.destroy_buffer(staging_buffer);
        };

        return buffer;
    }

    pub(crate) fn destroy_buffer(&mut self, buffer: vk::Buffer)
    {
        debug_assert!(self.buffer_allocations.contains_key(&buffer.as_raw()));

        let buffer_allocation = self.buffer_allocations.remove(&buffer.as_raw()).unwrap();
        self.vk_allocator
            .free(buffer_allocation)
            .expect("Failed to free vulkan memory allocation.");
        unsafe {
            self.hardware_interface()
                .device
                .destroy_buffer(buffer, None);
        }
    }

    pub(crate) fn destroy_image(&mut self, image: vk::Image)
    {
        debug_assert!(self.image_allocations.contains_key(&image.as_raw()));

        let image_allocation = self.image_allocations.remove(&image.as_raw()).unwrap();
        self.vk_allocator
            .free(image_allocation)
            .expect("Failed to free vulkan memory allocation.");
        unsafe {
            self.hardware_interface().device.destroy_image(image, None);
        }
    }

    pub(crate) unsafe fn update_buffer(
        &self,
        buffer: vk::Buffer,
        object_data: *const u8,
        size: usize,
    )
    {
        let memory_allocation = self.buffer_allocations.get(&buffer.as_raw()).unwrap();
        debug_assert!(size <= memory_allocation.size() as usize);

        let data_dst = memory_allocation.mapped_ptr().unwrap();

        copy_nonoverlapping(
            object_data as *const u8,
            data_dst.as_ptr().cast::<u8>(),
            size,
        );
    }

    pub(crate) fn create_image(
        &mut self,
        width: u32,
        height: u32,
        depth: u32,
        format: vk::Format,
        tiling: vk::ImageTiling,
        usage: vk::ImageUsageFlags,
        initial_layout: vk::ImageLayout,
    ) -> (vk::Image, vk::ImageView)
    {
        let image_type = if depth > 0
        {
            vk::ImageType::TYPE_3D
        }
        else
        {
            vk::ImageType::TYPE_2D
        };

        let create_info = vk::ImageCreateInfo {
            image_type,
            format,
            extent: vk::Extent3D {
                width,
                height,
                depth: u32::max(1, depth),
            },
            mip_levels: 1,
            array_layers: 1,
            samples: vk::SampleCountFlags::TYPE_1,
            tiling,
            usage,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            initial_layout,
            ..Default::default()
        };

        let image = unsafe {
            self.hardware_interface()
                .device
                .create_image(&create_info, None)
                .expect("Could not create vulkan image.")
        };
        let requirements = unsafe {
            self.hardware_interface()
                .device
                .get_image_memory_requirements(image)
        };

        let allocation = self
            .vk_allocator
            .allocate(&AllocationCreateDesc {
                name: "Image allocation",
                requirements,
                location: MemoryLocation::GpuOnly,
                linear: false,
                allocation_scheme: AllocationScheme::GpuAllocatorManaged,
            })
            .expect("Could not create allocation for vulkan image.");

        unsafe {
            self.hardware_interface()
                .device
                .bind_image_memory(image, allocation.memory(), allocation.offset())
                .expect("Faild to bind vulkan image.")
        };

        self.image_allocations.insert(image.as_raw(), allocation);

        return (image, self.create_image_view(&image, format, image_type));
    }

    pub(crate) fn create_image_view(
        &self,
        image: &vk::Image,
        format: vk::Format,
        image_type: vk::ImageType,
    ) -> vk::ImageView
    {
        let aspect = if format == vk::Format::D32_SFLOAT_S8_UINT
        {
            vk::ImageAspectFlags::DEPTH | vk::ImageAspectFlags::STENCIL
        }
        else
        {
            vk::ImageAspectFlags::COLOR
        };

        return Self::create_image_view_internal(
            &self.hardware_interface().device,
            image,
            format,
            aspect,
            match image_type
            {
                vk::ImageType::TYPE_1D => vk::ImageViewType::TYPE_1D,
                vk::ImageType::TYPE_2D => vk::ImageViewType::TYPE_2D,
                vk::ImageType::TYPE_3D => vk::ImageViewType::TYPE_3D,
                _ => panic!(),
            },
        );
    }

    pub(crate) fn copy_buffer_to_image(
        &mut self,
        buffer: &vk::Buffer,
        image: &vk::Image,
        width: u32,
        height: u32,
        depth: u32,
        aspect_mask: vk::ImageAspectFlags,
    )
    {
        let cmd_buffer = self.hardware_interface().begin_single_time_command();

        let mut regions = Vec::<vk::BufferImageCopy>::with_capacity(2);
        // You have to copy the depth and stencil buffers independently.
        let aspects: Vec<vk::ImageAspectFlags> = if aspect_mask
            .contains(vk::ImageAspectFlags::DEPTH)
            && aspect_mask.contains(vk::ImageAspectFlags::STENCIL)
        {
            vec![vk::ImageAspectFlags::DEPTH, vk::ImageAspectFlags::STENCIL]
        }
        else
        {
            vec![aspect_mask]
        };

        for aspect in aspects
        {
            regions.push(vk::BufferImageCopy {
                buffer_offset: 0,
                buffer_row_length: 0,
                buffer_image_height: 0,
                image_subresource: vk::ImageSubresourceLayers {
                    aspect_mask: aspect,
                    mip_level: 0,
                    base_array_layer: 0,
                    layer_count: 1,
                },
                image_offset: vk::Offset3D { x: 0, y: 0, z: 0 },
                image_extent: vk::Extent3D {
                    width,
                    height,
                    depth: u32::max(1, depth),
                },
            });
        }

        unsafe {
            self.hardware_interface().device.cmd_copy_buffer_to_image(
                cmd_buffer,
                *buffer,
                *image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                &regions.as_slice(),
            );
        }

        self.hardware_interface()
            .end_single_time_command(cmd_buffer);
    }

    pub(crate) fn read_buffer(&self, buffer: vk::Buffer) -> Vec<u8>
    {
        let staging_buffer_allocation =
            self.buffer_allocations.get(&buffer.as_raw()).unwrap();

        let container = vec![0_u8; staging_buffer_allocation.size() as usize];
        // Copy data from pointer onto buffer.
        unsafe {
            let gpu_data = staging_buffer_allocation.mapped_ptr().unwrap();

            copy_nonoverlapping(
                gpu_data.as_ptr().cast::<u8>(),
                container.as_ptr() as *mut u8,
                container.len(),
            );
        };

        container
    }

    pub(crate) fn read_into_buffer(&self, buffer: vk::Buffer, container: &mut [u8])
    {
        let staging_buffer_allocation =
            self.buffer_allocations.get(&buffer.as_raw()).unwrap();

        debug_assert!(staging_buffer_allocation.size() <= container.len() as u64);
        // Copy data from pointer onto buffer.
        unsafe {
            let gpu_data = staging_buffer_allocation.mapped_ptr().unwrap();

            copy_nonoverlapping(
                gpu_data.as_ptr().cast::<u8>(),
                container.as_ptr() as *mut u8,
                staging_buffer_allocation.size() as usize,
            );
        };
    }

    pub(crate) fn copy_image_to_buffer(
        &self,
        image: vk::Image,
        width: u32,
        height: u32,
        depth: u32,
        buffer: vk::Buffer,
    )
    {
        let cmd = self.hardware_interface().begin_single_time_command();

        let region = vk::BufferImageCopy {
            buffer_offset: 0,
            buffer_row_length: 0,
            buffer_image_height: 0,
            image_subresource: vk::ImageSubresourceLayers {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            },
            image_offset: vk::Offset3D { x: 0, y: 0, z: 0 },
            image_extent: vk::Extent3D {
                width,
                height,
                depth: u32::max(1, depth),
            },
        };

        unsafe {
            self.hardware_interface().device.cmd_copy_image_to_buffer(
                cmd,
                image,
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                buffer,
                &[region],
            )
        }

        self.hardware_interface().end_single_time_command(cmd);
    }

    pub(crate) fn read_image(
        &mut self,
        image: vk::Image,
        width: u32,
        height: u32,
        depth: u32,
        buffer: &mut [u8],
    )
    {
        let transient_buffer =
            self.create_buffer(buffer.len(), vk::BufferUsageFlags::TRANSFER_DST);

        self.copy_image_to_buffer(image, width, height, depth, transient_buffer);

        self.read_into_buffer(transient_buffer, buffer);

        self.destroy_buffer(transient_buffer);
    }

    pub(crate) fn new(hardware_interface: &mut HardwareInterface) -> Memory
    {
        let vk_allocator = vulkan::Allocator::new(&vulkan::AllocatorCreateDesc {
            instance: hardware_interface.instance.clone(),
            device: hardware_interface.device.clone(),
            physical_device: hardware_interface.physical_device.clone(),
            debug_settings: Default::default(),
            buffer_device_address: true,
            allocation_sizes: Default::default(),
        })
        .expect("Could not create allocator.");

        return Memory {
            hardware_interface,
            vk_allocator,
            image_allocations: BTreeMap::new(),
            buffer_allocations: BTreeMap::new(),
        };
    }

    // +| Internal |+
    // ========================================================================
    fn create_image_view_internal(
        device: &ash::Device,
        image: &vk::Image,
        format: vk::Format,
        image_aspect: vk::ImageAspectFlags,
        dimension: vk::ImageViewType,
    ) -> vk::ImageView
    {
        let subresource_range = vk::ImageSubresourceRange {
            aspect_mask: image_aspect,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
            ..Default::default()
        };

        let create_info = vk::ImageViewCreateInfo {
            image: *image,
            view_type: dimension,
            format: format,
            subresource_range,
            ..Default::default()
        };

        unsafe {
            device
                .create_image_view(&create_info, None)
                .expect("Could not create image view.")
        }
    }

    fn hardware_interface(&self) -> &HardwareInterface
    {
        unsafe { &*self.hardware_interface }
    }
}

impl Drop for Memory
{
    fn drop(&mut self)
    {
        let mut buffers = Vec::<u64>::new();
        for (buffer, _) in self.buffer_allocations.iter()
        {
            buffers.push(*buffer);
        }
        for buffer in buffers
        {
            self.destroy_buffer(vk::Buffer::from_raw(buffer));
        }
    }
}
