use clap::Parser;
use lib::compile_shader_crate;

pub mod lib;

#[derive(Parser, Debug)]
#[clap(author, version, about="Command line utility for compiling rust code to spirv.", long_about = None)]
struct Arguments
{
    /// Path to where the crate directory is.
    #[clap(short, long, value_parser)]
    input_path: String,
}

fn main()
{
    let args = Arguments::parse();
    std::fs::create_dir_all(&args.input_path).unwrap();

    let results = compile_shader_crate(&args.input_path);

    for result in results
    {
        println!("{}", result.display());
    }
}
