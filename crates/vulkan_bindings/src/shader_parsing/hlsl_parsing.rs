use std::fs;
use std::io::Write;
use std::path::Path;

use ash::*;
use hassle_rs::*;

use crate::shader_parsing::ShaderConfig;
use crate::shader_program::*;

static LIBDXCOMPILER: &[u8] = include_bytes!("../bin_data/linux/libdxcompiler.so");

fn compile_with_dxc(
    source_name: &str,
    shader_text: &str,
    entry_point: &str,
    target_profile: &str,
    args: &[&str],
    defines: &[(&str, Option<&str>)],
    path: &str,
) -> Vec<u8>
{
    let dxc = Dxc::new(Some(Path::new(path).to_path_buf())).unwrap();

    let compiler = dxc.create_compiler().unwrap();
    let library = dxc.create_library().unwrap();

    let blob = library
        .create_blob_with_encoding_from_str(shader_text)
        .unwrap();

    let result = compiler.compile(
        &blob,
        source_name,
        entry_point,
        target_profile,
        args,
        Some(&mut HlslIncluder {}),
        defines,
    );

    let blob = match result
    {
        Ok(data) => data,
        Err(err) =>
        {
            let error_blob = err.0.get_error_buffer().unwrap();
            let err = library.get_blob_as_string(&error_blob.into()).unwrap();
            panic!("On file: {}\n{}", source_name, err)
        }
    };

    return blob.get_result().unwrap().to_vec();
}

struct HlslIncluder {}

impl DxcIncludeHandler for HlslIncluder
{
    fn load_source(&mut self, filename: String) -> Option<String>
    {
        use std::io::Read;
        match std::fs::File::open(filename)
        {
            Ok(mut f) =>
            {
                let mut content = String::new();
                f.read_to_string(&mut content).ok()?;
                Some(content)
            }
            Err(_) => None,
        }
    }
}

fn create_file_if_not_exists(path: &str, data: &[u8])
{
    let path = Path::new(path);
    if !path.exists()
    {
        fs::create_dir_all(path.parent().unwrap()).unwrap();
        let mut file = std::fs::File::create(path).unwrap();
        file.write(data).unwrap();
    }
}

pub(crate) fn compile_to_spirv(
    name: &String,
    source: &String,
    stage: vk::ShaderStageFlags,
) -> Vec<u8>
{
    let stage_str = match stage
    {
        vk::ShaderStageFlags::VERTEX => "vs_6_5",
        vk::ShaderStageFlags::FRAGMENT => "ps_6_5",
        vk::ShaderStageFlags::COMPUTE => "cs_6_5",
        _ => todo!("{:?}", stage),
    };

    // Massive hack to be able to ship the dynamic library.
    // TODO(low): move this to a better place.
    create_file_if_not_exists("tmp/libdxcompiler.so", LIBDXCOMPILER);

    let spirv_data = compile_with_dxc(
        name,
        source,
        "main",
        stage_str,
        &vec!["-spirv", "-HV 2021", "/Itmp"],
        &vec![],
        "tmp/libdxcompiler.so",
    );

    return spirv_data;
}

pub(crate) fn compile_shader(names: &[String], sources: &[String]) -> Vec<Vec<u8>>
{
    let mut spirv_data = Vec::new();

    let _shader_config = ShaderConfig::default();
    for (i, name) in names.iter().enumerate()
    {
        let source = &sources[i];
        let stage = vk_stage_flags_from_string(name);
        spirv_data.push(compile_to_spirv(name, source, stage));
    }

    spirv_data
}
