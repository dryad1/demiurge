pub mod hlsl_parsing;
pub mod rust_gpu_parsing;
pub mod spirv_parsing;


use ash::*;
pub use rust_gpu_parsing::*;

use crate::shader_program::*;

#[derive(Debug)]
pub struct ShaderConfig
{
    pub(crate) depth_test: bool,
    pub(crate) depth_write: bool,
    pub(crate) line_width: f32,
    pub(crate) topology: vk::PrimitiveTopology,
    pub(crate) polygon_mode: vk::PolygonMode,
    pub(crate) use_viewport_extent: bool,
    pub(crate) render_extent: vk::Extent2D,
    pub(crate) cull_mode: vk::CullModeFlags,
    pub(crate) front_face: vk::FrontFace,
    pub(crate) depth_compare: vk::CompareOp,
}

impl Default for ShaderConfig
{
    fn default() -> Self
    {
        ShaderConfig {
            depth_test: true,
            depth_write: true,
            line_width: 3.0,
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            polygon_mode: vk::PolygonMode::FILL,
            use_viewport_extent: true,
            render_extent: vk::Extent2D {
                width: !0,
                height: !0,
            },
            cull_mode: vk::CullModeFlags::BACK,
            front_face: vk::FrontFace::CLOCKWISE,
            depth_compare: vk::CompareOp::LESS,
        }
    }
}


pub fn parse_shader_configurations(source: &String) -> ShaderConfig
{
    let pipeline_description_regex =
        regex::Regex::new(r#"\[\[ne::([a-z_]+)\((.*)\)\]\]"#).unwrap();

    let mut configs = ShaderConfig {
        ..Default::default()
    };

    // TODO(medium): each of these should be encountered just once. Add a check.
    for capture in pipeline_description_regex.captures_iter(&source)
    {
        let key = capture.get(1).unwrap().as_str().to_string();
        let val = capture.get(2).unwrap().as_str().to_string();

        let key = config_str_to_value(&key, &val);
        match key
        {
            ShaderInputConfigs::DepthTest(flag) => configs.depth_test = flag,
            ShaderInputConfigs::DepthWrite(flag) => configs.depth_write = flag,
            ShaderInputConfigs::DepthCompare(op) => configs.depth_compare = op,
            ShaderInputConfigs::Topology(val) => configs.topology = val,
            ShaderInputConfigs::LineWidth(val) => configs.line_width = val,
            ShaderInputConfigs::PolygonMode(val) => configs.polygon_mode = val,
            ShaderInputConfigs::Extent(val) => configs.render_extent = val,
            ShaderInputConfigs::CullMode(val) => configs.cull_mode = val,
            ShaderInputConfigs::FrontFace(val) => configs.front_face = val,
            ShaderInputConfigs::None =>
            {}
        }
    }

    return configs;
}
