use std::collections::{BTreeMap, HashSet};

use ash::*;
use regex::*;
use spirq::prelude::*;
use spirq::ty::*;

use crate::get_format_from_string;
use crate::shader_parsing::ShaderConfig;
use crate::shader_program::ShaderDescription;

pub fn parse_spirv(spirv_data: &[Vec<u8>], shader_name: &String) -> ShaderDescription
{
    // Construct reflection data.
    let entry_list: Vec<_> = spirv_data
        .iter()
        .map(|spirv_data| {
            ReflectConfig::new()
                .spv(spirv_data.clone())
                .ref_all_rscs(true)
                .combine_img_samplers(true)
                .gen_unique_names(true)
                .reflect()
                .unwrap()
        })
        .collect();

    let mut stages = Vec::new();
    let mut entry_names = Vec::new();
    let mut shader_stage_flags = vk::ShaderStageFlags::empty();
    for entry_point in entry_list.iter().flatten()
    {
        let flag = map_flags(&entry_point.exec_model);
        shader_stage_flags |= flag;
        stages.push(flag);
        entry_names.push(entry_point.name.clone());
    }

    // Vertex inputs.
    let mut input_binding_descriptions = BTreeMap::new();
    let mut input_attribute_descriptions = Vec::new();

    // Uniform data.
    let mut uniform_binding_size_map = BTreeMap::<u32, usize>::new();
    let mut uniform_layouts = Vec::<vk::DescriptorSetLayoutBinding>::new();

    // Fragment outputs.
    let mut attachment_outputs = Vec::new();

    let mut seen_input_names = HashSet::new();
    for entry_point in entry_list.iter().flatten()
    {
        for var in &entry_point.vars
        {
            match var
            {
                Variable::Input { name, location, ty } =>
                {
                    if entry_point.exec_model != ExecutionModel::Vertex
                    {
                        continue;
                    }

                    let name = name.clone().unwrap();
                    if seen_input_names.contains(&name)
                    {
                        continue;
                    }
                    seen_input_names.insert(name.clone());

                    parse_vertex_inputs(
                        &name,
                        &location,
                        &ty,
                        &mut input_attribute_descriptions,
                        &mut input_binding_descriptions,
                    );
                }
                Variable::Descriptor {
                    desc_bind,
                    desc_ty,
                    ty,
                    nbind,
                    name,
                    ..
                } =>
                {
                    let name = name.clone().unwrap_or("{not named}".to_string());
                    assert!(
                        desc_bind.set() > 0,
                        "Problem with shader {}.\nRustgpu reserves descriptor set 0. Uniform \"{}\" at binding {} attached to descriptor 0.",
                        shader_name,
                        name,
                        desc_bind.bind()
                    );
                    // TODO: if the same resource is shared across shader stages we need
                    // to flag it accordingly.
                    let stage = map_flags(&entry_point.exec_model);
                    let mut descriptor =
                        parse_uniform_descriptor(desc_bind, &desc_ty, &ty, *nbind);
                    descriptor.stage_flags = stage;

                    uniform_layouts.push(descriptor);
                    if descriptor.descriptor_type == vk::DescriptorType::UNIFORM_BUFFER
                    {
                        uniform_binding_size_map.insert(
                            desc_bind.bind(),
                            find_size(&ty, vk::Format::UNDEFINED) as usize,
                        );
                    }
                }
                Variable::Output { ty, .. } =>
                {
                    if entry_point.exec_model != ExecutionModel::Fragment
                    {
                        continue;
                    }

                    attachment_outputs.push(parse_fragment_output(&ty));
                }
                _ => panic!(),
            }
        }
    }

    ShaderDescription {
        attachment_formats: attachment_outputs,
        attributes: input_attribute_descriptions,
        vertex_bindings: input_binding_descriptions.values().cloned().collect(),
        entry_names,
        shader_stages: stages,
        spirv_sources: spirv_data.to_vec(),
        is_compute: shader_stage_flags.contains(vk::ShaderStageFlags::COMPUTE),
        uniform_binding_size_map,
        uniform_layouts,
        config: ShaderConfig::default(),
    }
}

fn parse_fragment_output(_ty: &Type) -> vk::Format { vk::Format::UNDEFINED }

fn parse_uniform_descriptor(
    desc_bind: &DescriptorBinding,
    desc_ty: &DescriptorType,
    _ty: &Type,
    nbind: u32,
) -> vk::DescriptorSetLayoutBinding<'static>
{
    match desc_ty
    {
        DescriptorType::UniformBuffer() => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
            descriptor_count: nbind,
            ..Default::default()
        },
        DescriptorType::StorageBuffer(_access_type) => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::STORAGE_BUFFER,
            descriptor_count: nbind,
            ..Default::default()
        },
        DescriptorType::CombinedImageSampler() => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: nbind,
            ..Default::default()
        },
        DescriptorType::StorageImage(_access_type) => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::STORAGE_IMAGE,
            descriptor_count: nbind,
            ..Default::default()
        },
        DescriptorType::SampledImage() => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::SAMPLED_IMAGE,
            descriptor_count: nbind,
            ..Default::default()
        },
        DescriptorType::Sampler() => vk::DescriptorSetLayoutBinding {
            binding: desc_bind.bind(),
            descriptor_type: vk::DescriptorType::SAMPLER,
            descriptor_count: nbind,
            ..Default::default()
        },

        _ => panic!(),
    }
}

fn parse_vertex_inputs(
    name: &String,
    location: &InterfaceLocation,
    ty: &Type,
    input_attribute_descriptions: &mut Vec<vk::VertexInputAttributeDescription>,
    input_binding_descriptions: &mut BTreeMap<u32, vk::VertexInputBindingDescription>,
)
{
    let rate = if name.contains(&"instance")
    {
        vk::VertexInputRate::INSTANCE
    }
    else
    {
        vk::VertexInputRate::VERTEX
    };

    let binding = get_binding_from_name(&name);

    let binding = match binding
    {
        Some(i) => i,
        None => input_binding_descriptions.len() as u32,
    };

    let binding_description = input_binding_descriptions.entry(binding).or_insert(
        vk::VertexInputBindingDescription {
            input_rate: rate,
            stride: 0,
            binding,
        },
    );

    debug_assert!(binding_description.input_rate == rate);
    debug_assert!(binding_description.binding == binding);

    let binding_stride = binding_description.stride;

    // TODO(low): delete once support for hlsl is dropped.
    // HLSL gives us no mechanism to define the format of an input
    // from the type alone.
    let format = if let Some(format) = format_from_name(&name)
    {
        format
    }
    else
    {
        format_from_type(&ty)
    };

    input_attribute_descriptions.push(vk::VertexInputAttributeDescription {
        location: location.loc(),
        binding,
        format,
        offset: binding_stride,
    });
    binding_description.stride += find_size(&ty, format);
}

fn find_uniform_array_count(ty: &Type) -> u32
{
    match ty
    {
        Type::Array(array) => array.nelement.unwrap(),
        _ => 1,
    }
}

fn find_size(ty: &Type, format: vk::Format) -> u32
{
    let size = match ty
    {
        Type::Scalar(scalar) => get_scalar_size(&scalar, format),
        Type::Vector(vector) => get_vector_size(&vector, format),
        Type::Matrix(MatrixType {
            vector_ty, nvector, ..
        }) => get_vector_size(&vector_ty, format) * nvector,
        Type::Struct(StructType { members, .. }) =>
        {
            let mut stride = 0;
            for member in members
            {
                // TODO(low): consider not doing this recursively.
                stride += find_size(&member.ty, format);
            }

            stride
        }

        _ => panic!(),
    };

    size
}

fn get_vector_size(vector: &VectorType, format: vk::Format) -> u32
{
    let VectorType { scalar_ty, nscalar } = vector;

    get_scalar_size(&scalar_ty, format) * nscalar
}

fn get_scalar_size(scalar: &ScalarType, format: vk::Format) -> u32
{
    // TODO: delete outer match when support for HLSL is dropped.
    match format
    {
        vk::Format::R8G8B8A8_UNORM => 1,
        _ =>
        {
            let res = match scalar
            {
                ScalarType::Integer { bits, .. } =>
                {
                    // Convert to bytes from bits.
                    bits / 8
                }
                ScalarType::Float { bits } =>
                {
                    // Convert to bytes from bits.
                    bits / 8
                }
                _ => panic!(),
            };

            res
        }
    }
}

fn get_vector_count(vector: &VectorType) -> u32
{
    let VectorType { nscalar, .. } = vector;

    *nscalar
}

fn get_binding_from_name(name: &String) -> Option<u32>
{
    let bind_regex = Regex::new(r"bind([0-9]+)").unwrap();
    if let Some(captures) = bind_regex.captures(name.to_lowercase().as_str())
    {
        Some((&captures[1]).parse::<u32>().unwrap())
    }
    else
    {
        None
    }
}

fn format_from_scalar(scalar: &ScalarType) -> vk::Format
{
    match scalar
    {
        ScalarType::Integer { bits, is_signed } => match (bits, is_signed)
        {
            (8, false) =>
            {
                return vk::Format::R8_UINT;
            }
            (8, true) =>
            {
                return vk::Format::R8_SINT;
            }
            (16, false) =>
            {
                return vk::Format::R16_UINT;
            }
            (16, true) =>
            {
                return vk::Format::R16_SINT;
            }
            (32, false) =>
            {
                return vk::Format::R32_UINT;
            }
            (32, true) =>
            {
                return vk::Format::R32_SINT;
            }
            (64, false) =>
            {
                return vk::Format::R64_UINT;
            }
            (64, true) =>
            {
                return vk::Format::R64_SINT;
            }
            _ => panic!(),
        },
        ScalarType::Float { bits } => match bits
        {
            16 =>
            {
                return vk::Format::R16_SFLOAT;
            }
            32 =>
            {
                return vk::Format::R32_SFLOAT;
            }
            64 =>
            {
                return vk::Format::R64_SFLOAT;
            }
            _ => panic!(),
        },
        _ => panic!(),
    }
}

fn format_from_name(name: &String) -> Option<vk::Format>
{
    let format_regex = Regex::new(r"FORMAT_([a-zA-Z0-9]+)").unwrap();
    if let Some(captures) = format_regex.captures(name)
    {
        let format_str = &captures[1];
        Some(get_format_from_string(&format_str))
    }
    else
    {
        None
    }
}

fn format_from_type(kind: &Type) -> vk::Format
{
    match kind
    {
        Type::Scalar(scalar) => format_from_scalar(&scalar),
        Type::Vector(VectorType { scalar_ty, nscalar }) =>
        {
            let scalar_format = format_from_scalar(&scalar_ty);

            match (scalar_format, nscalar)
            {
                (vk::Format::R8_UINT, 2) =>
                {
                    return vk::Format::R8G8_UINT;
                }
                (vk::Format::R8_SINT, 2) =>
                {
                    return vk::Format::R8G8_SINT;
                }
                (vk::Format::R16_UINT, 2) =>
                {
                    return vk::Format::R16G16_UINT;
                }
                (vk::Format::R16_SINT, 2) =>
                {
                    return vk::Format::R16G16_SINT;
                }
                (vk::Format::R32_UINT, 2) =>
                {
                    return vk::Format::R32G32_UINT;
                }
                (vk::Format::R32_SINT, 2) =>
                {
                    return vk::Format::R32G32_SINT;
                }
                (vk::Format::R64_UINT, 2) =>
                {
                    return vk::Format::R64G64_UINT;
                }
                (vk::Format::R64_SINT, 2) =>
                {
                    return vk::Format::R64G64_SINT;
                }

                (vk::Format::R8_UINT, 3) =>
                {
                    return vk::Format::R8G8B8_UINT;
                }
                (vk::Format::R8_SINT, 3) =>
                {
                    return vk::Format::R8G8B8_SINT;
                }
                (vk::Format::R16_UINT, 3) =>
                {
                    return vk::Format::R16G16B16_UINT;
                }
                (vk::Format::R16_SINT, 3) =>
                {
                    return vk::Format::R16G16B16_SINT;
                }
                (vk::Format::R32_UINT, 3) =>
                {
                    return vk::Format::R32G32B32_UINT;
                }
                (vk::Format::R32_SINT, 3) =>
                {
                    return vk::Format::R32G32B32_SINT;
                }
                (vk::Format::R64_UINT, 3) =>
                {
                    return vk::Format::R64G64B64_UINT;
                }
                (vk::Format::R64_SINT, 3) =>
                {
                    return vk::Format::R64G64B64_SINT;
                }

                (vk::Format::R8_UINT, 4) =>
                {
                    return vk::Format::R8G8B8A8_UINT;
                }
                (vk::Format::R8_SINT, 4) =>
                {
                    return vk::Format::R8G8B8A8_SINT;
                }
                (vk::Format::R16_UINT, 4) =>
                {
                    return vk::Format::R16G16B16A16_UINT;
                }
                (vk::Format::R16_SINT, 4) =>
                {
                    return vk::Format::R16G16B16A16_SINT;
                }
                (vk::Format::R32_UINT, 4) =>
                {
                    return vk::Format::R32G32B32A32_UINT;
                }
                (vk::Format::R32_SINT, 4) =>
                {
                    return vk::Format::R32G32B32A32_SINT;
                }
                (vk::Format::R64_UINT, 4) =>
                {
                    return vk::Format::R64G64B64A64_UINT;
                }
                (vk::Format::R64_SINT, 4) =>
                {
                    return vk::Format::R64G64B64A64_SINT;
                }

                (vk::Format::R16_SFLOAT, 2) =>
                {
                    return vk::Format::R16G16_SFLOAT;
                }
                (vk::Format::R32_SFLOAT, 2) =>
                {
                    return vk::Format::R32G32_SFLOAT;
                }
                (vk::Format::R64_SFLOAT, 2) =>
                {
                    return vk::Format::R64G64_SFLOAT;
                }

                (vk::Format::R16_SFLOAT, 3) =>
                {
                    return vk::Format::R16G16B16_SFLOAT;
                }
                (vk::Format::R32_SFLOAT, 3) =>
                {
                    return vk::Format::R32G32B32_SFLOAT;
                }
                (vk::Format::R64_SFLOAT, 3) =>
                {
                    return vk::Format::R64G64B64_SFLOAT;
                }

                (vk::Format::R16_SFLOAT, 4) =>
                {
                    return vk::Format::R16G16B16A16_SFLOAT;
                }
                (vk::Format::R32_SFLOAT, 4) =>
                {
                    return vk::Format::R32G32B32A32_SFLOAT;
                }
                (vk::Format::R64_SFLOAT, 4) =>
                {
                    return vk::Format::R64G64B64A64_SFLOAT;
                }

                _ => panic!(),
            }
        }
        _ => panic!(),
    }
}

fn map_flags(exec_model: &ExecutionModel) -> vk::ShaderStageFlags
{
    match exec_model
    {
        ExecutionModel::Vertex =>
        {
            return vk::ShaderStageFlags::VERTEX;
        }
        ExecutionModel::Fragment =>
        {
            return vk::ShaderStageFlags::FRAGMENT;
        }
        ExecutionModel::Geometry =>
        {
            return vk::ShaderStageFlags::GEOMETRY;
        }
        ExecutionModel::TessellationEvaluation =>
        {
            return vk::ShaderStageFlags::TESSELLATION_CONTROL;
        }
        ExecutionModel::TessellationControl =>
        {
            return vk::ShaderStageFlags::TESSELLATION_EVALUATION;
        }
        ExecutionModel::GLCompute =>
        {
            return vk::ShaderStageFlags::COMPUTE;
        }
        ExecutionModel::TaskEXT =>
        {
            return vk::ShaderStageFlags::TASK_EXT;
        }
        ExecutionModel::MeshEXT =>
        {
            return vk::ShaderStageFlags::MESH_EXT;
        }
        _ =>
        {
            panic!("Unsupported execution model {:?}", exec_model)
        }
    }
}
