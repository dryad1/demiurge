use std::collections::BTreeMap;
use std::mem::size_of;
use std::path::Path;
use std::str::FromStr;

use add_getters_setters::*;
use ash::*;

use crate::shader_parsing::*;

// +| Public |+ ================================================================
#[derive(AddGetter, AddGetterVal, AddGetterMut, AddSetter)]
pub(crate) struct ShaderProgram
{
    #[get]
    name: String,
    #[get]
    shader_stages: Vec<vk::ShaderStageFlags>,
    #[get]
    shader_entry_names: Vec<String>,
    #[get]
    vertex_bindings: Vec<vk::VertexInputBindingDescription>,
    #[get]
    attributes: Vec<vk::VertexInputAttributeDescription>,

    #[get]
    uniform_binding_size_map: BTreeMap<u32, usize>,
    #[get]
    uniform_layouts: Vec<vk::DescriptorSetLayoutBinding<'static>>,

    #[get_val]
    attachment_count: u32,
    #[get]
    attachment_formats: Vec<vk::Format>,

    #[get]
    shader_modules: Vec<vk::ShaderModule>,

    #[get]
    config: ShaderConfig,

    #[get_val]
    is_compute: bool,
}

impl ShaderProgram
{
    pub(crate) fn new(
        device: &ash::Device,
        instance: &Instance,
        shader_names: &[String],
        shader_sources: &[Vec<u8>],
        config: ShaderConfig,
    ) -> ShaderProgram
    {
        let name = Path::new(&shader_names[0])
            .file_stem()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();
        let info = spirv_parsing::parse_spirv(shader_sources, &name);

        let mut shader_modules =
            Vec::<vk::ShaderModule>::with_capacity(info.shader_stages.len());
        for (index, source) in info.spirv_sources.into_iter().enumerate()
        {
            use crate::hardware_interface::*;
            // TODO(low): allow to see the name externally.

            let shader_module = create_shader_module(device, &source);
            let i = index.min(shader_names.len() - 1);
            shader_module.set_name(&device, instance, &shader_names[i]);
            shader_modules.push(shader_module);
        }

        return ShaderProgram {
            name,
            shader_stages: info.shader_stages,
            shader_entry_names: info.entry_names,
            vertex_bindings: info.vertex_bindings,
            attributes: info.attributes,
            uniform_binding_size_map: info.uniform_binding_size_map,
            uniform_layouts: info.uniform_layouts,
            shader_modules,
            attachment_count: info.attachment_formats.len() as u32,
            attachment_formats: info.attachment_formats,
            config,
            is_compute: info.is_compute,
        };
    }
}

#[derive(Default, Debug)]
pub struct ShaderDescription
{
    pub(crate) entry_names: Vec<String>,

    pub(crate) shader_stages: Vec<vk::ShaderStageFlags>,
    pub(crate) vertex_bindings: Vec<vk::VertexInputBindingDescription>,
    pub(crate) attributes: Vec<vk::VertexInputAttributeDescription>,

    pub(crate) uniform_binding_size_map: BTreeMap<u32, usize>,
    pub(crate) uniform_layouts: Vec<vk::DescriptorSetLayoutBinding<'static>>,

    pub(crate) spirv_sources: Vec<Vec<u8>>,

    pub(crate) attachment_formats: Vec<vk::Format>,
    pub(crate) config: ShaderConfig,

    pub(crate) is_compute: bool,
}

impl ShaderDescription
{
    pub(crate) fn merge(&mut self, uniforms: UniformMetadata)
    {
        for new_layout in uniforms.uniform_blocks
        {
            let existing_layout = &mut self
                .uniform_layouts
                .iter_mut()
                .find(|x| x.binding == new_layout.binding);

            match existing_layout.as_mut()
            {
                Some(val) =>
                {
                    debug_assert!(
                        val.descriptor_type == new_layout.descriptor_type,
                        "Old: {:?} New: {:?}",
                        val.descriptor_type,
                        new_layout.descriptor_type
                    );
                    debug_assert!(val.descriptor_count == new_layout.descriptor_count);
                    debug_assert!(
                        self.uniform_binding_size_map.get(&val.binding).unwrap()
                            == uniforms.uniform_block_sizes.get(&val.binding).unwrap()
                    );
                    val.stage_flags |= uniforms.stage;
                }
                None =>
                {
                    if new_layout.descriptor_type == vk::DescriptorType::UNIFORM_BUFFER
                    {
                        // If the uniform is new and a uniform block, record the size
                        // of the binding location.
                        self.uniform_binding_size_map.insert(
                            new_layout.binding,
                            *uniforms
                                .uniform_block_sizes
                                .get(&new_layout.binding)
                                .unwrap(),
                        );
                    }
                    self.uniform_layouts.push(new_layout);
                }
            }
        }
    }
}

// +| Internal |+ ==============================================================
pub(crate) struct UniformMetadata
{
    pub(crate) uniform_blocks: Vec<vk::DescriptorSetLayoutBinding<'static>>,
    pub(crate) uniform_block_sizes: BTreeMap<u32, usize>,
    pub(crate) stage: vk::ShaderStageFlags,
}

pub(crate) enum ShaderInputConfigs
{
    None,
    DepthTest(bool),
    DepthWrite(bool),
    DepthCompare(vk::CompareOp),
    Topology(vk::PrimitiveTopology),
    LineWidth(f32),
    PolygonMode(vk::PolygonMode),
    Extent(vk::Extent2D),
    CullMode(vk::CullModeFlags),
    FrontFace(vk::FrontFace),
}

fn create_shader_module(device: &ash::Device, spirv_shader: &Vec<u8>)
    -> vk::ShaderModule
{
    let create_info = vk::ShaderModuleCreateInfo {
        code_size: spirv_shader.len() * size_of::<u8>(),
        p_code: spirv_shader.as_ptr() as *const u32,
        ..Default::default()
    };

    unsafe {
        device
            .create_shader_module(&create_info, None)
            .expect("Could not create shader module.")
    }
}

fn get_shader_extension(path: &String) -> &str
{
    let tokens: Vec<&str> = path.split(".").collect();
    let extension = match tokens.len()
    {
        2 | 3 => tokens[1],
        _ => panic!("unrecognized number of shader extensions."),
    };

    extension
}

pub(crate) fn vk_stage_flags_from_string(path: &String) -> vk::ShaderStageFlags
{
    let extension = get_shader_extension(path);
    match extension
    {
        "vert" => vk::ShaderStageFlags::VERTEX,
        "geom" => vk::ShaderStageFlags::GEOMETRY,
        "tesc" => vk::ShaderStageFlags::TESSELLATION_CONTROL,
        "tese" => vk::ShaderStageFlags::TESSELLATION_EVALUATION,
        "frag" => vk::ShaderStageFlags::FRAGMENT,
        "comp" => vk::ShaderStageFlags::COMPUTE,
        _ => panic!("Unrecognized shader extension {}.", extension),
    }
}

pub(crate) fn get_format_from_string(string: &str) -> vk::Format
{
    match string
    {
        "R8G8B8A8Unorm" => vk::Format::R8G8B8A8_UNORM,
        "R32G32B32A32Sfloat" => vk::Format::R32G32B32A32_SFLOAT,
        _ =>
        {
            panic!("Cannot deduce format from {}.", string)
        }
    }
}

pub(crate) fn config_str_to_value(key: &String, val: &String) -> ShaderInputConfigs
{
    match key.as_str()
    {
        "depth_test" =>
        {
            ShaderInputConfigs::DepthTest(bool::from_str(val.as_str()).unwrap())
        }
        "depth_write" =>
        {
            ShaderInputConfigs::DepthWrite(bool::from_str(val.as_str()).unwrap())
        }
        "depth_compare" => match val.as_str()
        {
            "less" => ShaderInputConfigs::DepthCompare(vk::CompareOp::LESS),
            "greater" => ShaderInputConfigs::DepthCompare(vk::CompareOp::GREATER),
            "always" => ShaderInputConfigs::DepthCompare(vk::CompareOp::ALWAYS),
            "never" => ShaderInputConfigs::DepthCompare(vk::CompareOp::NEVER),
            "not equal" => ShaderInputConfigs::DepthCompare(vk::CompareOp::NOT_EQUAL),
            "equal" => ShaderInputConfigs::DepthCompare(vk::CompareOp::EQUAL),
            "greater or equal" =>
            {
                ShaderInputConfigs::DepthCompare(vk::CompareOp::GREATER_OR_EQUAL)
            }
            "less or equal" =>
            {
                ShaderInputConfigs::DepthCompare(vk::CompareOp::LESS_OR_EQUAL)
            }

            _ => panic!(),
        },
        "topology" =>
        {
            use ShaderInputConfigs as SIC;
            match val.as_str()
            {
                "point list" => SIC::Topology(vk::PrimitiveTopology::POINT_LIST),
                "line list" => SIC::Topology(vk::PrimitiveTopology::LINE_LIST),
                "line strip" => SIC::Topology(vk::PrimitiveTopology::LINE_STRIP),
                "path list" => SIC::Topology(vk::PrimitiveTopology::LINE_LIST),
                "triangle fan" => SIC::Topology(vk::PrimitiveTopology::TRIANGLE_FAN),
                "triangle list" => SIC::Topology(vk::PrimitiveTopology::TRIANGLE_LIST),
                "triangle strip" => SIC::Topology(vk::PrimitiveTopology::TRIANGLE_STRIP),
                _ => panic!(),
            }
        }
        "line_width" => ShaderInputConfigs::LineWidth(f32::from_str(val).unwrap()),
        "polygon_mode" =>
        {
            use ShaderInputConfigs as SIC;
            match val.as_str()
            {
                "point" => SIC::PolygonMode(vk::PolygonMode::POINT),
                "line" => SIC::PolygonMode(vk::PolygonMode::LINE),
                "fill" => SIC::PolygonMode(vk::PolygonMode::FILL),
                _ => panic!(),
            }
        }
        "extent" =>
        {
            let split = val.split(",");
            let vec = split.collect::<Vec<&str>>();
            debug_assert!(vec.len() == 2);

            let width = u32::from_str(vec[0].trim()).unwrap();
            let height = u32::from_str(vec[1].trim()).unwrap();

            ShaderInputConfigs::Extent(vk::Extent2D { width, height })
        }
        "cull_mode" =>
        {
            use ShaderInputConfigs as SIC;
            match val.as_str()
            {
                "none" => SIC::CullMode(vk::CullModeFlags::NONE),
                "front" => SIC::CullMode(vk::CullModeFlags::FRONT),
                "back" => SIC::CullMode(vk::CullModeFlags::BACK),
                "front and back" => SIC::CullMode(vk::CullModeFlags::FRONT_AND_BACK),
                _ => panic!(),
            }
        }
        "front_face" =>
        {
            use ShaderInputConfigs as SIC;
            match val.as_str()
            {
                "clockwise" => SIC::FrontFace(vk::FrontFace::CLOCKWISE),
                "counter clockwise" => SIC::FrontFace(vk::FrontFace::COUNTER_CLOCKWISE),
                _ => panic!(),
            }
        }
        "vertex_format" =>
        {
            /* Ignore. This is used by the vertex input parser. */
            ShaderInputConfigs::None
        }

        _ => panic!("Unrecognized {}", key.as_str()),
    }
}
