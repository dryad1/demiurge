#![allow(dead_code)]

use ash::vk::Offset2D;
use ash::{vk, vk::Handle};
use dem_core::rendering::*;

use crate::hardware_interface::*;
use crate::image::*;
use crate::memory::*;

// +| Config |+ ================================================================

static MAX_FRAMES_IN_FLIGHT: u32 = 3;

// +| Public |+ ================================================================

pub(crate) struct Swapchain
{
    hardware_interface: *mut HardwareInterface,
    memory: *mut Memory,

    pub(crate) swapchain: vk::SwapchainKHR,
    pub(crate) images: Vec<vk::Image>,
    pub(crate) image_views: Vec<vk::ImageView>,
    pub(crate) depth_image: VulkanImage,

    pub(crate) render_finished_sems: Vec<vk::Semaphore>,
    pub(crate) in_flight_fences: Vec<vk::Fence>,
    pub(crate) img_available_sems: Vec<vk::Semaphore>,

    pub(crate) selected_format: vk::Format,
    pub(crate) selected_present_mode: vk::PresentModeKHR,

    pub(crate) vsync: VSync,
    pub(crate) extent: vk::Extent2D,
    pub(crate) current_frame: u32,
    pub(crate) active_frame_index: u32,
}

impl Swapchain
{
    pub(crate) fn update(&mut self, vsync: VSync)
    {
        if self.swapchain.as_raw() != 0
        {
            unsafe {
                let sc_device = ash::khr::swapchain::Device::new(
                    &self.hardware_interface().instance,
                    &self.hardware_interface().device,
                );
                sc_device.destroy_swapchain(self.swapchain, None);

                // On recreation, delete the old image views.
                for view in &self.image_views
                {
                    self.hardware_interface()
                        .device
                        .destroy_image_view(*view, None);
                }
            };
        }

        let (swapchain, extent, format, present_mode) =
            create_swapchain(&self.hardware_interface(), MAX_FRAMES_IN_FLIGHT, vsync);

        debug_assert!(extent.width > 0);
        debug_assert!(extent.height > 0);

        let sc_images = unsafe {
            let sc_device = ash::khr::swapchain::Device::new(
                &self.hardware_interface().instance,
                &self.hardware_interface().device,
            );
            sc_device
                .get_swapchain_images(swapchain)
                .expect("Failed to get swapchain images.")
        };

        self.image_views = Vec::with_capacity(sc_images.len());
        for (i, image) in sc_images.iter().enumerate()
        {
            let barrier = vk::ImageMemoryBarrier {
                old_layout: vk::ImageLayout::UNDEFINED,
                new_layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                src_queue_family_index: ash::vk::QUEUE_FAMILY_IGNORED,
                dst_queue_family_index: ash::vk::QUEUE_FAMILY_IGNORED,
                image: sc_images[i],
                subresource_range: vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::COLOR,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                    ..Default::default()
                },

                ..Default::default()
            };

            let command_buffer = self.hardware_interface().begin_single_time_command();
            unsafe {
                self.hardware_interface().device.cmd_pipeline_barrier(
                    command_buffer,
                    vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
                    vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    vk::DependencyFlags::default(),
                    &[],
                    &[],
                    &[barrier],
                );
            }
            self.hardware_interface()
                .end_single_time_command(command_buffer);
            unsafe {
                self.image_views.push(
                    <*const _>::as_ref(self.memory).unwrap().create_image_view(
                        image,
                        format.format,
                        vk::ImageType::TYPE_2D,
                    ),
                );
            }
        }

        self.images = sc_images;
        self.swapchain = swapchain;
        self.extent = extent;
        self.selected_present_mode = present_mode;
        self.selected_format = format.format;
        self.vsync = vsync;

        unsafe {
            self.depth_image = VulkanImage::from_gpu_only_info_2d(
                &mut *self.hardware_interface,
                &mut *self.memory,
                vk::Format::D32_SFLOAT_S8_UINT,
                extent,
                4,
                vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
            );
        }
    }

    pub(crate) fn new(
        hardware_interface: *mut HardwareInterface,
        memory: *mut Memory,
        vsync: VSync,
    ) -> Swapchain
    {
        let mut swapchain = unsafe {
            let render_finished_sems =
                (*hardware_interface).create_semaphores(MAX_FRAMES_IN_FLIGHT);
            let in_flight_fences =
                (*hardware_interface).create_fences(MAX_FRAMES_IN_FLIGHT);
            let img_available_sems =
                (*hardware_interface).create_semaphores(MAX_FRAMES_IN_FLIGHT);

            Swapchain {
                hardware_interface,
                memory,
                vsync,
                swapchain: vk::SwapchainKHR::from_raw(0),
                extent: vk::Extent2D {
                    width: 0,
                    height: 0,
                },
                images: Vec::new(),
                image_views: Vec::new(),
                depth_image: VulkanImage::empty_2d(
                    &mut *hardware_interface,
                    &mut *memory,
                    vk::Format::D32_SFLOAT_S8_UINT,
                    1,
                    vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
                ),
                render_finished_sems,
                in_flight_fences,
                img_available_sems,
                selected_format: vk::Format::B8G8R8A8_UNORM,
                selected_present_mode: vk::PresentModeKHR::FIFO,
                current_frame: 0,
                active_frame_index: 0,
            }
        };

        swapchain.update(vsync);
        debug_assert!(swapchain.swapchain.as_raw() != 0);

        return swapchain;
    }

    pub(crate) fn draw_to_screen(
        &self,
        cmd: &vk::CommandBuffer,
        render_request: &RenderRequest,
    )
    {
        let device = &self.hardware_interface().device;

        let color_attachment = vk::RenderingAttachmentInfoKHR {
            image_view: self.image_views[self.current_frame as usize],
            image_layout: vk::ImageLayout::ATTACHMENT_OPTIMAL_KHR,
            load_op: if render_request.should_clear
            {
                vk::AttachmentLoadOp::CLEAR
            }
            else
            {
                vk::AttachmentLoadOp::LOAD
            },
            store_op: vk::AttachmentStoreOp::STORE,
            clear_value: vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: render_request.color_clear,
                },
            },
            ..Default::default()
        };

        let depth_stencil_attachment = vk::RenderingAttachmentInfoKHR {
            image_view: self.depth_image.image_view,
            image_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            load_op: if render_request.should_clear
            {
                vk::AttachmentLoadOp::CLEAR
            }
            else
            {
                vk::AttachmentLoadOp::LOAD
            },
            store_op: vk::AttachmentStoreOp::STORE,
            clear_value: vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: render_request.depth_clear,
                    stencil: render_request.stencil_clear,
                },
            },
            ..Default::default()
        };

        let rendering_info = vk::RenderingInfoKHR {
            render_area: vk::Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: vk::Extent2D {
                    width: self.extent.width,
                    height: self.extent.height,
                },
            },
            layer_count: 1,
            color_attachment_count: 1,
            p_color_attachments: &color_attachment,
            p_depth_attachment: &depth_stencil_attachment,
            p_stencil_attachment: &depth_stencil_attachment,
            ..Default::default()
        };

        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: self.extent.width as f32,
            height: self.extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let scissor = vk::Rect2D {
            offset: vk::Offset2D {
                x: render_request.scissor.offset.0 as i32,
                y: render_request.scissor.offset.1 as i32,
            },
            extent: vk::Extent2D {
                width: if render_request.scissor.extent.0 == !0
                {
                    self.extent.width
                }
                else
                {
                    render_request.scissor.extent.0
                },
                height: if render_request.scissor.extent.1 == !0
                {
                    self.extent.height
                }
                else
                {
                    render_request.scissor.extent.1
                },
            },
        };
        unsafe {
            device.cmd_set_scissor(*cmd, 0, &[scissor]);
            device.cmd_set_viewport(*cmd, 0, &[viewport]);

            device.cmd_begin_rendering(*cmd, &rendering_info);
            device
                .wait_for_fences(
                    &[self.in_flight_fences[self.current_frame as usize]],
                    true,
                    u64::MAX,
                )
                .expect("To screen drendering fence failed.");
            self.hardware_interface().draw(
                &render_request.vertex_input,
                &render_request.vertex_modifiers,
            );
            device.cmd_end_rendering(*cmd);
        };
    }

    pub(crate) fn get_active_image(&self) -> vk::Image
    {
        return self.images[self.active_frame_index as usize];
    }

    pub(crate) fn sync(&mut self)
    {
        // Try to acquire an image until success or an unrecognized error occurs.
        unsafe {
            let mut result;
            loop
            {
                let sc_device = ash::khr::swapchain::Device::new(
                    &self.hardware_interface().instance,
                    &self.hardware_interface().device,
                );

                result = sc_device.acquire_next_image(
                    self.swapchain,
                    u64::MAX,
                    self.img_available_sems[self.current_frame as usize],
                    vk::Fence::default(),
                );
                match result
                {
                    Ok((index, _)) =>
                    {
                        self.active_frame_index = index;
                        break;
                    }
                    Err(val) => match val
                    {
                        vk::Result::ERROR_OUT_OF_DATE_KHR =>
                        {
                            self.update(self.vsync);
                            break;
                        }
                        _ => continue,
                    },
                }
            }

            let device = &self.hardware_interface().device;
            let graphic_queue = self.hardware_interface().graphics_queue;
            self.hardware_interface()
                .device
                .reset_fences(&[self.in_flight_fences[self.current_frame as usize]])
                .expect("Could not reset fences.");

            let wait_semaphores = &[self.img_available_sems[self.current_frame as usize]];
            let wait_stage: &[vk::PipelineStageFlags] =
                &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
            let signal_sempahores =
                &[self.render_finished_sems[self.current_frame as usize]];
            let submit_info = vk::SubmitInfo {
                wait_semaphore_count: wait_semaphores.len() as u32,
                p_wait_semaphores: wait_semaphores.as_ptr(),
                p_wait_dst_stage_mask: wait_stage.as_ptr(),
                signal_semaphore_count: signal_sempahores.len() as u32,
                p_signal_semaphores: signal_sempahores.as_ptr(),
                ..Default::default()
            };
            self.hardware_interface()
                .device
                .queue_submit(
                    graphic_queue,
                    &[submit_info],
                    self.in_flight_fences[self.current_frame as usize],
                )
                .expect("Command submission failed.");

            device
                .wait_for_fences(
                    &[self.in_flight_fences[self.current_frame as usize]],
                    true,
                    u64::MAX,
                )
                .expect("Failed to wait for fence.");

            let present_info = vk::PresentInfoKHR {
                wait_semaphore_count: signal_sempahores.len() as u32,
                p_wait_semaphores: signal_sempahores.as_ptr(),
                swapchain_count: 1,
                p_swapchains: &self.swapchain,
                p_image_indices: &self.active_frame_index,
                ..Default::default()
            };

            self.hardware_interface().transition_vk_image(
                &self.images[self.active_frame_index as usize],
                vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                vk::ImageLayout::PRESENT_SRC_KHR,
                vk::AccessFlags::COLOR_ATTACHMENT_READ,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            );

            let sc_device = ash::khr::swapchain::Device::new(
                &self.hardware_interface().instance,
                &self.hardware_interface().device,
            );
            let result = sc_device
                .queue_present(self.hardware_interface().graphics_queue, &present_info);

            self.hardware_interface().transition_vk_image(
                &self.images[self.active_frame_index as usize],
                vk::ImageLayout::PRESENT_SRC_KHR,
                vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            );

            match result
            {
                Ok(_) =>
                {}
                Err(vk::Result::ERROR_OUT_OF_DATE_KHR) =>
                {
                    self.update(self.vsync);
                }
                _ => panic!("Error {:?} when rendering.", result),
            }

            self.current_frame = (self.current_frame + 1) % MAX_FRAMES_IN_FLIGHT;
        };
    }

    fn hardware_interface(&self) -> &HardwareInterface
    {
        unsafe { &*self.hardware_interface }
    }

    fn memory(&self) -> &mut Memory { unsafe { &mut *self.memory } }
}

impl Drop for Swapchain
{
    fn drop(&mut self)
    {
        unsafe {
            for semaphore in &self.render_finished_sems
            {
                self.hardware_interface()
                    .device
                    .destroy_semaphore(*semaphore, None);
            }
            for semaphore in &self.img_available_sems
            {
                self.hardware_interface()
                    .device
                    .destroy_semaphore(*semaphore, None);
            }
            for fence in &self.in_flight_fences
            {
                self.hardware_interface().device.destroy_fence(*fence, None);
            }
            for view in &self.image_views
            {
                self.hardware_interface()
                    .device
                    .destroy_image_view(*view, None);
            }

            let sc_device = ash::khr::swapchain::Device::new(
                &self.hardware_interface().instance,
                &self.hardware_interface().device,
            );
            sc_device.destroy_swapchain(self.swapchain, None);
        }
    }
}
// +| Internal |+ ==============================================================

fn create_swapchain(
    hardware_interface: &HardwareInterface,
    min_image_count: u32,
    vsync: VSync,
) -> (
    vk::SwapchainKHR,
    vk::Extent2D,
    vk::SurfaceFormatKHR,
    vk::PresentModeKHR,
)
{
    let selected_format = hardware_interface.get_surface_format();
    let selected_present_mode = select_presentation_mode(&hardware_interface, vsync);

    let surface_capabilities = unsafe {
        let sc_instance = ash::khr::surface::Instance::new(
            &hardware_interface.entry,
            &hardware_interface.instance,
        );

        sc_instance.get_physical_device_surface_capabilities(
            hardware_interface.physical_device,
            hardware_interface.surface,
        )
    };
    let surface_capabilities =
        surface_capabilities.expect("Could not get surface capabilities.");

    let extent = vk::Extent2D {
        width: u32::clamp(
            surface_capabilities.current_extent.width,
            surface_capabilities.min_image_extent.width,
            surface_capabilities.max_image_extent.width,
        ),
        height: u32::clamp(
            surface_capabilities.current_extent.height,
            surface_capabilities.min_image_extent.height,
            surface_capabilities.max_image_extent.height,
        ),
    };

    let create_info = vk::SwapchainCreateInfoKHR {
        surface: hardware_interface.surface,
        min_image_count,
        image_format: selected_format.format,
        image_color_space: selected_format.color_space,
        image_extent: extent,
        image_array_layers: 1,
        image_usage: vk::ImageUsageFlags::COLOR_ATTACHMENT
            | vk::ImageUsageFlags::TRANSFER_SRC,
        image_sharing_mode: vk::SharingMode::EXCLUSIVE,
        queue_family_index_count: 0,
        p_queue_family_indices: std::ptr::null(),
        pre_transform: surface_capabilities.current_transform,
        composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE,
        present_mode: selected_present_mode,
        clipped: vk::TRUE,

        ..Default::default()
    };

    let output = unsafe {
        let sc_device = ash::khr::swapchain::Device::new(
            &hardware_interface.instance,
            &hardware_interface.device,
        );

        sc_device.create_swapchain(&create_info, None)
    };

    let swapchain = match output
    {
        Ok(sw) => sw,
        Err(result) =>
        {
            panic!("Failed to create swapchain with error {}", result)
        }
    };

    return (swapchain, extent, selected_format, selected_present_mode);
}

// +| Helpers |+ ===============================================================
fn select_presentation_mode(
    hardware_interface: &HardwareInterface,
    vsync: VSync,
) -> vk::PresentModeKHR
{
    let present_modes = unsafe {
        let sc_instance = ash::khr::surface::Instance::new(
            &hardware_interface.entry,
            &hardware_interface.instance,
        );

        sc_instance.get_physical_device_surface_present_modes(
            hardware_interface.physical_device,
            hardware_interface.surface,
        )
    };
    let present_modes =
        present_modes.expect("Could not get presentaiton mode for surface.");


    if vsync == VSync::ASYNCHRONOUS
    {
        let mode = present_modes
            .iter()
            .find(|mode| **mode == vk::PresentModeKHR::IMMEDIATE);
        if mode != None
        {
            return *mode.unwrap();
        }
    }

    let mode = present_modes
        .iter()
        .find(|mode| **mode == vk::PresentModeKHR::FIFO);
    if mode != None
    {
        return *mode.unwrap();
    }

    let mode = present_modes
        .iter()
        .find(|mode| **mode == vk::PresentModeKHR::FIFO_RELAXED);
    if mode != None
    {
        return *mode.unwrap();
    }

    let mode = present_modes
        .iter()
        .find(|mode| **mode == vk::PresentModeKHR::MAILBOX);
    if mode != None
    {
        return *mode.unwrap();
    }

    debug_assert!(false, "No compatible present mode was found.");
    return vk::PresentModeKHR::FIFO;
}
