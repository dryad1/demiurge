use std::ffi::CStr;

use ash::*;

// Helper function to convert [c_char; SIZE] to string
pub(crate) fn vk_to_string(raw_string_array: &[i8]) -> String
{
    let raw_string = unsafe {
        let pointer = raw_string_array.as_ptr();
        CStr::from_ptr(pointer)
    };

    raw_string
        .to_str()
        .expect("Failed to convert vulkan raw string.")
        .to_owned()
}

pub(crate) fn format_to_channel_pixel_size(format: vk::Format) -> u32
{
    match format
    {
        vk::Format::R8_UNORM =>
        {
            return 1;
        }
        vk::Format::R8G8_UNORM =>
        {
            return 1;
        }
        vk::Format::R8G8B8A8_SRGB =>
        {
            return 1;
        }
        vk::Format::R8G8B8A8_UNORM =>
        {
            return 1;
        }
        vk::Format::B8G8R8A8_UNORM =>
        {
            return 1;
        }
        vk::Format::R32G32B32A32_SINT =>
        {
            return 4;
        }
        vk::Format::R32_SINT =>
        {
            return 4;
        }
        vk::Format::R32_SFLOAT =>
        {
            return 4;
        }
        vk::Format::R32G32_SFLOAT =>
        {
            return 4;
        }
        vk::Format::R32G32B32_SFLOAT =>
        {
            return 4;
        }
        vk::Format::R32G32B32A32_SFLOAT =>
        {
            return 4;
        }
        vk::Format::R64G64_SFLOAT =>
        {
            return 8;
        }

        vk::Format::D32_SFLOAT_S8_UINT =>
        {
            return 5;
        }
        _ => todo!("{:?}", format),
    }
}
