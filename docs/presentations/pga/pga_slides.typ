#import "@preview/polylux:0.2.0": *

#import themes.simple: *

#set text(font: "Inria Sans")

#show: simple-theme.with(
  footer: [Demiurge],
)

#let slide_block(body) = {
  set align(center)
  rect(
    fill: rgb("eeeeee"),
    inset: 28pt,
    radius: 8pt,
    [
      #set align(left)
      #body
    ]
  )
}

#title-slide[
  = A Short introduction to Projective Geometric Algebra
  #v(2em)

  Makogan #footnote[Demiurge] #h(1em)

]

#slide[
  == Motivation

  What is PGA?

  It's a kind of math that can be used for representing linear transformations. That is rotation, translation, reflections (just like lin alg).

  Why do I care, we have Lin Alg already?

  It's easier ot understand, it's more numerically stable for many applications, it's easier to reason about in many cases. You are already using it (potentially).
]

#slide[
  == Examples of PGA

  #slide_block([
  - Vectors $x e_1 + y e_2 + z e_3$

  - Quaternions $a + b bold(i) + c bold(j) + d bold(k)$

  - Dual Quaternions $A + epsilon B$

  - This ugly thing $(v_1 times v_2) dot v_3$])

]

#centered-slide[
  Linear Algebra
]

#slide[
  == Linear Algebra

  As a gross oversimplification, Linear Algebra is the study of vectors.

  #set align(center)
  #image("images/vector.svg", width: 7cm)
]

#slide[
  == What can you do with a vector?

  #slide_block([
    - Add them $arrow(v_1) + arrow(v_2)$.

    - Multiply them by scalars $x arrow(v)$.

    - Special products $v_1 dot v_2$ or $v_1 times v_2$
  ])
]

#slide[
  == What is a vector?

  It is *not* a tuple $cancel((x, y, z))$. The tuple is a notation shorthand. A vector is this (in 3D):

  $ x e_1 + y e_2 + z e_3  $

  Those e's matter, they are *linear* elements, i.e. the little arrows we sometimes picture in our heads.
]

#slide[
  == Vectors are added together to make new vectors
  #set align(center)
  #image("images/vector_space.svg", width: 10cm)
]

#slide[
  == We can define a special product (inner or dot product)

  Notice how $e_1 + e_2$ cannot be further simplified. It's irreducible to its components.

  From lin alg we all know that $e_1 dot e_1 = 1; e_2 dot e_2 = 1$.

  We could also define the cross product and similar operations. But what if we went a little further?
]

#centered-slide[
  Geometric Algebra
]

#slide[
  == Bivectors

  If a vector is a little oriented length, what if we could represent little oriented areas?

  #set align(center)
  #table(
    columns: (auto, auto, auto),
    stroke: none,
    image("images/bivector_1.svg", width: 180pt),
    image("images/bivector_3.svg", width: 180pt),
    image("images/bivector_2.svg", width: 180pt)
  )
]


#slide[
  == New Basis Element

  We already know $e_1, e_2, e_3$ from classic lin alg, we know that e.g. $e_1 dot e_1 = 1$. But we could define a magic element $e_0$ such that $e_0 dot e_0 = 0$ just as easily.

  If you know about Dual Quaternions, this is the basis for the dual numbers.
]

#slide[
  == Geometric product (1)

  Let's define a magical product between vectors. In 3D:

  #set align(center)
  #set text(
    font: "Linux Libertine",
    size: 14pt,
  )
  #table(
    columns: (auto, auto, auto, auto, auto),
    inset: 10pt,
    [],      [$e_0$],   [$e_1$],    [$e_2$],    [$e_3$],
    [$e_0$], [$0$],     [$e_(01)$], [$e_(02)$], [$e_(03)$],
    [$e_1$], [$e_10$],  [$1$],      [$e_(12)$], [$e_(13)$],
    [$e_2$], [$e_20$],  [$e_(21)$], [$1$],      [$e_(23)$],
    [$e_3$], [$e_30$],  [$e_(31)$], [$e_(32)$],  [$1$]
  )
]

#slide[
  == Geometric product (2)

  #set text(
    font: "Linux Libertine",
    size: 24pt,
  )
  With $e_(32) = -e_(23)$, $e_(31) = -e_(13)$, $e_(21) = -e_(12)$, $e_10 = -e_(10)$, etc.

  And then we can further do $e_1 times e_2 times e_3 = e_(123)$ and $e_0 times e_1 times e_2 times e_3 = e_(0123)$ etc.

  We now can compute something like this:

  #set text(
    font: "Linux Libertine",
    size: 20pt,
  )
  $ &(x_0e_0 + x_1e_1 + x_2e_2 + x_3e_3 + x_4e_1e_2 + x_5e_1e_3 + x_6e_2e_3 + x_7 e_1e_2e_3 + s) \
  times &(y_0e_0 + y_1e_1 + y_2e_2 + y_3e_3 + y_4e_1e_2 + y_5e_1e_3 + y_6e_2e_3 + y_7 e_1e_2e_3 + t) $

  (Don't worry I won't expand it out)
]

#slide[
  == What's the point?

  There are other products we can define, the inner product, the meet product, the join product... But why? What's the point of all of this?
]

#slide[
  == Rotation on a plane in 3D with Lin Alg (1)

  GLM function to rotate around an axis by a given angle:

  #set text(
    font: "Linux Libertine",
    size: 5pt,
  )

  #slide_block(
    ```rs
    template<typename T, qualifier Q>
    GLM_FUNC_QUALIFIER mat<4, 4, T, Q> rotate(mat<4, 4, T, Q> const& m, T angle, vec<3, T, Q> const& v)
    {
      T const a = angle;
      T const c = cos(a);
      T const s = sin(a);

      vec<3, T, Q> axis(normalize(v));
      vec<3, T, Q> temp((T(1) - c) * axis);

      mat<4, 4, T, Q> Rotate;
      Rotate[0][0] = c + temp[0] * axis[0];
      Rotate[0][1] = temp[0] * axis[1] + s * axis[2];
      Rotate[0][2] = temp[0] * axis[2] - s * axis[1];

      Rotate[1][0] = temp[1] * axis[0] - s * axis[2];
      Rotate[1][1] = c + temp[1] * axis[1];
      Rotate[1][2] = temp[1] * axis[2] + s * axis[0];

      Rotate[2][0] = temp[2] * axis[0] + s * axis[1];
      Rotate[2][1] = temp[2] * axis[1] - s * axis[0];
      Rotate[2][2] = c + temp[2] * axis[2];

      mat<4, 4, T, Q> Result;
      Result[0] = m[0] * Rotate[0][0] + m[1] * Rotate[0][1] + m[2] * Rotate[0][2];
      Result[1] = m[0] * Rotate[1][0] + m[1] * Rotate[1][1] + m[2] * Rotate[1][2];
      Result[2] = m[0] * Rotate[2][0] + m[1] * Rotate[2][1] + m[2] * Rotate[2][2];
      Result[3] = m[3];
      return Result;
    }
  ```)
]

#slide[
  == Rotation on a plane in 3D with Lin Alg (2)

  And with that defined:

  #slide_block(
  ```rs
    let axis = v_1.cross(v_2);
    let angle = -(v1.dot(v2).acos());
    let rotation = rotate(angle, axis);
  ```)
]

#slide[
  == Rotation on a plane with PGA

  #slide_block(
  ```rs
    let rotation = (v_2 * v_1).sqrt();
    //                  ^
    //          Geometric product
  ```)
]

#slide[
  == Other advantages

  - Affine matrices use 16 floats.

  - PGA motors use 8 floats.

  - No need for normalization most of the time.

  - Shorter code.
]

#focus-slide[
  == Demo Time!
]


#centered-slide[
  == Questions?
]