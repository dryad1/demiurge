//! Ported to Rust from <https://github.com/Tw1ddle/Sky-Shader/blob/master/src/shaders/glsl/sky.fragment>

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

pub use spirv_std::glam::{Mat2, Vec2, Vec3, Vec4};
use spirv_std::spirv;

#[spirv(fragment)]
pub fn main_fs(color: Vec4, output: &mut Vec4)
{
    *output = Vec4::new(color.x, color.y, color.z, 1.0); //
}

#[spirv(vertex)]
pub fn main_vs(
    #[spirv(uniform, descriptor_set = 1, binding = 0)] transform: &Mat2,
    position_bind0: Vec2,
    color_bind0: Vec3,
    #[spirv(position, invariant)] out_pos: &mut Vec4,
    out_color: &mut Vec4,
)
{
    let res: Vec2 = *transform * position_bind0;
    *out_pos = Vec4::new(res.x, res.y, 0., 1.);

    *out_color = Vec4::new(color_bind0.x, color_bind0.y, color_bind0.z, 1.0);
}
