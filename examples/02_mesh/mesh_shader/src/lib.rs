/*
#ifdef never
[[ne::topology(triangle list)]]
[[ne::line_width(3)]]
[[ne::polygon_mode(fill)]]
#endif
*/

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use spirv_std::{arch::IndexUnchecked, glam::*, image::*, num_traits::Float, spirv};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

fn lambert(
    position: Vec3,
    normal: Vec3,
    albedo: Vec3,
    light_position: Vec3,
    light_color: Vec3,
) -> Vec4
{
    let l = (light_position - position).normalize();

    let n = normal.normalize();

    let color = (l.dot(n) * albedo * light_color).extend(1.);

    return color;
}

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(
    in_normal: Vec3,
    in_uv: Vec3,
    in_position: Vec3,
    #[spirv(descriptor_set = 1, binding = 1)] image: &[SampledTexture2D; 69],
    // use this for boundless arrays
    // #[spirv(descriptor_set = 1, binding = 1)] image: &RuntimeArray<SampledTexture2D>,
    output: &mut Vec4,
)
{
    // let albedo = unsafe { image.index(index).sample(in_uv.xy()) };
    let index = in_uv.z.round() as usize;
    let albedo = unsafe { image.index_unchecked(index).sample(in_uv.xy()) };

    *output = lambert(
        in_position,
        in_normal,
        albedo.xyz(),
        Vec3::new(5., 1000., 5.),
        Vec3::new(1., 1., 1.).normalize() * 1.5 + Vec3::splat(0.5),
    );
}

// Vertex shader
pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    in_normal: Vec3,
    in_uv: Vec3,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] camera_ubo: &CamUBO,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    out_normal: &mut Vec3,
    out_uv: &mut Vec3,
    out_position: &mut Vec3,
)
{
    *screen_pos =
        camera_ubo.proj * camera_ubo.view * camera_ubo.model * in_position.extend(1.0);

    *out_position = (camera_ubo.model * in_position.extend(1.0)).xyz();
    *out_normal = (camera_ubo.model * in_normal.extend(0.0)).xyz();
    *out_uv = in_uv;
}
