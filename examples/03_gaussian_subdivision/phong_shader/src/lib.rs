/*
#ifdef never
[[ne::topology(triangle list)]]
[[ne::line_width(1)]]
[[ne::polygon_mode(fill)]]
[[ne::cull_mode(back)]]
#endif
*/

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use spirv_std::{glam::*, spirv};

fn lambert(
    position: Vec3,
    normal: Vec3,
    albedo: Vec3,
    light_position: Vec3,
    light_color: Vec3,
) -> Vec4
{
    let l = (light_position - position).normalize();

    let n = normal.normalize();

    let color = (l.dot(n).max(0.5) * albedo * light_color).extend(1.);

    return color;
}

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(
    in_normal: Vec3,
    in_position: Vec3,
    output: &mut Vec4
)
{
    let albedo = in_position.normalize();
    *output = lambert(
        in_position,
        in_normal,
        albedo,
        Vec3::new(5., 5., 5.),
        Vec3::new(1., 1., 1.),
    );
}

// Vertex shader
pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    in_normal: Vec3,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] camera_ubo: &CamUBO,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    out_normal: &mut Vec3,
    out_position: &mut Vec3,
)
{
    *screen_pos =
        camera_ubo.proj * camera_ubo.view * camera_ubo.model * in_position.extend(1.0);
    
    *out_normal = (camera_ubo.model * in_normal.extend(0.0)).xyz();
    *out_position = (camera_ubo.model * in_position.extend(1.0)).xyz();
}
