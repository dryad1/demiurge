/*
#ifdef never
[[ne::depth_compare(always)]]
[[ne::topology(line list)]]
[[ne::line_width(3)]]
[[ne::polygon_mode(line)]]
#endif
*/

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use shader_utils::pga::Motor;
use spirv_std::{glam::*, spirv, RuntimeArray};

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(output: &mut Vec4) { *output = Vec4::new(1., 1., 1., 1.); }

// Vertex shader
pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn blend_motors(
    vid: u32,
    inverse_bind_motors: &RuntimeArray<Motor>,
    animation_motors: &RuntimeArray<Motor>,
) -> Motor
{
    let m1 = unsafe {
        animation_motors
            .index(vid as usize)
            .mul(&inverse_bind_motors.index(vid as usize))
    };

    return m1;
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    #[spirv(vertex_index)] in_vertex_id: u32,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] camera_ubo: &CamUBO,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 3)]
    inverse_bind_motors: &RuntimeArray<Motor>,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 4)]
    animation_motors: &RuntimeArray<Motor>,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
)
{
    let skin_motor = blend_motors(in_vertex_id, inverse_bind_motors, animation_motors);
    let position = skin_motor.sandwich(&in_position.extend(1.0));
    let mut position = position / position.w;
    position.w = 1.;

    *screen_pos = camera_ubo.proj * camera_ubo.view * camera_ubo.model * position;
}
