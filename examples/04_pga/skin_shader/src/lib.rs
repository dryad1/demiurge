#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use shader_utils::pga::Motor;
use spirv_std::{
    arch::IndexUnchecked,
    glam::*,
    image::*,
    num_traits::Pow,
    spirv,
    RuntimeArray,
};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

fn blinn_phong(
    position: Vec3,
    normal: Vec3,
    camera_position: Vec3,
    albedo: Vec3,
    light_position: Vec3,
    light_color: Vec3,
    specularity: f32,
) -> Vec4
{
    let l = (light_position - position).normalize();

    let n = (normal).normalize();
    let e = (camera_position - position).normalize();
    let h = (e + l).normalize();

    let intensity = light_color.length();
    let light_color = light_color.normalize();
    let color = (albedo * n.dot(l).max(0.)
        + light_color * h.dot(n).pow(specularity).max(0.))
    .extend(1.);

    color
}

// Fragment shader
#[spirv(fragment)]
pub fn main_fs(
    in_position: Vec3,
    in_normal: Vec3,
    in_uv: Vec3,
    in_cam_pos: Vec3,
    #[spirv(descriptor_set = 1, binding = 1)] textures: &[SampledTexture2D; 1],
    output: &mut Vec4,
)
{
    *output = Vec4::splat(1.);
    let index = in_uv.z as usize;
    let albedo = unsafe { textures.index_unchecked(index).sample(in_uv.xy()) };

    *output = blinn_phong(
        in_position,
        in_normal,
        in_cam_pos,
        albedo.xyz(),
        Vec3::new(5., 5., 15.),
        Vec3::new(1., 1., 1.).normalize(),
        10000.,
    ) + Vec4::splat(0.1);
}

// Vertex shader
pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn blend_motors(
    in_position: &Vec3,
    v_joints: &IVec4,
    v_weights: &Vec4,
    inverse_bind_motors: &RuntimeArray<Motor>,
    animation_motors: &RuntimeArray<Motor>,
) -> Vec4
{
    unsafe {
        let id = v_joints.x as usize;
        let m1 = animation_motors
            .index(id)
            .mul(&inverse_bind_motors.index(id));
        let id = v_joints.y as usize;
        let m2 = animation_motors
            .index(id)
            .mul(&inverse_bind_motors.index(id));
        let id = v_joints.z as usize;
        let m3 = animation_motors
            .index(id)
            .mul(&inverse_bind_motors.index(id));
        let id = v_joints.w as usize;
        let m4 = animation_motors
            .index(id)
            .mul(&inverse_bind_motors.index(id));

        let position = in_position.extend(1.0);
        let p1 = m1.sandwich(&position);
        let p2 = m2.sandwich(&position);
        let p3 = m3.sandwich(&position);
        let p4 = m4.sandwich(&position);

        v_weights.x * p1 + v_weights.y * p2 + v_weights.z * p3 + v_weights.w * p4
    }
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    in_normal: Vec3,
    in_uv: Vec3,
    in_weights: Vec4,
    in_joints: IVec4,
    #[spirv(vertex_index)] in_vertex_id: u32,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] camera_ubo: &CamUBO,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 3)]
    inverse_bind_motors: &RuntimeArray<Motor>,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 4)]
    animation_motors: &RuntimeArray<Motor>,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    out_position: &mut Vec3,
    out_normal: &mut Vec3,
    out_uv: &mut Vec3,
    out_cam_pos: &mut Vec3,
)
{
    let position = in_position.extend(1.);
    let position = blend_motors(
        &in_position,
        &in_joints,
        &in_weights,
        inverse_bind_motors,
        animation_motors,
    );

    *screen_pos = camera_ubo.proj * camera_ubo.view * camera_ubo.model * position;

    *out_position = position.xyz();
    *out_normal = (camera_ubo.model * in_normal.normalize().extend(0.0)).xyz();
    *out_uv = in_uv;
    *out_cam_pos = -camera_ubo.view.col(3).xyz();
}
