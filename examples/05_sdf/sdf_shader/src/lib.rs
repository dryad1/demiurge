#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

pub use spirv_std::glam::{Mat2, Vec2, Vec3, Vec4};
use spirv_std::{arch::IndexUnchecked, image::*, spirv};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;


#[spirv(fragment)]
pub fn main_fs(
    in_uv: Vec2,
    #[spirv(descriptor_set = 1, binding = 1)] sdf: &SampledTexture2D,
    output: &mut Vec4,
)
{
    *output = sdf.sample(in_uv);
}

#[spirv(vertex)]
pub fn main_vs(
    #[spirv(vertex_index)] in_vertex_id: u32,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    uv: &mut Vec2,
)
{
    let vertices = [
        Vec2::new(-1., -1.),
        Vec2::new(1., -1.),
        Vec2::new(-1., 1.),
        Vec2::new(1., -1.),
        Vec2::new(1., 1.),
        Vec2::new(-1., 1.),
    ];

    let coord = unsafe { vertices.index_unchecked(in_vertex_id as usize % 6) };

    *screen_pos = Vec4::new(coord.x, coord.y, 0.5, 1.);
    *uv = *coord / 2. - Vec2::splat(0.5);
}
