#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

pub use spirv_std::glam::*;
use spirv_std::{arch::IndexUnchecked, image::*, num_traits::Float, spirv};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

#[spirv(fragment)]
pub fn main_fs(
    in_uv: Vec2,
    #[spirv(descriptor_set = 1, binding = 1)] sdf: &SampledTexture2D,
    output: &mut Vec4,
)
{
    *output = sdf.sample(in_uv);
}

struct GridData
{
    extents: Vec4,
    corner: Vec4,
    resolution: IVec4,
    cell_size: f32,
    p1: u32,
    p2: u32,
    p3: u32,
}

impl GridData
{
    fn grid_coord(&self, coord: &Vec2) -> usize
    {
        let width = (self.extents.x / self.cell_size).ceil();
        let height = (self.extents.y / self.cell_size).ceil();

        let x = coord.x.clamp(0., width - 1.);
        let y = coord.y.clamp(0., height - 1.);

        return (x * height + y) as usize;
    }
}

#[spirv(vertex)]
pub fn main_vs(
    #[spirv(vertex_index)] in_vertex_id: u32,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] grid_data: &GridData,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    uv: &mut Vec2,
)
{
    let vertices = [
        Vec2::new(-1., -1.),
        Vec2::new(1., -1.),
        Vec2::new(-1., 1.),
        Vec2::new(1., -1.),
        Vec2::new(1., 1.),
        Vec2::new(-1., 1.),
    ];

    let i = in_vertex_id / 6;
    let j = in_vertex_id / 6;

    let x_res = grid_data.extents.x / grid_data.cell_size;
    let vert = unsafe { vertices.index_unchecked(in_vertex_id as usize % 6) };
    let coord = ((*vert + Vec2::new(i as f32, j as f32)) * (x_res * 0.000001 + 1.0)
        - Vec2::new(0.5, 0.5))
        * 2.;

    *screen_pos = Vec4::new(coord.x, coord.y, 0.5, 1.);
    *uv = coord / 2. - Vec2::splat(0.5);
}
