#![allow(dead_code)]
use std::f32::consts::PI;
use std::vec::*;

use algebra::Vec3;
use pga::*;

use crate::lyndermeyer::*;

const ROT_ANGLE: f32 = (1.0 / 8.0) * 2.0 * PI;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Alphabet
{
    Leaf,
    Branch,
    Pop(f32),
    Push(f32),
}

pub fn binary_tree_rules_exp(token: &(String, Vec<ParamType>)) -> String
{
    let mut next_string = "".to_string();
    match token.0.as_str()
    {
        "L" =>
            {
                next_string.push('B');
                next_string.push_str(format!("[({ROT_ANGLE})").as_str());
                next_string.push('L');
                next_string.push_str(format!("](-{ROT_ANGLE})").as_str());
                next_string.push('L');
            }
        "B" =>
            {
                next_string.push('B');
                next_string.push('B');
            }
        _ =>
            {
                next_string.push_str(token_to_string(&token).as_str());
            }
    }

    next_string
}

pub fn geometry_rules_exp(
    mut turtle: Turtle,
    turtle_stack: &mut Vec<Turtle>,
    token: &(String, Vec<ParamType>),
) -> (Turtle, TurtleMovement)
{
    match token.0.as_str()
    {
        "L" =>
            {
                turtle.position += turtle.direction;
                turtle.id += 1;
                (turtle, TurtleMovement::Forward)
            }
        "B" =>
            {
                turtle.position += turtle.direction;
                turtle.id += 1;
                (turtle, TurtleMovement::Forward)
            }
        "[" => {
            turtle_stack.push(turtle.clone());

            let angle = token.1[0].to_float().unwrap() as f32;
            let rot = Rotor::from_axis_angle(angle, &Vec3::new(0.0, 0.0, 1.0));
            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle, TurtleMovement::Stop)
        }
        "]" => {
            turtle = turtle_stack.pop().unwrap();

            let angle = token.1[0].to_float().unwrap() as f32;
            let rot = Rotor::from_axis_angle(angle, &Vec3::new(0.0, 0.0, 1.0));

            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle.clone(), TurtleMovement::Teleport(turtle.id))
        }
       _ => unreachable!()
    }
}
