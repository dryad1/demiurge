use std::vec::*;

use algebra::Vec3;
use dem_gui::egui::TextBuffer;
use nom::bytes::complete::is_not;
use nom::character::char;
use nom::sequence::delimited;
use nom::*;
use nom::bytes::streaming::take;

fn parens(input: &str) -> IResult<&str, &str>
{
    delimited(char('('), is_not(")"), char(')')).parse(input)
}

pub fn trim_whitespace(s: &str) -> String {
    let mut result = String::with_capacity(s.len());
    s.split_whitespace().for_each(|w| {
        if !result.is_empty() {
            result.push(' ');
        }
        result.push_str(w);
    });
    result
}

#[derive(Debug)]
pub enum ParamType {
    String(String),
    Integer(i64),
    Float(f64),
}

impl ParamType {
    pub fn to_string(&self) -> Option<&str> {
        match self { Self::String(s) => Some(s), _ => None }
    }

    pub fn to_integer(&self) -> Option<i64> {
        match self { Self::Integer(s) => Some(*s), _ => None }
    }

    pub fn to_float(&self) -> Option<f64> {
        match self { Self::Float(s) => Some(*s), _ => None }
    }
}

pub fn parse_token(input: &str) -> ParamType {
    let input = input.trim();

    let res = input.parse::<i64>();
    if res.is_ok() {
        return ParamType::Integer(res.unwrap());
    }

    let res = input.parse::<f64>();
    if res.is_ok() {
        return ParamType::Float(res.unwrap());
    }

    ParamType::String(input.to_string())
}

fn character(s: &str) -> IResult<&str, &str> {
    take(1_usize)(s)
}

pub struct LGrammarIterator<'a> {
    current: &'a str,
}

impl <'a> LGrammarIterator<'a> {
    pub fn new(input: &'a str) -> LGrammarIterator<'a> {
        Self{
            current: input,
        }
    }
}

impl Iterator for LGrammarIterator<'_> {
    type Item = (String, Vec<ParamType>);

    fn next(&mut self) -> Option<Self::Item> {
        if self.current.is_empty() {
            return None;
        }

        let (mut new_tail, current_symbol) =  character(self.current).unwrap();
        let mut out_params = Vec::new();
        if !new_tail.is_empty() && new_tail.chars().nth(0).unwrap() == '(' {
            let (nn_tail, params) = parens(new_tail).unwrap();

            for param in params.split(',') {
                out_params.push(parse_token(&param));
            }

            new_tail = nn_tail;
        }

        self.current = new_tail;

        Some((current_symbol.to_string(), out_params))
    }
}

pub fn expand_pattern(
    axiom: &str,
    rules: &dyn Fn(&(String, Vec<ParamType>)) -> String,
    iterations: u32,
) -> String
{
    let mut res = axiom.to_string();
    for _ in 0..iterations {
        let mut new_res = "".to_string();
        for token in LGrammarIterator::new(res.as_str()) {
            let expanded = rules(&token);
            new_res.push_str(expanded.as_str())
        }
        res = new_res;
    }

    res
}

pub fn token_to_string(token: &(String, Vec<ParamType>)) -> String {
    let mut res = token.0.clone();
    if token.1.is_empty() {
        return res;
    }

    res.push('(');
    for param in &token.1 {
        let val = match param {
            ParamType::String(s) => s.clone(),
            ParamType::Float(f) => f.to_string(),
            ParamType::Integer(int) => int.to_string(),
        };

        res.push_str(&val);
        res.push_str(", ");
    }

    res.pop();
    res.pop();
    res.push(')');

    res
}

pub fn strip_arguments(
    instructions: &str,
) -> String
{
    let mut res = "".to_string();
    for token in LGrammarIterator::new(instructions) {
        res.push_str(&token.0)
    }

    res
}

#[derive(Debug, Clone)]
pub struct Turtle
{
    pub position: Vec3,
    pub direction: Vec3,
    pub id: usize,
}

pub enum TurtleMovement
{
    Forward,
    Stop,
    Teleport(usize), // Id of the turtle we teleported to.
}

pub fn generate_topological_skeleton(
    sequence: &str,
    geometry_rules: &dyn Fn(
        Turtle,
        &mut Vec<Turtle>,
        &(String, Vec<ParamType>),
    ) -> (Turtle, TurtleMovement),
) -> (Vec<Vec3>, Vec<usize>)
{
    let mut turtle_stack = Vec::<Turtle>::new();

    let mut turtle = Turtle {
        position: Vec3::new(0.0, 0.0, 0.0),
        direction: Vec3::new(0.0, 1.0, 0.0),
        id: 0,
    };

    let mut skeleton_points = vec![turtle.position];
    let mut skeleton = vec![0];

    let mut turtle_count = 0;
    let mut parent = 0;
    for token in LGrammarIterator::new(sequence)
    {
        let (new_turtle, turtle_instruction) =
            geometry_rules(turtle.clone(), &mut turtle_stack, &token);

        match turtle_instruction
        {
            TurtleMovement::Forward =>
            {
                skeleton.push(parent);
                turtle_count += 1;
                // After a teleport the parent may be a random index. Restore the
                // id to the current turtle count.
                parent = turtle_count;

                skeleton_points.push(new_turtle.position);
            }
            // If the turtle teleported then the parent is different.
            TurtleMovement::Teleport(id) =>
            {
                parent = id;
            }
            _ =>
            {}
        }
        turtle = new_turtle;
        turtle.id = turtle_count; // Always restore the current turtle id at the
                                  // end.
    }

    (skeleton_points, skeleton)
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests
{
    use std::f32::consts::PI;
    use crate::binary_tree_rules::binary_tree_rules_exp;
    use super::*;
    const ROT_ANGLE: f32 = (1.0 / 8.0) * 2.0 * PI;

    #[derive(Debug, Clone, Copy, PartialEq)]
    pub enum Alphabet
    {
        Leaf,
        Branch,
        Pop(f32),
        Push(f32),
    }

    #[test]
    fn test_generate_sequence()
    {
        let result = expand_pattern(&"L", &binary_tree_rules_exp, 2);
        println!("{:?}", strip_arguments(&result));
        assert!(strip_arguments(&result) == "BB[B[L]L]B[L]L");
    }
}
