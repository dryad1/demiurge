use std::rc::Rc;
use std::{cell::RefCell, mem::size_of};

use algebra::*;
use camera::{ArcballCamera, CameraProperties};
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use dual_contouring::dual_contouring;
use geometry::compute_normals;
use geometry::sdf;
use graphs::compact_line_graph;
use graphs::depth_first_search::DFSIterator;
use graphs::skeleton_hierarchy_to_graph;
use lyndermeyer::*;
use pga::*;
use topology::find_leaves;
use voronoi_reconstruction::*;
use vulkan_bindings::RenderContext;

use crate::skeleton_graph::SkeletonGraph;

#[allow(dead_code)]
mod binary_tree_rules;
mod fractal_plant_rules;
mod lyndermeyer;
mod skeleton_graph;

#[inline]
pub fn bytes_of<T>(t: &T) -> &[u8]
{
    unsafe { std::slice::from_raw_parts(t as *const T as *const u8, size_of::<T>()) }
}

fn generate_cylinder(
    p1: &CPoint,
    p2: &CPoint,
    radius: f32,
) -> (Vec<Vec3>, Vec<Vec3>, Vec<usize>)
{
    let c = MultiVec3D::from_vec3(&p1.to_vec());
    let d = MultiVec3D::from_vec3(&p2.to_vec());

    let a = MultiVec3D::from_vec3(&Vec3::new(0.0, 0.0, 0.0));
    let b = MultiVec3D::from_vec3(&Vec3::new(0.0, 1.0, 0.0));

    let r0 = MultiVec3D::sqrt(c / a); // Translation from a to c.
    let l2 = c & d; // Make target line.
    let l1 = a & b; // Make starting line.
    let l1 = r0 * l1 / r0; // offset the starting line to the joint.

    let motor = MultiVec3D::sqrt(l2 / l1) * r0;

    let (mut verts, connect) = dual_contouring(
        &|p: &Vec3| {
            let a = Vec3::new(0.0, 0.0, 0.0);
            let b = Vec3::new(0.0, 1.0, 0.0);

            sdf::capsule(p, &a, &b, radius)
        },
        (10, 20, 10),
        &Vec3::new(-radius * 1.1, -radius, -radius * 1.1),
        &Vec3::new(2.1 * radius, 1.0 + radius * 2.0, 2.1 * radius),
    );

    let mut normals = compute_normals(&verts, &connect);

    for v in &mut verts
    {
        *v = (motor * CPoint::from_vector(v) * motor.reverse()).to_vec();
    }

    for n in &mut normals
    {
        *n = (motor * MultiVec3D::from_vec3(n) * motor.reverse()).to_vec();
    }

    (verts, normals, connect)
}

fn compute_trunk_weights(skeleton: &Vec<usize>) -> Vec<i32>
{
    let leaves = find_leaves(skeleton);
    let mut weights = vec![0; skeleton.len()];

    for i in leaves
    {
        let mut node = i;
        let mut distance = 0;
        loop
        {
            distance += 1;
            weights[node] += distance;

            // Only the root is its own parent.
            if skeleton[node] == node
            {
                break;
            }

            node = skeleton[node];
        }
    }

    weights
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct Mvp
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn init<'io>() -> (IoContext<'io>, RenderContext, Rc<RefCell<ArcballCamera>>)
{
    let mut io_context = IoContext::new("L system", (800, 800), true);
    let render_context = RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update
                {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    (io_context, render_context, camera)
}

fn main()
{
    // let (mut io_context, mut render_context, camera) = init();

    // let plant_shader =
    //     render_context.add_shader(vec!["examples/08_l_system/plant_shader/".
    // to_string()]);

    // let result = generate_sequence(
    //     &vec![binary_tree_rules::Alphabet::Leaf],
    //     &binary_tree_rules::binary_tree_rules,
    //     5,
    // );
    // let (skeleton_points, skeleton) =
    //     generate_topological_skeleton(&result, &binary_tree_rules::geometry_rules);

    let result = expand_pattern(
        &"L",
        &binary_tree_rules::binary_tree_rules_exp,
        5,
    );

    let (skeleton_points, skeleton) =
        generate_topological_skeleton(&result, &binary_tree_rules::geometry_rules_exp);

    let trunk_weights = compute_trunk_weights(&skeleton);

    // let mut verts = Vec::new();
    // let mut normals = Vec::new();
    // let mut connect = Vec::new();

    let skt = skeleton.iter().map(|i| *i as u64).collect();
    let graph = skeleton_hierarchy_to_graph(&skt);

    let skeleton_graph =
        SkeletonGraph::from_graph(skeleton_points.clone(), graph.clone(), true);

    let root = 0 as u64;
    let (chains, sparse_graph, edge_chain_map) = compact_line_graph(root, &graph);

    let tree_sdf_exp = |p: &Vec3| {
        let (proj, d, edge) = skeleton_graph.project(p);
        let parent = edge[0].min(edge[1]);
        let depth = skeleton_graph.node_distance(parent);

        let pp = skeleton_graph.pos(parent);
        let param = (pp - proj).norm() + depth;

        let radius = 0.8;
        let decay = 0.5;

        d - radius * (-param * 2.5 / skeleton_graph.max_distance()).exp()
    };

    let core_distance = |p: &Vec3| {
        let mut final_depth = f32::MAX;
        DFSIterator::new(root as usize, |node, depth, parent| {
            if depth > 0
            {
                let mut key = [*node as u64, *parent as u64];
                key.sort();

                let chain_id = edge_chain_map.get(&key).unwrap();

                let mut tendril: Vec<_> = chains[*chain_id]
                    .iter()
                    .map(|i| skeleton_points[*i as usize].clone())
                    .collect();
                tendril.reverse();

                let d = sdf::polyline_sdf(p, &tendril, 0., 1.0);
                final_depth = d.min(final_depth);
            }

            sparse_graph
                .get(&(*node as u64))
                .expect(format!("{}", node).as_str())
                .iter()
                .map(|n| *n as usize)
        })
        .count();

        final_depth
    };

    let radius = trunk_weights[0] as f32;

    use std::time::{Duration, Instant};
    let now = Instant::now();
    let (verts, connect) =
        voronoi_meshing(&tree_sdf_exp, |p| (core_distance(p) / 2.5).max(0.001));
    println!("elapsed voronoi {} ms", now.elapsed().as_millis());

    let connect = connect.iter().map(|l| l.to_vec()).collect::<Vec<_>>();
    use chicken_wire::wavefront_loader::{ColoredVerts, ObjData};
    ObjData::export(&(&verts, &connect), "sdf_mesh.obj");

    const SAVE_FIGURE: bool = false;

    std::process::exit(0);

    // let (mut verts, connect) = dual_contouring(
    //     &tree_sdf,
    //     (200, 300, 200),
    //     &Vec3::new(-10., -10., -10.),
    //     &Vec3::new(20., 40., 20.),
    // );

    let connect = connect
        .iter()
        .flat_map(|l| l.iter().copied())
        .collect::<Vec<_>>();
    let mut normals = compute_normals(&verts, &connect);

    // while io_context.poll_io(&mut None)
    // {
    //     render_context.start_frame(&io_context);

    //     let connect: Vec<_> = connect.iter().map(|&i| i as u32).collect();
    //     let gpu_mesh = GraphicsInput::new()
    //         .add_attribute_buffer_from_slice(&verts, &mut render_context)
    //         .add_attribute_buffer_from_slice(&normals, &mut render_context)
    //         .set_index_buffer_from_slice(&connect, &mut render_context);

    //     let mvp = Mvp {
    //         model: Mat4::identity(),
    //         view: camera.borrow().view_matrix(),
    //         proj: camera.borrow().proj_matrix(),
    //     };

    //     let render_request = RenderRequest::new(plant_shader)
    //         .vertex_input(gpu_mesh.clone())
    //         .add_ubo(0, mvp)
    //         .build();

    //     render_context.draw(&render_request);
    //     render_context.free_input(gpu_mesh);
    // }
}
