#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

pub use spirv_std::glam::*;
use spirv_std::spirv;

#[spirv(fragment)]
pub fn main_fs(in_normal: Vec4, out_color: &mut Vec4)
{
    *out_color = in_normal.xyz().extend(1.0);
}

pub struct CamUBO
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

#[spirv(vertex)]
pub fn main_vs(
    in_position_bind0: Vec3,
    in_normal_bind1: Vec3,
    #[spirv(uniform, descriptor_set = 1, binding = 0)] cam_ubo: &CamUBO,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    out_normal: &mut Vec4,
)
{
    *screen_pos =
        cam_ubo.proj * cam_ubo.view * cam_ubo.model * in_position_bind0.extend(1.0);
    *out_normal = cam_ubo.model * in_normal_bind1.extend(1.0);
}
