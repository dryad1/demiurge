use std::collections::BTreeMap;
use std::collections::BTreeSet;

use algebra::*;
use geometry::project_point_onto_segment;
use graphs::compact_line_graph;
use graphs::depth_first_search::DFSIterator;
use rstar::RTree;
use tritet::Tetgen;
use voronoi_reconstruction::TreePoint;

pub struct SkeletonGraph
{
    points: Vec<Vec3>,
    topology: Vec<Vec<u64>>,
    /// Polyline edges of the sparse graph.
    chains: Vec<Vec<u64>>,
    /// Graph structure when polyline subgraphs are treated as edges.
    sparse_graph: BTreeMap<u64, Vec<u64>>,
    /// Map from a sparse edge to the chain of nodes in the underlying graph
    /// that make up its polyline.
    edge_chain_map: BTreeMap<[u64; 2], usize>,
    /// Depth of a node in the sparse graph.
    depths: Vec<usize>,
    /// Distance from the root to this node in world units.
    distances: Vec<f32>,
    max_distance: f32,
    /// Voronoi diagram of the graph points. Used as an acceleration structure.
    voronoi_diagram: Vec<BTreeSet<usize>>,
    /// Rtree of the graph points. Used as an acceleration structure for nearest
    /// neighbour computations.
    rtree: RTree<TreePoint>,
}

impl SkeletonGraph
{
    /// Create a `SkeletonGraph` from a graph, assuming that the root is the 0
    /// node.
    pub fn from_graph(points: Vec<Vec3>, topology: Vec<Vec<u64>>, planar: bool) -> Self
    {
        let root = 0 as u64;
        let (chains, sparse_graph, edge_chain_map) = compact_line_graph(root, &topology);

        let mut depths = vec![0; points.len()];
        DFSIterator::new(root as usize, |node, depth, _parent| {
            depths[*node] = depth;

            sparse_graph
                .get(&(*node as u64))
                .expect(format!("{}", node).as_str())
                .iter()
                .map(|n| *n as usize)
        })
        .count();

        let mut max_distance = 0.;
        let mut distances = vec![0.; points.len()];
        DFSIterator::new(root as usize, |node, depth, parent| {
            distances[*node] +=
                distances[*parent] + (points[*node] - points[*parent]).norm();
            max_distance = max_distance.max(distances[*node]);

            if depths[*node] == 0
            {
                depths[*node] = depth;
            }

            topology[*node].iter().map(|i| *i as usize)
        })
        .count();

        let mut rtree = RTree::new();
        let mut tetgen =
            Tetgen::new(points.len() + planar as usize * 2, None, None, None).unwrap();
        for (i, p) in points.iter().enumerate()
        {
            tetgen
                .set_point(i, 0, p.x as f64, p.y as f64, p.z as f64)
                .unwrap();
            rtree.insert(TreePoint(*p, i));
        }

        if planar
        {
            tetgen.set_point(points.len(), 0, 0., 0., 10.).unwrap();
            tetgen.set_point(points.len() + 1, 0, 0., 0., -10.).unwrap();
        }

        tetgen.generate_delaunay(false).unwrap();

        let mut voronoi_diagram = vec![BTreeSet::new(); points.len()];
        for index in 0..tetgen.out_ncell()
        {
            let tet_points: Vec<_> = (0..tetgen.out_cell_npoint())
                .map(|m| tetgen.out_cell_point(index, m))
                .collect();

            for i in 0..tet_points.len()
            {
                let p1 = tet_points[i];
                let p2 = tet_points[(i + 1) % tet_points.len()];

                if p1 >= points.len() || p2 >= points.len()
                {
                    continue;
                }
                voronoi_diagram[p1].insert(p2);
                voronoi_diagram[p2].insert(p1);
            }
        }

        let mut dbg = Vec::new();
        for index in 0..tetgen.out_ncell()
        {
            let tet_points: Vec<_> = (0..tetgen.out_cell_npoint())
                .map(|m| tetgen.out_cell_point(index, m))
                .collect();

            for i in 0..tet_points.len()
            {
                let p1 = tet_points[i];
                let p2 = tet_points[(i + 1) % tet_points.len()];

                if p1 >= points.len() || p2 >= points.len()
                {
                    continue;
                }

                dbg.push([p1, p2]);
            }
        }

        use chicken_wire::wavefront_loader::{ColoredVerts, ObjData};
        let mut colors = Vec::new();
        for &v in &distances
        {
            colors.push(Vec3::new(v, v, v) * 100.0);
        }
        ObjData::export(
            &ColoredVerts {
                points: &points,
                colors: &colors,
            },
            "skeleton_colors.obj",
        );
        ObjData::export(&(&points, &dbg), "dbg_edge.obj");

        Self {
            points,
            topology,
            sparse_graph,
            edge_chain_map,
            chains,
            depths,
            distances,
            max_distance,
            voronoi_diagram, // change this to voronoi diagram of sparse graph instead.
            rtree,
        }
    }

    pub fn project(&self, p: &Vec3) -> (Vec3, f32, [usize; 2])
    {
        let nearest_neighbour = self
            .rtree
            .nearest_neighbor(&TreePoint(*p, usize::MAX))
            .unwrap();

        let mut closest_distance = f32::MAX;
        let mut projection = Vec3::default();
        let mut endpoints = [0, 0];

        let mut dbg = Vec::new();
        for c in self.voronoi_diagram[nearest_neighbour.1]
            .iter()
            .chain([nearest_neighbour.1 as usize].iter())
        {
            // This is a fake point that was added to break coplanarity.
            if *c >= self.points.len()
            {
                continue;
            }
            for n in self.topology[*c].iter()
            {
                let proj = project_point_onto_segment(
                    &self.points[*c as usize],
                    &self.points[*n as usize],
                    p,
                );

                dbg.push([*c as usize, *n as usize]);

                let distance = (proj - p).norm_squared();
                if distance < closest_distance
                {
                    closest_distance = distance;
                    projection = proj;
                    endpoints = [*c as usize, *n as usize];
                }
            }
        }

        (projection, closest_distance.sqrt(), endpoints)
    }

    pub fn node_depth(&self, nid: usize) -> usize { self.depths[nid] }

    pub fn node_distance(&self, nid: usize) -> f32 { self.distances[nid] }

    pub fn pos(&self, nid: usize) -> Vec3 { self.points[nid] }

    pub fn max_distance(&self) -> f32 { self.max_distance }
}
