/*
#ifdef never
[[ne::topology(triangle strip)]]
[[ne::line_width(3)]]
[[ne::polygon_mode(fill)]]
#endif
 */

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use shader_utils::ray_tracing::*;
use shader_utils::voxel_oct_tree::*;
use spirv_std::{
    arch::IndexUnchecked,
    glam::*,
    image::*,
    memory::{Scope, Semantics},
    spirv,
    RuntimeArray,
};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;

// useful:
// https://github.com/EmbarkStudios/rust-gpu/blob/54f6978c25b7e168ded04e720b996625b3654ebd/crates/rustc_codegen_spirv/src/symbols.rs#L42
#[spirv(fragment)]
pub fn main_fs(
    #[spirv(descriptor_set = 1, binding = 0)] volume: &Storage3D,
    #[spirv(uniform, descriptor_set = 1, binding = 1)] window_data: &WindowData,
    #[spirv(uniform, descriptor_set = 1, binding = 2)] voctree_meta: &VoctreeMetadata,
    #[spirv(uniform, descriptor_set = 1, binding = 3)] volume_data: &VoxelVolumeMeta,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 4)] counter: &mut [u32; 1],
    #[spirv(storage_buffer, descriptor_set = 1, binding = 5)] nodes: &mut RuntimeArray<
        Node<SimpleNodeData>,
    >,
    #[spirv(frag_coord)] screen_pos: Vec4,
    out_color: &mut Vec4,
)
{
    let window_dimensions = window_data.dimensions.xy();
    let view_matrix = window_data.view_matrix;
    let screen_position = screen_pos.xy();

    let display_width = window_dimensions.x;
    let display_height = window_dimensions.y;
    let aspect_ratio = display_width / display_height;

    let x = screen_position.x * 2.0 / display_width - 1.0;
    let y = screen_position.y * 2.0 / display_height - 1.0;

    let screen_ray = generate_ray(&Vec2::new(x, y), aspect_ratio, 45.);
    let ray = (view_matrix.inverse() * screen_ray.extend(0.)).xyz();
    let cam_pos = (view_matrix.inverse() * Vec4::new(0., 0., 0., 1.)).xyz();

    let node_count = unsafe { counter.index_unchecked_mut(0_usize) };
    let voctree =
        GpuVoctree::<SimpleNodeData>::new(voctree_meta.clone(), node_count, nodes);

    let (cam_pos, ray) = create_ray_from_camera_data(
        &screen_pos.xy(),
        &window_data.dimensions.xy(),
        &window_data.view_matrix,
    );

    let (was_hit, hit_node, iter_count) = trace_svo(cam_pos, ray, &voctree);

    // let (was_hit, color) = trace_voxel_grid(cam_pos, ray, &volume_data, &volume);
    // *out_color = color.xyz().extend(1.0);

    if was_hit
    {
        *out_color = voctree.node_at(hit_node as usize).data.color;
    }
    else
    {
        *out_color = Vec3::splat(0.).extend(1.);
    }
}

#[spirv(vertex)]
pub fn main_vs(
    #[spirv(vertex_index)] in_vertex_id: u32,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
)
{
    let vertices = [
        Vec3::new(-100.0, -100.0, 0.0),
        Vec3::new(100.0, -1000.0, 0.0),
        Vec3::new(0.0, 100.0, 0.0),
    ];

    let coord = unsafe { vertices.index_unchecked(in_vertex_id as usize).extend(1.0) };
    *screen_pos = coord;
}
