#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![allow(internal_features)]

extern crate bytemuck;
extern crate spirv_std;

use shader_utils::{atomic_inc_u32, voxel_oct_tree::*};
use spirv_std::{
    arch::IndexUnchecked,
    glam::*,
    image::*,
    memory::{Scope, Semantics},
    num_traits::Float,
    spirv,
    RuntimeArray,
};

type Texture2D = Image!(2D, type=f32, sampled);
type SampledTexture2D = SampledImage<Texture2D>;
type Storage3D = Image!(3D, sampled = false, format = rgba32f);

#[spirv(fragment)]
pub fn main_fs(
    in_normalized_position: Vec3,
    in_position: Vec3,
    in_uv: Vec3,
    in_normal: Vec3,
    #[spirv(descriptor_set = 1, binding = 0)] volume: &Storage3D,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 4)] counter: &mut [u32; 1],
    #[spirv(storage_buffer, descriptor_set = 1, binding = 5)] nodes: &mut RuntimeArray<
        SimpleNodeData,
    >,
    #[spirv(storage_buffer, descriptor_set = 1, binding = 6)] corners: &mut RuntimeArray<
        Vec4,
    >,
    #[spirv(descriptor_set = 1, binding = 7)] images: &[SampledTexture2D; 69],
)
{
    let index = in_uv.z.round() as usize;
    let mut albedo = unsafe { images.index_unchecked(index).sample(in_uv.xy()) };

    albedo.w = 1.;

    let node_count = unsafe { counter.index_unchecked_mut(0_usize) };
    let index = atomic_inc_u32(node_count);
    let node = unsafe { nodes.index_mut(index as usize) };
    *node = SimpleNodeData {
        color: albedo,
        normal: in_normal.extend(1.),
    };
    let corner = unsafe { corners.index_mut(index as usize) };
    *corner = (in_position).extend(1.);


    // TODO(high): make this number a uniform.
    let coords = in_normalized_position * 128.0;
    unsafe {
        volume.write(
            IVec3::new(coords.x as i32, coords.y as i32, coords.z as i32),
            albedo,
        )
    };
}

#[spirv(vertex)]
pub fn main_vs(
    in_position: Vec3,
    in_normal: Vec3,
    in_uv: Vec3,
    #[spirv(uniform, descriptor_set = 1, binding = 8)] normalization_mat: &Mat4,
    #[spirv(position, invariant)] screen_pos: &mut Vec4,
    out_normalized_position: &mut Vec3,
    out_position: &mut Vec3,
    out_uv: &mut Vec3,
    out_normal: &mut Vec3,
)
{
    let normal = *normalization_mat * in_normal.extend(0.0);
    let position = *normalization_mat * in_position.extend(1.0);

    if normal.x > normal.y && normal.x > normal.z
    {
        *screen_pos = Vec4::new(position.y, position.z, 0.1, 1.0);
    }
    else if normal.y > normal.z
    {
        *screen_pos = Vec4::new(position.z, position.x, 0.1, 1.0);
    }
    else
    {
        *screen_pos = Vec4::new(position.x, position.y, 0.1, 1.0);
    }

    *out_uv = in_uv;
    *out_normalized_position = (position.xyz() + 1.) / 2.;
    *out_position = in_position;
    *out_normal = in_normal;
}
