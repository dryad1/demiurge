#![allow(dead_code)]
use std::f32::consts::PI;
use std::vec::*;

use algebra::Vec3;
use pga::*;

use crate::lyndermeyer::*;

const ROT_ANGLE: f32 = (1.0 / 8.0) * 2.0 * PI;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Alphabet {
    Leaf,
    Branch,
    Pop(f32),
    Push(f32),
}

pub fn binary_tree_rules(symbol: &Alphabet) -> Vec<Alphabet> {
    let mut next_string = Vec::<Alphabet>::new();
    match &symbol {
        Alphabet::Leaf => {
            next_string.push(Alphabet::Branch);
            next_string.push(Alphabet::Push(ROT_ANGLE));
            next_string.push(Alphabet::Leaf);
            next_string.push(Alphabet::Pop(-ROT_ANGLE));
            next_string.push(Alphabet::Leaf);
        }
        Alphabet::Branch => {
            next_string.push(Alphabet::Branch);
            next_string.push(Alphabet::Branch);
        }
        val => {
            next_string.push(**val);
        }
    }

    next_string
}

pub fn geometry_rules(
    mut turtle: Turtle,
    turtle_stack: &mut Vec<Turtle>,
    symbol: &Alphabet,
) -> (Turtle, TurtleMovement) {
    match *symbol {
        Alphabet::Branch => {
            turtle.position += turtle.direction;
            turtle.id += 1;
            (turtle, TurtleMovement::Forward)
        }
        Alphabet::Leaf => {
            turtle.position += turtle.direction;
            turtle.id += 1;
            (turtle, TurtleMovement::Forward)
        }
        Alphabet::Pop(angle) => {
            turtle = turtle_stack.pop().unwrap();

            let rot = Rotor::from_axis_angle(angle, &Vec3::new(0.0, 0.0, 1.0));

            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle.clone(), TurtleMovement::Teleport(turtle.id))
        }
        Alphabet::Push(angle) => {
            turtle_stack.push(turtle.clone());

            let rot = Rotor::from_axis_angle(angle, &Vec3::new(0.0, 0.0, 1.0));
            turtle.direction = rot
                .sandwich(&CPoint::from_vector(&turtle.direction))
                .to_vec();
            (turtle, TurtleMovement::Stop)
        }
    }
}
