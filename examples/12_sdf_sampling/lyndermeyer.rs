use std::vec::*;

use algebra::Vec3;

pub fn generate_sequence<Alphabet>(
    axiom: &Vec<Alphabet>,
    rules: &dyn Fn(&Alphabet) -> Vec<Alphabet>,
    iterations: u32,
) -> Vec<Alphabet>
where
    Alphabet: Clone,
{
    let mut current_string = (*axiom).clone();
    for _ in 0..iterations {
        let mut next_string = Vec::<Alphabet>::new();
        for symbol in &current_string {
            next_string.append(&mut rules(symbol));
        }
        current_string = next_string;
    }
    current_string
}

#[derive(Debug, Clone)]
pub struct Turtle {
    pub position: Vec3,
    pub direction: Vec3,
    pub id: usize,
}

pub enum TurtleMovement {
    Forward,
    Stop,
    Teleport(usize), // Id of the turtle we telported to.
}

pub fn generate_topological_skeleton<Alphabet>(
    sequence: &Vec<Alphabet>,
    geometry_rules: &dyn Fn(
        Turtle,
        &mut Vec<Turtle>,
        &Alphabet,
    ) -> (Turtle, TurtleMovement),
) -> (Vec<Vec3>, Vec<usize>) {
    let mut turtle_stack = Vec::<Turtle>::new();

    let mut turtle = Turtle {
        position: Vec3::new(0.0, 0.0, 0.0),
        direction: Vec3::new(0.0, 1.0, 0.0),
        id: 0,
    };

    let mut skeleton_points = vec![turtle.position];
    let mut skeleton = vec![0];

    let mut turtle_count = 0;
    let mut parent = 0;
    for symbol in sequence {
        let (new_turtle, draw_state) =
            geometry_rules(turtle.clone(), &mut turtle_stack, symbol);

        match draw_state {
            TurtleMovement::Forward => {
                skeleton.push(parent);
                turtle_count += 1;
                // After a teleport the parent may be a random index. Restore the
                // id to the current turtle count.
                parent = turtle_count;

                skeleton_points.push(new_turtle.position);
            }
            // If the turtle teleported then the parent is different.
            TurtleMovement::Teleport(id) => {
                parent = id;
            }
            _ => {}
        }
        turtle = new_turtle;
        turtle.id = turtle_count; // Always resture the current turtle id at the
                                  // end.
    }

    (skeleton_points, skeleton)
}

// +| Tests |+ =================================================================
#[cfg(test)]
mod tests {
    use std::f32::consts::PI;

    use super::*;
    const ROT_ANGLE: f32 = (1.0 / 8.0) * 2.0 * PI;

    #[derive(Debug, Clone, Copy, PartialEq)]
    pub enum Alphabet {
        Leaf,
        Branch,
        Pop(f32),
        Push(f32),
    }

    pub fn binary_tree_rules(symbol: &Alphabet) -> Vec<Alphabet> {
        let mut next_string = Vec::<Alphabet>::new();
        match &symbol {
            Alphabet::Leaf => {
                next_string.push(Alphabet::Branch);
                next_string.push(Alphabet::Push(ROT_ANGLE));
                next_string.push(Alphabet::Leaf);
                next_string.push(Alphabet::Pop(-ROT_ANGLE));
                next_string.push(Alphabet::Leaf);
            }
            Alphabet::Branch => {
                next_string.push(Alphabet::Branch);
                next_string.push(Alphabet::Branch);
            }
            val => {
                next_string.push(**val);
            }
        }

        next_string
    }

    #[test]
    fn test_generate_sequence() {
        let result = generate_sequence(&vec![Alphabet::Leaf], &binary_tree_rules, 2);
        assert!(
            result
                == vec![
                    Alphabet::Branch,
                    Alphabet::Branch,
                    Alphabet::Push(ROT_ANGLE),
                    Alphabet::Branch,
                    Alphabet::Push(ROT_ANGLE),
                    Alphabet::Leaf,
                    Alphabet::Pop(-ROT_ANGLE),
                    Alphabet::Leaf,
                    Alphabet::Pop(-ROT_ANGLE),
                    Alphabet::Branch,
                    Alphabet::Push(ROT_ANGLE),
                    Alphabet::Leaf,
                    Alphabet::Pop(-ROT_ANGLE),
                    Alphabet::Leaf
                ]
        );
    }
}
