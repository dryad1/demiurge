use core::f32::consts::PI;
use std::cell::RefCell;
use std::rc::Rc;

use algebra::*;
use camera::ArcballCamera;
use chicken_wire::wavefront_loader::ObjData;
use dem_core::{io::*, rendering::*};
use dem_peripherals::*;
use geometry::project_onto_plane;
use hedge::mesh::HalfMesh;
use voronoi_reconstruction::*;
use vulkan_bindings::RenderContext;

fn round_box(p: &Vec3, b: &Vec3, r: f32) -> f32
{
    let mut q = p.abs() - b + Vec3::new(r, r, r);

    for i in 0..3
    {
        q[i] = q[i].max(0.0);
    }
    let d = (q.x.max(q.y).max(q.z)).min(0.0);

    return q.norm() + d - r;
}

#[allow(dead_code)]
#[derive(Copy, Clone)]
struct Mvp
{
    model: Mat4,
    view: Mat4,
    proj: Mat4,
}

fn init<'io>() -> (IoContext<'io>, RenderContext, Rc<RefCell<ArcballCamera>>)
{
    let mut io_context = IoContext::new("SDF Sampling", (800, 800), true);
    let render_context = RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);

    let (width, height) = io_context.window().get_window_size();
    let camera = ArcballCamera::new(width, height, 10.0);

    let camera = Rc::new(RefCell::new(camera));
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::LeftDrag,
            Box::new(move |x, y, ox, oy| {
                if !camera.borrow().should_update
                {
                    return;
                }
                ArcballCamera::update_camera_angles(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context.window().add_cursor_callback(
            MouseState::RightDrag,
            Box::new(move |x, y, ox, oy| {
                ArcballCamera::update_camera_position(
                    &mut camera.borrow_mut(),
                    x as f32,
                    y as f32,
                    ox as f32,
                    oy as f32,
                )
            }),
        );
    }
    {
        let camera = camera.clone();
        io_context
            .window()
            .add_scroll_callback(Box::new(move |ox, oy| {
                ArcballCamera::update_camera_zoom(
                    &mut camera.borrow_mut(),
                    ox as f32,
                    oy as f32,
                )
            }));
    }

    (io_context, render_context, camera)
}

fn main()
{
    let (mut io_context, mut render_context, camera) = init();

    let plant_shader = render_context
        .add_shader(vec!["examples/12_sdf_sampling/plant_shader/".to_string()]);

    let sphere = |p: &Vec3| {
        return p.norm() - 1.0;
    };

    let sdf_box = |p: &Vec3| {
        return round_box(p, &Vec3::new(1.0, 1.0, 1.0), 0.1);
    };

    let (points, faces) = point_reconstruction(&sdf_box, 0.01);

    let faces: Vec<_> = faces.into_iter().map(|v| v.to_vec()).collect();

    ObjData::export(&points, "sdf_samples.obj");
    ObjData::export(&(points, faces), "sdf_mesh.obj");
    // println!("{:?}", verts.len());
    // let mut normals = compute_normals(&verts, &connect);

    // while io_context.poll_io(&mut None) {
    //     render_context.start_frame(&io_context);

    //     let connect: Vec<_> = connect.iter().map(|&i| i as u32).collect();
    //     let gpu_mesh = GraphicsInput::new()
    //         .add_attribute_buffer_from_slice(&verts, &mut render_context)
    //         .add_attribute_buffer_from_slice(&normals, &mut render_context)
    //         .set_index_buffer_from_slice(&connect, &mut render_context);

    //     let mvp = Mvp {
    //         model: Mat4::identity(),
    //         view: camera.borrow().view_matrix(),
    //         proj: camera.borrow().proj_matrix(),
    //     };

    //     let render_request = RenderRequest::new(plant_shader)
    //         .vertex_input(gpu_mesh.clone())
    //         .add_ubo(0, mvp)
    //         .build();

    //     render_context.draw(&render_request);
    //     render_context.free_input(gpu_mesh);
    // }
}
