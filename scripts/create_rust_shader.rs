//! ```cargo
//! [dependencies]
//! clap = { version = "4.5.4", features = ["derive"] }
//! handlebars = "5.1.2"
//! serde_json = "1.0.117"
//! tempdir = "0.3.7"
//! ```

use std::io::Write;
use std::process::Command;

use clap::Parser;
use handlebars::*;
use serde_json::{json, Map, Value};
use tempdir::TempDir;


#[derive(Parser, Debug)]
#[clap(author, version, about="Command line utility for generating rust shader crates.", long_about = None)]
struct Arguments
{
    /// Path to where the crate must be created.
    #[clap(short, long, value_parser)]
    crate_path: String,

    /// Whether to include a special crate having useful shader utilities.
    #[clap(short, long, value_parser)]
    include_shader_utils: bool,
}

fn main()
{
    let args = Arguments::parse();

    std::fs::create_dir_all(&args.crate_path).unwrap();

    let path = std::path::Path::new(&args.crate_path);
    let name = path.file_name().unwrap();
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_file("cargo_file", "./templates/shader_cargo.hbs")
        .unwrap();
    handlebars
        .register_template_file("lib_file", "./templates/shader_lib.hbs")
        .unwrap();

    let data = json!(
        {
            "package_name": name.to_str().unwrap(),
            "shader_utils": args.include_shader_utils
        }
    );

    let rendered = handlebars.render("cargo_file", &data).unwrap();


    let cargo_path = path.join("Cargo.toml");
    std::fs::create_dir_all(path).unwrap();

    let mut file = std::fs::File::create(cargo_path).unwrap();
    file.write_all(rendered.as_bytes()).unwrap();

    let rendered = handlebars.render("lib_file", &json!({})).unwrap();

    let lib_path = path.join("src");
    std::fs::create_dir_all(&lib_path).unwrap();
    let lib_path = lib_path.join("lib.rs");
    let mut file = std::fs::File::create(lib_path).unwrap();
    file.write_all(rendered.as_bytes()).unwrap();
}
