// runs "cargo build --release" in the rustbuilder directory

use std::env;
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;

const RUSTBUILDER: &str = "crates/vulkan_bindings/src/rustbuilder";

fn main()
{
    let path = PathBuf::from(RUSTBUILDER);
    let fullpath = &path.canonicalize().unwrap();

    let mut target = path.clone();
    target.push("target/release/rustbuilder");

    // verify if rustbuilder has already been built
    if target.exists()
    {
        return;
    }

    // verify correct path, then set cwd to path
    if path.exists()
    {
        env::set_current_dir(path).expect(
            format!("Couldn't set working directory to: {:?}", fullpath).as_str(),
        );
        println!("Building target in: {:?}...", fullpath);
    }
    else
    {
        panic!("could not locate {:?}; Not a valid directory.", fullpath);
    }

    let output = match Command::new("cargo")
        .arg("+nightly-2023-09-30")
        .arg("build")
        .arg("--release")
        .output()
    {
        Ok(v) => v,
        Err(e) => panic!("Couldn't build release in: {:?} \n {}", fullpath, e),
    };

    std::io::stdout().write_all(&output.stdout).unwrap();
    std::io::stderr().write_all(&output.stderr).unwrap();
}
